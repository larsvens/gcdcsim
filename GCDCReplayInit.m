%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Init script for GCDCReplay environment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% % clear workspace and add source dirs
% clear; close all;
% currentDir = pwd;
% dirToAdd = '\ReplayData';
% addpath(genpath(strcat(currentDir,'\',dirToAdd)));
% dirToAdd = '\GCDC2012-Library';
% addpath(genpath(strcat(currentDir,'\',dirToAdd)));
% 
% % load bus definitions
% load('busDefinitions');
% 
% % Load data 
% % fileName2Read = 'garageAcrossStreetAndBack_160204_1711';
% fileName2Read = 'around_the_track_with_GPS_wo_xpc.mat';
% load(fileName2Read);



% store logged data in timeseries format

% VehRaw
timeVehRaw = Results.VehRaw.data(:,12);
timeVehRaw = timeVehRaw - timeVehRaw(1); % make t0=0;
MtrLongVel = timeseries(Results.VehRaw.data(:,1),timeVehRaw,'Name','MtrLongVel');
MtrLongVelTS = timeseries(Results.VehRaw.data(:,2),timeVehRaw,'Name','MtrLongVelTS');
MtrLongAcc = timeseries(Results.VehRaw.data(:,3),timeVehRaw,'Name','MtrLongAcc');
MtrLongAccTS = timeseries(Results.VehRaw.data(:,4),timeVehRaw,'Name','MtrLongAccTS');
IMULongAcc = timeseries(Results.VehRaw.data(:,5),timeVehRaw,'Name','IMULongAcc');
IMULongAccTS = timeseries(Results.VehRaw.data(:,6),timeVehRaw,'Name','IMULongAccTS');
IMULatAcc = timeseries(Results.VehRaw.data(:,7),timeVehRaw,'Name','IMULatAcc');
IMULatAccTS = timeseries(Results.VehRaw.data(:,8),timeVehRaw,'Name','IMULatAccTS');
IMUYawrate = timeseries(Results.VehRaw.data(:,9),timeVehRaw,'Name','IMUYawrate');
IMUYawrateTS = timeseries(Results.VehRaw.data(:,10),timeVehRaw,'Name','IMUYawrateTS');
IMUTimestamp = timeseries(Results.VehRaw.data(:,11),timeVehRaw,'Name','IMUTimestamp');

% GPS
 % gps timestamp seems to be in milliseconds while the CAN data is in
 % seconds

% timeGPS = (1/1000)*Results.GPSRaw.data(:,10);
% timeGPS = timeGPS - timeGPS(1); % make t0=0;

timeGPS = timeVehRaw;

lat = timeseries(Results.GPSRaw.data(:,1),timeGPS,'Name','lat');
latTS = timeseries(Results.GPSRaw.data(:,2),timeGPS,'Name','latTS');
lon = timeseries(Results.GPSRaw.data(:,3),timeGPS,'Name','lon');
lonTS = timeseries(Results.GPSRaw.data(:,4),timeGPS,'Name','lonTS');
hgt = timeseries(Results.GPSRaw.data(:,5),timeGPS,'Name','hgt');
hgtTS = timeseries(Results.GPSRaw.data(:,6),timeGPS,'Name','hgtTS');
hdng = timeseries(Results.GPSRaw.data(:,7),timeGPS,'Name','hdng');
hdngTS = timeseries(Results.GPSRaw.data(:,8),timeGPS,'Name','hdngTS');
gpsLongVel = timeseries(Results.GPSRaw.data(:,9),timeGPS,'Name','gpsLongVel'); % convert from km/h
gpsLongVelTS = timeseries(Results.GPSRaw.data(:,10),timeGPS,'Name','gpsLongVelTS');
gpsStatus = timeseries(Results.GPSRaw.data(:,11),timeGPS,'Name','gpsStatus');
gpsStatusTS = timeseries(Results.GPSRaw.data(:,12),timeGPS,'Name','gpsStatusTS');
gpsTime1 = timeseries(Results.GPSRaw.data(:,13),timeGPS,'Name','gpsTime1');
gpsTime2 = timeseries(Results.GPSRaw.data(:,14),timeGPS,'Name','gpsTime2');
gpsQuality = timeseries(Results.GPSRaw.data(:,15),timeGPS,'Name','gpsQuality');
gpsNrOfSats = timeseries(Results.GPSRaw.data(:,16),timeGPS,'Name','gpsNrOfSats');
   
Tend = timeVehRaw(end);
