function [ road_heading ] = createLaneDataHeading( lane_data_enu )
%CREATELANEDATAHEADING This function generates the Heading of the road from
%the LaneDataENU. This heading is used for Goncalo's Crabbing.

xEastLL = lane_data_enu(:,1);
yNorthLL = lane_data_enu(:,2);
xEastML = lane_data_enu(:,3);
yNorthML = lane_data_enu(:,4);
xEastRL = lane_data_enu(:,5);
yNorthRL = lane_data_enu(:,6);


% Compute the yaw (in radians)
yaw_LL = atan2(diff(yNorthLL), diff(xEastLL));
yaw_ML = atan2(diff(yNorthML), diff(xEastML));
yaw_RL = atan2(diff(yNorthRL), diff(xEastRL));

% Convert into heading (in degrees, defined clockwise from Y-North)

heading_LL = 90 - rad2deg(yaw_LL);
heading_ML = 90 - rad2deg(yaw_ML);
heading_RL = 90 - rad2deg(yaw_RL);

if max(abs(heading_LL - heading_ML)) > 1 || ...
        max(abs(heading_LL - heading_RL)) > 1 || ...
        max(abs(heading_RL - heading_ML)) > 1

    % This error happens because the function is not ready for
    % headings that are unwrapped (Easy to fix)
    error(['Transition detected in angles.', ... 
        'Output average road heading will be wrong.']);
    
end
    
road_heading = (heading_LL + heading_ML + heading_RL)/3;

road_heading = [road_heading(1); road_heading];

end

