function [vehicle1,vehicle2,vehicle3,....
          vehicle4,vehicle5,vehicle6,...
          vehicle7,vehicle8,vehicle9,vehicle10] = vehicleStructureBuffer( vehicleStructure )
%TRAFFICCONTROLHANDLER Create a circular buffer containing all values of
%the RSU information received from the traffic control.
        
        persistent TRAFFIC_BUFFER INDEX MAX_LENGTH

        if isempty(TRAFFIC_BUFFER)
            INDEX = 1;
            MAX_LENGTH = 10;
            initStructure = initializeVehicleStructure(vehicleStructure);
            TRAFFIC_BUFFER = repmat(initStructure,1,MAX_LENGTH);
        end
        
        %What IDs are already stored in TRAFFIC_BUFFER
        existing_structs = TRAFFIC_BUFFER;
        
        %Loop through TRAFFIC_BUFFER and see if the ID already exists
        rowIndex=0;
        for i=1:length(existing_structs)
                if existing_structs(i).nodeID == vehicleStructure.nodeID
                    rowIndex = i;
                end
        end
        
        %If the value does not exist input the new value on the last row
        %else replace the old value of the corresponding ID with the new.
        if (rowIndex == 0)
            TRAFFIC_BUFFER(INDEX) = vehicleStructure;
            INDEX = INDEX + 1;
            if (INDEX > MAX_LENGTH)
                INDEX = 1;
            end
        else
            TRAFFIC_BUFFER(rowIndex) = vehicleStructure;
        end
        
        %Output the buffer
        vehicle1 = TRAFFIC_BUFFER(1);
        vehicle2 = TRAFFIC_BUFFER(2);
        vehicle3 = TRAFFIC_BUFFER(3);
        vehicle4 = TRAFFIC_BUFFER(4);
        vehicle5 = TRAFFIC_BUFFER(5);
        vehicle6 = TRAFFIC_BUFFER(6);
        vehicle7 = TRAFFIC_BUFFER(7);
        vehicle8 = TRAFFIC_BUFFER(8);
        vehicle9 = TRAFFIC_BUFFER(9);
        vehicle10 = TRAFFIC_BUFFER(10);
end