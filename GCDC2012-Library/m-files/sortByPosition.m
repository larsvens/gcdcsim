function sortedObjects = sortByPosition(egoVehicle,unsortedObjects)
% This function sorts the outputmatrix from a truck so that they are sorted
% in the order in terms of position relative to our truck. The positions
% of the objects are also now relative to ours. 

        %Create function for calculating relative distance to all objects
        
        calcRelativePosition(egoVehicle.Position,unsortedObjects)
        
        sortedObjects = sortrows(unsortedObjects,2);
    
end

            