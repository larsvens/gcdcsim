
function emptyStruct = initializeSpeedIndicationStructure()
%Function that sets all initial values of the vehicleStructure to
%corresponding NA value
       
    emptyStruct.nodeID_lsb              = uint32(intmax('uint32'));
    emptyStruct.nodeID_hsb              = uint32(intmax('uint32'));
    emptyStruct.speedLocation1Lon       = -1;
    emptyStruct.speedLocation1Lat       = -1;
    emptyStruct.maxSpeed1               = -1;
    emptyStruct.heading1                = -1;
    emptyStruct.speedLocation2Lon       = -1;
    emptyStruct.speedLocation2Lat       = -1;
    emptyStruct.maxSpeed2               = -1;
    emptyStruct.heading2                = -1;
    emptyStruct.speedLocation3Lon       = -1;
    emptyStruct.speedLocation3Lat       = -1;
    emptyStruct.maxSpeed3               = -1;
    emptyStruct.heading3                = -1;
    
end



