function elems = createBusElements(propNames,propValues)

%!!! S�tt dimensions extra f�r matriserna

for i = 1:size( propValues , 1 )
   elems(i) = Simulink.BusElement; 
   elems(i).Dimensions = 1; 
   elems(i).Complexity = 'real';
   elems(i).DataType = 'double'; 
   for j = 1:length(propNames) 
       elems(i).(propNames{j}) = propValues{i,j}; 
   end
end

for i=1:length(elems)
    if isequal(elems(i).Name(end-1:end),'sb')
        elems(i).DataType = 'uint32';
    end
end

i=1;
while i<length(elems)
    if isequal(elems(i).Name(end-2:end),'sec')
        elems(i).DataType = 'uint32';
        elems(i+1).DataType = 'int32';
        i=i+1;
    end
    i=i+1;
end

for i=1:length(elems)
    if isequal(elems(i).Name(end-5:end-1),'Color')
        elems(i).DataType = 'int32';
    end
end

for i=1:length(elems)
    if isequal(elems(i).Name(end-4:end),'State')
        elems(i).DataType = 'int32';
    end
end