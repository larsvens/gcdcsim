function [ vehicleArray ] = changeVehicleDataTypes( vehicleStruct )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    %Initialize size of vehicleArray
    vehicleArray = zeros(1,16);
    
    vehicleArray(1) = cast(vehicleStruct.nodeID_lsb,'double');
    vehicleArray(2) = cast(vehicleStruct.nodeID_hsb,'double');                  
    vehicleArray(3) = vehicleStruct.vehiclePositionLon;
    vehicleArray(4) = vehicleStruct.vehiclePositionLat;            
    vehicleArray(5) = cast(vehicleStruct.vehiclePostitionTimeStamp_sec,'double');
    vehicleArray(6) = cast(vehicleStruct.vehiclePostitionTimeStamp_msec,'double'); 
    vehicleArray(7) = vehicleStruct.vehiclePositionAccuracy;
    vehicleArray(8) = vehicleStruct.vehicleVelocity;
    vehicleArray(9) = vehicleStruct.vehicleHeading;  
    vehicleArray(10) = vehicleStruct.vehicleAcceleration;   
    vehicleArray(11) = vehicleStruct.vehicleYawRate;
    vehicleArray(12) = cast(vehicleStruct.platoonLeaderID_lsb,'double');
    vehicleArray(13) = cast(vehicleStruct.platoonLeaderID_hsb,'double');     
    vehicleArray(14) = cast(vehicleStruct.platoonState,'double');    
    vehicleArray(15) = vehicleStruct.vehicleWidth;  
    vehicleArray(16) = vehicleStruct.vehicleLength;

end

