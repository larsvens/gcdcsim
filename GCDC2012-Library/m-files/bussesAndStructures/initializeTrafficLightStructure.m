
function emptyStruct = initializeTrafficLightStructure()
%Function that sets all initial values of the vehicleStructure to
%corresponding NA value
       
    emptyStruct.nodeID_lsb              = uint32(intmax('uint32'));
    emptyStruct.nodeID_hsb              = uint32(intmax('uint32'));
    emptyStruct.rsuPositionLon          = -1;
    emptyStruct.rsuPositionLat          = -1;
    emptyStruct.trafficColor1           = int32(hex2dec('FFFF'));
    emptyStruct.time1_sec               = uint32(hex2dec('FFFF'));
    emptyStruct.time1_msec              = int32(hex2dec('FFFF'));
    emptyStruct.trafficColor2           = int32(hex2dec('FFFF'));
    emptyStruct.time2_sec               = uint32(hex2dec('FFFF'));
    emptyStruct.time2_msec              = int32(hex2dec('FFFF'));
    emptyStruct.trafficColor3           = int32(hex2dec('FFFF'));
    emptyStruct.time3_sec               = uint32(hex2dec('FFFF'));
    emptyStruct.time3_msec              = int32(hex2dec('FFFF'));
    emptyStruct.trafficColor4           = int32(hex2dec('FFFF'));
    emptyStruct.time4_sec               = uint32(hex2dec('FFFF'));
    emptyStruct.time4_msec              = int32(hex2dec('FFFF'));
end