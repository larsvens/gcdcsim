function emptyStruct = initializeVehicleStructure()
%Function that sets all initial values of the vehicleStructure to
%corresponding NA value
       
    emptyStruct.nodeID_lsb                      = uint32(intmax('uint32'));
    emptyStruct.nodeID_hsb                      = uint32(intmax('uint32'));
    emptyStruct.vehiclePositionLon              = hex2dec('FF');
    emptyStruct.vehiclePositionLat              = hex2dec('FF');
    emptyStruct.vehiclePositionTimeStamp_sec    = uint32(hex2dec('FFFF'));
    emptyStruct.vehiclePositionTimeStamp_msec   = int32(hex2dec('FFFF'));
    emptyStruct.vehiclePositionAccuracy         = hex2dec('FF');
    emptyStruct.vehicleVelocity                 = hex2dec('FF');
    emptyStruct.vehicleHeading                  = hex2dec('FF');
    emptyStruct.vehicleAcceleration             = hex2dec('FF');
    emptyStruct.vehicleYawRate                  = hex2dec('FF');
    emptyStruct.platoonLeaderID_lsb             = uint32(hex2dec('FFFF'));
    emptyStruct.platoonLeaderID_hsb             = uint32(hex2dec('FFFF'));
    emptyStruct.platoonState                    = int32(hex2dec('FF'));
    emptyStruct.vehicleWidth                    = hex2dec('FF');
    emptyStruct.vehicleLength                   = hex2dec('FF');
end



