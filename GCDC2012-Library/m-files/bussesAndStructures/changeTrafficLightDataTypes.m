function [ trafficLightArray ] = changeTrafficLightDataTypes( trafficLightStruct )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    %Initialize size of vehicleArray
    trafficLightArray = zeros(1,16);
    
    trafficLightArray(1) = cast(trafficLightStruct.nodeID_lsb,'double');
    trafficLightArray(2) = cast(trafficLightStruct.nodeID_hsb,'double');                  
    trafficLightArray(3) = cast(trafficLightStruct.rsuPositionLon,'double');
    trafficLightArray(4) = cast(trafficLightStruct.rsuPositionLat,'double');            
    trafficLightArray(5) = cast(trafficLightStruct.trafficLightColor1,'double');
    trafficLightArray(6) = cast(trafficLightStruct.time1_sec,'double'); 
    trafficLightArray(7) = cast(trafficLightStruct.time1_msec,'double');
    trafficLightArray(8) = cast(trafficLightStruct.trafficLightColor2,'double');
    trafficLightArray(9) = cast(trafficLightStruct.time2_sec,'double');  
    trafficLightArray(10) = cast(trafficLightStruct.time2_msec,'double');   
    trafficLightArray(11) = cast(trafficLightStruct.trafficLightColor3,'double');
    trafficLightArray(12) = cast(trafficLightStruct.time3_sec,'double');
    trafficLightArray(13) = cast(trafficLightStruct.time3_msec,'double');     
    trafficLightArray(14) = cast(trafficLightStruct.trafficLightColor4,'double');    
    trafficLightArray(15) = cast(trafficLightStruct.time4_sec,'double');  
    trafficLightArray(16) = cast(trafficLightStruct.time4_msec,'double');


end

