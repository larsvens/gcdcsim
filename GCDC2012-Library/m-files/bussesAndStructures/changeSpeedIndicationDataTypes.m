function [ speedIndicationArray ] = changeSpeedIndicationDataTypes( speedIndicationStruct )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    speedIndicationArray = zeros(1,14);

    speedIndicationArray(1) = cast(speedIndicationStruct.nodeID_lsb,'double');
    speedIndicationArray(2) = cast(speedIndicationStruct.nodeID_hsb,'double');                  
    speedIndicationArray(3) =  speedIndicationStruct.speedLocation1Lon;
    speedIndicationArray(4) =  speedIndicationStruct.speedLocation1Lat;            
    speedIndicationArray(5) =  speedIndicationStruct.maxSpeed1;
    speedIndicationArray(6) =  speedIndicationStruct.heading1; 
    speedIndicationArray(7) =  speedIndicationStruct.speedLocation2Lon;
    speedIndicationArray(8) =  speedIndicationStruct.speedLocation2Lat;
    speedIndicationArray(9) =  speedIndicationStruct.maxSpeed2;  
    speedIndicationArray(10) =  speedIndicationStruct.heading2;   
    speedIndicationArray(11) =  speedIndicationStruct.speedLocation3Lon;
    speedIndicationArray(12) =  speedIndicationStruct.speedLocation3Lat;
    speedIndicationArray(13) =  speedIndicationStruct.maxSpeed3;     
    speedIndicationArray(14) =  speedIndicationStruct.heading3;    

end

