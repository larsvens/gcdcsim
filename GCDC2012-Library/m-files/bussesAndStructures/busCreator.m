
%% value_and_status - Holds a value and a signal status for the signal

%%%%%%%%%%%% Create Busses %%%%%%%%%%%%
vehicleBus                  = Simulink.Bus;
dynamicVehicleBus           = Simulink.Bus;
staticVehicleBus            = Simulink.Bus;
trafficLightBus             = Simulink.Bus;
lightInfoBus                = Simulink.Bus;
speedIndicationBus          = Simulink.Bus;
staticRSUInfoBus            = Simulink.Bus;

vehicleBus.Elements = createBusElements( ...
    {'Name'}, ...
    {'nodeID_lsb'; 'nodeID_hsb'; 'vehiclePositionLon'; 'vehiclePositionLat';...
    'vehiclePositionTimeStamp_sec'; 'vehiclePositionTimeStamp_msec'; 'vehiclePositionAccuracy';...
    'vehicleVelocity';'vehicleHeading'; 'vehicleAcceleration'; 'vehicleYawRate'; 'platoonLeaderID_lsb';...
    'platoonLeaderID_hsb';'platoonState'; 'vehicleWidth'; 'vehicleLength'} ...
);

dynamicVehicleBus.Elements = createBusElements( ...
    {'Name'}, ...
        {'nodeID_lsb'; 'nodeID_hsb'; 'vehiclePositionLon'; 'vehiclePositionLat';...
    'vehiclePositionTimeStamp_sec'; 'vehiclePositionTimeStamp_msec'; 'vehiclePositionAccuracy';...
    'vehicleVelocity';'vehicleHeading'; 'vehicleAcceleration'; 'vehicleYawRate'; 'platoonLeaderID_lsb';...
    'platoonLeaderID_hsb';'platoonState'} ...
);
                   
staticVehicleBus.Elements = createBusElements( ...
    {'Name'}, ...
    {'nodeID_lsb'; 'nodeID_hsb'; 'vehicleWidth'; 'vehicleLength'} ...
);

trafficLightBus.Elements = createBusElements( ...
    {'Name'}, ...
    {'nodeID_lsb'; 'nodeID_hsb'; 'rsuPositionLon'; 'rsuPositionLat'; 'trafficColor1'; 'time1_sec'; 'time1_msec'; 'trafficColor2'; ...
    'time2_sec'; 'time2_msec'; 'trafficColor3'; 'time3_sec'; 'time3_msec'; 'trafficColor4'; 'time4_sec'; 'time4_msec';} ...
);

lightInfoBus.Elements = createBusElements( ...
    {'Name'}, ...
    {'nodeID_lsb'; 'nodeID_hsb'; 'trafficColor1'; 'time1_sec'; 'time1_msec'; 'trafficColor2'; ...
    'time2_sec'; 'time2_msec'; 'trafficColor3'; 'time3_sec'; 'time3_msec'; 'trafficColor4'; 'time4_sec'; 'time4_msec';} ...
);

staticRSUInfoBus.Elements = createBusElements( ...
    {'Name'}, ...
    {'nodeID_lsb'; 'nodeID_hsb'; 'rsuPositionLon'; 'rsuPositionLat'} ...
);

speedIndicationBus.Elements = createBusElements( ...
    {'Name'}, ...
    {'nodeID_lsb'; 'nodeID_hsb'; 'speedLocation1Lon'; 'speedLocation1Lat'; 'maxSpeed1'; 'heading1'; 'speedLocation2Lon'; ...
    'speedLocation2Lat'; 'maxSpeed2'; 'heading2'; 'speedLocation3Lon'; 'speedLocation3Lat'; 'maxSpeed3'; 'heading3'} ...
);


%%%%%%%%%%%% Create Elements %%%%%%%%%%%%
% nodeID                      = Simulink.BusElement;
% vehiclePostitionLon         = Simulink.BusElement;
% vehiclePostitionLat         = Simulink.BusElement;
% vehiclePostitionTimeStamp   = Simulink.BusElement;
% vehicleVelocity             = Simulink.BusElement;
% vehiclePositionAccuracy     = Simulink.BusElement;
% vehicleHeading              = Simulink.BusElement;
% vehicleAcceleration			= Simulink.BusElement;
% vehicleYawRate              = Simulink.BusElement;
% platoonLeaderID             = Simulink.BusElement;
% platoonState			    = Simulink.BusElement;
% %vehicleLength               = Simulink.BusElement;
% %vehicleWidth                = Simulink.BusElement;
% 
% rsuPositionLon              = Simulink.BusElement;
% rsuPositionLat              = Simulink.BusElement;
% trafficLightColor1          = Simulink.BusElement;
% time1                       = Simulink.BusElement;
% trafficLightColor2          = Simulink.BusElement;
% time2                       = Simulink.BusElement;
% trafficLightColor3          = Simulink.BusElement;
% time3                       = Simulink.BusElement;
% trafficLightColor4          = Simulink.BusElement;
% time4                       = Simulink.BusElement;
% 
% speedLocation1Lon           = Simulink.BusElement;
% speedLocation1Lat           = Simulink.BusElement;
% maxSpeed1                   = Simulink.BusElement;
% heading1                    = Simulink.BusElement;
% speedLocation2Lon           = Simulink.BusElement;
% speedLocation2Lat           = Simulink.BusElement;
% maxSpeed2                   = Simulink.BusElement;
% heading2                    = Simulink.BusElement;
% speedLocation3Lon           = Simulink.BusElement;
% speedLocation3Lat           = Simulink.BusElement;
% maxSpeed3                   = Simulink.BusElement;
% heading3                    = Simulink.BusElement;

% nodeID.name                     = 'nodeID_lsb'; 'nodeID_hsb';
% vehiclePostitionLon.name        = 'vehiclePositionLon';
% vehiclePostitionLat.name        = 'vehiclePositionLat';
% vehiclePostitionTimeStamp.name  = 'vehiclePostitionTimeStamp';
% vehicleVelocity.name            = 'vehicleVelocity';
% vehiclePositionAccuracy.name    = 'vehiclePositionAccuracy';
% vehicleHeading.name             = 'vehicleHeading';
% vehicleAcceleration.name        = 'vehicleAcceleration';
% vehicleYawRate.name             = 'vehicleYawRate';
% platoonLeaderID.name            = 'platoonLeaderID';
% platoonState.name               = 'platoonState';
% vehicleLength.name              = 'vehicleLength';
% vehicleWidth.name               = 'vehicleWidth';
% 
% rsuPositionLon.name             = 'rsuPositionLon';
% rsuPositionLat.name             = 'rsuPositionLat';
% trafficLightColor1.name         = 'trafficLightColor1';
% time1.name                      = 'time1';
% trafficLightColor2.name         = 'trafficLightColor2';
% time2.name                      = 'time2';
% trafficLightColor3.name         = 'trafficLightColor3';
% time3.name                      = 'time3';
% trafficLightColor4.name         = 'trafficLightColor4';
% time4.name                      = 'time4';
% 
% 
% speedLocation1Lon.name           = 'speedLocation1Lon';
% speedLocation1Lat.name           = 'speedLocation1Lat';
% maxSpeed1.name                   = 'maxSpeed1';
% heading1.name                    = 'heading1';
% speedLocation2Lon.name           = 'speedLocation2Lon';
% speedLocation2Lat.name           = 'speedLocation2Lat';
% maxSpeed2.name                   = 'maxSpeed2';
% heading2.name                    = 'heading2';
% speedLocation3Lon.name           = 'speedLocation3Lon';
% speedLocation3Lat.name           = 'speedLocation3Lat';
% maxSpeed3.name                   = 'maxSpeed3';
% heading3.name                    = 'heading3';

% vehicleBus.Elements = [nodeID, vehiclePostitionLon, vehiclePostitionLat, ...
%                        vehiclePostitionTimeStamp, vehiclePositionAccuracy, vehicleVelocity, vehicleHeading,...
%                        vehicleAcceleration, vehicleYawRate, platoonLeaderID, platoonState,]% vehicleWidth, vehicleLength];
%                    
% dynamicVehicleBus.Elements = [nodeID, vehiclePostitionLon, vehiclePostitionLat, ...
%                        vehiclePostitionTimeStamp, vehiclePositionAccuracy, vehicleVelocity, vehicleHeading,...
%                        vehicleAcceleration, vehicleYawRate, platoonLeaderID, platoonState];
% 
% trafficLightBus.Elements = [nodeID, rsuPositionLon, rsuPositionLat, trafficLightColor1, time1, ...
%                             trafficLightColor2, time2, trafficLightColor3, time3, trafficLightColor4, time4];
% 
% lightInfoBus.Elements = [nodeID, trafficLightColor1, time1, trafficLightColor2, time2, ...
%                         trafficLightColor3, time3, trafficLightColor4, time4];
%                         
% staticRSUInfoBus.Elements = [nodeID, rsuPositionLon, rsuPositionLat];
%                         
% speedIndicationBus.Elements = [nodeID, speedLocation1Lon, speedLocation1Lat, maxSpeed1, heading1, ...
%                                speedLocation2Lon, speedLocation2Lat, maxSpeed2, heading2, ...
%                                speedLocation3Lon, speedLocation3Lat, maxSpeed3, heading3];
                   
                   

