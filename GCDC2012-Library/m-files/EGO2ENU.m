function [xEast, yNorth, heading, yaw] = EGO2ENU(vFwd, vLft, yawrate, initxEast, inityNorth, initHeading, dt)
    % Description:
    % Propagates vehicle in ENU coordinate system

    persistent xEastOld yNorthOld yawOld;

    if isempty(xEastOld)
        xEastOld = initxEast;
    end

    if isempty(yNorthOld)
        yNorthOld = inityNorth;
    end

    if isempty(yawOld)
        yawOld = 90 - initHeading;
        if yawOld < -180
            yawOld = yawOld + 360;
        end
    end

    vEast  = vFwd*cosd(yawOld) - vLft*sind(yawOld);
    vNorth = vFwd*sind(yawOld) + vLft*cosd(yawOld);

    xEast  = xEastOld + vEast*dt;
    yNorth = yNorthOld + vNorth*dt;
    yaw = yawOld + yawrate*dt;

    xEastOld = xEast;
    yNorthOld = yNorth;
    yawOld = yaw;

    heading = 90-yaw;
    if heading > 360
        heading = heading-360;
    end

end
