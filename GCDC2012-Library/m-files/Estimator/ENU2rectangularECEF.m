function ECEFr = ENU2rectangularECEF(ECEFg0,ECEFr0,ENU)
% ENU2rectangularECEF transforms ENU coordinates into rectangular ECEF 
% coordinates.
%                                   ECEFg0      - Origin of the geodetic 
%                                               ECEF coordinate reference 
%                                               system
%                                   ECEFr0      - Origin of the rectangular
%                                               ECEF coordinate reference
%                                               system
%                                   ENU         - ENU coordinates

% From report:
% ECEFg0(1,1) = latitude
% ECEFg0(2,1) = longitude

    %% transformation matrix from local tangent frame ENU to rectangular ECEF
    R_t_e=[ -sin(ECEFg0(2,1))   -sin(ECEFg0(1,1))*cos(ECEFg0(2,1))  cos(ECEFg0(1,1))*cos(ECEFg0(2,1));              
            cos(ECEFg0(2,1))    -sin(ECEFg0(1,1))*sin(ECEFg0(2,1))  cos(ECEFg0(1,1))*sin(ECEFg0(2,1));         
            0                   cos(ECEFg0(1,1))                    sin(ECEFg0(1,1))];              
    %% transformation from local tangent frame ENU to rectangular ECEF
    ECEFr=ECEFr0+R_t_e*ENU;

end