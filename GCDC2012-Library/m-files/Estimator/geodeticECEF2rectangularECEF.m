function ECEFr = geodeticECEF2rectangularECEF(ECEFg)
% geodeticECEF2rectangularECEF transforms geodetic ECEF coordinates into 
% rectangular ECEF coordinates.
%                                   ECEFg       - Origin of the geodetic 
%                                               ECEF coordinate reference 
%                                               system


% Note:
% ECEFg(1,2,3) in degrees
% ECEFg0(1,1) = latitude
% ECEFg0(2,1) = longitude

    %% initialization
    persistent a b e;
    if isempty(a)
        a=6378137.0;            % semi-major axis a [m]  
        b=6356752.314245;       % semi_minor axis b [m]
        e=sqrt(1-(b^2/a^2));    % first eccentricity
    end
    
    %% transformation from geodetic ECEF to rectangular ECEF
    ECEFr=zeros(3,1);
    Rn=a/sqrt(1-(e*sin(ECEFg(1,1)))^2);
    ECEFr(1,1)=(Rn + ECEFg(3,1))*cos(ECEFg(1,1))*cos(ECEFg(2,1));
    ECEFr(2,1)=(Rn + ECEFg(3,1))*cos(ECEFg(1,1))*sin(ECEFg(2,1));
    ECEFr(3,1)=(Rn*(1 - e^2) + ECEFg(3,1))*sin(ECEFg(1,1));

end

