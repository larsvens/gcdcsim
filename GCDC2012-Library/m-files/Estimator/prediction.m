function xhatOutPred=prediction(xhat,dt,numSlots,ax_coef)
% prediction makes the prediction of the current state based on the current 
% estimation (estimation from the state BUFFTIME seconds ago).
%                                   xhat        - State vector
%                                   dt          - Timestep
%                                   numSlots    - Number of slots that make
%                                               up the buffer (BUFFTIME/dt) 
%                                   ax_coef     - Acceleration coefficient
%                                               to model the system with
%                                               different approaches (from
%                                               zero acceleration up to
%                                               constant acceleration)

    %% calculation of the prediction
    xhat_replica=xhat;                                                              % copy a replica of the estimated state
    for i=1:(numSlots-1)
        xhat_replica=state_propagation(xhat_replica,dt,ax_coef);
        xhat_replica(5)=mod(xhat_replica(5)+pi,2*pi)-pi;                            % take care of the phase/yaw
        if(abs(xhat_replica(7))>sin(1.57)*abs(xhat_replica(3)))
            xhat_replica(7)=sin(1.57)*sign(xhat_replica(7))*abs(xhat_replica(3));   % take care of the vertical speed
        end
    end
    
    %% output the result
    xhatOutPred=xhat_replica;
    
end