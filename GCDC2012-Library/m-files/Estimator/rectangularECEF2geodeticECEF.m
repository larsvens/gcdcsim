function ECEFg = rectangularECEF2geodeticECEF(ECEFr)
    % rectangularECEF2geodeticECEF transforms rectangular ECEF into geodetic 
    % ECEF coordinates. 
%                                   ECEFr       - Rectangular ECEF
%                                               coordinates

% Note:
% ECEFg = [phi, lambda, hgt] (phi = latitude and lambda = longitude)
% lambda and phi in radians

    %% definition of WGS-84 ellipsoid parameters
    persistent a b e;
    if isempty(a)
        a=6378137.0;            % semi-major axis a [m]  
        b=6356752.314245;       % semi_minor axis b [m]
        e=sqrt(1-(b^2/a^2));    % first eccentricity
    end
    
    
    %% calculate explicitly the longitude
    % lambda=atan2(y,x)
    lambda=atan2(ECEFr(2,1),ECEFr(1,1));
    
    
    %% iterate to get a numeric solution for phi and h
    
    %% 1.initialization
    last_phi=nan;
    phi=nan;
    last_h=0;
    h=0;
    Rn=a;
    % p=sqrt(x^2 + y^2)
    p=sqrt(ECEFr(1,1)^2 + ECEFr(2,1)^2);
    
    %% 2.perform iteration until convergence
    while convergenceChecker(phi,last_phi,h,last_h)
        last_phi=phi;
        last_h=h;
        % sin_phi=z/((1 - e^2)*Rn + h)
        sin_phi=ECEFr(3,1)/((1 - e^2)*Rn + h);
        % phi=atan((z + e^2*Rn*sin_phi)/p);
        phi=atan((ECEFr(3,1) + e^2*Rn*sin_phi)/p);
        Rn=a/sqrt(1 - (e*sin_phi)^2);
        h=p/cos(phi) - Rn;
    end
        
    
    %% output the results

    ECEFg=[ phi; 
            lambda;
            h];
end

function answer = convergenceChecker(phi,last_phi,h,last_h)
    if isnan(phi)
        answer=1;
        return;
    %% the algorithm converges to nanoradian accuracy in latitude and 
    %% centimeter accuracy in longitude
    else if (abs(phi-last_phi)<1e-9) && (abs(h-last_h)<0.01)
            answer=0;
            return;
        else
            answer=1;
            return;
        end
    end
end