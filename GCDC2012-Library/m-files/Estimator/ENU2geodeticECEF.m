function [lat,lon,hgt] = ENU2geodeticECEF(ECEFg0,xEast,yNorth,zUp)
    
        ENU = [xEast; yNorth; zUp];
        ECEFr0 = geodeticECEF2rectangularECEF(ECEFg0);
        ECEFr = ENU2rectangularECEF(ECEFg0,ECEFr0,ENU);
        ECEFg = rectangularECEF2geodeticECEF(ECEFr);
        lat = (180/pi)*ECEFg(1,1);
        lon = (180/pi)*ECEFg(2,1);
        hgt = ECEFg(3,1);
end