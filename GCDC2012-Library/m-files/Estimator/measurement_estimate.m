function yhat=measurement_estimate(xhat)
% measurement_estimate returns the estimation of the measurements based on 
% the state vector.
%                                   xhat        - State vector

    % Y=["east_gps","north_gps",vx_can,ax_can,yaw_gps,wz_can,vd_gps,"up_gps",vg_gps]; 
    
    %% estimated measurements
    yhat=zeros(9,1);
    yhat(1,1)=xhat(1);                  % "east_gps_est"
    yhat(2,1)=xhat(2);                  % "north_gps_est"
    yhat(3,1)=(1+xhat(9))*xhat(3);      % vx_can_est
    yhat(4,1)=xhat(4)+xhat(10);         % ax_can_est    ???? The bias had a negative sign
    yhat(5,1)=xhat(5);                  % yaw_gps_est
    yhat(6,1)=xhat(6)+xhat(11);         % wz_can_est    ???? The bias had a negative sign
    yhat(7,1)=xhat(7);                  % vd_gps_est
    yhat(8,1)=xhat(8);                  % "up_gps_est"
    yhat(9,1)=xhat(3);                  % vg_gps_est

end