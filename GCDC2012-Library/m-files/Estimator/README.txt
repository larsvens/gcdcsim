Description of the files located in the folder "Kinematic_EKF":
    
    * Files belonging to the EKF kernel:
        - state_propagation.m
        - F_matrix.m
        - H_matrix.m
        - measurement_estimate.m
        - measurement_vector.m
        - prediction.m
    * Simulink model files:
        - kinematic_EKF.mdl
        - kinematic_EKF_without_scopes.mdl
    * Change of coordinates files:
        - ENU2rectangularECEF.m
        - rectangularECEF2ENU.m
        - rectangularECEF2geodeticECEF.m
        - geodeticECEF2rectangularECEF.m
    * Simulation data files:
        - heati_messages.mat: data from Dennis
        - Altamashi_messages.mat: data from Altamash
    