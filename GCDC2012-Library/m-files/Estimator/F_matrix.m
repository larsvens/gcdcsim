function F=F_matrix(xhat,dt,ax_coef)
% F_matrix returns the linearization of the process model (the Jacobian).
%                                   xhat        - State vector
%                                   dt          - Timestep
%                                   ax_coef     - Acceleration coefficient
%                                               to model the system with
%                                               different approaches (from
%                                               zero acceleration up to
%                                               constant acceleration)

    % X=[east,north,vx,ax,yaw,wz,vd,up,eta,b_ax,b_wz];

    vx=xhat(3,1);       % estimated longitudinal speed
    yaw=xhat(5,1);      % estimated yaw
    vd=xhat(7,1);       % estimated vertical speed

    %% linearized discrete process model
    % in the case the vehicle is standstill
    if vx == 0
        
        F =    [1, 0,                                    0,         0,                                   0,     0,   0, 0, 0, 0, 0;
                0, 1,                                    0,         0,                                   0,     0,   0, 0, 0, 0, 0;
                0, 0,                                    1,        dt,                                   0,     0,   0, 0, 0, 0, 0;
                0, 0,                                    0,   ax_coef,                                   0,     0,   0, 0, 0, 0, 0;
                0, 0,                                    0,         0,                                   1,    dt,   0, 0, 0, 0, 0;
                0, 0,                                    0,         0,                                   0,     1,   0, 0, 0, 0, 0;
                0, 0,                                    0,         0,                                   0,     0,   1, 0, 0, 0, 0;
                0, 0,                                    0,         0,                                   0,     0,  dt, 1, 0, 0, 0;
                0, 0,                                    0,         0,                                   0,     0,   0, 0, 1, 0, 0;
                0, 0,                                    0,         0,                                   0,     0,   0, 0, 0, 1, 0;
                0, 0,                                    0,         0,                                   0,     0,   0, 0, 0, 0, 1];
    
    % in the case the vehicle is moving
    else
        
        F =    [1, 0,   sin(yaw)*vx/(vx^2 - vd^2)^(1/2)*dt,         0,     cos(yaw)*(vx^2 - vd^2)^(1/2)*dt,     0,   -sin(yaw)*vd/(vx^2 - vd^2)^(1/2)*dt, 0, 0, 0, 0;
                0, 1,   cos(yaw)*vx/(vx^2 - vd^2)^(1/2)*dt,         0,    -sin(yaw)*(vx^2 - vd^2)^(1/2)*dt,     0,   -cos(yaw)*vd/(vx^2 - vd^2)^(1/2)*dt, 0, 0, 0, 0;
                0, 0,                                    1,        dt,                                   0,     0,                                     0, 0, 0, 0, 0;
                0, 0,                                    0,   ax_coef,                                   0,     0,                                     0, 0, 0, 0, 0;
                0, 0,                                    0,         0,                                   1,    dt,                                     0, 0, 0, 0, 0;
                0, 0,                                    0,         0,                                   0,     1,                                     0, 0, 0, 0, 0;
                0, 0,                                    0,         0,                                   0,     0,                                     1, 0, 0, 0, 0;
                0, 0,                                    0,         0,                                   0,     0,                                    dt, 1, 0, 0, 0;
                0, 0,                                    0,         0,                                   0,     0,                                     0, 0, 1, 0, 0;
                0, 0,                                    0,         0,                                   0,     0,                                     0, 0, 0, 1, 0;
                0, 0,                                    0,         0,                                   0,     0,                                     0, 0, 0, 0, 1];
        
    end
      
end