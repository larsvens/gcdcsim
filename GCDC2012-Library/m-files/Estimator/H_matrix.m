function H=H_matrix(xlen,ylen,xhat)
% H_matrix returns the linearization of the measurement model (the 
% Jacobian).
%                                   xlen        - Length of the state
%                                               vector
%                                   ylen        - Length of the measurement
%                                               vector
%                                   xhat        - State vector

    % Y=[east_gps,north_gps,vx_can,ax_can,yaw_gps,wz_can,vd_gps,up_gps,vg_gps]; 

    vx_est=xhat(3,1);      % estimated longitudinal speed
    eta_est=xhat(9,1);     % estimated speed scale factor
    
    %% linearized measurement model
    H=zeros(ylen,xlen);
    % jacobian terms for the east_gps term row
    H(1,1)=1;              % east
    % jacobian terms for the north_gps term row
    H(2,2)=1;              % north
    % jacobian terms for the vx_can term row
    H(3,3)=1+eta_est;      % vx
    H(3,9)=vx_est;         % eta
    % jacobian terms for the ax_can term row
    H(4,4)=1;              % ax
    H(4,10)=1;             % ba            
    % jacobian terms for the yaw_gps term row
    H(5,5)=1;              % yaw
    % jacobian terms for the wz_can term row
    H(6,6)=1;              % wz
    H(6,11)=1;             % bw            
    % jacobian terms for the vd_gps term row
    H(7,7)=1;              % vd
    % jacobian terms for the up_gps term row
    H(8,8)=1;              % up
    % jacobian terms for the vg_gps term row
    H(9,3)=1;              % vx

end

