function ENU = rectangularECEF2ENU(ECEFg0,ECEFr0,ECEFr)
% rectangularECEF2ENU transforms rectangular ECEF coordinates into ENU 
% coordinates. 
%                                   ECEFg0      - Origin of the geodetic 
%                                               ECEF coordinate reference 
%                                               system
%                                   ECEFr0      - Origin of the rectangular
%                                               ECEF coordinate reference
%                                               system
%                                   ECEFr       - Rectangular ECEF
%                                               coordinates

    %% transformation matrix from rectangular ECEF to local tangent frame ENU
    R_e_t=[ -sin(ECEFg0(2,1))                   cos(ECEFg0(2,1))                    0;
            -sin(ECEFg0(1,1))*cos(ECEFg0(2,1))  -sin(ECEFg0(1,1))*sin(ECEFg0(2,1))  cos(ECEFg0(1,1));
            cos(ECEFg0(1,1))*cos(ECEFg0(2,1))   cos(ECEFg0(1,1))*sin(ECEFg0(2,1))   sin(ECEFg0(1,1))];
    %% transformation from rectangular ECEF to local tangent frame ENU
    ENU=R_e_t*(ECEFr-ECEFr0);

end

