function [xFwd, yRgt] = ENU2Ego(xEast,yNorth,egoXEast,egoYNorth,egoHeading)
    % Description:
    % converts a coordinate pair xEast,yNorth to the ego coordinate system
    % defined as xFwd (m) in forward direction and yRgt (m) in right
    % direction
    % heading in degrees
    % xEast,yNorth row vectors (m)
    
%     disp(size(xEast));
    
    R = [cosd(egoHeading) -sind(egoHeading);
         sind(egoHeading)  cosd(egoHeading)];
    relPosENU = [xEast-egoXEast;yNorth-egoYNorth];
    posEgo = R*relPosENU;
    xFwd = posEgo(2,:);
    yRgt = posEgo(1,:);
end