function xhat=state_propagation(xhat,dt,ax_coef)
% state_propagation propagates the state one timestep.
%                                   xhat        - State vector
%                                   dt          - Timestep
%                                   ax_coef     - Acceleration coefficient
%                                               to model the system with
%                                               different approaches (from
%                                               zero acceleration up to
%                                               constant acceleration)
 
    % X=[east,north,vx,ax,yaw,wz,vd,up,eta,b_ax,b_wz];

    %% propagation of the state
%     %initially
    xhat(1,1)=xhat(1,1)+sqrt(xhat(3,1)^2-xhat(7,1)^2)*cos(xhat(5,1))*dt;     % east
    xhat(2,1)=xhat(2,1)+sqrt(xhat(3,1)^2-xhat(7,1)^2)*sin(xhat(5,1))*dt;     % north
%     xhat(1,1)=xhat(1,1)+sqrt(xhat(3,1)^2-xhat(7,1)^2)*sin(xhat(5,1)+pi/2)*dt;% east
%     xhat(2,1)=xhat(2,1)+sqrt(xhat(3,1)^2-xhat(7,1)^2)*cos(xhat(5,1)+pi/2)*dt;% north
    xhat(3,1)=xhat(3,1)+xhat(4,1)*dt;                                        % vx
    xhat(4,1)=ax_coef*xhat(4,1);                                             % ax
    xhat(5,1)=xhat(5,1)+xhat(6,1)*dt;                                        % yaw
    xhat(6,1)=xhat(6,1);                                                     % wz
    xhat(7,1)=xhat(7,1);                                                     % vd
    xhat(8,1)=xhat(8,1)+xhat(7,1)*dt;                                        % up
    xhat(9,1)=xhat(9,1);                                                     % eta
    xhat(10,1)=xhat(10,1);                                                   % b_ax
    xhat(11,1)=0;%xhat(11,1);                                                   % b_wz
%     disp(xhat(4,1))
    
end