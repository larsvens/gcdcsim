function meas=measurement_vector(slotMeasurements,ylen,xhat,ECEFg0,ECEFr0)
% measurement_vector returns the measurement vector. In this vector the
% geodetic ECEF coordinates from the GPS are transformed to ENU
% coordinates.
%                                   slotMeasurements - measurements stored 
%                                               in the buffer corresponding
%                                               to this time step
%                                   ylen        - Length of the measurement
%                                               vector
%                                   xhat        - State vector
%                                   ECEFg0      - Origin of the geodetic 
%                                               ECEF coordinate reference 
%                                               system
%                                   ECEFr0      - Origin of the rectangular
%                                               ECEF coordinate reference
%                                               system

    % Y=[east_gps,north_gps,vx_can,ax_can,yaw_gps,wz_can,vd_gps,up_gps,vg_gps]; 
    
    %% calculation of a estimated east_gps, north_gps or up_gps
    % If there is one of east_gps, north_gps or up_gps, the other ones have
    % to be estimated to do the transformation to the ENU coordinate
    % reference system.
    if ((isnan(slotMeasurements(1))||isnan(slotMeasurements(2))||isnan(slotMeasurements(8)))&&~(isnan(slotMeasurements(1))&&isnan(slotMeasurements(2))&&isnan(slotMeasurements(8))))
        ECEFr=ENU2rectangularECEF(ECEFg0,ECEFr0,[xhat(1,1) xhat(2,1) xhat(8,1)]');
        ECEFg=rectangularECEF2geodeticECEF(ECEFr);
        if isnan(slotMeasurements(1))
            slotMeasurements(1)=real(ECEFg(2,1));
        end
        if isnan(slotMeasurements(2))
            slotMeasurements(2)=real(ECEFg(1,1));
        end
        if isnan(slotMeasurements(8))
            slotMeasurements(8)=real(ECEFg(3,1));
        end
    end

    %% computation of geodetic coordinates to get enu coordinates
    ECEFr=geodeticECEF2rectangularECEF([slotMeasurements(1) slotMeasurements(2) slotMeasurements(8)]');
    % transformation from rectangular ECEF to local tangent frame ENU
    ENU=rectangularECEF2ENU(ECEFg0,ECEFr0,ECEFr);
    
    %% measurement vector
    meas=zeros(ylen,1);
    meas(1,1)=ENU(1);                               % east_gps
    meas(2,1)=ENU(2);                               % north_gps
    meas(3,1)=slotMeasurements(3);                  % vx_can
    meas(4,1)=slotMeasurements(4);                  % ax_can
    meas(5,1)=mod(slotMeasurements(5)+pi,2*pi)-pi;  % yaw_gps   take care of the phase/yaw
    meas(6,1)=slotMeasurements(6);                  % wz_can
    meas(7,1)=slotMeasurements(7);                  % vd_gps
    meas(8,1)=ENU(3);                               % up_gps
    meas(9,1)=slotMeasurements(9);                  % vg_gps

end