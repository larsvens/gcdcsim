function alpha = pos2head(lat1,lon1,lat2,lon2,head1)

    dlon = lon2- lon1;
    
    theta = atan(sind(dlon)*cosd(lat2)/(cosd(lat1)*sind(lat2)-sind(lat1)*cosd(lat2)*cosd(dlon)));
    
    if(theta < 0) 
		if((head1 + abs(theta)) <= pi)
            alpha=head1 + abs(theta);
		else
			alpha=abs(2*pi - head1 - abs(theta));
        end
    else
		if(abs(head1-theta) <= pi)
			alpha=abs(head1 - theta);
		else
			alpha=abs(2*pi - head1 + theta);
        end
    end
    
    

end