function systemStatus = onoffLogic(onSwitch,offSwitch)
% Function for operating the On/Off switch in the truck, works according to
% the logic table below.
%
%          On/Off|   1   |   0   |
%          -----------------------
%             1  |   0   |   1   |
%          -----------------------
%             0  |   0   |  Keep |
%

        persistent SYSTEM_STATUS

        if isempty(SYSTEM_STATUS)
            SYSTEM_STATUS=0;
        end

        if (onSwitch == 1 && offSwitch ~= 1)
            SYSTEM_STATUS = 1;
        elseif (offSwitch ~= 0)
            SYSTEM_STATUS = 0;
        else
            
        end
        
        systemStatus = SYSTEM_STATUS;
end

            