
#define S_FUNCTION_NAME  RMCparser
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <string.h>
#include <windows.h>
#include <stddef.h>
#include <stdlib.h>

#define RMCINPUTS 11

/*================*
 * Build checking *
 *================*/


/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *   Setup sizes of the various vectors.
 */
static void mdlInitializeSizes(SimStruct *S)
{

    int_T i;
    
    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, DYNAMICALLY_SIZED);
    ssSetInputPortDirectFeedThrough(S, 0, 1);
    ssSetInputPortDataType(S, 0, SS_UINT8);

    ssSetNumOutputPorts(S,RMCINPUTS);
    
    for (i=0; i<RMCINPUTS; i++){
        ssSetOutputPortWidth(S, i, 1);
        ssSetOutputPortDataType(S, i, SS_DOUBLE);
    }
    
}


/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specifiy that we inherit our sample time from the driving block.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
    ssSetModelReferenceSampleTimeDefaultInheritance(S); 
}

/* Function: mdlOutputs =======================================================
 * Abstract:
 *    y = 2*u
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    //Create pointer to input signal(NMEA ARRAY)
    InputInt8PtrsType inputArray = ssGetInputPortSignalPtrs(S,0);
    int_T width = ssGetInputPortWidth(S,0);

    
    //Create other variables
    char **inputAscii;
    char format[3];
    int type;
    
    int i;
    int j;
    int comma[15];
    
    double *data;
    double tmpd;
    int tmpi;
    char tmpc;
    
    inputAscii = &inputArray[0];
    comma[0]=0;
    j=1;
    //iterate through input buffer and input indeces of comma signs in a vector
    for (i=1; i<=width; i++) {
        
        if (**inputAscii == 44) {
            comma[j]=i;
            j++;
        }
        else if (**inputAscii == 42) {
            comma[j]=i;
            break;
        }
        inputAscii++;
    }
    
    for (i=0; i<RMCINPUTS; i++){
        void *y = ssGetOutputPortSignal( S, i );
        data = y;
        inputAscii = &inputArray[comma[i+1]-1];
        
        switch (i+1) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
                strcpy( format, "%lf");
                type = 1;
                break;
            case 2:
                strcpy( format, "%c");
                type = 3;
                break;
            case 4:
            case 6:
            case 11:
                type = 4;
                break;
            case 9:
                strcpy( format, "%d");
                type = 2;
                break;
        }
        
        if (type == 1) { 
            for (j=1; comma[i+1]-j>comma[i]; j++){
                inputAscii--;
                sscanf(*inputAscii,format,&tmpd); 
            }
            *data = (double)tmpd;
        }
        else if (type == 2){
            for (j=1; comma[i+1]-j>comma[i]; j++){
                inputAscii--;
                sscanf(*inputAscii,format,&tmpi); 
            }
            *data = (int)tmpi;
        }
        else if (type == 3){
            for (j=1; comma[i+1]-j>comma[i]; j++){
                inputAscii--;
                sscanf(*inputAscii,format,&tmpc); 
            }
            *data = (int)tmpc;
        }        
        
        if (comma[i+1]-comma[i]==1){
            *data = (int)65535;
        }
        
        
        if (type == 4) {
            switch (i+1){
                case 4:
                    *data = (int)78;
                    break;
                case 6:
                    *data = (int)69;
                    break;
                case 11:
                    *data = (int)87;
                    break;
            }
        
        }
    }
//     
//             inputAscii = &inputArray[comma[1]-1];
//         for (j=1; comma[1]-j>comma[0]; j++){
//             inputAscii--;
//             sscanf(*inputAscii,"%lf",&tmpd); 
    
    
    //y1[0] = tmpd;*/
    
    // To access value at input *u[i], to send to output y[i] = *u[i]
    //y0[0] = *inputArray[0];
    //y1[0] = tmpd;//*inputArray[1];
    //y2[0] = tmpi;//*inputArray[2];
    //y3[0] = comma[2];//*inputArray[3];
}


/* Function: mdlTerminate =====================================================
 * Abstract:
 *    No termination needed, but we are required to have this routine.
 */
static void mdlTerminate(SimStruct *S)
{
}



#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
