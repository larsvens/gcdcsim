/* $Revision: 1.3 $ $Date: 2002/03/25 03:57:47 $ */
/* xpctimeinfo.c - xPC Target, non-inlined S-function driver for xPC kernel timing information */
/* Copyright 1996-2002 The MathWorks, Inc.
*/

#define 	S_FUNCTION_LEVEL 	2
#undef 		S_FUNCTION_NAME
#define 	S_FUNCTION_NAME 	synchronizetime
#include 	<stddef.h>
#include 	<stdlib.h>

#include 	"simstruc.h" 

#ifdef 		MATLAB_MEX_FILE
#include 	"mex.h"
#endif

#ifndef 	MATLAB_MEX_FILE
#include 	<windows.h>
#include 	<math.h>
#include 	"time_xpcimport.h"
#endif



/* Input Arguments */
#define NUMBER_OF_ARGS          (0)



#define NO_I_WORKS              (0)

#define NO_R_WORKS              (0)


static void mdlInitializeSizes(SimStruct *S)
{

  ssSetNumSFcnParams(S, 0);
  if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
	return; /* Parameter mismatch will be reported by Simulink */
  }
  
  //DEBUG Output signal
  ssSetNumOutputPorts(S, 1);
  ssSetOutputPortWidth(S, 0, 8);
  
  //Input signal
  ssSetNumInputPorts(S, 3);
  ssSetInputPortWidth(S, 0, 1);
  ssSetInputPortWidth(S, 1, 1);
  ssSetInputPortWidth(S, 2, 1);
  ssSetInputPortDataType(S, 0, SS_DOUBLE);
  ssSetInputPortDataType(S, 1, SS_DOUBLE);
  ssSetInputPortDataType(S, 2, SS_DOUBLE);
}
 
static void mdlInitializeSampleTimes(SimStruct *S)
{
   
    ssSetSampleTime(S, 0, 0.01);
	ssSetOffsetTime(S, 0, 0.0);
	ssSetModelReferenceSampleTimeDefaultInheritance(S);
}

static void mdlOutputs(SimStruct *S, int_T tid)
{

#ifndef MATLAB_MEX_FILE
    
    //Create pointers to input signals, datePtr points to a date string ddmmyy
    //timePtr points to a UTC time hhmmss.cc
    InputRealPtrsType datePtr = ssGetInputPortRealSignalPtrs(S,0);
    InputRealPtrsType timePtr = ssGetInputPortRealSignalPtrs(S,1);
    InputRealPtrsType statePtr = ssGetInputPortRealSignalPtrs(S,2);
    
    //Create output signal pointer for DEBUGGING
    real_T *y = ssGetOutputPortSignal(S,0);
    
    int year = 2000 + (*datePtr[0]) - floor((*datePtr[0])/100)*100;
    int month = floor((*datePtr[0])/100)-floor((*datePtr[0])/10000)*100;
    int day = floor((*datePtr[0])/10000);
    int hour = floor((*timePtr[0])/10000);
    int min = floor((*timePtr[0])/100)-floor((*timePtr[0])/10000)*100;
    int sec = floor((*timePtr[0]))-floor((*timePtr[0])/100)*100;
    int msec = (floor((*timePtr[0])*100)-floor((*timePtr[0]))*100)*10;
    int timeDiff;
    
    //Create SYSTEMTIME structures for time synchronization
    SYSTEMTIME systemTime;
    SYSTEMTIME *systemTimePtr;
    systemTimePtr = &systemTime;    
    
    //Fetch the current systemtime to check for drift
    GetSystemTime(systemTimePtr);
    
//     //Calculate time difference in milliseconds
     timeDiff = ((*systemTimePtr).wSecond - sec)*1000 + ((*systemTimePtr).wMilliseconds - msec);
//     
    //If the drift exceeds 20 milliseconds and the GPS status is active set the clock right
    if ( *statePtr[0] == 0 || timeDiff > 20) //originally 150 ms
    {
        (*systemTimePtr).wYear = year;
        (*systemTimePtr).wMonth = month;
        (*systemTimePtr).wDay = day;
        //wDayOfWeek is not used so just set to 1
        (*systemTimePtr).wDayOfWeek = 1;
        (*systemTimePtr).wHour = hour;
        (*systemTimePtr).wMinute = min;
        (*systemTimePtr).wSecond = sec;
        (*systemTimePtr).wMilliseconds = msec;
        
        SetSystemTime(systemTimePtr);
    }

	//DEBUGGING	OUTPUTS
    y[0]=sec;
    y[1]=msec;
    y[2]=*statePtr[0];
    y[3]=systemTime.wHour;
    y[4]=systemTime.wMinute;
    y[5]=systemTime.wSecond;
    y[6]=systemTime.wMilliseconds;
    y[7]=timeDiff;

#endif
        
}


static void mdlTerminate(SimStruct *S)
{   
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif


