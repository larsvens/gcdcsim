/* $Revision: 1.3 $ $Date: 2002/03/25 03:57:47 $ */
/* xpctimeinfo.c - xPC Target, non-inlined S-function driver for xPC kernel timing information */
/* Copyright 1996-2002 The MathWorks, Inc.
*/

#define 	S_FUNCTION_LEVEL 	2
#undef 		S_FUNCTION_NAME
#define 	S_FUNCTION_NAME 	generateTimeStamp

#include 	<stddef.h>
#include 	<stdlib.h>

#include 	"simstruc.h" 

#ifdef 		MATLAB_MEX_FILE
#include 	"mex.h"
#endif

#ifndef 	MATLAB_MEX_FILE
#include 	<windows.h>
#include 	<math.h>
#include 	"time_xpcimport.h"
#endif

#define TICKS_PER_SECOND 10000000  //Number of 100ns ticks per second
// #define EPOCH_DIFF 116444736000000000 //Number of 100nanoseconds periods from 1601-01-01-00:00 to 1970-01-01-00:00
// #define EPOCH_DIFF 125911584000000000 //Number of 100nanoseconds periods from 1601-01-01-00:00 to 2000-01-01-00:00
#define    EPOCH_DIFF 127173888000000000 //Number of 100nanoseconds periods from 1601-01-01-00:00 to 2004-01-01-00:00
// #define EPOCH_DIFF    127173924000000000 //Number of 100nanoseconds periods from 1601-01-01-00:00 to 2004-01-01-01:00AM

/* Input Arguments */
#define NUMBER_OF_ARGS          (0)



#define NO_I_WORKS              (0)

#define NO_R_WORKS              (0)


static void mdlInitializeSizes(SimStruct *S)
{

  ssSetNumSFcnParams(S, 0);
  if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
	return; /* Parameter mismatch will be reported by Simulink */
  }
  
  //Output signal
  ssSetNumOutputPorts(S, 2);
  ssSetOutputPortWidth(S, 0, 1);
  ssSetOutputPortWidth(S, 1, 1);

}
 
static void mdlInitializeSampleTimes(SimStruct *S)
{
   
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
	ssSetOffsetTime(S, 0, 0.0);
	ssSetModelReferenceSampleTimeDefaultInheritance(S);
}

static void mdlOutputs(SimStruct *S, int_T tid)
{
    
    //every 10th second update clock from GPS
    //Otherwise just output time

#ifndef MATLAB_MEX_FILE

    //Pointer to output signals [sec,msec]
    real_T *y0 = (real_T *)ssGetOutputPortSignal(S,0);
    real_T *y1 = (real_T *)ssGetOutputPortSignal(S,1);
    
    //Variables for handling timestamp conversion
    ULARGE_INTEGER windowsEpochTime;
    double unixTime;
    double unixTime_sec;
    double unixTime_msec;
    
    //Create FILETIME structures for time synchronization
    FILETIME fileTime;
    FILETIME *fileTimePtr;
    fileTimePtr = &fileTime;
    
    //Get system time from SpeedGoat Real Time Target represented as number 
    // of 100-ns intervals since windows epoch
    GetSystemTimeAsFileTime(fileTimePtr);
    
    //Put the variables from the FILETIME structure in the windowsEpochTime 
    windowsEpochTime.LowPart =(*fileTimePtr).dwLowDateTime;
    windowsEpochTime.HighPart =(*fileTimePtr).dwHighDateTime;
    
    //Convert Windows epoch to unix epoch for timestamping
    unixTime = windowsEpochTime.QuadPart - EPOCH_DIFF;
    unixTime = unixTime/TICKS_PER_SECOND;
    
	//Extract seconds and milliseconds seperately
    unixTime_sec = floor(unixTime);
    unixTime_msec = floor((unixTime-unixTime_sec)*1000);
    
    y0[0] = unixTime_sec;
    y1[0] = unixTime_msec;

#endif
        
}




static void mdlTerminate(SimStruct *S)
{   
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif


