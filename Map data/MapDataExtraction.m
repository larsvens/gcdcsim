% script to extract map data from .csv file

clear,close all;

% save data to matlab table
data = readtable('A270LeftSideMarker.csv');

% extract latitude and longitude data
lat = str2num(char(data.lat));
lon = str2num(char(data.lon));

% flip direction
% lat = flip(lat);
% lon = flip(lon);

lat0 = lat(1);
lon0 = lon(1);


% plot check
plot(lon,lat,'.',lon(1:10),lat(1:10),'.',lon0,lat0,'*')

% save to .mat
leftLMCoords = [lon lat];
save('LeftLMData','leftLMCoords');