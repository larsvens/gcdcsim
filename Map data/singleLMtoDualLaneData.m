% script to construct dual lane data from single (leftmost) lane marking data
% lane width = 3.35 m
clear;close all;

% load left lane data
load('LeftLMData'); 
lonLL = leftLMCoords(:,1);
latLL = leftLMCoords(:,2);

% convert to ENU
lat0 = latLL(1);
lon0 = lonLL(1);

figure; plot(lonLL,latLL,'.',lon0,lat0,'*')

h0 = 0; % meters above ellipsoid approximation
h = h0*ones(size(latLL));
spheroid = wgs84Ellipsoid;
[xEastLL,yNorthLL,zUp] = geodetic2enu(latLL,lonLL,h,lat0,lon0,h0,spheroid);

% check plot
figure; plot(lonLL,latLL,'.',lonLL(1:10),latLL(1:10),'.',lon0,lat0,'*')
% figure; plot(xEastLL,yNorthLL,'.')
% figure; plot(zUpLL)

% interpolate to get more points
n = 5000; % desired number of points (n=5k --> average 1 m between points)
% xEastLL = interp1(linspace(0,1,length(xEastLL))',xEastLL,linspace(0,1,n)','linear');
% yNorthLL = interp1(linspace(0,1,length(yNorthLL))',yNorthLL,linspace(0,1,n)','linear');
% zUp = interp1(linspace(0,1,length(zUp))',zUp,linspace(0,1,n)','linear');
xEastLL_new = linspace(max(xEastLL),min(xEastLL),n)';
yNorthLL = interp1(xEastLL,yNorthLL,xEastLL_new,'linear');
zUp = interp1(linspace(0,1,length(zUp))',zUp,linspace(0,1,n)','linear');
xEastLL = xEastLL_new;

% calculate xEast and yNorth for rightmost lane marking
laneWidth = 3.35;
alpha = atan(diff(yNorthLL)./diff(xEastLL));
alpha = [alpha; alpha(end)];
xEastOffset = 2*laneWidth*sin(alpha);
yNorthOffset = 2*laneWidth*cos(alpha);
xEastRL = xEastLL - xEastOffset;
yNorthRL = yNorthLL + yNorthOffset;

% check plot
%figure; plot(xEastLL,yNorthLL,'.',xEastRL,yNorthRL,'.')

% calculate xEast and yNorth for middle lane marking
xEastML = xEastLL - 0.5*xEastOffset;
yNorthML = yNorthLL + 0.5*yNorthOffset;

% put the origin at ML(1)
shiftXEast = xEastML(1);
shiftYNorth = yNorthML(1);
xEastLL = xEastLL - shiftXEast;
yNorthLL = yNorthLL - shiftYNorth;
xEastML = xEastML - shiftXEast;
yNorthML = yNorthML - shiftYNorth;
xEastRL = xEastRL - shiftXEast;
yNorthRL = yNorthRL - shiftYNorth;


% figure; plot(xEastVehInit,yNorthVehInit,'*')
% hold on;
% plot(xEastLL,yNorthLL,'.',xEastRL,yNorthRL,'.', xEastML, yNorthML, '.')
% hold off;

% save ENU coordinates to struct
laneDataENU.xEastLL = xEastLL;
laneDataENU.yNorthLL = yNorthLL;
laneDataENU.xEastML = xEastML;
laneDataENU.yNorthML = yNorthML;
laneDataENU.xEastRL = xEastRL;
laneDataENU.yNorthRL = yNorthRL;


% Origin for conversion to geocecic
xEast0  = xEastML(1);
yNorth0 = yNorthML(1);

% define ML(1) as origin in geodecic
[lat0_,lon0_,~] = enu2geodetic(xEast0,yNorth0,0,lat0,lon0,h0,spheroid);

% convert all three lane markings back to lat and lon
[latLL_,lonLL_,~] = enu2geodetic(xEastLL,yNorthLL,zUp,lat0_,lon0_,h0,spheroid);
[latRL_,lonRL_,~] = enu2geodetic(xEastRL,yNorthRL,zUp,lat0_,lon0_,h0,spheroid);
[latML_,lonML_,~] = enu2geodetic(xEastML,yNorthML,zUp,lat0_,lon0_,h0,spheroid);



% check plot
%figure; plot(lonLL_,latLL_,'.',lonML_,latML_,'.',lonRL_,latRL_,'.',lonLL,latLL,'*')

% save to .mat
laneData = struct;
laneData.lonLL = lonLL_;
laneData.latLL = latLL_;
laneData.lonML = lonML_;
laneData.latML = latML_;
laneData.lonRL = lonRL_;
laneData.latRL = latRL_;
%laneData.LonVehInit = LonVehInit_;
%laneData.LatVehInit = LatVehInit_;
%laneData.VehOrientInit  = alpha(1); %Measured on the x axis

save('DualLaneData','laneData','laneDataENU')


% save geodecic origin to .mat
geodeticOrigin.lat = lat0_;
geodeticOrigin.lon = lon0_;

save('geodeticOrigin','geodeticOrigin')


