function [ latError, aheadLatError, headingErrorDegrees ] = getLatErrorFunction(xFwdCL,yLftCL)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

latError = yLftCL(1,1);

% disp('Iteration')
% 
% disp(yRgtCL(1,1))

look_ahead_distance = 5;



delta_x = diff(xFwdCL);
delta_y = diff(yLftCL);



angles = atan2(delta_y, delta_x);
angles_degrees = angles*(180/pi);

% disp(angles_degrees(1:5))

distance_traveled = (delta_x.^2 + delta_y.^2).^.5;

distance_traveled = [0; cumsum(distance_traveled)];

% disp(distance_traveled)

[~,look_ahead_index] = min((distance_traveled-look_ahead_distance).^2);

% disp(look_ahead_index)

aheadLatError = yLftCL(look_ahead_index,1); 

headingErrorDegrees = angles_degrees(1);

% disp(latError)

end

