function [ currentRatio ] = laneChangingRatioFunction( currentIndex, endIndex )
%LANECHANGINGRATIOFUNCTION This function outputs a ratio (between 0 and 1)
% corresponding to the ammount of lane change between original lane (0) and
% final lane (1).
% As inputs the function receives the currentIndex of the maneuver, and
% the endIndex of the maneuver (which equals the index length of the 
% maneuver)

% angleRange = (pi/2)*0.5;
% amplitude = abs(tan(angleRange));
% 
% currentRatio = tan(-angleRange + (currentIndex/endIndex)*2*angleRange);
% currentRatio = currentRatio/amplitude;


% OPTION 1

angleRange = (pi/2)*0.5;
amplitude = abs(tan(angleRange));

currentRatio = tan(-angleRange + (currentIndex/endIndex)*2*angleRange);
currentRatio = currentRatio/amplitude;

currentRatio = 0.5 + 0.5*currentRatio;

% OPTION 2

% smoothingFactor = 4;
% amplitude = atan(smoothingFactor);
% 
% currentRatio = atan(-smoothingFactor + 2*smoothingFactor*(currentIndex/endIndex));
% currentRatio = currentRatio/amplitude;

% currentRatio = 1 + 0.5*currentRatio;

end

