function [xFwd, yRgt] = ENU2Ego(xEast,yNorth,egoXEast,egoYNorth,egoHeading)
    % Description:
    % converts a coordinate pair xEast,yNorth to the ego coordinate system
    % defined as xFwd (m) in forward direction and yRgt (m) in right
    % direction
    % heading in degrees
    % xEast,yNorth row vectors (m)
    
    egoHeading = -egoHeading; 
    
    R = [cosd(egoHeading) -sind(egoHeading);
         sind(egoHeading)  cosd(egoHeading)];
    relPosENU = [xEast-egoXEast;yNorth-egoYNorth];
    posEgo = R*relPosENU;
    xFwd = posEgo(1,:);
    yRgt = posEgo(2,:);
end