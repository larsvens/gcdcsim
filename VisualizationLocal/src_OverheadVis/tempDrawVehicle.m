%test drawvehicle
clear; close all;
% input data:
% *gps coordinate of vehicle center 
% *heading 

%test1
x = [1 1 3 3 1];
y = [1 2 2 1 1];

%plot (x,y)


% test2 
heading = pi/3;
cXY = [3;
       3];
   
LW = [2; 
      1];

% rotation matrix
R = [cos(heading) -sin(heading);
     sin(heading)  cos(heading)];

corners = [cXY(1)-LW(1)/2 cXY(1)-LW(1)/2 cXY(1)+LW(1)/2 cXY(1)+LW(1)/2 cXY(1)-LW(1)/2;
           cXY(2)-LW(2)/2 cXY(2)+LW(2)/2 cXY(2)+LW(2)/2 cXY(2)-LW(2)/2 cXY(2)-LW(2)/2];

% plot unrotated
plot(corners(1,:),corners(2,:))
axis([0 5 0 5]);

% rotate
[m,n] = size(corners);
newCorners = zeros(m,n);
for i=1:n
    newCorners(:,i) = R*(corners(:,i)-cXY)+cXY;
end

% plot rotated
hold on
plot(newCorners(1,:),newCorners(2,:),'r*')
hold off


%test3
corners = [-LW(1)/2 -LW(1)/2 LW(1)/2  LW(1)/2 -LW(1)/2;
           -LW(2)/2  LW(2)/2 LW(2)/2 -LW(2)/2 -LW(2)/2];
% rotate
[m,n] = size(corners);
newCorners = zeros(m,n);
for i=1:n
    newCorners(:,i) = R*corners(:,i)+cXY;
end
% plot rotated
hold on
plot(newCorners(1,:),newCorners(2,:),'g')
hold off

