% test ENU2geodetic
clear; close all;
% matlab
ECEFg0 = [51.474511; 5.632483; 0]; % lat lon height
point1 = [0; 0; 0]; % xEast,yNorth,zUp
point2 = [100; 50; 0]; % xEast,yNorth,zUp
spheroid = wgs84Ellipsoid;

xEast = linspace(0,100,10)';
yNorth= sin(0.1*xEast);
zUp = 0;

% testplot
%figure; plot(xEast,yNorth)

N = length(xEast);
latM = zeros(N,1);
lonM = zeros(N,1);
latK = zeros(N,1);
lonK = zeros(N,1);
for i = 1:N
    % Matlab
    [latM(i),lonM(i),hgtM(i)] = enu2geodetic(xEast(i),yNorth(i),zUp,ECEFg0(1),ECEFg0(2),ECEFg0(3),spheroid);

    % KHAN
    [latK(i),lonK(i),hgtK(i)] = ENU2geodeticECEF((pi/180)*ECEFg0,xEast(i),yNorth(i),zUp);
end


figure; 
plot(lonM,latM)
hold on
plot(lonK,latK,'*')
%legend('matlab','khan')
hold off