function tuningParams = getInitTuningCell
    % Description
    % defines initial tuning in a cell
    tuningParams = zeros(12,2);
    tuningParams(1,1) = 3;
    tuningParams(1,2) = 1;
    tuningParams(2,:)=[50 50];

end