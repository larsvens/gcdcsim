function [DrivableAreaPositionWeights,DrivableAreaHeadingWeights,...
DrivableAreaCurvatureWeights,EgoHeadingWeights,...
EgoCurvatureWeights,EgoPositionWeights,...
PreviousPositionWeights,PreviousHeadingWeights,...
PreviousCurvatureWeights,TargetPositionWeights,...
TargetHeadingWeights,TargetCurvatureWeights,...
EnableLateralStartOffset,LateralStartOffsetBasedOnPreviousTrail] = tuningCellToProperties(TuningDataCell)
    % Description:
    % translates Cell array in tuningtable to obj params
    DrivableAreaPositionWeights = cell2mat(TuningDataCell(1,1:2));
    DrivableAreaHeadingWeights = cell2mat(TuningDataCell(2,1:2));
    DrivableAreaCurvatureWeights = cell2mat(TuningDataCell(3,1:2));
    EgoHeadingWeights = cell2mat(TuningDataCell(4,1:2));
    EgoCurvatureWeights = cell2mat(TuningDataCell(5,1:2));
    EgoPositionWeights = cell2mat(TuningDataCell(6,1:2));
    PreviousPositionWeights = cell2mat(TuningDataCell(7,1:2));
    PreviousHeadingWeights = cell2mat(TuningDataCell(8,1:2));
    PreviousCurvatureWeights = cell2mat(TuningDataCell(9,1:2));
    TargetPositionWeights = cell2mat(TuningDataCell(10,1:2));
    TargetHeadingWeights = cell2mat(TuningDataCell(11,1:2));
    TargetCurvatureWeights = cell2mat(TuningDataCell(12,1:2));
    
    EnableLateralStartOffset = cell2mat(TuningDataCell(14,1));
    LateralStartOffsetBasedOnPreviousTrail = cell2mat(TuningDataCell(15,1));
end
