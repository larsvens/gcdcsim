classdef GUIMain < handle
    % This is the main class of a tuning tool for the lateral path planning
    % algorithm 
    %
    %______________________________________________________________________
        
    properties (Access = private)% Handles to graphics objects
        % Figures tabgroups and tabs
        hFigMain                 	% main figure object
        hTabGroupControl            % main tab group
        hTabDataInput               % data input tab
        hTabOverheadVis             % overhead visuaization tab
        hTabFreqAnalysis            % frequency analysis tab
        hTabRideQuality             % ride quality evaluation tab
        hTabInfo                    % information tab
        % Data input tab
        hPanelTuningInput
        hPanelLoadInput
        hTableTuning
        hInputWeightTuning
        hPushButtonLoad
        hTextBoxLoadStatus
        hTextBoxFileName
        hTextBoxLongVelocity
        % OverheadVis tab
        hOverheadVisEgo           % overhead visualization object local coordinate sys
        hOverheadVisENU             % overhead visualization object Geodetic coordinate sys
        hSlider                     % slider for overhead visualization
        hPanelOverheadVis           % main overhead visualization panel
        hTableOHInfo
    end
    properties (Access = public)    % Data used in the GUI
        % Data input tab
        fileName
        timeRange
        sampleTime
        fileType
        scenario
        
        % Ovehead vis tab
        laneDataENU
    end
    
    % Constructor & initialization methods
    methods 
        function obj = GUIMain
            % Constructor
            % creates the main GUI object
            % add sources
            homePath = fileparts(mfilename('fullpath'));
            addpath(genpath([homePath,'\src_DataInput']));
            addpath(genpath([homePath,'\src_OverheadVis']));
            addpath(genpath([homePath,'\Classes']));
            % create UI components
            obj.createUIComponents();
            % initiate UI components
            obj.initUIComponents();
            % Set callbacks
            obj.setCallbacks();
        end
        
        function createUIComponents(obj)
            % Create all of the UI components of the graphical interface
            % and populate the properties of the object
           
            % Create main figure object
            obj.hFigMain = figure(...
                'MenuBar','none',...
                'Color','w',...
                'Name','PathPlannerTuningTool ',...
                'NumberTitle','off',...
                'units','normalized',...
                'outerposition',[0 .03 1 .97],...
                'HandleVisibility','off',...
                'Tag','figureControl',...
                'ToolBar','figure',...
                'Visible', 'off');
            
            %define standard distances in plot windows
            stdWidth = 0.2;
            stdHeight = 0.18;
            center = 0.5;
            stdDist = .05;
            
            % Create main tab group
            obj.hTabGroupControl = uitabgroup(obj.hFigMain);
            
            %___________________________
            % Data input tab
            % create tab
            obj.hTabDataInput = uitab(obj.hTabGroupControl, 'Title', 'Data Input');
            set(obj.hTabDataInput,'BackgroundColor','w')
            
            % create DataInput panel
            obj.hPanelLoadInput = PanelObject([center-stdWidth,stdDist,2*stdWidth,1-2*stdDist],...
                'Loaded data',12,'w',obj.hTabDataInput);
            
            % create Tuning input table
            ColumnWidth = num2cell(100*[1 1]);
            obj.hTableTuning = TableObject([center-3.3/2*stdWidth,center-0.5*stdHeight,3.3*stdWidth,2.18*stdHeight],...
                {'Filename',...
                'Filetype',...
                'Scenario',...
                'TimeRange'},...
                {[]},[false false],ColumnWidth,obj.hPanelLoadInput.hPanel);
            

            % create load pushbutton
            obj.hPushButtonLoad = PushButton([center-stdWidth,stdDist,2*stdWidth,2*stdDist],'Load Data',[1 .5 0],obj.hPanelLoadInput.hPanel);
           
            % create loadstatus textbox
            obj.hTextBoxLoadStatus = TextBoxObject([center-stdWidth,3*stdDist,2*stdWidth,stdDist],obj.hPanelLoadInput.hPanel);            
   
            
            %___________________________
            % Overhead visualization tab
            % create tab
            obj.hTabOverheadVis = uitab(obj.hTabGroupControl, 'Title', 'Overhead Visualization');
            set(obj.hTabOverheadVis,'BackgroundColor','w')
            % create OverheadVisEgo
            obj.hOverheadVisEgo = OverheadVisObject([0.05,0.05,0.35,0.9],...
                'Ego coordinate system','yRgt [m]','xFwd [m]',[50 70],[0 25], obj.hTabOverheadVis);
            % create OverheadVisENU
            obj.hOverheadVisENU = OverheadVisObject([0.45,0.54,0.5,0.4],...
                'ENU coordinate system','xEast [m]','yNorth [m]',[160 70],[0,0], obj.hTabOverheadVis);
            % create main OverheadVis IO panel
            obj.hPanelOverheadVis = PanelObject([0.45,0.05,0.5,0.4],...
                '',12,'w',obj.hTabOverheadVis);
            % create slider object
            obj.hSlider = SliderObject([stdDist,stdDist,1-2*stdDist,0.05],'',[.005 .01],obj.hPanelOverheadVis.hPanel);
            % create OHVis info table object
            obj.hTableOHInfo = TableObject([stdDist,0.2,1-2*stdDist,0.7],...
                {'Time','latitude','longitude'},...
                {''},false,...
                'auto',obj.hPanelOverheadVis.hPanel);
            % Show GUI
            set(obj.hFigMain, 'Visible', 'on');
        end
        
        function initUIComponents(obj)
            % set init strings in data input text boxes
            obj.hTextBoxLoadStatus.setText('Load status: Ready');     
        end
        
        function setCallbacks(obj)
            % Assign the callback functions for the GUI components
            set(obj.hSlider.getUICtrlHandle, 'Callback', @obj.sliderCallback);
            set(obj.hPushButtonLoad.getUICtrlHandle, 'Callback', @obj.loadButtonCallback);
        end        
        
        function loadData(obj)
            % update load status text box
            obj.hTextBoxLoadStatus.setText('Load status: Loading...');
            
            % read simout file 
            [obj.fileName,PathName,~] = uigetfile('mat','Select data to analyze');
            S1 = load(strcat(PathName,obj.fileName));
            % load static map data
            if exist('..\DualLaneData.mat', 'file')
                S2 = load('..\DualLaneData');
            else
                if exist('DualLaneData.mat', 'file')
                    S2 = load('DualLaneData');
                else
                    error(['DualLaneData.mat file not found in the current ', ...
                        'or parent directory']);
                end
            end
            
            % times
            obj.timeRange = S1.simlog1.EgoData.GPS.lon.Time(end);
            obj.sampleTime = 0.01;%S1.simlog1.Tsim.Data(1); 
            % convert to ENU coordinates (use first coordinates of ML as reference)
            % note: export ENU origin from sim
            spheroid = wgs84Ellipsoid;
            lat0 = S2.laneData.latML(1);
            lon0 = S2.laneData.lonML(1);
            h0 = 0;
            xEastLL = S2.laneDataENU.xEastLL';
            yNorthLL = S2.laneDataENU.yNorthLL';
            xEastML = S2.laneDataENU.xEastML';
            yNorthML = S2.laneDataENU.yNorthML';
            xEastRL = S2.laneDataENU.xEastRL';
            yNorthRL = S2.laneDataENU.yNorthRL';
            
            %_________
            % ENU plot            
            % Create lane marking objects in ENU plot
            m = length(S1.simlog1.EgoData.GPS.lon.Time);
            % left lane marking (ID 1)
            xEastLL = repmat(xEastLL,m,1);
            yNorthLL = repmat(yNorthLL,m,1); 
            obj.hOverheadVisENU.addLine('k','none',8,obj.hFigMain);
            obj.hOverheadVisENU.initiateLine(xEastLL,yNorthLL,1);
            % middle lane marking (ID 2)
            xEastML = repmat(xEastML,m,1);
            yNorthML = repmat(yNorthML,m,1);
            obj.hOverheadVisENU.addLine('k','none',8,obj.hFigMain);
            obj.hOverheadVisENU.initiateLine(xEastML,yNorthML,2);
            % right lane marking (ID 3)
            xEastRL = repmat(xEastRL,m,1);
            yNorthRL = repmat(yNorthRL,m,1);
            obj.hOverheadVisENU.addLine('k','none',8,obj.hFigMain);
            obj.hOverheadVisENU.initiateLine(xEastRL,yNorthRL,3);
            % Create vehicle objects in ENU plot
            % EGOVehicle
            egoVehTrajENU = zeros(length(S1.simlog1.EgoData.GPS.lat.Data),3);
            [egoVehTrajENU(:,1),egoVehTrajENU(:,2),~] = geodetic2enu(S1.simlog1.EgoData.GPS.lat.Data,...
                S1.simlog1.EgoData.GPS.lon.Data,0,lat0,lon0,h0,spheroid);
            egoVehTrajENU(:,3) = S1.simlog1.EgoData.GPS.heading.Data;
            obj.hOverheadVisENU.addVehicle('b','none',8,obj.hFigMain);
            EGOLength = 2.55;   % RCV length
            EGOWidth = 1.70;    % RCV width
            EGOVehID = 1;
            obj.hOverheadVisENU.initiateVehicle(EGOLength,EGOWidth,egoVehTrajENU,EGOVehID);
            % dummyVehicles
            nrOfDummies = 5;
            for i = 1:nrOfDummies  
%                 S1.simlog1.DummyVehicleState.xEast.Data
                dummyVehTrajENU = zeros(length(S1.simlog1.WorldState.vehicle1.vehicleStateBus.xEast.Data),3);
                dummyVehTrajENU(:,1) = getfield(S1,'simlog1','WorldState', ['vehicle' num2str(i)], 'vehicleStateBus','xEast','Data');
                dummyVehTrajENU(:,2) = getfield(S1,'simlog1','WorldState', ['vehicle' num2str(i)], 'vehicleStateBus','yNorth','Data');
                dummyVehTrajENU(:,3) = getfield(S1,'simlog1','WorldState', ['vehicle' num2str(i)], 'vehicleStateBus','heading','Data');
                obj.hOverheadVisENU.addVehicle('r','none',8,obj.hFigMain);
                dummyVehLength = 4.5;
                dummyVehWidth = 2.7;
                dummyVehID = i+1; % Ego ID = 1
                obj.hOverheadVisENU.initiateVehicle(dummyVehLength,dummyVehWidth,dummyVehTrajENU,dummyVehID);
            end
            
            %_________
            % Ego plot             
            % Create lane marking objects in Ego plot
            % left lane marking (ID 1)
            xFwdLL = S1.simlog1.laneMarkings.xFwdLL.Data;
            yLftLL = S1.simlog1.laneMarkings.yLftLL.Data;
            obj.hOverheadVisEgo.addLine('k','none',8,obj.hFigMain);
            obj.hOverheadVisEgo.initiateLine(yLftLL,xFwdLL,1);
            % middle lane marking (ID 2)
            xFwdML = S1.simlog1.laneMarkings.xFwdML.Data;
            yLftML = S1.simlog1.laneMarkings.yLftML.Data;
            obj.hOverheadVisEgo.addLine('k','none',8,obj.hFigMain);
            obj.hOverheadVisEgo.initiateLine(yLftML,xFwdML,2);
            % right lane marking (ID 3)
            xFwdRL = S1.simlog1.laneMarkings.xFwdRL.Data;
            yLftRL = S1.simlog1.laneMarkings.yLftRL.Data;
            obj.hOverheadVisEgo.addLine('k','none',8,obj.hFigMain);
            obj.hOverheadVisEgo.initiateLine(yLftRL,xFwdRL,3);
            % CenterLine (EgoLineID = 4)
            xFwdCL = S1.simlog1.centerLine.xFwdCL.Data;
            yLftCL = S1.simlog1.centerLine.yLftCL.Data;
            obj.hOverheadVisEgo.addLine('r','.',8,obj.hFigMain);
            obj.hOverheadVisEgo.initiateLine(yLftCL,xFwdCL,4);
            % Create vehicle objects in Ego plot
            obj.hOverheadVisEgo.addVehicle('b','none',8,obj.hFigMain);
            trajectoriesEgo = zeros(length(S1.simlog1.EgoData.GPS.lat.Data),3);
            obj.hOverheadVisEgo.initiateVehicle(EGOLength,EGOWidth,trajectoriesEgo,1);
            obj.hOverheadVisEgo.updateVehicles(1);

            % draw plots in GUI
            firstSliderIndex = 1;
            obj.hOverheadVisENU.updateLines(firstSliderIndex);
            obj.hOverheadVisENU.updateVehicles(firstSliderIndex);
            obj.hOverheadVisENU.plotOverheadVis;
            
            obj.hOverheadVisEgo.updateLines(firstSliderIndex);
            obj.hOverheadVisENU.updateVehicles(firstSliderIndex);
            obj.hOverheadVisEgo.plotOverheadVis;
        end         
    end % methods
    
    
    % Callback methods
    methods
        %_____________________________
        % Data input callbacks        
        function loadButtonCallback(obj,~,~)
            % Description
            % execute loadData when run button is klicked and update simstatus text box
            % update loadstatus text box
            obj.hTextBoxLoadStatus.setText('Load status: Choosing file...');
            pause(0.1);
            obj.loadData();
            pause(0.1);
            obj.hTextBoxLoadStatus.setText('Load status: File loaded');
            
            % set entries in data input table
            obj.fileType = 'local simulation';
            obj.scenario = 'scenario1';
            
            
            obj.hTableTuning.setData({obj.fileName; obj.fileType; obj.scenario; obj.timeRange});
        end
        
        
        
        %_________________________________
        % Overhead visualization callbacks
        function sliderCallback(obj,~,~)
            % Descripition:
            % Silder Callback
            % update slider value
            obj.hSlider.update(); 
            % get current time
            newTime = obj.hSlider.getValue*obj.timeRange;    
            % set new data in OH info table
            obj.hTableOHInfo.setData([newTime; 0; 0;]);
            % calculate time sample represented by current slider value
            sliderSample = round(obj.hSlider.getValue*obj.timeRange/obj.sampleTime)+1;
            % update lines
            obj.hOverheadVisEgo.updateLines(sliderSample);
            % redraw plot
            obj.hOverheadVisEgo.reDraw(sliderSample);
            % redraw plot
            obj.hOverheadVisENU.reDraw(sliderSample); 
            
            

            
            % update axis for ENU view
            [xEgo,yEgo,~] = obj.hOverheadVisENU.getEgoVehiclePosition;
            obj.hOverheadVisENU.updateAxis(xEgo, yEgo);
            
            % update lane data in EGO view
%             [egoXEast,egoYNorth,egoHeading] = obj.hOverheadVisENU.getEgoVehiclePosition;
%             [xEastLL,yNorthLL,xEastML,yNorthML,xEastRL,yNorthRL] = obj.hOverheadVisENU.getLaneMarkingData;
%             [xFwdLL,yRgtLL] = ENU2Ego(xEastLL',yNorthLL',egoXEast,egoYNorth,egoHeading);
%             [xFwdML,yRgtML] = ENU2Ego(xEastML',yNorthML',egoXEast,egoYNorth,egoHeading);
%             [xFwdRL,yRgtRL] = ENU2Ego(xEastRL',yNorthRL',egoXEast,egoYNorth,egoHeading);
%             laneDataEgo = {xFwdLL; yRgtLL; xFwdML; yRgtML; xFwdRL; yRgtRL};



            
        end
    end % callback methods
end % class


