

classdef SignalViewerLineObject < LineObject
    %Description
    %This class is used together with the SignalViewerObject-class. Each
    %line in the SignalViewer graph is then a SignalViewerLineObject.
    
    properties (Access = private)
        timeData %time vector in signal viewer
        signalData %filtered acc or jerk 
        rawSignalData    %raw acc or jerk data
        rawTimeData     %time data for raw acc or jerk
        rawSignalDataShort    %raw acc or jerk data, equal length to filtered data
        rawTimeDataShort     %time data for raw acc or jerk, equal length to filtered data
        samplingTime
    end
    
    methods
        %Constructor
        function SignalViewerLineObj=SignalViewerLineObject(theParent,theColor,theMarker,...
                theMarkerSize,theFigParent)
            %Call superclass constructor
            SignalViewerLineObj@LineObject(theParent,...
                theColor, theMarker, theMarkerSize, theFigParent); 
        end
        
        function setLineData(SignalViewerLineObj,XData,YData)
            %Description:
            %sets the properties timeData and signalData
            SignalViewerLineObj.timeData = XData;
            SignalViewerLineObj.signalData = YData;
            
            if ~isempty(SignalViewerLineObj.rawSignalData)
                indexStart=find(SignalViewerLineObj.rawTimeData==XData(1));
                SignalViewerLineObj.rawTimeDataShort=XData;
                SignalViewerLineObj.rawSignalDataShort=SignalViewerLineObj.rawSignalData(indexStart:indexStart+length(XData)-1);
            end
            %Make the rawShort data equal length as filtered data
        end
        
        function setRawData(SignalViewerLineObj,XData,YData)
            %Description:
            %sets the properties rawTimeData and rawSignalData
            SignalViewerLineObj.rawTimeData = XData;
            SignalViewerLineObj.rawSignalData = YData;
            SignalViewerLineObj.samplingTime =  single(XData(end)-XData(1))/(length(XData)-1);
            
            
        end
        
        function [XData,YData] = getLineData(SignalViewerLineObj)
            %Description:
            %gets the properties timeData and signalData
            XData = SignalViewerLineObj.timeData;
            YData = SignalViewerLineObj.signalData;
        end
        
        function [XData,YData, samplingTime] = getRawData(SignalViewerLineObj)
            %Description:
            %gets the properties rawTimeData and rawSignalData and
            %samplingstime
            XData = SignalViewerLineObj.rawTimeData;
            YData = SignalViewerLineObj.rawSignalData;
            samplingTime = SignalViewerLineObj.samplingTime;
        end
        
       function [XData,YData] = getRawDataShort(SignalViewerLineObj)
            %Description:
            %gets the properties rawTimeData and rawSignalData and
            %samplingstime
            XData = SignalViewerLineObj.rawTimeDataShort;
            YData = SignalViewerLineObj.rawSignalDataShort;
        end
        
    
    end
end
