classdef TextBoxObject < handle
    properties
        hTextBox             % handle to textbox
        string
    end
    
    methods
        %Constructor
        function textBoxObj = TextBoxObject(thePosition,theParent)
            % Create the textbox
            textBoxObj.hTextBox = uicontrol('Style','text',...
                'BackgroundColor','w',...
                'String',[],...
                'parent',theParent,...
                'Units','normalized',...
                'position',thePosition);
        end
        
        function setText(textBoxObj,string)
            % Description
            % input string and write to text box and textBoxObj property
            textBoxObj.string = string;
            set(textBoxObj.hTextBox,'String',string);
        end
    end
end