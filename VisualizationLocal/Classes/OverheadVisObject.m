classdef OverheadVisObject < handle
    %Description 
    %Class defining the OverheadVisObject object associated to a graph
    %visualizing trails and paths for the tuning tool
    
    properties (Access = private)
        hAxesOverheadVis            % handle to axes where overhead visualization is plotted
        LineObjects = {};           % cell where child objects are stored
        hLineObjects = {};          % cell where handles to matlab line objects are stored
        NrOfLineObjects = 0;        % number of line child objects
        VehicleObjects = {};        % cell where vehicle objects are stored
        hVehicleObjects = {};       % cell where handles to matlab line objects (for vehs) are stored
        NrOfVehicleObjects = 0;     % number of vehicle objects
        axisRange                   % x and y range of the plot window
        axisOffset                  % origin offset in plot window  
    end
    
    methods
        %Constructor
        function OverheadVisObj = OverheadVisObject(position, theTitleString, theXLabel,...
                theYLabel, theAxisRange, theAxisOffset, theParent)
            OverheadVisObj.hAxesOverheadVis = axes(...
                'Parent',theParent,'Position',position);
            
            xlabel( OverheadVisObj.hAxesOverheadVis, theXLabel)
            ylabel( OverheadVisObj.hAxesOverheadVis, theYLabel)
            xMin = -theAxisRange(1)/2+theAxisOffset(1);
            xMax = theAxisRange(1)/2+theAxisOffset(1);
            yMin = -theAxisRange(2)/2+theAxisOffset(2);
            yMax = theAxisRange(2)/2+theAxisOffset(2);
            axis(OverheadVisObj.hAxesOverheadVis,[xMin xMax yMin yMax])
            OverheadVisObj.axisRange = theAxisRange;
            OverheadVisObj.axisOffset = theAxisOffset;
            title(OverheadVisObj.hAxesOverheadVis,theTitleString)
            set(OverheadVisObj.hAxesOverheadVis,'XMinorGrid','on')
            set(OverheadVisObj.hAxesOverheadVis,'YMinorGrid','on')
        end
    end
    
    methods
                
        function addLine(OverheadVisObj,theColor,theMarker,theMarkerSize,hFigMain)
            %Creates and adds a line object to the LineObjects cell
            newOverheadVisLineObj = OverheadVisLineObject(OverheadVisObj,...
                theColor,theMarker,theMarkerSize,hFigMain);

            OverheadVisObj.LineObjects{end+1} = newOverheadVisLineObj;
            OverheadVisObj.NrOfLineObjects = size(OverheadVisObj.LineObjects, 2);
        end

        function addVehicle(OverheadVisObj,theColor,theMarker,theMarkerSize,hFigMain)
            % Creates and adds a vehicle object to the vehicleObjects cell
            newVehicleObject = OverheadVisVehicleObject(OverheadVisObj,...
                theColor,theMarker,theMarkerSize,hFigMain);
                        
            OverheadVisObj.VehicleObjects{end+1} = newVehicleObject;
            OverheadVisObj.NrOfVehicleObjects = size(OverheadVisObj.VehicleObjects, 2);
        end
        
        function plotOverheadVis(OverheadVisObj)
            %Plot all LineObjects
            if isempty(OverheadVisObj.hLineObjects)
                for i=1:OverheadVisObj.NrOfLineObjects
                    theLineObject=OverheadVisObj.LineObjects{i};
                    [XData,YData] = theLineObject.getLine;
                    OverheadVisObj.hLineObjects{i} = line(XData,YData,...
                        'Color', theLineObject.lineColor,...
                        'Marker', theLineObject.lineMarker,...
                        'MarkerSize', theLineObject.lineMarkerSize,...
                        'LineWidth',0.5,...
                        'LineStyle','--',...
                        'Parent', OverheadVisObj.hAxesOverheadVis);                    
                end  
            else
                 OverheadVisObj.reDraw(1);
            end
            
            %Plot all vehicle objects
            for i=1:OverheadVisObj.NrOfVehicleObjects
                theVehicleObject = OverheadVisObj.VehicleObjects{i};
                corners = theVehicleObject.getCorners;
                
                OverheadVisObj.hVehicleObjects{i} = line(corners(1,:),corners(2,:),...
                    'Color', theVehicleObject.lineColor,...
                    'Marker', theVehicleObject.lineMarker,...
                    'MarkerSize', theVehicleObject.lineMarkerSize,...
                    'LineWidth',1,...
                    'LineStyle','-',...
                    'Parent', OverheadVisObj.hAxesOverheadVis);     
            end
        end
        
        function initiateLine(OverheadVisObj,XData,YData,lineID)
           % sets data (for all time indices) in line object with ID "lineID"
           OverheadVisObj.LineObjects{lineID}.setData(XData,YData);
        end
        
        function updateLines(OverheadVisObj,sliderIndex)
            % set new data in line objects
            for i=1:OverheadVisObj.NrOfLineObjects
                OverheadVisObj.LineObjects{i}.setLine(sliderIndex);
            end
        end
        
        function [XData,YData] = getLineData(OverheadVisObj,lineID)
           % gets data in line object with ID "lineID"
           [XData,YData] = OverheadVisObj.LineObjects{lineID}.getData;
        end

        function initiateVehicle(OverheadVisObj,length,width,trajectories,vehID)
            % set size and trajectories in vehicle objects
            OverheadVisObj.VehicleObjects{vehID}.setSize(length,width);
            OverheadVisObj.VehicleObjects{vehID}.setTrajectory(trajectories(:,1),...
                trajectories(:,2),trajectories(:,3));
        end        
        
        function updateVehicles(OverheadVisObj,sliderIndex)
            % set pose data in vehicle objects
            for i=1:OverheadVisObj.NrOfVehicleObjects
                OverheadVisObj.VehicleObjects{i}.setPose(sliderIndex);
            end
        end
        
        function [xEgo,yEgo,headingEgo] = getEgoVehiclePosition(OverheadVisObj)
            % get position data in vehicle objects
            [xEgo,yEgo,headingEgo] = OverheadVisObj.VehicleObjects{1}.getPos;
        end
        
        function reDraw(OverheadVisObj,sliderIndex)
            % reDraw OverheadVis graph
            
            % line objects
            for i = 1:OverheadVisObj.NrOfLineObjects
                theLineObj = OverheadVisObj.LineObjects{i};
                theLineObj.setLine(sliderIndex);
                [lineX,lineY] = theLineObj.getLine;
                set(OverheadVisObj.hLineObjects{i},'XData',lineX);
                set(OverheadVisObj.hLineObjects{i},'YData',lineY);
            end
            %vehicle objects
            for i = 1:OverheadVisObj.NrOfVehicleObjects
                theVehicleObj = OverheadVisObj.VehicleObjects{i};
                theVehicleObj.setPose(sliderIndex);
                corners = theVehicleObj.getCorners;
                set(OverheadVisObj.hVehicleObjects{i},'XData',corners(1,:));
                set(OverheadVisObj.hVehicleObjects{i},'YData',corners(2,:));
            end
        end
        
        function updateAxis(OverheadVisObj,centerX,centerY)
            % set new plot limits in x and y
            set(OverheadVisObj.hAxesOverheadVis,'XLim',[centerX-0.5*OverheadVisObj.axisRange(1)...
                centerX+0.5*OverheadVisObj.axisRange(1)])
            set(OverheadVisObj.hAxesOverheadVis,'YLim',[centerY-0.5*OverheadVisObj.axisRange(2)...
                centerY+0.5*OverheadVisObj.axisRange(2)])
        end
    end
end