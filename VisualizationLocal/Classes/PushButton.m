classdef PushButton
    properties (Access = private)
        hPushButton
    end
    methods
        % Constructor
        function PushButtonObj = PushButton(thePosition,theString,theColor,theParent)
            PushButtonObj.hPushButton = uicontrol('Style','pushbutton',...
                'Units','normalized',...
                'Position', thePosition,...
                'String', theString,...
                'BackgroundColor',theColor,...
                'parent',theParent);
        end
        
        function handle = getUICtrlHandle(TimeInputObj)
            % Description:
            % returns uicontrol handle
            handle = TimeInputObj.hPushButton;
        end         
    end
end
