classdef LineObject <handle
    
    properties
        lineColor %color to use when plotting the step response
        lineMarker %marker to use when plotting the step response
        lineMarkerSize %size of marker to use when plotting the step response
        axesParent %handle to the parent which is of type axes
        hContextMenu %context menu which shows relevant information about the step response line
    end
    
    methods
        
        %Constructor
        function lineObj=LineObject(theParent,...
                theColor, theMarker, theMarkerSize, theFigParent)
             
            lineObj.axesParent=theParent;
            lineObj.lineColor=theColor;
            lineObj.lineMarker=theMarker;
            lineObj.lineMarkerSize=theMarkerSize;
            lineObj.hContextMenu = uicontextmenu('Parent', theFigParent);
        end
        
        function setAdditionalContextMenuInfo(LineObject, contextMenuText)
            
            for i=1:length(contextMenuText)
                 
                uimenu(LineObject.hContextMenu,'Label',...
                 contextMenuText{i},...
                 'ForegroundColor', LineObject.lineColor);
            end
        end
    end
end
