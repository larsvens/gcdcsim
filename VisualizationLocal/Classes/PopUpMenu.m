classdef PopUpMenu
    properties
        hPopUpMenu
    end
    methods
        % Constructor
        function PopUpMenuObj = PopUpMenu(thePosition,theAlternatives,theParent)
            PopUpMenuObj.hPopUpMenu = uicontrol('Style','popupmenu',...
                'Units','normalized',...
                'Position',thePosition,...
                'String',theAlternatives,...
                'parent',theParent);
        end
    end
end
