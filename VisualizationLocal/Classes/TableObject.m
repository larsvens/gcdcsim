classdef TableObject < handle
    properties
        hTable             % handle to uitable
        data
        theRowNames
        theColumnNames
    end
    
    methods
        %Constructor
        function tableObj = TableObject(position,theRowNames,theColumnNames,columnEditable,theColumnWidth,theParent)
            % Create the uitable
            tableObj.hTable = uitable('Data',tableObj.data,...
                'parent',theParent,...
                'Units','normalized',...
                'position',position,...
                'RowName',theRowNames,...
                'ColumnName',theColumnNames,... 
                'ColumnWidth',theColumnWidth,...
                'ColumnEditable', columnEditable);
            
            tableObj.theRowNames=theRowNames;
            tableObj.theColumnNames=theColumnNames;
            
        end
        
        function setData(tableObj,data)
            % Description
            % input data and write to table and data property
            % (executed at gui start and at update of non editable table)
            tableObj.data = data;
            set(tableObj.hTable,'Data',data);
        end
        
        function data = getData(tableObj)
           %Get data in table
           data=tableObj.data;      
        end
        
        function [rowNames,columnNames] = getTableNames(tableObj)
           %Get data in table
           rowNames=tableObj.theRowNames;
           columnNames=tableObj.theColumnNames;
        end
        
        function updateDataFromUI(tableObj)
            % Description
            % reads table and updates data property
            % (executed for editable table at callback)
            tableObj.data = get(tableObj.hTable,'Data');
        end
        
        function handle = getUICtrlHandle(TimeInputObj)
            % Description:
            % returns uicontrol handle
            handle = TimeInputObj.hTable;
        end
    end
end