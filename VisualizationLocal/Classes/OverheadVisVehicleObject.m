classdef OverheadVisVehicleObject < LineObject
    %Description
    %Vehicle object for the overhead visuelization class
    
    properties (Access = private)
        length
        width
        corners_0
        x           % (m) 
        y           % (m) 
        heading     % (degrees)
        xEastTraj
        yNorthTraj
        headingTraj
    end
    
    methods
        %Constructor
        function vehicleObj = OverheadVisVehicleObject(theParent,theColor,...
                theMarker,theMarkerSize,theFigParent)
            %Call superclass constructor
            vehicleObj@LineObject(theParent, theColor, theMarker, theMarkerSize, theFigParent);
        end
        
        function setSize(vehicleObj,length,width)
            %Description:
            %sets the length and width of the vehicle
            vehicleObj.length = length;
            vehicleObj.width = width;
            vehicleObj.corners_0 = [-width/2   -width/2 width/2  width/2   -width/2  0 width/2;
                                    -length/2  length/2 length/2 -length/2 -length/2 0 -length/2];
        end
        
        function setTrajectory(vehicleObj, xEastTraj, yNorthTraj, headingTraj)
            %Description
            %sets the vehicle trajectory
            vehicleObj.xEastTraj = xEastTraj;
            vehicleObj.yNorthTraj = yNorthTraj;
            vehicleObj.headingTraj = headingTraj;
        end
        
        function setPose(vehicleObj,index)
            %Description:
            %sets the position and heading of the vehicle
            vehicleObj.x = vehicleObj.xEastTraj(index);
            vehicleObj.y = vehicleObj.yNorthTraj(index);
            vehicleObj.heading = vehicleObj.headingTraj(index);
        end
        
        function [length,width] = getSize(vehicleObj)
            %Description:
            %gets the length and width of the vehicle
            length = vehicleObj.length;
            width = vehicleObj.width;
        end
        
        function [x,y,heading] = getPos(vehicleObj)
            %Description:
            %gets the position and heading of the vehicle
            x = vehicleObj.x;
            y = vehicleObj.y;
            heading = vehicleObj.heading;            
        end
        
        function newCorners = getCorners(vehicleObj)
            %Description:
            %calculates the positions of vehicle corners for plot,
            %concidering position and heading of the vehicle
            
            % tmp
            vehicleObj.heading = -vehicleObj.heading;
            
            [m,n] = size(vehicleObj.corners_0);
            pos = [vehicleObj.x; vehicleObj.y];
            R = [cosd(vehicleObj.heading) -sind(vehicleObj.heading);
                 sind(vehicleObj.heading)  cosd(vehicleObj.heading)];
            newCorners = zeros(m,n);
            for i=1:n
                newCorners(:,i) = R*vehicleObj.corners_0(:,i) + pos;
            end
        end
    end
end