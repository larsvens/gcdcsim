classdef InputObject < handle
    properties (Access = private)
        hInput              % handle to uicontrol 'edit' object
        hTitle                  % handle to uicontrol 'text' object
        currentValue            % current InputObject representrd as double
    end
    
    methods
        %Constructor
        function InputObj = InputObject(thePosition,theTitle,theParent)
            InputObj.hInput = uicontrol('Style','edit',...
                'String','',...
                'parent',theParent,...
                'Units','normalized',...
                'Position',thePosition);
            InputObj.hTitle = uicontrol('Style','text',...
                'String',theTitle,...
                'parent',theParent,...
                'Units','normalized',...
                'Position',thePosition+[0,.04,0,0]);
        end
        
        function setValue(InputObj,Value)
            % Description:
            % set value to time input object
            set(InputObj.hInput,'String',num2str(Value));
            InputObj.currentValue = Value;
        end
        
        function value = getValue(InputObj)
            % Description:
            % get value from time input object
            value = InputObj.currentValue;
        end
        
        function update(InputObj)
            % Description:
            % sets the property currentValue to user input
            InputObj.currentValue = str2double(get(InputObj.hInput,'String'));
        end
        
        function handle = getUICtrlHandle(InputObj)
            % Description:
            % returns uicontrol handle
            handle = InputObj.hInput;
        end
    end
end