classdef OverheadVisLineObject < LineObject
    %Description
    %This class is used together with the OverheadVisObject-class. Each
    %path or trail in the overhead visualization plot is a pathObject.
    
    properties (Access = private)
        lineX
        lineY
        XData
        YData              
    end
    
    methods
        %Constructor
        function overheadVisLineObj = OverheadVisLineObject(theParent,theColor,...
                theMarker,theMarkerSize,theFigParent)
            %Call superclass constructor
            overheadVisLineObj@LineObject(theParent,...
                theColor, theMarker, theMarkerSize, theFigParent);
        end
        
        function setData(overheadVisLineObj,XData,YData)
            %sets the properties x-data and y-data
            overheadVisLineObj.XData = XData;
            overheadVisLineObj.YData = YData;
        end

        function [XData,YData] = getData(overheadVisLineObj)
            % gets the properties x-data and y-data
            XData = overheadVisLineObj.XData;
            YData = overheadVisLineObj.YData;
        end
        
        function setLine(overheadVisLineObj,index)
           % sets lineX and lineY to data at given index
           % rows are data from one time index
           overheadVisLineObj.lineX = overheadVisLineObj.XData(index,:);
           overheadVisLineObj.lineY = overheadVisLineObj.YData(index,:);
        end
        
        function [lineX,lineY] = getLine(overheadVisLineObj)
           % gets current lineX and lineY
           lineX = overheadVisLineObj.lineX;
           lineY = overheadVisLineObj.lineY;
        end
    end
end