classdef PanelObject < handle
    properties
        hPanel             % handle to uipanel
    end
    
    methods
        %Constructor
        function PanelObj = PanelObject(position,theTitle,theFontSize,theBGColor,theParent)
            PanelObj.hPanel = uipanel('Title',theTitle,...
                'parent',theParent,...
                'FontSize',theFontSize,...
                'BackgroundColor',theBGColor,...
                'Position',position);
        end
    end
end