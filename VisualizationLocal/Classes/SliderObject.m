classdef SliderObject < handle
    properties (Access = private)
        hSlider % handle to uicontrol object slider
        hText   % handle to uicontrol object text
        sliderValue = 0 % current value of slider
    end
    
    methods
        % Constructor
        function sliderObj = SliderObject(position,titleString,sliderStep,theParent)
            sliderObj.hSlider = uicontrol('Style','slider',...
                'parent',theParent,...
                'Units','normalized',...
                'Position',position,...
                'Min',0,'Max',1,...
                'BackgroundColor','w',...
                'SliderStep',sliderStep);
            sliderObj.hText = uicontrol('Style','text',...
                'String',titleString,...
                'parent',theParent,...
                'Units','normalized',...
                'FontSize', 10,...
                'BackgroundColor','w',...
                'Position',position*[1;1;0;0]+[.75,.01,0.05,0.02]);
        end
        
        function update(sliderObj)
            % Descrpition:
            % updates parameter sliderValue
            sliderObj.sliderValue = get(sliderObj.hSlider,'Value');
        end
        
        function value = getValue(sliderObj)
            % Description
            % gets current sliderValue
            value = sliderObj.sliderValue;
        end
        
        function handle = getUICtrlHandle(TimeInputObj)
            % Description:
            % returns uicontrol handle
            handle = TimeInputObj.hSlider;
        end        
    end
end