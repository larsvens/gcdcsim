function [xFwdCL, yRgtCL] = getCenterLineFunction(xFwdLL, yRgtLL, xFwdML, yRgtML, xFwdRL, yRgtRL, rightLane)
% Description:
% limits range of lanemarking data and calculates the center line between two lane markings

% calculate center line
% N = int32(50);
N = length(xFwdLL);
xFwdCL= zeros(N,1);
yRgtCL= zeros(N,1);

if rightLane ~= 0 && rightLane ~= 1
    
    xFwdCLRight = (xFwdML(1:N)+xFwdRL(1:N))/2;
    yRgtCLRight = (yRgtML(1:N)+yRgtRL(1:N))/2;
    
    xFwdCLLeft = (xFwdML(1:N)+xFwdLL(1:N))/2;
    yRgtCLLeft = (yRgtML(1:N)+yRgtLL(1:N))/2;
    
    xFwdCL(1:N) = rightLane*xFwdCLRight(1:N) + (1-rightLane)*xFwdCLLeft(1:N);
    yRgtCL(1:N) = rightLane*yRgtCLRight(1:N) + (1-rightLane)*yRgtCLLeft(1:N);
        
else


    if rightLane          % right lane

%         xFwdCL(1:N) = xFwdML(1:N)+(xFwdML(1:N)-xFwdRL(1:N))/2;
%         yRgtCL(1:N) = yRgtML(1:N)+(yRgtML(1:N)-yRgtRL(1:N))/2;
        xFwdCL(1:N) = (xFwdML(1:N)+xFwdRL(1:N))/2;
        yRgtCL(1:N) = (yRgtML(1:N)+yRgtRL(1:N))/2;

    else

%         xFwdCL(1:N) = xFwdML(1:N)+(xFwdML(1:N)-xFwdLL(1:N))/2;
%         yRgtCL(1:N) = yRgtML(1:N)+(yRgtML(1:N)-yRgtLL(1:N))/2;
        xFwdCL(1:N) = (xFwdML(1:N)+xFwdLL(1:N))/2;
        yRgtCL(1:N) = (yRgtML(1:N)+yRgtLL(1:N))/2;


    end
    
end

% latError = yRgtCL_(1);

% define linearly spaced xFwd axis
% range = 50; %(m)
% n = 100;
% xFwdLinSpace = linspace(0,range,n);
% yRgtCL = interp1(xFwdCL_,yRgtCL_,xFwdLinSpace,'linear','extrap')';
% xFwdCL = xFwdLinSpace';

end

