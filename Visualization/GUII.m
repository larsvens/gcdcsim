function varargout = GUII(varargin)
% GUII MATLAB code for GUII.fig
%      GUII, by itself, creates a new GUII or raises the existing
%      singleton*.
%
%      H = GUII returns the handle to a new GUII or the handle to
%      the existing singleton*.
%
%      GUII('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUII.M with the given input arguments.
%
%      GUII('Property','Value',...) creates a new GUII or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUII before GUII_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUII_OpeningFcn via varargin.
%
%      *See GUII Options on GUIDE's Tools menu.  Choose "GUII allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES


% Edit the above text to modify the response to help GUII

% Last Modified by GUIDE v2.5 18-Nov-2015 17:08:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUII_OpeningFcn, ...
                   'gui_OutputFcn',  @GUII_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);

if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT


% --- Executes just before GUII is made visible.
function GUII_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLABd
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUII (see VARARGIN)

handles.dt = 0.1;


%% Insert an image
imshow('Map_Straight_road_bigger.png');
hold on;

% create an axes that spans the whole gui
background_h = axes('unit', 'normalized', 'position', [0 0 1 1]); 
% import the background image and show it on the axes
bg = imread('Map_Straight_road_bigger.png'); imagesc(bg);
% prevent plotting over the background and turn the axis off
set(background_h,'handlevisibility','off','visible','off')
% making sure the background is behind all the other uicontrols
uistack(background_h, 'bottom');


% Check where in the map are the lanes
% There needs to exist the Map file
map_lanes = find(~im2bw(imread('Map_Straight_road_bigger.png'), 0.2));
% From the map_lanes, the indexes of the Black cells are stores. The lane
% starts at 3rd cell and finished at the 4th.
handles.laneStart      = map_lanes(1);
handles.laneEnd        = map_lanes(2);
handles.laneWidth      = handles.laneEnd - handles.laneStart;
handles.truckWidth     = (handles.laneWidth - handles.laneWidth*0.15)/2;
handles.truckLength    = 120;
handles.SimTime        = 200;
LoggedData             = load('SimLog1');
handles.Veh_positions  = LoggedData.ans;
% handles.laneWidth_int  = floor(handles.laneWidth);
% Lane_Left_MidPoint   = asd;
% Lane_Right_MidPoint  = asd;

handles.V1 = Truck(handles.truckLength,handles.truckWidth, handles.V1_vx, handles.V1_vy, handles.V1_yawrate, 'Left', ...
    handles.V1_yawangle, handles.laneStart, handles.laneWidth);
handles.V2 = Truck(handles.truckLength,handles.truckWidth, handles.V2_vx, handles.V2_vy, handles.V2_yawrate, 'Right',...
    handles.V2_yawangle, handles.laneStart, handles.laneWidth);

% Insert trucks
% Truck No1
% It will initialize at the Right Lane
handles.truck1_h = imshow('truck1.png');
set(handles.truck1_h,'XData', [handles.V1.X handles.V1.X + handles.V1.length], 'YData', [handles.V1.Y handles.V1.Y + handles.V1.width]);
hold on;

% Truck No2
handles.truck2_h = imshow('truck2.png');

set(handles.truck2_h,'XData', [handles.V2.X handles.V2.X + handles.V2.length], 'YData', [handles.V2.Y handles.V2.Y + handles.V2.width]);




% Choose default command line output for GUII
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUII wait for user response (see UIRESUME)
% uiwait(handles.figure1);




% --- Outputs from this function are returned to the command line.
function varargout = GUII_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function V1_vx_Callback(hObject, eventdata, handles)
% hObject    handle to V1_vx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V1_vx as text
%        str2double(get(hObject,'String')) returns contents of V1_vx as a double

handles.V1.xVel = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function V1_vx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V1_vx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function V2_vx_Callback(hObject, eventdata, handles)
% hObject    handle to V2_vx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V2_vx as text
%        str2double(get(hObject,'String')) returns contents of V2_vx as a double
handles.V2.yVel = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function V2_vx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V2_vx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function V2_lane_Callback(hObject, eventdata, handles)
% hObject    handle to V2_lane (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V2_lane as text
%        str2double(get(hObject,'String')) returns contents of V2_lane as a double
handles.V2.lane = get(hObject,'String');

% update handles
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function V2_lane_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V2_lane (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function V2_x_Callback(hObject, eventdata, handles)
% hObject    handle to V2_row (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V2_row as text
%        str2double(get(hObject,'String')) returns contents of V2_row as a double
handles.V2.X = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function V2_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V2_row (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Update_pushbutton.
function Update_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Update_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% % Update the vehicle states according to the user inputs.
% v1_vx = get(handles.V1_vx,'string');
% v1_row = get(handles.V1_row,'string');
% v2_lane = get(handles.V2_lane,'string');
% v2_row = get(handles.V2_row,'string');

assignin('base','SimTime', handles.SimTime); 

% Update for vehicle 1
if strcmp(handles.V1.lane,'Left') %Left lane
    handles.V1.Y = handles.laneStart + 5;
elseif strcmp(handles.V1.lane,'Right') %Right lane
    handles.V1.Y = handles.laneStart + handles.laneWidth/2 + 5;
end


set(handles.truck1_h,'XData', [handles.V1.X handles.V1.X + handles.V1.length], 'YData', [handles.V1.Y handles.V1.Y + handles.V1.width]);

% Update for vehicle 2
if strcmp(handles.V2.lane,'Left') %Left lane
    handles.V2.Y = handles.laneStart + 5;
elseif strcmp(handles.V2.lane,'Right') %Right lane
    handles.V2.Y = handles.laneStart + handles.laneWidth/2 + 5;
end

set(handles.truck2_h,'XData', [handles.V2.X handles.V2.X + handles.V2.length], 'YData', [handles.V2.Y handles.V2.Y + handles.V2.width]);


% --- Executes on button press in Start_pushbutton.
function Start_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Start_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if strcmp(handles.V1.lane,'Left') %Left lane
    handles.V1.Y = handles.laneStart + 5;
elseif strcmp(handles.V1.lane,'Right') %Right lane
    handles.V1.Y = handles.laneStart + handles.laneWidth/2 + 5;
end

if strcmp(handles.V2.lane,'Left') %Left lane
    handles.V2.Y = handles.laneStart + 5;
elseif strcmp(handles.V2.lane,'Right') %Right lane
    handles.V2.Y = handles.laneStart + handles.laneWidth/2 + 5;
end
assignin('base','SimTime', handles.SimTime);
State_initialization


% if the timer was initialized before, then delete it.
if (~isempty(timerfind))
    delete(timerfind);
end

% Initialize timer and start it.
% timer_h = timer('TimerFcn',@TimerFunction,cell(truck1_h,truck2_h),'ExecutionMode','fixedRate', 'Period', 0.01);
% % timer_h.StartFcn(timer_h);
% timer_h.StartFcn = {@TimerFunction,truck1_h,truck2_h};

timer_h = timer('Period', 0.005,'ExecutionMode', 'fixedRate');
timer_h.TimerFcn = {@TimerFunction,handles,Veh_positions};
start(timer_h);

if strcmp(timer_h.running,'off')
    delete(timer_h);
end




% --- Executes on button press in Stop_pushbutton.
function Stop_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to Stop_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global wordspace_index
wordspace_index = 1;
delete(timerfind);
handles.V1.X = 10;
handles.V2.X = 10;
set(handles.truck1_h,'XData', [handles.V1.X handles.V1.X + handles.V1.length], 'YData', [handles.V1.Y handles.V1.Y + handles.V1.width]);
set(handles.truck2_h,'XData', [handles.V2.X handles.V2.X + handles.V2.length], 'YData', [handles.V2.Y handles.V2.Y + handles.V2.width]);



function V1_vy_Callback(hObject, eventdata, handles)
% hObject    handle to V1_vy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V1_vy as text
%        str2double(get(hObject,'String')) returns contents of V1_vy as a double

handles.V1.yVel = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function V1_vy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V1_vy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function V2_vy_Callback(hObject, eventdata, handles)
% hObject    handle to V2_vy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V2_vy as text
%        str2double(get(hObject,'String')) returns contents of V2_vy as a double

handles.V2.yVel = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function V2_vy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V2_vy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function V1_yawrate_Callback(hObject, eventdata, handles)
% hObject    handle to V1_yawrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V1_yawrate as text
%        str2double(get(hObject,'String')) returns contents of V1_yawrate as a double

handles.V1.yawrate = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function V1_yawrate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V1_yawrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function V2_yawrate_Callback(hObject, eventdata, handles)
% hObject    handle to V2_yawrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V2_yawrate as text
%        str2double(get(hObject,'String')) returns contents of V2_yawrate as a double

handles.V2.yawrate = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function V2_yawrate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V2_yawrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function V1_x_Callback(hObject, eventdata, handles)
% hObject    handle to V1_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V1_x as text
%        str2double(get(hObject,'String')) returns contents of V1_x as a double

handles.V1.X = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function V1_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V1_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function V1_lane_Callback(hObject, eventdata, handles)
% hObject    handle to V1_lane (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V1_lane as text
%        str2double(get(hObject,'String')) returns contents of V1_lane as a double

handles.V1.lane = get(hObject,'String');

% update handles
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function V1_lane_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V1_lane (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function V1_yawangle_Callback(hObject, eventdata, handles)
% hObject    handle to V1_yawangle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V1_yawangle as text
%        str2double(get(hObject,'String')) returns contents of V1_yawangle as a double

handles.V1_yawangle = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function V1_yawangle_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V1_yawangle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function V2_yawangle_Callback(hObject, eventdata, handles)
% hObject    handle to V2_yawangle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of V2_yawangle as text
%        str2double(get(hObject,'String')) returns contents of V2_yawangle as a double
handles.V2.yawangle = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function V2_yawangle_CreateFcn(hObject, eventdata, handles)
% hObject    handle to V2_yawangle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SimTime_Callback(hObject, eventdata, handles)
% hObject    handle to SimTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SimTime as text
%        str2double(get(hObject,'String')) returns contents of SimTime as a double

handles.SimTime = str2double(get(hObject,'String'));

% update handles
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function SimTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SimTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Time_Callback(hObject, eventdata, handles)
% hObject    handle to Time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Time as text
%        str2double(get(hObject,'String')) returns contents of Time as a double



% --- Executes during object creation, after setting all properties.
function Time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
SimulationPoint = get(hObject,'Value');
global wordspace_index
wordspace_index = floor(SimulationPoint*200*100 + 1);
handles.V1.X = 1000*(handles.Veh_positions.EgoGPSData.Longitude.Data(wordspace_index,1));
handles.V1.Y = handles.laneStart + 5 + handles.Veh_positions.EgoGPSData.Latitude.Data(wordspace_index,1);
handles.V2.X = 1000*(handles.Veh_positions.EstimationData.vehiclePositionLon.Data(wordspace_index));
handles.V2.Y = handles.laneStart + 5 + handles.Veh_positions.EstimationData.vehiclePositionLat.Data(wordspace_index);
disp(handles.Veh_positions.EstimationData.vehiclePositionLon.Data(wordspace_index))
disp(handles.laneStart + 5 + handles.Veh_positions.EstimationData.vehiclePositionLat.Data(wordspace_index))

% disp(handles.Veh_positions.EgoVehicleModelData.X_new.Data(wordspace_index))
% disp(handles.Veh_positions.EgoVehicleModelData.Y_new.Data(wordspace_index))
set(handles.truck1_h,'XData', [handles.V1.X handles.V1.X + handles.V1.length], 'YData', [handles.V1.Y handles.V1.Y + handles.V1.width]);
set(handles.truck2_h,'XData', [handles.V2.X handles.V2.X + handles.V2.length], 'YData', [handles.V2.Y handles.V2.Y + handles.V2.width]);



% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
