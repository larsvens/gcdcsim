%% Initialize parameters 
clear all;
clc;
dt = 0.1;

%% Start the GUI
figure


%% Insert an image
imshow('Map_Straight_road_bigger.png');
hold on;


% Check where in the map are the lanes
% There needs to exist the Map file
map_lanes = find(~im2bw(imread('Map_Straight_road_bigger.png'), 0.2));
% From the map_lanes, the indexes of the Black cells are stores. The lane
% starts at 3rd cell and finished at the 4th.
Lane_start      = map_lanes(3);
Lane_end        = map_lanes(4);
Lane_width      = Lane_end - Lane_start;
Lane_width_int  = floor(Lane_width);
Truck_width     = (Lane_width - Lane_width*0.15)/2;
Truck_length    = 140;
Truck_width_int = floor(Truck_width);
% Lane_Left_MidPoint  = asd;
% Lane_Right_MidPoint = asd;

V1 = Truck(Truck_length,Truck_width,'Left', 2,Lane_start, Lane_width_int);
V2 = Truck(Truck_length,Truck_width,'Right', 2,Lane_start, Lane_width_int);


%% Insert trucks 
% Truck No1
% It will initialize at the Right Lane
truck1_h = imshow('truck1.png');
set(truck1_h,'XData', [V1.X V1.X + V1.length], 'YData', [V1.Y V1.Y + V1.width]);
hold on;

% Truck No2
truck2_h = imshow('truck2.png');

set(truck2_h,'XData', [V2.X V2.X + V2.length], 'YData', [V2.Y V2.Y + V2.width]);


% if the timer was initialized before, then delete it.
if (~isempty(timerfind))
    delete(timerfind);
end


% Text describing which vehicle is edited
Vehicle_column1_h = uicontrol('Style','text',...
                'Position',[225 165 120 20],...
                'String','Vehicle 1');
            
% Text describing which vehicle is edited
Vehicle_column2_h = uicontrol('Style','text',...
                'Position',[400 165 120 20],...
                'String','Vehicle 2');           
 

% Text to describe The attribute that is changed
Vehicle_Lane_text_h = uicontrol('Style','text',...
                'Position',[90 128 120 20],...
                'String','Lane');
            

Vehicle_Row_text__h = uicontrol('Style','text',...
                'Position',[90 88 120 20],...
                'String','Row');
    

% Insert text editor for the initial Truck Values
edit_V1_lane_h = uicontrol('style','edit',...
                    'units', 'normalized',...
                    'position', [0.15 0.18 0.1 0.05]);    

edit_V1_row_h = uicontrol('style','edit',...
                    'units', 'normalized',...
                    'position', [0.15 0.12 0.1 0.05]);    

edit_V2_lane_h = uicontrol('style','edit',...
                    'units', 'normalized',...
                    'position', [0.27 0.18 0.1 0.05]);    

edit_V2_row_h = uicontrol('style','edit',...
                    'units', 'normalized',...
                    'position', [0.27 0.12 0.1 0.05]); 

% Update button
update_but_h = uicontrol('style', 'pushbutton',...
                        'string', 'Update',...
                        'units', 'normalized',...
                        'position', [0.4 0.3 0.15 0.1],...
                        'callback', {@update_vehicles,edit_V1_lane_h,edit_V1_row_h,edit_V2_lane_h,edit_V2_row_h,V1,V2,Lane_start, Lane_width,truck1_h,truck2_h});
 

start_but_h = uicontrol('style', 'pushbutton',...
                        'string', 'Start Simulation',...
                        'units', 'normalized',...
                        'position', [0.4 0.15 0.15 0.1],...
                        'callback', {@start_simulation,V1,V2,truck1_h,truck2_h});
                    
stop_but_h = uicontrol('style', 'pushbutton',...
                        'string', 'Stop Simulation',...
                        'units', 'normalized',...
                        'position', [0.6 0.15 0.15 0.1],...
                        'callback', {@stop_simulation,V1,V2,truck1_h,truck2_h});









