function TimerFunction(obj, event, handles,Veh_positions)
global wordspace_index
persistent y1_init y2_init x1_init x2_init
% if isempty(wordspace_index)
%     wordspace_index = 1;
% end
if isempty(x1_init)
    x1_init = handles.V1.X;
    wordspace_index = 1;
end
if isempty(x2_init)
    x2_init = handles.V2.X;
end
if isempty(y1_init)
    y1_init = handles.V1.Y;
end
if isempty(y2_init)
    y2_init = handles.V2.Y;
end

% disp('Hello')
% y1_drift =  Veh_positions(1,2);
% y2_drift =  Veh_positions(1,4);
handles.V1.X = Veh_positions(wordspace_index,1);
handles.V2.X = Veh_positions(wordspace_index,5);
handles.V1.Y = Veh_positions(wordspace_index,3);
handles.V2.Y = Veh_positions(wordspace_index,7);

% disp(Veh_positions(wordspace_index + 1,1) - Veh_positions(wordspace_index,1));
% disp(V2.X)

set(handles.truck1_h,'XData', [handles.V1.X handles.V1.X + handles.V1.length], 'YData', [handles.V1.Y handles.V1.Y + handles.V1.width]);
set(handles.truck2_h,'XData', [handles.V2.X handles.V2.X + handles.V2.length], 'YData', [handles.V2.Y handles.V2.Y + handles.V2.width]);

wordspace_index = wordspace_index + 1;
save_to_base(1);

end