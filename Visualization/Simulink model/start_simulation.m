function start_simulation(object_handle,event,V1,V2,truck1_h,truck2_h)

State_initialization
save_to_base(1);

% if the timer was initialized before, then delete it.
if (~isempty(timerfind))
    delete(timerfind);
end

% Initialize timer and start it.
% timer_h = timer('TimerFcn',@TimerFunction,cell(truck1_h,truck2_h),'ExecutionMode','fixedRate', 'Period', 0.01);
% % timer_h.StartFcn(timer_h);

timer_h = timer('Period', 0.01,'ExecutionMode', 'fixedRate');

timer_h.TimerFcn = {@TimerFunction,V1,V2,Veh_positions,truck1_h,truck2_h};

start(timer_h);

% timer_h.StartFcn = {@TimerFunction,truck1_h,truck2_h};


if strcmp(timer_h.running,'off')
    delete(timer_h);
end

% timer_h = timer('TimerFcn',@(x,y)disp('Hello World!'),'StartDelay',1);

end