function update_vehicles(object_handle,event, v1_lane_handle,  v1_row_handle, v2_lane_handle, v2_row_handle, V1, V2,laneStart,...
    laneWidth,V1_handle,V2_handle)
% Update the vehicle states according to the user inputs.
v1_lane = get(v1_lane_handle,'string');
v1_row = get(v1_row_handle,'string');
v2_lane = get(v2_lane_handle,'string');
v2_row = get(v2_row_handle,'string');

% Update for vehicle 1
if strcmp(v1_lane,'Left') %Left lane
    V1.Y = laneStart + 5;
elseif strcmp(v1_lane,'Right') %Right lane
    V1.Y = laneStart + laneWidth/2 + 5;
end

if strcmp(v1_row,'1'); % Meaning we want the vehicle to be in the first row of vehicles
    V1.X = V1.length + 40;
elseif strcmp(v1_row,'2'); % Meaning the vehicle will start at the second row
    V1.X = 10;
end
set(V1_handle,'XData', [V1.X V1.X + V1.length], 'YData', [V1.Y V1.Y + V1.width]);


% Update for vehicle 2
if strcmp(v2_lane,'Left') %Left lane
    V2.Y = laneStart + 5;
elseif strcmp(v2_lane,'Right') %Right lane
    V2.Y = laneStart + laneWidth/2 + 5;
end

if strcmp(v2_row, '1'); % Meaning we want the vehicle to be in the first row of vehicles
    V2.X = V2.length + 40;
elseif strcmp(v2_row,'2'); % Meaning the vehicle will start at the second row
    V2.X = 10;
end
set(V2_handle,'XData', [V2.X V2.X + V2.length], 'YData', [V2.Y V2.Y + V2.width]);


end