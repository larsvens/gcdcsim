classdef Truck
    properties
        % lane: Left:1 Right:2 
        length, width, lane, xVel, yVel, yawRate, X, Y, yawAngle
    end
    methods 
        % Constructor
        function obj = Truck(len, wid, vel_x, vel_y, yaw_rate, lan, yaw_angle, laneStart, laneWidth)
            obj.length   = len;
            obj.width    = wid;
            obj.lane     = lan;
            obj.xVel     = vel_x;
            obj.yVel     = vel_y;
            obj.yawRate  = yaw_rate;
            obj.X        = 10;
            obj.lane     = lan;
            obj.yawAngle = yaw_angle;
            
            if strcmp(obj.lane,'Left') %Left lane
                obj.Y = laneStart + 5;
            elseif strcmp(obj.lane,'Right') %Right lane
                obj.Y = laneStart + laneWidth/2 + 5;
            end
        end        

    end
end
