% Frequency for the different functions

assignin('base','dt',0.01);      
assignin('base','ctrl_dt',0.1);
assignin('base','model_dt',0.01); 
% assignin('base','SimTime', handles.SimTime ); 


% Vehicle 1 initial states
vx1       = 25/3.6; % in km/h
vy1       = 0/3.6; % in km/h
yaw_rate1 = 0;     % in rad/s
dx1       = handles.V1.X;     % in meters
dy1       = handles.V1.Y;     % in meters
yaw_init1 = 0;     % in rad

% Vehicle 2 initial states
vx2       = 25/3.6; % in km/h
vy2       = 0/3.6; % in km/h
yaw_rate2 = 0;     % in rad/s
dx2       = handles.V2.X;     % in meters
dy2       = handles.V2.Y;     % in meters
yaw_init2 = 0;     % in rad


Veh_init1 = [vx1,vy1,yaw_rate1,dx1,dy1,yaw_init1];     %fields are: vx,vy,yaw,dx,dy,yaw_angle
Veh_init2 = [vx2,vy2,yaw_rate2,dx2,dy2,yaw_init2];    %fields are: vx,vy,yaw,dx,dy,yaw_angle
assignin('base','Veh_init1',[vx1,vy1,yaw_rate1,dx1,dy1,yaw_init1]);
assignin('base','Veh_init2',[vx2,vy2,yaw_rate2,dx2,dy2,yaw_init2]);
save_to_base(1);

% Run simulation
sim('bicycle_model.slx')

% 
% plot(Veh_positions(:,1),Veh_positions(:,2),'*')
% hold on
% plot(Veh_positions(:,3),Veh_positions(:,4),'*')



%% Google Maps Implementation
% plot(vx_ref,vy_ref)
% hold on
% plot(vx_ref1,vy_ref1)
% hold on
% plot(vx_ref2,vy_ref2)


% Lat=[59.350931; 59.35095 ];
% Lon=[18.067634; 18.06765];
% [x,y,utmzone] = wgs2utm(Lat,Lon);
% 
% wgs2utm(Lat,Lon)
% 
% 
% % GoogleMap projection
% lat = [59.350931];
% lon = [18.067634];
% plot(lon,lat,'.r','MarkerSize',20)
% % plot_google_map
% 
% % Google map
% plot_google_map('maptype', 'roadmap');

