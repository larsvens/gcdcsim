function [xFwdCL, yLftCL, currentRightLaneRatio] = LateralTrajectoryGeneratorFunction(xFwdLL, yLftLL, xFwdML, yLftML, xFwdRL, yLftRL, rightLane, t)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

persistent prevRightLane
persistent iterationsChanging
persistent rightToLeft

timeToChangeLanes = 10;
sampleFrequency = 100;

maxIterationsChanging = timeToChangeLanes*sampleFrequency;

currentRightLaneRatio = double(rightLane);

if isempty(prevRightLane)      
    
   prevRightLane = rightLane;
   iterationsChanging = -1;
   rightToLeft = 0;
   
   currentRightLaneRatio = double(rightLane);
   
   [xFwdCL, yLftCL] = getCenterLineFunction(xFwdLL, yLftLL, xFwdML, yLftML, xFwdRL, yLftRL, rightLane);
            
   return;
    
end

if prevRightLane ~= rightLane
    
    if prevRightLane
        prevRightLaneStr = '1';
        rightToLeft = 1;
    else
        prevRightLaneStr = '0';
        rightToLeft = 0;
    end
    
    if rightLane
        rightLaneStr = '1';
    else
        rightLaneStr = '0';
    end
    
%     disp(['Requested change from ', prevRightLaneStr, ' to ', rightLaneStr]);      
%     disp(get_param('lateralController','SimulationTime'));
%     disp(t)
    iterationsChanging = 0;
    
    currentRightLaneRatio = double(prevRightLane);
    
end

prevRightLane = rightLane;

if iterationsChanging ~= -1
   
    iterationsChanging = iterationsChanging + 1;
%     disp(iterationsChanging)

    currentRightLaneRatio = laneChangingRatioFunction(iterationsChanging,maxIterationsChanging);
    
    if rightToLeft
       
        currentRightLaneRatio = 1 - currentRightLaneRatio;
        
    end
        
end

if iterationsChanging == maxIterationsChanging

%     disp('Lane change complete'); 
%     disp(get_param('lateralController','SimulationTime'));
%     disp(t)
    iterationsChanging = -1;
    
    currentRightLaneRatio = double(rightLane);
    
end

[xFwdCL, yLftCL] = getCenterLineFunction(xFwdLL, yLftLL, xFwdML, yLftML, xFwdRL, yLftRL, currentRightLaneRatio);


end

