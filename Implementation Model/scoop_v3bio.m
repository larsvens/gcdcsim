function bio=scoop_v3bio
bio = [];
bio(1).blkName='Constant';
bio(1).sigName='';
bio(1).portIdx=0;
bio(1).dim=[1,1];
bio(1).sigWidth=1;
bio(1).sigAddress='&scoop_v3_B.Constant_g';
bio(1).ndims=2;
bio(1).size=[];

bio(getlenBIO) = bio(1);

bio(2).blkName='Constant1';
bio(2).sigName='';
bio(2).portIdx=0;
bio(2).dim=[40,1];
bio(2).sigWidth=40;
bio(2).sigAddress='&scoop_v3_B.Constant1[0]';
bio(2).ndims=2;
bio(2).size=[];


bio(3).blkName='Model/p1';
bio(3).sigName='';
bio(3).portIdx=0;
bio(3).dim=[12,1];
bio(3).sigWidth=12;
bio(3).sigAddress='&scoop_v3_B.Model_o1[0]';
bio(3).ndims=2;
bio(3).size=[];


bio(4).blkName='Model/p2';
bio(4).sigName='';
bio(4).portIdx=1;
bio(4).dim=[1,1];
bio(4).sigWidth=1;
bio(4).sigAddress='&scoop_v3_B.Model_o2';
bio(4).ndims=2;
bio(4).size=[];


bio(5).blkName='Model/p3';
bio(5).sigName='';
bio(5).portIdx=2;
bio(5).dim=[1,1];
bio(5).sigWidth=1;
bio(5).sigAddress='&scoop_v3_B.Model_o3';
bio(5).ndims=2;
bio(5).size=[];


bio(6).blkName='Model/p4';
bio(6).sigName='';
bio(6).portIdx=3;
bio(6).dim=[1,1];
bio(6).sigWidth=1;
bio(6).sigAddress='&scoop_v3_B.Model_o4';
bio(6).ndims=2;
bio(6).size=[];


bio(7).blkName='Model/p5';
bio(7).sigName='';
bio(7).portIdx=4;
bio(7).dim=[1,1];
bio(7).sigWidth=1;
bio(7).sigAddress='&scoop_v3_B.Model_o5';
bio(7).ndims=2;
bio(7).size=[];


bio(8).blkName='Rate Transition10';
bio(8).sigName='';
bio(8).portIdx=0;
bio(8).dim=[1,1];
bio(8).sigWidth=1;
bio(8).sigAddress='&scoop_v3_B.RateTransition10';
bio(8).ndims=2;
bio(8).size=[];


bio(9).blkName='Rate Transition11';
bio(9).sigName='';
bio(9).portIdx=0;
bio(9).dim=[1,1];
bio(9).sigWidth=1;
bio(9).sigAddress='&scoop_v3_B.RateTransition11_c';
bio(9).ndims=2;
bio(9).size=[];


bio(10).blkName='Rate Transition12';
bio(10).sigName='';
bio(10).portIdx=0;
bio(10).dim=[1,1];
bio(10).sigWidth=1;
bio(10).sigAddress='&scoop_v3_B.RateTransition12_a';
bio(10).ndims=2;
bio(10).size=[];


bio(11).blkName='Rate Transition13';
bio(11).sigName='';
bio(11).portIdx=0;
bio(11).dim=[1,1];
bio(11).sigWidth=1;
bio(11).sigAddress='&scoop_v3_B.RateTransition13';
bio(11).ndims=2;
bio(11).size=[];


bio(12).blkName='Rate Transition14';
bio(12).sigName='';
bio(12).portIdx=0;
bio(12).dim=[1,1];
bio(12).sigWidth=1;
bio(12).sigAddress='&scoop_v3_B.RateTransition14';
bio(12).ndims=2;
bio(12).size=[];


bio(13).blkName='Rate Transition15';
bio(13).sigName='';
bio(13).portIdx=0;
bio(13).dim=[1,1];
bio(13).sigWidth=1;
bio(13).sigAddress='&scoop_v3_B.RateTransition15';
bio(13).ndims=2;
bio(13).size=[];


bio(14).blkName='Rate Transition16';
bio(14).sigName='';
bio(14).portIdx=0;
bio(14).dim=[1,1];
bio(14).sigWidth=1;
bio(14).sigAddress='&scoop_v3_B.RateTransition16';
bio(14).ndims=2;
bio(14).size=[];


bio(15).blkName='Rate Transition17';
bio(15).sigName='';
bio(15).portIdx=0;
bio(15).dim=[1,1];
bio(15).sigWidth=1;
bio(15).sigAddress='&scoop_v3_B.RateTransition17';
bio(15).ndims=2;
bio(15).size=[];


bio(16).blkName='Rate Transition18';
bio(16).sigName='';
bio(16).portIdx=0;
bio(16).dim=[1,1];
bio(16).sigWidth=1;
bio(16).sigAddress='&scoop_v3_B.RateTransition18_k';
bio(16).ndims=2;
bio(16).size=[];


bio(17).blkName='Rate Transition4';
bio(17).sigName='';
bio(17).portIdx=0;
bio(17).dim=[1,1];
bio(17).sigWidth=1;
bio(17).sigAddress='&scoop_v3_B.RateTransition4_n';
bio(17).ndims=2;
bio(17).size=[];


bio(18).blkName='Rate Transition5';
bio(18).sigName='';
bio(18).portIdx=0;
bio(18).dim=[1,1];
bio(18).sigWidth=1;
bio(18).sigAddress='&scoop_v3_B.RateTransition5_l';
bio(18).ndims=2;
bio(18).size=[];


bio(19).blkName='Rate Transition6';
bio(19).sigName='';
bio(19).portIdx=0;
bio(19).dim=[1,1];
bio(19).sigWidth=1;
bio(19).sigAddress='&scoop_v3_B.RateTransition6_dd';
bio(19).ndims=2;
bio(19).size=[];


bio(20).blkName='Add';
bio(20).sigName='';
bio(20).portIdx=0;
bio(20).dim=[1,1];
bio(20).sigWidth=1;
bio(20).sigAddress='&scoop_v3_B.Add';
bio(20).ndims=2;
bio(20).size=[];


bio(21).blkName='Switch3';
bio(21).sigName='';
bio(21).portIdx=0;
bio(21).dim=[2,1];
bio(21).sigWidth=2;
bio(21).sigAddress='&scoop_v3_B.Switch3[0]';
bio(21).ndims=2;
bio(21).size=[];


bio(22).blkName='Unit Delay1';
bio(22).sigName='';
bio(22).portIdx=0;
bio(22).dim=[1,1];
bio(22).sigWidth=1;
bio(22).sigAddress='&scoop_v3_B.UnitDelay1';
bio(22).ndims=2;
bio(22).size=[];


bio(23).blkName='Unit Delay3';
bio(23).sigName='';
bio(23).portIdx=0;
bio(23).dim=[1,1];
bio(23).sigWidth=1;
bio(23).sigAddress='&scoop_v3_B.UnitDelay3';
bio(23).ndims=2;
bio(23).size=[];


bio(24).blkName='LongitudinalController/Truck/p1';
bio(24).sigName='';
bio(24).portIdx=0;
bio(24).dim=[1,1];
bio(24).sigWidth=1;
bio(24).sigAddress='&scoop_v3_B.Truck_o1';
bio(24).ndims=2;
bio(24).size=[];


bio(25).blkName='LongitudinalController/Truck/p2';
bio(25).sigName='';
bio(25).portIdx=1;
bio(25).dim=[1,1];
bio(25).sigWidth=1;
bio(25).sigAddress='&scoop_v3_B.Truck_o2';
bio(25).ndims=2;
bio(25).size=[];


bio(26).blkName='LongitudinalController/Truck/p3';
bio(26).sigName='';
bio(26).portIdx=2;
bio(26).dim=[15,1];
bio(26).sigWidth=15;
bio(26).sigAddress='&scoop_v3_B.Truck_o3[0]';
bio(26).ndims=2;
bio(26).size=[];


bio(27).blkName='MonitoringAndLogging /IMULatAcc';
bio(27).sigName='';
bio(27).portIdx=0;
bio(27).dim=[1,1];
bio(27).sigWidth=1;
bio(27).sigAddress='&scoop_v3_B.IMULatAcc_en';
bio(27).ndims=2;
bio(27).size=[];


bio(28).blkName='MonitoringAndLogging /IMULatAccTS';
bio(28).sigName='';
bio(28).portIdx=0;
bio(28).dim=[1,1];
bio(28).sigWidth=1;
bio(28).sigAddress='&scoop_v3_B.IMULatAccTS';
bio(28).ndims=2;
bio(28).size=[];


bio(29).blkName='MonitoringAndLogging /IMULongAcc';
bio(29).sigName='';
bio(29).portIdx=0;
bio(29).dim=[1,1];
bio(29).sigWidth=1;
bio(29).sigAddress='&scoop_v3_B.IMULongAcc_o';
bio(29).ndims=2;
bio(29).size=[];


bio(30).blkName='MonitoringAndLogging /IMULongAccTS';
bio(30).sigName='';
bio(30).portIdx=0;
bio(30).dim=[1,1];
bio(30).sigWidth=1;
bio(30).sigAddress='&scoop_v3_B.IMULongAccTS';
bio(30).ndims=2;
bio(30).size=[];


bio(31).blkName='MonitoringAndLogging /IMUTS';
bio(31).sigName='';
bio(31).portIdx=0;
bio(31).dim=[1,1];
bio(31).sigWidth=1;
bio(31).sigAddress='&scoop_v3_B.IMUTS';
bio(31).ndims=2;
bio(31).size=[];


bio(32).blkName='MonitoringAndLogging /IMUYawrate';
bio(32).sigName='';
bio(32).portIdx=0;
bio(32).dim=[1,1];
bio(32).sigWidth=1;
bio(32).sigAddress='&scoop_v3_B.IMUYawrate_m';
bio(32).ndims=2;
bio(32).size=[];


bio(33).blkName='MonitoringAndLogging /IMUYawrateTS';
bio(33).sigName='';
bio(33).portIdx=0;
bio(33).dim=[1,1];
bio(33).sigWidth=1;
bio(33).sigAddress='&scoop_v3_B.IMUYawrateTS';
bio(33).ndims=2;
bio(33).size=[];


bio(34).blkName='MonitoringAndLogging /MtrLongAcc';
bio(34).sigName='';
bio(34).portIdx=0;
bio(34).dim=[1,1];
bio(34).sigWidth=1;
bio(34).sigAddress='&scoop_v3_B.MtrLongAcc_a';
bio(34).ndims=2;
bio(34).size=[];


bio(35).blkName='MonitoringAndLogging /MtrLongAccTS';
bio(35).sigName='';
bio(35).portIdx=0;
bio(35).dim=[1,1];
bio(35).sigWidth=1;
bio(35).sigAddress='&scoop_v3_B.MtrLongAccTS';
bio(35).ndims=2;
bio(35).size=[];


bio(36).blkName='MonitoringAndLogging /MtrLongVel';
bio(36).sigName='';
bio(36).portIdx=0;
bio(36).dim=[1,1];
bio(36).sigWidth=1;
bio(36).sigAddress='&scoop_v3_B.MtrLongVel_px';
bio(36).ndims=2;
bio(36).size=[];


bio(37).blkName='MonitoringAndLogging /MtrLongVelTS';
bio(37).sigName='';
bio(37).portIdx=0;
bio(37).dim=[1,1];
bio(37).sigWidth=1;
bio(37).sigAddress='&scoop_v3_B.MtrLongVelTS';
bio(37).ndims=2;
bio(37).size=[];


bio(38).blkName='MonitoringAndLogging /gpsAltitude';
bio(38).sigName='';
bio(38).portIdx=0;
bio(38).dim=[1,1];
bio(38).sigWidth=1;
bio(38).sigAddress='&scoop_v3_B.gpsAltitude';
bio(38).ndims=2;
bio(38).size=[];


bio(39).blkName='MonitoringAndLogging /gpsAltitudeTS';
bio(39).sigName='';
bio(39).portIdx=0;
bio(39).dim=[1,1];
bio(39).sigWidth=1;
bio(39).sigAddress='&scoop_v3_B.gpsAltitudeTS';
bio(39).ndims=2;
bio(39).size=[];


bio(40).blkName='MonitoringAndLogging /gpsHdng';
bio(40).sigName='';
bio(40).portIdx=0;
bio(40).dim=[1,1];
bio(40).sigWidth=1;
bio(40).sigAddress='&scoop_v3_B.gpsHdng';
bio(40).ndims=2;
bio(40).size=[];


bio(41).blkName='MonitoringAndLogging /gpsHdngTS';
bio(41).sigName='';
bio(41).portIdx=0;
bio(41).dim=[1,1];
bio(41).sigWidth=1;
bio(41).sigAddress='&scoop_v3_B.gpsHdngTS';
bio(41).ndims=2;
bio(41).size=[];


bio(42).blkName='MonitoringAndLogging /gpsLongVel';
bio(42).sigName='';
bio(42).portIdx=0;
bio(42).dim=[1,1];
bio(42).sigWidth=1;
bio(42).sigAddress='&scoop_v3_B.gpsLongVel';
bio(42).ndims=2;
bio(42).size=[];


bio(43).blkName='MonitoringAndLogging /gpsLongVelTS';
bio(43).sigName='';
bio(43).portIdx=0;
bio(43).dim=[1,1];
bio(43).sigWidth=1;
bio(43).sigAddress='&scoop_v3_B.gpsLongVelTS';
bio(43).ndims=2;
bio(43).size=[];


bio(44).blkName='MonitoringAndLogging /gpsNrOfSats';
bio(44).sigName='';
bio(44).portIdx=0;
bio(44).dim=[1,1];
bio(44).sigWidth=1;
bio(44).sigAddress='&scoop_v3_B.gpsNrOfSats';
bio(44).ndims=2;
bio(44).size=[];


bio(45).blkName='MonitoringAndLogging /gpsPosLat';
bio(45).sigName='';
bio(45).portIdx=0;
bio(45).dim=[1,1];
bio(45).sigWidth=1;
bio(45).sigAddress='&scoop_v3_B.gpsPosLat';
bio(45).ndims=2;
bio(45).size=[];


bio(46).blkName='MonitoringAndLogging /gpsPosLatTS';
bio(46).sigName='';
bio(46).portIdx=0;
bio(46).dim=[1,1];
bio(46).sigWidth=1;
bio(46).sigAddress='&scoop_v3_B.gpsPosLatTS';
bio(46).ndims=2;
bio(46).size=[];


bio(47).blkName='MonitoringAndLogging /gpsPosLon';
bio(47).sigName='';
bio(47).portIdx=0;
bio(47).dim=[1,1];
bio(47).sigWidth=1;
bio(47).sigAddress='&scoop_v3_B.gpsPosLon';
bio(47).ndims=2;
bio(47).size=[];


bio(48).blkName='MonitoringAndLogging /gpsPosLonTS';
bio(48).sigName='';
bio(48).portIdx=0;
bio(48).dim=[1,1];
bio(48).sigWidth=1;
bio(48).sigAddress='&scoop_v3_B.gpsPosLonTS';
bio(48).ndims=2;
bio(48).size=[];


bio(49).blkName='MonitoringAndLogging /gpsQuality';
bio(49).sigName='';
bio(49).portIdx=0;
bio(49).dim=[1,1];
bio(49).sigWidth=1;
bio(49).sigAddress='&scoop_v3_B.gpsQuality';
bio(49).ndims=2;
bio(49).size=[];


bio(50).blkName='MonitoringAndLogging /gpsStatus';
bio(50).sigName='';
bio(50).portIdx=0;
bio(50).dim=[1,1];
bio(50).sigWidth=1;
bio(50).sigAddress='&scoop_v3_B.gpsStatus';
bio(50).ndims=2;
bio(50).size=[];


bio(51).blkName='MonitoringAndLogging /gpsStatusTS';
bio(51).sigName='';
bio(51).portIdx=0;
bio(51).dim=[1,1];
bio(51).sigWidth=1;
bio(51).sigAddress='&scoop_v3_B.gpsStatusTS';
bio(51).ndims=2;
bio(51).size=[];


bio(52).blkName='MonitoringAndLogging /gpsUTCtime_ddmmyy';
bio(52).sigName='';
bio(52).portIdx=0;
bio(52).dim=[1,1];
bio(52).sigWidth=1;
bio(52).sigAddress='&scoop_v3_B.gpsUTCtime_ddmmyy';
bio(52).ndims=2;
bio(52).size=[];


bio(53).blkName='MonitoringAndLogging /gpsUTCtime_hhmmsscc';
bio(53).sigName='';
bio(53).portIdx=0;
bio(53).dim=[1,1];
bio(53).sigWidth=1;
bio(53).sigAddress='&scoop_v3_B.gpsUTCtime_hhmmsscc';
bio(53).ndims=2;
bio(53).size=[];


bio(54).blkName='Serial IN (Trimble)/Rate Transition';
bio(54).sigName='';
bio(54).portIdx=0;
bio(54).dim=[1,1];
bio(54).sigWidth=1;
bio(54).sigAddress='&scoop_v3_B.RateTransition_j';
bio(54).ndims=2;
bio(54).size=[];


bio(55).blkName='Serial IN (Trimble)/FIFO read';
bio(55).sigName='';
bio(55).portIdx=0;
bio(55).dim=[257,1];
bio(55).sigWidth=257;
bio(55).sigAddress='&scoop_v3_B.FIFOread[0]';
bio(55).ndims=2;
bio(55).size=[];


bio(56).blkName='StaticVehicleParams/length';
bio(56).sigName='';
bio(56).portIdx=0;
bio(56).dim=[1,1];
bio(56).sigWidth=1;
bio(56).sigAddress='&scoop_v3_B.length';
bio(56).ndims=2;
bio(56).size=[];


bio(57).blkName='StaticVehicleParams/rearAxleLocation';
bio(57).sigName='';
bio(57).portIdx=0;
bio(57).dim=[1,1];
bio(57).sigWidth=1;
bio(57).sigAddress='&scoop_v3_B.rearAxleLocation';
bio(57).ndims=2;
bio(57).size=[];


bio(58).blkName='StaticVehicleParams/width';
bio(58).sigName='';
bio(58).portIdx=0;
bio(58).dim=[1,1];
bio(58).sigWidth=1;
bio(58).sigAddress='&scoop_v3_B.width';
bio(58).ndims=2;
bio(58).size=[];


bio(59).blkName='StaticVehicleParams1/length';
bio(59).sigName='';
bio(59).portIdx=0;
bio(59).dim=[1,1];
bio(59).sigWidth=1;
bio(59).sigAddress='&scoop_v3_B.length_n';
bio(59).ndims=2;
bio(59).size=[];


bio(60).blkName='StaticVehicleParams1/rearAxleLocation';
bio(60).sigName='';
bio(60).portIdx=0;
bio(60).dim=[1,1];
bio(60).sigWidth=1;
bio(60).sigAddress='&scoop_v3_B.rearAxleLocation_j';
bio(60).ndims=2;
bio(60).size=[];


bio(61).blkName='StaticVehicleParams1/width';
bio(61).sigName='';
bio(61).portIdx=0;
bio(61).dim=[1,1];
bio(61).sigWidth=1;
bio(61).sigAddress='&scoop_v3_B.width_i';
bio(61).ndims=2;
bio(61).size=[];


bio(62).blkName='Subsystem/MATLAB Function/p1';
bio(62).sigName='ECEFg0';
bio(62).portIdx=0;
bio(62).dim=[3,1];
bio(62).sigWidth=3;
bio(62).sigAddress='&scoop_v3_B.ECEFg0_h[0]';
bio(62).ndims=2;
bio(62).size=[];


bio(63).blkName='Subsystem/MATLAB Function/p2';
bio(63).sigName='ECEFr0';
bio(63).portIdx=1;
bio(63).dim=[3,1];
bio(63).sigWidth=3;
bio(63).sigAddress='&scoop_v3_B.ECEFr0_h[0]';
bio(63).ndims=2;
bio(63).size=[];


bio(64).blkName='Subsystem/MATLAB Function/p3';
bio(64).sigName='originInitialized';
bio(64).portIdx=2;
bio(64).dim=[1,1];
bio(64).sigWidth=1;
bio(64).sigAddress='&scoop_v3_B.originInitialized_b';
bio(64).ndims=2;
bio(64).size=[];


bio(65).blkName='Subsystem/ECEFg0_';
bio(65).sigName='ECEFg0';
bio(65).portIdx=0;
bio(65).dim=[3,1];
bio(65).sigWidth=3;
bio(65).sigAddress='&scoop_v3_B.ECEFg0[0]';
bio(65).ndims=2;
bio(65).size=[];


bio(66).blkName='Subsystem/ECEFr0_';
bio(66).sigName='ECEFr0';
bio(66).portIdx=0;
bio(66).dim=[3,1];
bio(66).sigWidth=3;
bio(66).sigAddress='&scoop_v3_B.ECEFr0[0]';
bio(66).ndims=2;
bio(66).size=[];


bio(67).blkName='Subsystem/originInitialized_';
bio(67).sigName='originInitialized';
bio(67).portIdx=0;
bio(67).dim=[1,1];
bio(67).sigWidth=1;
bio(67).sigAddress='&scoop_v3_B.originInitialized';
bio(67).ndims=2;
bio(67).size=[];


bio(68).blkName='Subsystem1/MATLAB Function/p1';
bio(68).sigName='distance_to_front';
bio(68).portIdx=0;
bio(68).dim=[1,1];
bio(68).sigWidth=1;
bio(68).sigAddress='&scoop_v3_B.distance_to_front';
bio(68).ndims=2;
bio(68).size=[];


bio(69).blkName='Subsystem1/MATLAB Function/p2';
bio(69).sigName='absolute_speed_of_front';
bio(69).portIdx=1;
bio(69).dim=[1,1];
bio(69).sigWidth=1;
bio(69).sigAddress='&scoop_v3_B.absolute_speed_of_front';
bio(69).ndims=2;
bio(69).size=[];


bio(70).blkName='Subsystem1/MATLAB Function/p3';
bio(70).sigName='acceleration_of_front';
bio(70).portIdx=2;
bio(70).dim=[1,1];
bio(70).sigWidth=1;
bio(70).sigAddress='&scoop_v3_B.acceleration_of_front';
bio(70).ndims=2;
bio(70).size=[];


bio(71).blkName='Subsystem1/Clock';
bio(71).sigName='';
bio(71).portIdx=0;
bio(71).dim=[1,1];
bio(71).sigWidth=1;
bio(71).sigAddress='&scoop_v3_B.Clock';
bio(71).ndims=2;
bio(71).size=[];


bio(72).blkName='Subsystem1/DistToFront';
bio(72).sigName='';
bio(72).portIdx=0;
bio(72).dim=[1,1];
bio(72).sigWidth=1;
bio(72).sigAddress='&scoop_v3_B.DistToFront';
bio(72).ndims=2;
bio(72).size=[];


bio(73).blkName='Subsystem1/FwdLeftMioLatDist';
bio(73).sigName='';
bio(73).portIdx=0;
bio(73).dim=[1,1];
bio(73).sigWidth=1;
bio(73).sigAddress='&scoop_v3_B.FwdLeftMioLatDist';
bio(73).ndims=2;
bio(73).size=[];


bio(74).blkName='Subsystem1/FwdLeftMioLongDist';
bio(74).sigName='';
bio(74).portIdx=0;
bio(74).dim=[1,1];
bio(74).sigWidth=1;
bio(74).sigAddress='&scoop_v3_B.FwdLeftMioLongDist';
bio(74).ndims=2;
bio(74).size=[];


bio(75).blkName='Subsystem1/FwdMioLaDist';
bio(75).sigName='';
bio(75).portIdx=0;
bio(75).dim=[1,1];
bio(75).sigWidth=1;
bio(75).sigAddress='&scoop_v3_B.FwdMioLaDist';
bio(75).ndims=2;
bio(75).size=[];


bio(76).blkName='Subsystem1/FwdMioLongDist';
bio(76).sigName='';
bio(76).portIdx=0;
bio(76).dim=[1,1];
bio(76).sigWidth=1;
bio(76).sigAddress='&scoop_v3_B.FwdMioLongDist';
bio(76).ndims=2;
bio(76).size=[];


bio(77).blkName='Subsystem1/FwdRightMioLatDist';
bio(77).sigName='';
bio(77).portIdx=0;
bio(77).dim=[1,1];
bio(77).sigWidth=1;
bio(77).sigAddress='&scoop_v3_B.FwdRightMioLatDist';
bio(77).ndims=2;
bio(77).size=[];


bio(78).blkName='Subsystem1/FwdRightMioLongDist';
bio(78).sigName='';
bio(78).portIdx=0;
bio(78).dim=[1,1];
bio(78).sigWidth=1;
bio(78).sigAddress='&scoop_v3_B.FwdRightMioLongDist';
bio(78).ndims=2;
bio(78).size=[];


bio(79).blkName='Subsystem1/RTS1_Long_dist';
bio(79).sigName='';
bio(79).portIdx=0;
bio(79).dim=[1,1];
bio(79).sigWidth=1;
bio(79).sigAddress='&scoop_v3_B.RTS1_Long_dist';
bio(79).ndims=2;
bio(79).size=[];


bio(80).blkName='Subsystem1/RTS2_Long_dist';
bio(80).sigName='';
bio(80).portIdx=0;
bio(80).dim=[1,1];
bio(80).sigWidth=1;
bio(80).sigAddress='&scoop_v3_B.RTS2_Long_dist';
bio(80).ndims=2;
bio(80).size=[];


bio(81).blkName='Subsystem1/RTS3_Long_dist';
bio(81).sigName='';
bio(81).portIdx=0;
bio(81).dim=[1,1];
bio(81).sigWidth=1;
bio(81).sigAddress='&scoop_v3_B.RTS3_Long_dist';
bio(81).ndims=2;
bio(81).size=[];


bio(82).blkName='Subsystem1/RTS4_Long_dist';
bio(82).sigName='';
bio(82).portIdx=0;
bio(82).dim=[1,1];
bio(82).sigWidth=1;
bio(82).sigAddress='&scoop_v3_B.RTS4_Long_dist';
bio(82).ndims=2;
bio(82).size=[];


bio(83).blkName='Subsystem1/RTS5_Long_dist';
bio(83).sigName='';
bio(83).portIdx=0;
bio(83).dim=[1,1];
bio(83).sigWidth=1;
bio(83).sigAddress='&scoop_v3_B.RTS5_Long_dist';
bio(83).ndims=2;
bio(83).size=[];


bio(84).blkName='Subsystem1/RTS6_Long_dist';
bio(84).sigName='';
bio(84).portIdx=0;
bio(84).dim=[1,1];
bio(84).sigWidth=1;
bio(84).sigAddress='&scoop_v3_B.RTS6_Long_dist';
bio(84).ndims=2;
bio(84).size=[];


bio(85).blkName='Subsystem1/is_valid_MIO';
bio(85).sigName='';
bio(85).portIdx=0;
bio(85).dim=[1,1];
bio(85).sigWidth=1;
bio(85).sigAddress='&scoop_v3_B.is_valid_MIO';
bio(85).ndims=2;
bio(85).size=[];


bio(86).blkName='Subsystem1/is_valid_left_MIO';
bio(86).sigName='';
bio(86).portIdx=0;
bio(86).dim=[1,1];
bio(86).sigWidth=1;
bio(86).sigAddress='&scoop_v3_B.is_valid_left_MIO';
bio(86).ndims=2;
bio(86).size=[];


bio(87).blkName='Subsystem1/is_valid_right_MIO';
bio(87).sigName='';
bio(87).portIdx=0;
bio(87).dim=[1,1];
bio(87).sigWidth=1;
bio(87).sigAddress='&scoop_v3_B.is_valid_right_MIO';
bio(87).ndims=2;
bio(87).size=[];


bio(88).blkName='Subsystem1/long_dist_MIO1';
bio(88).sigName='';
bio(88).portIdx=0;
bio(88).dim=[1,1];
bio(88).sigWidth=1;
bio(88).sigAddress='&scoop_v3_B.long_dist_MIO1';
bio(88).ndims=2;
bio(88).size=[];


bio(89).blkName='Subsystem1/Rate Transition1';
bio(89).sigName='';
bio(89).portIdx=0;
bio(89).dim=[1,1];
bio(89).sigWidth=1;
bio(89).sigAddress='&scoop_v3_B.RateTransition1_k';
bio(89).ndims=2;
bio(89).size=[];


bio(90).blkName='UDP IN (Cohda)1/Data Type Conversion';
bio(90).sigName='isNewMsg';
bio(90).portIdx=0;
bio(90).dim=[1,1];
bio(90).sigWidth=1;
bio(90).sigAddress='&scoop_v3_B.isNewMsg_e';
bio(90).ndims=2;
bio(90).size=[];


bio(91).blkName='UDP IN (Cohda)1/Logical Operator';
bio(91).sigName='isNewMsg';
bio(91).portIdx=0;
bio(91).dim=[1,1];
bio(91).sigWidth=1;
bio(91).sigAddress='&scoop_v3_B.isNewMsg';
bio(91).ndims=2;
bio(91).size=[];


bio(92).blkName='UDP IN (Cohda)1/Rate Transition1';
bio(92).sigName='isNewMsg';
bio(92).portIdx=0;
bio(92).dim=[1,1];
bio(92).sigWidth=1;
bio(92).sigAddress='&scoop_v3_B.isNewMsg_a';
bio(92).ndims=2;
bio(92).size=[];


bio(93).blkName='UDP IN (Cohda)1/Rate Transition10';
bio(93).sigName='msgType';
bio(93).portIdx=0;
bio(93).dim=[1,1];
bio(93).sigWidth=1;
bio(93).sigAddress='&scoop_v3_B.msgType_a';
bio(93).ndims=2;
bio(93).size=[];


bio(94).blkName='UDP IN (Cohda)1/Rate Transition4';
bio(94).sigName='';
bio(94).portIdx=0;
bio(94).dim=[1,1];
bio(94).sigWidth=1;
bio(94).sigAddress='&scoop_v3_B.RateTransition4_f';
bio(94).ndims=2;
bio(94).size=[];


bio(95).blkName='UDP IN (Cohda)1/S-Function Builder';
bio(95).sigName='msgType';
bio(95).portIdx=0;
bio(95).dim=[1,1];
bio(95).sigWidth=1;
bio(95).sigAddress='&scoop_v3_B.msgType';
bio(95).ndims=2;
bio(95).size=[];


bio(96).blkName='UDP OUT (Cohda)1/Rate Transition';
bio(96).sigName='';
bio(96).portIdx=0;
bio(96).dim=[1,1];
bio(96).sigWidth=1;
bio(96).sigAddress='&scoop_v3_B.RateTransition_e';
bio(96).ndims=2;
bio(96).size=[];


bio(97).blkName='UDP OUT (Cohda)1/Packet Width';
bio(97).sigName='';
bio(97).portIdx=0;
bio(97).dim=[1,1];
bio(97).sigWidth=1;
bio(97).sigAddress='&scoop_v3_ConstB.PacketWidth';
bio(97).ndims=2;
bio(97).size=[];


bio(98).blkName='V2V Out Convert/MATLAB Function10';
bio(98).sigName='VehRespoTimeDelayETSI';
bio(98).portIdx=0;
bio(98).dim=[1,1];
bio(98).sigWidth=1;
bio(98).sigAddress='&scoop_v3_B.VehRespoTimeDelayETSI_j';
bio(98).ndims=2;
bio(98).size=[];


bio(99).blkName='V2V Out Convert/MATLAB Function11';
bio(99).sigName='LatRefPositionETSI';
bio(99).portIdx=0;
bio(99).dim=[1,1];
bio(99).sigWidth=1;
bio(99).sigAddress='&scoop_v3_B.LatRefPositionETSI_b';
bio(99).ndims=2;
bio(99).size=[];


bio(100).blkName='V2V Out Convert/MATLAB Function12';
bio(100).sigName='LonRefPositionETSI';
bio(100).portIdx=0;
bio(100).dim=[1,1];
bio(100).sigWidth=1;
bio(100).sigAddress='&scoop_v3_B.LonRefPositionETSI_k';
bio(100).ndims=2;
bio(100).size=[];


bio(101).blkName='V2V Out Convert/MATLAB Function13';
bio(101).sigName='PosConfETSI';
bio(101).portIdx=0;
bio(101).dim=[1,1];
bio(101).sigWidth=1;
bio(101).sigAddress='&scoop_v3_B.PosConfETSI';
bio(101).ndims=2;
bio(101).size=[];


bio(102).blkName='V2V Out Convert/MATLAB Function14/p1';
bio(102).sigName='VehHeadingValETSI';
bio(102).portIdx=0;
bio(102).dim=[1,1];
bio(102).sigWidth=1;
bio(102).sigAddress='&scoop_v3_B.VehHeadingValETSI_h';
bio(102).ndims=2;
bio(102).size=[];


bio(103).blkName='V2V Out Convert/MATLAB Function14/p2';
bio(103).sigName='VehHeadingConfETSI';
bio(103).portIdx=1;
bio(103).dim=[1,1];
bio(103).sigWidth=1;
bio(103).sigAddress='&scoop_v3_B.VehHeadingConfETSI_f';
bio(103).ndims=2;
bio(103).size=[];


bio(104).blkName='V2V Out Convert/MATLAB Function15/p1';
bio(104).sigName='VehSpeedValETSI';
bio(104).portIdx=0;
bio(104).dim=[1,1];
bio(104).sigWidth=1;
bio(104).sigAddress='&scoop_v3_B.VehSpeedValETSI_c';
bio(104).ndims=2;
bio(104).size=[];


bio(105).blkName='V2V Out Convert/MATLAB Function15/p2';
bio(105).sigName='VehSpeedConfETSI';
bio(105).portIdx=1;
bio(105).dim=[1,1];
bio(105).sigWidth=1;
bio(105).sigAddress='&scoop_v3_B.VehSpeedConfETSI_i';
bio(105).ndims=2;
bio(105).size=[];


bio(106).blkName='V2V Out Convert/MATLAB Function16/p1';
bio(106).sigName='VehYawRateValETSI';
bio(106).portIdx=0;
bio(106).dim=[1,1];
bio(106).sigWidth=1;
bio(106).sigAddress='&scoop_v3_B.VehYawRateValETSI_f';
bio(106).ndims=2;
bio(106).size=[];


bio(107).blkName='V2V Out Convert/MATLAB Function16/p2';
bio(107).sigName='VehYawRateConfETSI';
bio(107).portIdx=1;
bio(107).dim=[1,1];
bio(107).sigWidth=1;
bio(107).sigAddress='&scoop_v3_B.VehYawRateConfETSI_l';
bio(107).ndims=2;
bio(107).size=[];


bio(108).blkName='V2V Out Convert/MATLAB Function17/p1';
bio(108).sigName='LongAccValETSI';
bio(108).portIdx=0;
bio(108).dim=[1,1];
bio(108).sigWidth=1;
bio(108).sigAddress='&scoop_v3_B.LongAccValETSI_fa';
bio(108).ndims=2;
bio(108).size=[];


bio(109).blkName='V2V Out Convert/MATLAB Function17/p2';
bio(109).sigName='LongAccConfETSI';
bio(109).portIdx=1;
bio(109).dim=[1,1];
bio(109).sigWidth=1;
bio(109).sigAddress='&scoop_v3_B.LongAccConfETSI_o';
bio(109).ndims=2;
bio(109).size=[];


bio(110).blkName='V2V Out Convert/MATLAB Function18';
bio(110).sigName='PosGenDeltaTimeETSI';
bio(110).portIdx=0;
bio(110).dim=[1,1];
bio(110).sigWidth=1;
bio(110).sigAddress='&scoop_v3_B.PosGenDeltaTimeETSI';
bio(110).ndims=2;
bio(110).size=[];


bio(111).blkName='V2V Out Convert/MATLAB Function3';
bio(111).sigName='StatioTypeETSI';
bio(111).portIdx=0;
bio(111).dim=[1,1];
bio(111).sigWidth=1;
bio(111).sigAddress='&scoop_v3_B.StatioTypeETSI_a';
bio(111).ndims=2;
bio(111).size=[];


bio(112).blkName='V2V Out Convert/MATLAB Function5';
bio(112).sigName='VehicleLengthETSI';
bio(112).portIdx=0;
bio(112).dim=[1,1];
bio(112).sigWidth=1;
bio(112).sigAddress='&scoop_v3_B.VehicleLengthETSI_f';
bio(112).ndims=2;
bio(112).size=[];


bio(113).blkName='V2V Out Convert/MATLAB Function6';
bio(113).sigName='VehicleRearAxleLocETSI';
bio(113).portIdx=0;
bio(113).dim=[1,1];
bio(113).sigWidth=1;
bio(113).sigAddress='&scoop_v3_B.VehicleRearAxleLocETSI';
bio(113).ndims=2;
bio(113).size=[];


bio(114).blkName='V2V Out Convert/MATLAB Function7';
bio(114).sigName='VehicleWidthETSI';
bio(114).portIdx=0;
bio(114).dim=[1,1];
bio(114).sigWidth=1;
bio(114).sigAddress='&scoop_v3_B.VehicleWidthETSI_f';
bio(114).ndims=2;
bio(114).size=[];


bio(115).blkName='V2V Out Convert/MATLAB Function8';
bio(115).sigName='CtrlTypeETSI';
bio(115).portIdx=0;
bio(115).dim=[1,1];
bio(115).sigWidth=1;
bio(115).sigAddress='&scoop_v3_B.CtrlTypeETSI_l';
bio(115).ndims=2;
bio(115).size=[];


bio(116).blkName='V2V Out Convert/MATLAB Function9';
bio(116).sigName='VehRespoTimeCnstETSI';
bio(116).portIdx=0;
bio(116).dim=[1,1];
bio(116).sigWidth=1;
bio(116).sigAddress='&scoop_v3_B.VehRespoTimeCnstETSI_i';
bio(116).ndims=2;
bio(116).size=[];


bio(117).blkName='V2V Out Convert/Res';
bio(117).sigName='RefTimeETSI';
bio(117).portIdx=0;
bio(117).dim=[1,1];
bio(117).sigWidth=1;
bio(117).sigAddress='&scoop_v3_B.RefTimeETSI';
bio(117).ndims=2;
bio(117).size=[];


bio(118).blkName='V2V Out Convert/Res 0.002';
bio(118).sigName='MioBearingETSI';
bio(118).portIdx=0;
bio(118).dim=[1,1];
bio(118).sigWidth=1;
bio(118).sigAddress='&scoop_v3_B.MioBearingETSI';
bio(118).ndims=2;
bio(118).size=[];


bio(119).blkName='V2V Out Convert/Res 0.01';
bio(119).sigName='MioRangeETSI';
bio(119).portIdx=0;
bio(119).dim=[1,1];
bio(119).sigWidth=1;
bio(119).sigAddress='&scoop_v3_B.MioRangeETSI';
bio(119).ndims=2;
bio(119).size=[];


bio(120).blkName='V2V Out Convert/Res 0.1';
bio(120).sigName='TraveledDistCZETSI';
bio(120).portIdx=0;
bio(120).dim=[1,1];
bio(120).sigWidth=1;
bio(120).sigAddress='&scoop_v3_B.TraveledDistCZETSI';
bio(120).ndims=2;
bio(120).size=[];


bio(121).blkName='V2V Out Convert/Res 1';
bio(121).sigName='MioIdETSI';
bio(121).portIdx=0;
bio(121).dim=[1,1];
bio(121).sigWidth=1;
bio(121).sigAddress='&scoop_v3_B.MioIdETSI';
bio(121).ndims=2;
bio(121).size=[];


bio(122).blkName='V2V Out Convert/Res 2';
bio(122).sigName='VehicleRoleETSI';
bio(122).portIdx=0;
bio(122).dim=[1,1];
bio(122).sigWidth=1;
bio(122).sigAddress='&scoop_v3_B.VehicleRoleETSI';
bio(122).ndims=2;
bio(122).size=[];


bio(123).blkName='V2V Out Convert/Res 3';
bio(123).sigName='StationIdETSI';
bio(123).portIdx=0;
bio(123).dim=[1,1];
bio(123).sigWidth=1;
bio(123).sigAddress='&scoop_v3_B.StationIdETSI';
bio(123).ndims=2;
bio(123).size=[];


bio(124).blkName='V2V Out Convert/Res1 0.01';
bio(124).sigName='MioRangeRateETSI';
bio(124).portIdx=0;
bio(124).dim=[1,1];
bio(124).sigWidth=1;
bio(124).sigAddress='&scoop_v3_B.MioRangeRateETSI';
bio(124).ndims=2;
bio(124).size=[];


bio(125).blkName='V2V Out Convert/Res1 0.1';
bio(125).sigName='TimeDelayETSI';
bio(125).portIdx=0;
bio(125).dim=[1,1];
bio(125).sigWidth=1;
bio(125).sigAddress='&scoop_v3_B.TimeDelayETSI';
bio(125).ndims=2;
bio(125).size=[];


bio(126).blkName='V2V Out Convert/Res2';
bio(126).sigName='ForwardIdETSI';
bio(126).portIdx=0;
bio(126).dim=[1,1];
bio(126).sigWidth=1;
bio(126).sigAddress='&scoop_v3_B.ForwardIdETSI';
bio(126).ndims=2;
bio(126).size=[];


bio(127).blkName='V2V Out Convert/Res2 0.01';
bio(127).sigName='CruiseSpeedETSI';
bio(127).portIdx=0;
bio(127).dim=[1,1];
bio(127).sigWidth=1;
bio(127).sigAddress='&scoop_v3_B.CruiseSpeedETSI';
bio(127).ndims=2;
bio(127).size=[];


bio(128).blkName='V2V Out Convert/Res2 0.2';
bio(128).sigName='BackwardIdETSI';
bio(128).portIdx=0;
bio(128).dim=[1,1];
bio(128).sigWidth=1;
bio(128).sigAddress='&scoop_v3_B.BackwardIdETSI';
bio(128).ndims=2;
bio(128).size=[];


bio(129).blkName='V2V Out Convert/Sec2msec2';
bio(129).sigName='';
bio(129).portIdx=0;
bio(129).dim=[1,1];
bio(129).sigWidth=1;
bio(129).sigAddress='&scoop_v3_B.Sec2msec2';
bio(129).ndims=2;
bio(129).size=[];


bio(130).blkName='V2V Out Convert/Rate Transition1';
bio(130).sigName='LatRefPositionXpc';
bio(130).portIdx=0;
bio(130).dim=[1,1];
bio(130).sigWidth=1;
bio(130).sigAddress='&scoop_v3_B.LatRefPositionXpc';
bio(130).ndims=2;
bio(130).size=[];


bio(131).blkName='V2V Out Convert/Rate Transition10';
bio(131).sigName='VehicleWidthXpc';
bio(131).portIdx=0;
bio(131).dim=[1,1];
bio(131).sigWidth=1;
bio(131).sigAddress='&scoop_v3_B.VehicleWidthXpc';
bio(131).ndims=2;
bio(131).size=[];


bio(132).blkName='V2V Out Convert/Rate Transition11';
bio(132).sigName='CtrlTypeXpc';
bio(132).portIdx=0;
bio(132).dim=[1,1];
bio(132).sigWidth=1;
bio(132).sigAddress='&scoop_v3_B.CtrlTypeXpc';
bio(132).ndims=2;
bio(132).size=[];


bio(133).blkName='V2V Out Convert/Rate Transition12';
bio(133).sigName='VehRespoTimeCnstXpc';
bio(133).portIdx=0;
bio(133).dim=[1,1];
bio(133).sigWidth=1;
bio(133).sigAddress='&scoop_v3_B.VehRespoTimeCnstXpc';
bio(133).ndims=2;
bio(133).size=[];


bio(134).blkName='V2V Out Convert/Rate Transition13';
bio(134).sigName='VehRespoTimeDelayXpc';
bio(134).portIdx=0;
bio(134).dim=[1,1];
bio(134).sigWidth=1;
bio(134).sigAddress='&scoop_v3_B.VehRespoTimeDelayXpc';
bio(134).ndims=2;
bio(134).size=[];


bio(135).blkName='V2V Out Convert/Rate Transition14';
bio(135).sigName='MioBearingXpc';
bio(135).portIdx=0;
bio(135).dim=[1,1];
bio(135).sigWidth=1;
bio(135).sigAddress='&scoop_v3_B.MioBearingXpc_j';
bio(135).ndims=2;
bio(135).size=[];


bio(136).blkName='V2V Out Convert/Rate Transition15';
bio(136).sigName='MioRangeRateXpc';
bio(136).portIdx=0;
bio(136).dim=[1,1];
bio(136).sigWidth=1;
bio(136).sigAddress='&scoop_v3_B.MioRangeRateXpc_l';
bio(136).ndims=2;
bio(136).size=[];


bio(137).blkName='V2V Out Convert/Rate Transition16';
bio(137).sigName='PosConfEllipseInterval95Xpc';
bio(137).portIdx=0;
bio(137).dim=[1,1];
bio(137).sigWidth=1;
bio(137).sigAddress='&scoop_v3_B.PosConfEllipseInterval95Xpc';
bio(137).ndims=2;
bio(137).size=[];


bio(138).blkName='V2V Out Convert/Rate Transition17';
bio(138).sigName='VehHeadingValXpc';
bio(138).portIdx=0;
bio(138).dim=[1,1];
bio(138).sigWidth=1;
bio(138).sigAddress='&scoop_v3_B.VehHeadingValXpc';
bio(138).ndims=2;
bio(138).size=[];


bio(139).blkName='V2V Out Convert/Rate Transition18';
bio(139).sigName='VehHeadingConfXpc';
bio(139).portIdx=0;
bio(139).dim=[1,1];
bio(139).sigWidth=1;
bio(139).sigAddress='&scoop_v3_B.VehHeadingConfXpc';
bio(139).ndims=2;
bio(139).size=[];


bio(140).blkName='V2V Out Convert/Rate Transition19';
bio(140).sigName='VehSpeedValXpc';
bio(140).portIdx=0;
bio(140).dim=[1,1];
bio(140).sigWidth=1;
bio(140).sigAddress='&scoop_v3_B.VehSpeedValXpc';
bio(140).ndims=2;
bio(140).size=[];


bio(141).blkName='V2V Out Convert/Rate Transition2';
bio(141).sigName='PositionGenerationDeltaTimeXpc';
bio(141).portIdx=0;
bio(141).dim=[1,1];
bio(141).sigWidth=1;
bio(141).sigAddress='&scoop_v3_B.PositionGenerationDeltaTimeXp_k';
bio(141).ndims=2;
bio(141).size=[];


bio(142).blkName='V2V Out Convert/Rate Transition20';
bio(142).sigName='VehSpeedConfXpc';
bio(142).portIdx=0;
bio(142).dim=[1,1];
bio(142).sigWidth=1;
bio(142).sigAddress='&scoop_v3_B.VehSpeedConfXpc';
bio(142).ndims=2;
bio(142).size=[];


bio(143).blkName='V2V Out Convert/Rate Transition21';
bio(143).sigName='VehYawRateValXpc';
bio(143).portIdx=0;
bio(143).dim=[1,1];
bio(143).sigWidth=1;
bio(143).sigAddress='&scoop_v3_B.VehYawRateValXpc';
bio(143).ndims=2;
bio(143).size=[];


bio(144).blkName='V2V Out Convert/Rate Transition22';
bio(144).sigName='VehYawRateConfXpc';
bio(144).portIdx=0;
bio(144).dim=[1,1];
bio(144).sigWidth=1;
bio(144).sigAddress='&scoop_v3_B.VehYawRateConfXpc';
bio(144).ndims=2;
bio(144).size=[];


bio(145).blkName='V2V Out Convert/Rate Transition23';
bio(145).sigName='LongAccValXpc';
bio(145).portIdx=0;
bio(145).dim=[1,1];
bio(145).sigWidth=1;
bio(145).sigAddress='&scoop_v3_B.LongAccValXpc';
bio(145).ndims=2;
bio(145).size=[];


bio(146).blkName='V2V Out Convert/Rate Transition24';
bio(146).sigName='LongAccConfXpc';
bio(146).portIdx=0;
bio(146).dim=[1,1];
bio(146).sigWidth=1;
bio(146).sigAddress='&scoop_v3_B.LongAccConfXpc';
bio(146).ndims=2;
bio(146).size=[];


bio(147).blkName='V2V Out Convert/Rate Transition25';
bio(147).sigName='TimeDelayXpc';
bio(147).portIdx=0;
bio(147).dim=[1,1];
bio(147).sigWidth=1;
bio(147).sigAddress='&scoop_v3_B.TimeDelayXpc_l';
bio(147).ndims=2;
bio(147).size=[];


bio(148).blkName='V2V Out Convert/Rate Transition26';
bio(148).sigName='CruiseSpeedXpc';
bio(148).portIdx=0;
bio(148).dim=[1,1];
bio(148).sigWidth=1;
bio(148).sigAddress='&scoop_v3_B.CruiseSpeedXpc_b';
bio(148).ndims=2;
bio(148).size=[];


bio(149).blkName='V2V Out Convert/Rate Transition27';
bio(149).sigName='MergeReqFlagXpc';
bio(149).portIdx=0;
bio(149).dim=[1,1];
bio(149).sigWidth=1;
bio(149).sigAddress='&scoop_v3_B.MergeReqFlagXpc_j';
bio(149).ndims=2;
bio(149).size=[];


bio(150).blkName='V2V Out Convert/Rate Transition28';
bio(150).sigName='STOMXpc';
bio(150).portIdx=0;
bio(150).dim=[1,1];
bio(150).sigWidth=1;
bio(150).sigAddress='&scoop_v3_B.STOMXpc_c';
bio(150).ndims=2;
bio(150).size=[];


bio(151).blkName='V2V Out Convert/Rate Transition29';
bio(151).sigName='MergingFlagXpc';
bio(151).portIdx=0;
bio(151).dim=[1,1];
bio(151).sigWidth=1;
bio(151).sigAddress='&scoop_v3_B.MergingFlagXpc_b';
bio(151).ndims=2;
bio(151).size=[];


bio(152).blkName='V2V Out Convert/Rate Transition30';
bio(152).sigName='ForwardIdXpc';
bio(152).portIdx=0;
bio(152).dim=[1,1];
bio(152).sigWidth=1;
bio(152).sigAddress='&scoop_v3_B.ForwardIdXpc_m';
bio(152).ndims=2;
bio(152).size=[];


bio(153).blkName='V2V Out Convert/Rate Transition31';
bio(153).sigName='BackwardIdXpc';
bio(153).portIdx=0;
bio(153).dim=[1,1];
bio(153).sigWidth=1;
bio(153).sigAddress='&scoop_v3_B.BackwardIdXpc_n';
bio(153).ndims=2;
bio(153).size=[];


bio(154).blkName='V2V Out Convert/Rate Transition32';
bio(154).sigName='TailVehFlagXpc';
bio(154).portIdx=0;
bio(154).dim=[1,1];
bio(154).sigWidth=1;
bio(154).sigAddress='&scoop_v3_B.TailVehFlagXpc_h';
bio(154).ndims=2;
bio(154).size=[];


bio(155).blkName='V2V Out Convert/Rate Transition33';
bio(155).sigName='HeadVehFlagXpc';
bio(155).portIdx=0;
bio(155).dim=[1,1];
bio(155).sigWidth=1;
bio(155).sigAddress='&scoop_v3_B.HeadVehFlagXpc_l';
bio(155).ndims=2;
bio(155).size=[];


bio(156).blkName='V2V Out Convert/Rate Transition34';
bio(156).sigName='PlatoonIdXpc';
bio(156).portIdx=0;
bio(156).dim=[1,1];
bio(156).sigWidth=1;
bio(156).sigAddress='&scoop_v3_B.PlatoonIdXpc_d';
bio(156).ndims=2;
bio(156).size=[];


bio(157).blkName='V2V Out Convert/Rate Transition35';
bio(157).sigName='TraveledDistCZXpc';
bio(157).portIdx=0;
bio(157).dim=[1,1];
bio(157).sigWidth=1;
bio(157).sigAddress='&scoop_v3_B.TraveledDistCZXpc_k';
bio(157).ndims=2;
bio(157).size=[];


bio(158).blkName='V2V Out Convert/Rate Transition36';
bio(158).sigName='IntentionXpc';
bio(158).portIdx=0;
bio(158).dim=[1,1];
bio(158).sigWidth=1;
bio(158).sigAddress='&scoop_v3_B.IntentionXpc_d';
bio(158).ndims=2;
bio(158).size=[];


bio(159).blkName='V2V Out Convert/Rate Transition37';
bio(159).sigName='EntranceLaneCZXpc';
bio(159).portIdx=0;
bio(159).dim=[1,1];
bio(159).sigWidth=1;
bio(159).sigAddress='&scoop_v3_B.EntranceLaneCZXpc';
bio(159).ndims=2;
bio(159).size=[];


bio(160).blkName='V2V Out Convert/Rate Transition38';
bio(160).sigName='IntersVehCntrXpc';
bio(160).portIdx=0;
bio(160).dim=[1,1];
bio(160).sigWidth=1;
bio(160).sigAddress='&scoop_v3_B.IntersVehCntrXpc_h';
bio(160).ndims=2;
bio(160).size=[];


bio(161).blkName='V2V Out Convert/Rate Transition39';
bio(161).sigName='PairAckFlagXpc';
bio(161).portIdx=0;
bio(161).dim=[1,1];
bio(161).sigWidth=1;
bio(161).sigAddress='&scoop_v3_B.PairAckFlagXpc_i';
bio(161).ndims=2;
bio(161).size=[];


bio(162).blkName='V2V Out Convert/Rate Transition40';
bio(162).sigName='RefTimeXpc';
bio(162).portIdx=0;
bio(162).dim=[1,1];
bio(162).sigWidth=1;
bio(162).sigAddress='&scoop_v3_B.RefTimeXpc';
bio(162).ndims=2;
bio(162).size=[];


bio(163).blkName='V2V Out Convert/Rate Transition41';
bio(163).sigName='CauseCodeXpc';
bio(163).portIdx=0;
bio(163).dim=[1,1];
bio(163).sigWidth=1;
bio(163).sigAddress='&scoop_v3_B.CauseCodeXpc';
bio(163).ndims=2;
bio(163).size=[];


bio(164).blkName='V2V Out Convert/Rate Transition42';
bio(164).sigName='SubCauseCodeXpc';
bio(164).portIdx=0;
bio(164).dim=[1,1];
bio(164).sigWidth=1;
bio(164).sigAddress='&scoop_v3_B.SubCauseCodeXpc';
bio(164).ndims=2;
bio(164).size=[];


bio(165).blkName='V2V Out Convert/Rate Transition43';
bio(165).sigName='DrivLaneStatusXpc';
bio(165).portIdx=0;
bio(165).dim=[1,1];
bio(165).sigWidth=1;
bio(165).sigAddress='&scoop_v3_B.DrivLaneStatusXpc';
bio(165).ndims=2;
bio(165).size=[];


bio(166).blkName='V2V Out Convert/Rate Transition44';
bio(166).sigName='LanePosXpc';
bio(166).portIdx=0;
bio(166).dim=[1,1];
bio(166).sigWidth=1;
bio(166).sigAddress='&scoop_v3_B.LanePosXpc_p';
bio(166).ndims=2;
bio(166).size=[];


bio(167).blkName='V2V Out Convert/Rate Transition45';
bio(167).sigName='ParticipantsReadyXpc';
bio(167).portIdx=0;
bio(167).dim=[1,1];
bio(167).sigWidth=1;
bio(167).sigAddress='&scoop_v3_B.ParticipantsReadyXpc_c';
bio(167).ndims=2;
bio(167).size=[];


bio(168).blkName='V2V Out Convert/Rate Transition46';
bio(168).sigName='StartScenarioXpc';
bio(168).portIdx=0;
bio(168).dim=[1,1];
bio(168).sigWidth=1;
bio(168).sigAddress='&scoop_v3_B.StartScenarioXpc_h';
bio(168).ndims=2;
bio(168).size=[];


bio(169).blkName='V2V Out Convert/Rate Transition47';
bio(169).sigName='EoSXpc';
bio(169).portIdx=0;
bio(169).dim=[1,1];
bio(169).sigWidth=1;
bio(169).sigAddress='&scoop_v3_B.EoSXpc_h';
bio(169).ndims=2;
bio(169).size=[];


bio(170).blkName='V2V Out Convert/Rate Transition48';
bio(170).sigName='VehicleRoleXpc';
bio(170).portIdx=0;
bio(170).dim=[1,1];
bio(170).sigWidth=1;
bio(170).sigAddress='&scoop_v3_B.VehicleRoleXpc';
bio(170).ndims=2;
bio(170).size=[];


bio(171).blkName='V2V Out Convert/Rate Transition49';
bio(171).sigName='MioRangeXpc';
bio(171).portIdx=0;
bio(171).dim=[1,1];
bio(171).sigWidth=1;
bio(171).sigAddress='&scoop_v3_B.MioRangeXpc_n';
bio(171).ndims=2;
bio(171).size=[];


bio(172).blkName='V2V Out Convert/Rate Transition50';
bio(172).sigName='VehicleLengthXpc';
bio(172).portIdx=0;
bio(172).dim=[1,1];
bio(172).sigWidth=1;
bio(172).sigAddress='&scoop_v3_B.VehicleLengthXpc';
bio(172).ndims=2;
bio(172).size=[];


bio(173).blkName='V2V Out Convert/Rate Transition51';
bio(173).sigName='VehicleRearAxleLocationXpc';
bio(173).portIdx=0;
bio(173).dim=[1,1];
bio(173).sigWidth=1;
bio(173).sigAddress='&scoop_v3_B.VehicleRearAxleLocationXpc';
bio(173).ndims=2;
bio(173).size=[];


bio(174).blkName='V2V Out Convert/Rate Transition6';
bio(174).sigName='StationIDXpc';
bio(174).portIdx=0;
bio(174).dim=[1,1];
bio(174).sigWidth=1;
bio(174).sigAddress='&scoop_v3_B.StationIDXpc';
bio(174).ndims=2;
bio(174).size=[];


bio(175).blkName='V2V Out Convert/Rate Transition7';
bio(175).sigName='LonRefPositionXpc';
bio(175).portIdx=0;
bio(175).dim=[1,1];
bio(175).sigWidth=1;
bio(175).sigAddress='&scoop_v3_B.LonRefPositionXpc';
bio(175).ndims=2;
bio(175).size=[];


bio(176).blkName='V2V Out Convert/Rate Transition8';
bio(176).sigName='MioIdXpc';
bio(176).portIdx=0;
bio(176).dim=[1,1];
bio(176).sigWidth=1;
bio(176).sigAddress='&scoop_v3_B.MioIdXpc_b';
bio(176).ndims=2;
bio(176).size=[];


bio(177).blkName='V2V Out Convert/Rate Transition9';
bio(177).sigName='StatioTypeXpc';
bio(177).portIdx=0;
bio(177).dim=[1,1];
bio(177).sigWidth=1;
bio(177).sigAddress='&scoop_v3_B.StatioTypeXpc';
bio(177).ndims=2;
bio(177).size=[];


bio(178).blkName='V2V Out Convert/-+1571';
bio(178).sigName='';
bio(178).portIdx=0;
bio(178).dim=[1,1];
bio(178).sigWidth=1;
bio(178).sigAddress='&scoop_v3_B.u571';
bio(178).ndims=2;
bio(178).size=[];


bio(179).blkName='V2V Out Convert/-+32767';
bio(179).sigName='';
bio(179).portIdx=0;
bio(179).dim=[1,1];
bio(179).sigWidth=1;
bio(179).sigAddress='&scoop_v3_B.u2767';
bio(179).ndims=2;
bio(179).size=[];


bio(180).blkName='V2V Out Convert/-1-14';
bio(180).sigName='LanePosETSI';
bio(180).portIdx=0;
bio(180).dim=[1,1];
bio(180).sigWidth=1;
bio(180).sigAddress='&scoop_v3_B.LanePosETSI';
bio(180).ndims=2;
bio(180).size=[];


bio(181).blkName='V2V Out Convert/0-10000';
bio(181).sigName='';
bio(181).portIdx=0;
bio(181).dim=[1,1];
bio(181).sigWidth=1;
bio(181).sigAddress='&scoop_v3_B.u0000';
bio(181).ndims=2;
bio(181).size=[];


bio(182).blkName='V2V Out Convert/0-3';
bio(182).sigName='PlatoonIdETSI';
bio(182).portIdx=0;
bio(182).dim=[1,1];
bio(182).sigWidth=1;
bio(182).sigAddress='&scoop_v3_B.PlatoonIdETSI';
bio(182).ndims=2;
bio(182).size=[];


bio(183).blkName='V2V Out Convert/0-3 ';
bio(183).sigName='IntersVehCntrETSI';
bio(183).portIdx=0;
bio(183).dim=[1,1];
bio(183).sigWidth=1;
bio(183).sigAddress='&scoop_v3_B.IntersVehCntrETSI';
bio(183).ndims=2;
bio(183).size=[];


bio(184).blkName='V2V Out Convert/0-361';
bio(184).sigName='';
bio(184).portIdx=0;
bio(184).dim=[1,1];
bio(184).sigWidth=1;
bio(184).sigAddress='&scoop_v3_B.u61';
bio(184).ndims=2;
bio(184).size=[];


bio(185).blkName='V2V Out Convert/0-5001';
bio(185).sigName='';
bio(185).portIdx=0;
bio(185).dim=[1,1];
bio(185).sigWidth=1;
bio(185).sigAddress='&scoop_v3_B.u001';
bio(185).ndims=2;
bio(185).size=[];


bio(186).blkName='V2V Out Convert/0-65535';
bio(186).sigName='';
bio(186).portIdx=0;
bio(186).dim=[1,1];
bio(186).sigWidth=1;
bio(186).sigAddress='&scoop_v3_B.u5535';
bio(186).ndims=2;
bio(186).size=[];


bio(187).blkName='V2V Out Convert/1-14';
bio(187).sigName='DrivLaneStatusETSI';
bio(187).portIdx=0;
bio(187).dim=[1,1];
bio(187).sigWidth=1;
bio(187).sigAddress='&scoop_v3_B.DrivLaneStatusETSI';
bio(187).ndims=2;
bio(187).size=[];


bio(188).blkName='V2V Out Convert/1-3';
bio(188).sigName='IntentionETSI';
bio(188).portIdx=0;
bio(188).dim=[1,1];
bio(188).sigWidth=1;
bio(188).sigAddress='&scoop_v3_B.IntentionETSI';
bio(188).ndims=2;
bio(188).size=[];


bio(189).blkName='V2V Out Convert/1-4';
bio(189).sigName='EntranceLaneCZETSI';
bio(189).portIdx=0;
bio(189).dim=[1,1];
bio(189).sigWidth=1;
bio(189).sigAddress='&scoop_v3_B.EntranceLaneCZETSI';
bio(189).ndims=2;
bio(189).size=[];


bio(190).blkName='V2V Out Convert/8bit sat6';
bio(190).sigName='CauseCodeETSI';
bio(190).portIdx=0;
bio(190).dim=[1,1];
bio(190).sigWidth=1;
bio(190).sigAddress='&scoop_v3_B.CauseCodeETSI';
bio(190).ndims=2;
bio(190).size=[];


bio(191).blkName='V2V Out Convert/8bit sat7';
bio(191).sigName='SubCauseCodeETSI';
bio(191).portIdx=0;
bio(191).dim=[1,1];
bio(191).sigWidth=1;
bio(191).sigAddress='&scoop_v3_B.SubCauseCodeETSI';
bio(191).ndims=2;
bio(191).size=[];


bio(192).blkName='V2V Out Convert/boolean sat';
bio(192).sigName='MergeReqFlagETSI';
bio(192).portIdx=0;
bio(192).dim=[1,1];
bio(192).sigWidth=1;
bio(192).sigAddress='&scoop_v3_B.MergeReqFlagETSI';
bio(192).ndims=2;
bio(192).size=[];


bio(193).blkName='V2V Out Convert/boolean sat1';
bio(193).sigName='STOMETSI';
bio(193).portIdx=0;
bio(193).dim=[1,1];
bio(193).sigWidth=1;
bio(193).sigAddress='&scoop_v3_B.STOMETSI';
bio(193).ndims=2;
bio(193).size=[];


bio(194).blkName='V2V Out Convert/boolean sat2';
bio(194).sigName='MergingFlagETSI';
bio(194).portIdx=0;
bio(194).dim=[1,1];
bio(194).sigWidth=1;
bio(194).sigAddress='&scoop_v3_B.MergingFlagETSI';
bio(194).ndims=2;
bio(194).size=[];


bio(195).blkName='V2V Out Convert/boolean sat3';
bio(195).sigName='TailVehFlagETSI';
bio(195).portIdx=0;
bio(195).dim=[1,1];
bio(195).sigWidth=1;
bio(195).sigAddress='&scoop_v3_B.TailVehFlagETSI';
bio(195).ndims=2;
bio(195).size=[];


bio(196).blkName='V2V Out Convert/boolean sat4';
bio(196).sigName='HeadVehFlagETSI';
bio(196).portIdx=0;
bio(196).dim=[1,1];
bio(196).sigWidth=1;
bio(196).sigAddress='&scoop_v3_B.HeadVehFlagETSI';
bio(196).ndims=2;
bio(196).size=[];


bio(197).blkName='V2V Out Convert/boolean sat5';
bio(197).sigName='PairAckFlagETSI';
bio(197).portIdx=0;
bio(197).dim=[1,1];
bio(197).sigWidth=1;
bio(197).sigAddress='&scoop_v3_B.PairAckFlagETSI';
bio(197).ndims=2;
bio(197).size=[];


bio(198).blkName='V2V Out Convert/boolean sat6';
bio(198).sigName='ParticipantsReadyETSI';
bio(198).portIdx=0;
bio(198).dim=[1,1];
bio(198).sigWidth=1;
bio(198).sigAddress='&scoop_v3_B.ParticipantsReadyETSI';
bio(198).ndims=2;
bio(198).size=[];


bio(199).blkName='V2V Out Convert/boolean sat7';
bio(199).sigName='StartScenarioETSI';
bio(199).portIdx=0;
bio(199).dim=[1,1];
bio(199).sigWidth=1;
bio(199).sigAddress='&scoop_v3_B.StartScenarioETSI';
bio(199).ndims=2;
bio(199).size=[];


bio(200).blkName='V2V Out Convert/boolean sat8';
bio(200).sigName='EoSETSI';
bio(200).portIdx=0;
bio(200).dim=[1,1];
bio(200).sigWidth=1;
bio(200).sigAddress='&scoop_v3_B.EoSETSI';
bio(200).ndims=2;
bio(200).size=[];


bio(201).blkName='V2V Out Convert/GenerateTimestamp/p1';
bio(201).sigName='';
bio(201).portIdx=0;
bio(201).dim=[1,1];
bio(201).sigWidth=1;
bio(201).sigAddress='&scoop_v3_B.GenerateTimestamp_o1';
bio(201).ndims=2;
bio(201).size=[];


bio(202).blkName='V2V Out Convert/GenerateTimestamp/p2';
bio(202).sigName='';
bio(202).portIdx=1;
bio(202).dim=[1,1];
bio(202).sigWidth=1;
bio(202).sigAddress='&scoop_v3_B.GenerateTimestamp_o2';
bio(202).ndims=2;
bio(202).size=[];


bio(203).blkName='V2V Out Convert/Sum';
bio(203).sigName='UnixTimeStamp';
bio(203).portIdx=0;
bio(203).dim=[1,1];
bio(203).sigWidth=1;
bio(203).sigAddress='&scoop_v3_B.UnixTimeStamp';
bio(203).ndims=2;
bio(203).size=[];


bio(204).blkName='reference signal routing/MATLAB Function/p1';
bio(204).sigName='mtrTrq';
bio(204).portIdx=0;
bio(204).dim=[1,1];
bio(204).sigWidth=1;
bio(204).sigAddress='&scoop_v3_B.sf_MATLABFunction_f.mtrTrq';
bio(204).ndims=2;
bio(204).size=[];


bio(205).blkName='reference signal routing/MATLAB Function/p2';
bio(205).sigName='brkTrq';
bio(205).portIdx=1;
bio(205).dim=[1,1];
bio(205).sigWidth=1;
bio(205).sigAddress='&scoop_v3_B.sf_MATLABFunction_f.brkTrq';
bio(205).ndims=2;
bio(205).size=[];


bio(206).blkName='reference signal routing/MATLAB Function1/p1';
bio(206).sigName='mtrTrq';
bio(206).portIdx=0;
bio(206).dim=[1,1];
bio(206).sigWidth=1;
bio(206).sigAddress='&scoop_v3_B.sf_MATLABFunction1_e.mtrTrq';
bio(206).ndims=2;
bio(206).size=[];


bio(207).blkName='reference signal routing/MATLAB Function1/p2';
bio(207).sigName='brkTrq';
bio(207).portIdx=1;
bio(207).dim=[1,1];
bio(207).sigWidth=1;
bio(207).sigAddress='&scoop_v3_B.sf_MATLABFunction1_e.brkTrq';
bio(207).ndims=2;
bio(207).size=[];


bio(208).blkName='reference signal routing/Gain';
bio(208).sigName='';
bio(208).portIdx=0;
bio(208).dim=[1,1];
bio(208).sigWidth=1;
bio(208).sigAddress='&scoop_v3_B.Gain';
bio(208).ndims=2;
bio(208).size=[];


bio(209).blkName='reference signal routing/Rate Transition4';
bio(209).sigName='';
bio(209).portIdx=0;
bio(209).dim=[1,1];
bio(209).sigWidth=1;
bio(209).sigAddress='&scoop_v3_B.RateTransition4_c';
bio(209).ndims=2;
bio(209).size=[];


bio(210).blkName='referenceRouting/GhostInitDist';
bio(210).sigName='';
bio(210).portIdx=0;
bio(210).dim=[1,1];
bio(210).sigWidth=1;
bio(210).sigAddress='&scoop_v3_B.GhostInitDist';
bio(210).ndims=2;
bio(210).size=[];


bio(211).blkName='referenceRouting/Discrete-Time Integrator';
bio(211).sigName='';
bio(211).portIdx=0;
bio(211).dim=[1,1];
bio(211).sigWidth=1;
bio(211).sigAddress='&scoop_v3_B.DiscreteTimeIntegrator';
bio(211).ndims=2;
bio(211).size=[];


bio(212).blkName='referenceRouting/Discrete-Time Integrator1';
bio(212).sigName='';
bio(212).portIdx=0;
bio(212).dim=[1,1];
bio(212).sigWidth=1;
bio(212).sigAddress='&scoop_v3_B.DiscreteTimeIntegrator1';
bio(212).ndims=2;
bio(212).size=[];


bio(213).blkName='referenceRouting/kmh2ms1';
bio(213).sigName='';
bio(213).portIdx=0;
bio(213).dim=[1,1];
bio(213).sigWidth=1;
bio(213).sigAddress='&scoop_v3_B.kmh2ms1';
bio(213).ndims=2;
bio(213).size=[];


bio(214).blkName='referenceRouting/kmh2ms2';
bio(214).sigName='';
bio(214).portIdx=0;
bio(214).dim=[1,1];
bio(214).sigWidth=1;
bio(214).sigAddress='&scoop_v3_B.kmh2ms2';
bio(214).ndims=2;
bio(214).size=[];


bio(215).blkName='referenceRouting/kmh2ms3';
bio(215).sigName='';
bio(215).portIdx=0;
bio(215).dim=[1,1];
bio(215).sigWidth=1;
bio(215).sigAddress='&scoop_v3_B.kmh2ms3';
bio(215).ndims=2;
bio(215).size=[];


bio(216).blkName='referenceRouting/Multiport Switch';
bio(216).sigName='';
bio(216).portIdx=0;
bio(216).dim=[3,1];
bio(216).sigWidth=3;
bio(216).sigAddress='&scoop_v3_B.MultiportSwitch[0]';
bio(216).ndims=2;
bio(216).size=[];


bio(217).blkName='referenceRouting/Rate Transition2';
bio(217).sigName='';
bio(217).portIdx=0;
bio(217).dim=[1,1];
bio(217).sigWidth=1;
bio(217).sigAddress='&scoop_v3_B.RateTransition2_du';
bio(217).ndims=2;
bio(217).size=[];


bio(218).blkName='referenceRouting/Rate Transition4';
bio(218).sigName='';
bio(218).portIdx=0;
bio(218).dim=[1,1];
bio(218).sigWidth=1;
bio(218).sigAddress='&scoop_v3_B.RateTransition4';
bio(218).ndims=2;
bio(218).size=[];


bio(219).blkName='referenceRouting/Rate Transition6';
bio(219).sigName='';
bio(219).portIdx=0;
bio(219).dim=[1,1];
bio(219).sigWidth=1;
bio(219).sigAddress='&scoop_v3_B.RateTransition6_d';
bio(219).ndims=2;
bio(219).size=[];


bio(220).blkName='referenceRouting/Sum';
bio(220).sigName='tgtVel';
bio(220).portIdx=0;
bio(220).dim=[1,1];
bio(220).sigWidth=1;
bio(220).sigAddress='&scoop_v3_B.tgtVel';
bio(220).ndims=2;
bio(220).size=[];


bio(221).blkName='referenceRouting/Sum1';
bio(221).sigName='';
bio(221).portIdx=0;
bio(221).dim=[1,1];
bio(221).sigWidth=1;
bio(221).sigAddress='&scoop_v3_B.Sum1';
bio(221).ndims=2;
bio(221).size=[];


bio(222).blkName='CAN IN (Platform)/Truck/MATLAB Function/p1';
bio(222).sigName='latOverrideEngaged';
bio(222).portIdx=0;
bio(222).dim=[1,1];
bio(222).sigWidth=1;
bio(222).sigAddress='&scoop_v3_B.latOverrideEngaged';
bio(222).ndims=2;
bio(222).size=[];


bio(223).blkName='CAN IN (Platform)/Truck/MATLAB Function/p2';
bio(223).sigName='longOverrideEngaged';
bio(223).portIdx=1;
bio(223).dim=[1,1];
bio(223).sigWidth=1;
bio(223).sigAddress='&scoop_v3_B.longOverrideEngaged';
bio(223).ndims=2;
bio(223).size=[];


bio(224).blkName='CAN IN (Platform)/Truck/IMULatAcc';
bio(224).sigName='';
bio(224).portIdx=0;
bio(224).dim=[1,1];
bio(224).sigWidth=1;
bio(224).sigAddress='&scoop_v3_B.IMULatAcc';
bio(224).ndims=2;
bio(224).size=[];


bio(225).blkName='CAN IN (Platform)/Truck/IMULongAcc';
bio(225).sigName='';
bio(225).portIdx=0;
bio(225).dim=[1,1];
bio(225).sigWidth=1;
bio(225).sigAddress='&scoop_v3_B.IMULongAcc';
bio(225).ndims=2;
bio(225).size=[];


bio(226).blkName='CAN IN (Platform)/Truck/IMUYawrate';
bio(226).sigName='';
bio(226).portIdx=0;
bio(226).dim=[1,1];
bio(226).sigWidth=1;
bio(226).sigAddress='&scoop_v3_B.IMUYawrate';
bio(226).ndims=2;
bio(226).size=[];


bio(227).blkName='CAN IN (Platform)/Truck/MtrLongAcc';
bio(227).sigName='';
bio(227).portIdx=0;
bio(227).dim=[1,1];
bio(227).sigWidth=1;
bio(227).sigAddress='&scoop_v3_B.MtrLongAcc';
bio(227).ndims=2;
bio(227).size=[];


bio(228).blkName='CAN IN (Platform)/Truck/MtrLongVel';
bio(228).sigName='';
bio(228).portIdx=0;
bio(228).dim=[1,1];
bio(228).sigWidth=1;
bio(228).sigAddress='&scoop_v3_B.MtrLongVel';
bio(228).ndims=2;
bio(228).size=[];


bio(229).blkName='CAN IN (Platform)/Truck/Silent_On//Off';
bio(229).sigName='Silent_On/Off';
bio(229).portIdx=0;
bio(229).dim=[1,1];
bio(229).sigWidth=1;
bio(229).sigAddress='&scoop_v3_B.Silent_OnOff';
bio(229).ndims=2;
bio(229).size=[];


bio(230).blkName='CAN IN (Platform)/Truck/Switch1_Off';
bio(230).sigName='Switch1_Off';
bio(230).portIdx=0;
bio(230).dim=[1,1];
bio(230).sigWidth=1;
bio(230).sigAddress='&scoop_v3_B.Switch1_Off';
bio(230).ndims=2;
bio(230).size=[];


bio(231).blkName='CAN IN (Platform)/Truck/Switch1_On';
bio(231).sigName='Switch1_On';
bio(231).portIdx=0;
bio(231).dim=[1,1];
bio(231).sigWidth=1;
bio(231).sigAddress='&scoop_v3_B.Switch1_On';
bio(231).ndims=2;
bio(231).size=[];


bio(232).blkName='CAN IN (Platform)/Truck/Switch1_Up';
bio(232).sigName='Switch1_Up';
bio(232).portIdx=0;
bio(232).dim=[1,1];
bio(232).sigWidth=1;
bio(232).sigAddress='&scoop_v3_B.Switch1_Up';
bio(232).ndims=2;
bio(232).size=[];


bio(233).blkName='CAN IN (Platform)/Truck/accPedalPos';
bio(233).sigName='AccPedalPosition';
bio(233).portIdx=0;
bio(233).dim=[1,1];
bio(233).sigWidth=1;
bio(233).sigAddress='&scoop_v3_B.AccPedalPosition';
bio(233).ndims=2;
bio(233).size=[];


bio(234).blkName='CAN IN (Platform)/Truck/brakePedalPos';
bio(234).sigName='BrakePedalPos';
bio(234).portIdx=0;
bio(234).dim=[1,1];
bio(234).sigWidth=1;
bio(234).sigAddress='&scoop_v3_B.BrakePedalPos';
bio(234).ndims=2;
bio(234).size=[];


bio(235).blkName='CAN IN (Platform)/Truck/Rate Transition1';
bio(235).sigName='';
bio(235).portIdx=0;
bio(235).dim=[1,1];
bio(235).sigWidth=1;
bio(235).sigAddress='&scoop_v3_B.RateTransition1_l';
bio(235).ndims=2;
bio(235).size=[];


bio(236).blkName='CAN IN (Platform)/Truck/Rate Transition11';
bio(236).sigName='';
bio(236).portIdx=0;
bio(236).dim=[1,1];
bio(236).sigWidth=1;
bio(236).sigAddress='&scoop_v3_B.RateTransition11';
bio(236).ndims=2;
bio(236).size=[];


bio(237).blkName='CAN IN (Platform)/Truck/Rate Transition12';
bio(237).sigName='';
bio(237).portIdx=0;
bio(237).dim=[1,1];
bio(237).sigWidth=1;
bio(237).sigAddress='&scoop_v3_B.RateTransition12';
bio(237).ndims=2;
bio(237).size=[];


bio(238).blkName='CAN IN (Platform)/Truck/Rate Transition13';
bio(238).sigName='MtrLongVel';
bio(238).portIdx=0;
bio(238).dim=[2,1];
bio(238).sigWidth=2;
bio(238).sigAddress='&scoop_v3_B.MtrLongVel_p[0]';
bio(238).ndims=2;
bio(238).size=[];


bio(239).blkName='CAN IN (Platform)/Truck/Rate Transition14';
bio(239).sigName='IMUYawrate';
bio(239).portIdx=0;
bio(239).dim=[2,1];
bio(239).sigWidth=2;
bio(239).sigAddress='&scoop_v3_B.IMUYawrate_a[0]';
bio(239).ndims=2;
bio(239).size=[];


bio(240).blkName='CAN IN (Platform)/Truck/Rate Transition15';
bio(240).sigName='MtrLongAcc';
bio(240).portIdx=0;
bio(240).dim=[2,1];
bio(240).sigWidth=2;
bio(240).sigAddress='&scoop_v3_B.MtrLongAcc_m[0]';
bio(240).ndims=2;
bio(240).size=[];


bio(241).blkName='CAN IN (Platform)/Truck/Rate Transition16';
bio(241).sigName='IMULongAcc';
bio(241).portIdx=0;
bio(241).dim=[2,1];
bio(241).sigWidth=2;
bio(241).sigAddress='&scoop_v3_B.IMULongAcc_n[0]';
bio(241).ndims=2;
bio(241).size=[];


bio(242).blkName='CAN IN (Platform)/Truck/Rate Transition17';
bio(242).sigName='IMULatAcc';
bio(242).portIdx=0;
bio(242).dim=[2,1];
bio(242).sigWidth=2;
bio(242).sigAddress='&scoop_v3_B.IMULatAcc_e[0]';
bio(242).ndims=2;
bio(242).size=[];


bio(243).blkName='CAN IN (Platform)/Truck/Rate Transition18';
bio(243).sigName='';
bio(243).portIdx=0;
bio(243).dim=[1,1];
bio(243).sigWidth=1;
bio(243).sigAddress='&scoop_v3_B.RateTransition18';
bio(243).ndims=2;
bio(243).size=[];


bio(244).blkName='CAN IN (Platform)/Truck/Rate Transition19';
bio(244).sigName='IMUTimestamp';
bio(244).portIdx=0;
bio(244).dim=[1,1];
bio(244).sigWidth=1;
bio(244).sigAddress='&scoop_v3_B.IMUTimestamp';
bio(244).ndims=2;
bio(244).size=[];


bio(245).blkName='CAN IN (Platform)/Truck/Rate Transition2';
bio(245).sigName='';
bio(245).portIdx=0;
bio(245).dim=[1,1];
bio(245).sigWidth=1;
bio(245).sigAddress='&scoop_v3_B.RateTransition2_d';
bio(245).ndims=2;
bio(245).size=[];


bio(246).blkName='CAN IN (Platform)/Truck/Rate Transition3';
bio(246).sigName='';
bio(246).portIdx=0;
bio(246).dim=[1,1];
bio(246).sigWidth=1;
bio(246).sigAddress='&scoop_v3_B.RateTransition3_c';
bio(246).ndims=2;
bio(246).size=[];


bio(247).blkName='CAN IN (Platform)/Truck/Rate Transition5';
bio(247).sigName='';
bio(247).portIdx=0;
bio(247).dim=[1,1];
bio(247).sigWidth=1;
bio(247).sigAddress='&scoop_v3_B.RateTransition5_p';
bio(247).ndims=2;
bio(247).size=[];


bio(248).blkName='CAN IN (Platform)/Truck/Rate Transition6';
bio(248).sigName='';
bio(248).portIdx=0;
bio(248).dim=[1,1];
bio(248).sigWidth=1;
bio(248).sigAddress='&scoop_v3_B.RateTransition6';
bio(248).ndims=2;
bio(248).size=[];


bio(249).blkName='CAN IN (Platform)/Truck/Rate Transition7';
bio(249).sigName='';
bio(249).portIdx=0;
bio(249).dim=[1,1];
bio(249).sigWidth=1;
bio(249).sigAddress='&scoop_v3_B.RateTransition7';
bio(249).ndims=2;
bio(249).size=[];


bio(250).blkName='CAN IN (Platform)/Truck/Rate Transition8';
bio(250).sigName='';
bio(250).portIdx=0;
bio(250).dim=[1,1];
bio(250).sigWidth=1;
bio(250).sigAddress='&scoop_v3_B.RateTransition8';
bio(250).ndims=2;
bio(250).size=[];


bio(251).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp1/p1';
bio(251).sigName='';
bio(251).portIdx=0;
bio(251).dim=[1,1];
bio(251).sigWidth=1;
bio(251).sigAddress='&scoop_v3_B.AppendLatTimestamp1_o1';
bio(251).ndims=2;
bio(251).size=[];


bio(252).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp1/p2';
bio(252).sigName='';
bio(252).portIdx=1;
bio(252).dim=[1,1];
bio(252).sigWidth=1;
bio(252).sigAddress='&scoop_v3_B.AppendLatTimestamp1_o2';
bio(252).ndims=2;
bio(252).size=[];


bio(253).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp2/p1';
bio(253).sigName='';
bio(253).portIdx=0;
bio(253).dim=[1,1];
bio(253).sigWidth=1;
bio(253).sigAddress='&scoop_v3_B.AppendLatTimestamp2_o1';
bio(253).ndims=2;
bio(253).size=[];


bio(254).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp2/p2';
bio(254).sigName='';
bio(254).portIdx=1;
bio(254).dim=[1,1];
bio(254).sigWidth=1;
bio(254).sigAddress='&scoop_v3_B.AppendLatTimestamp2_o2';
bio(254).ndims=2;
bio(254).size=[];


bio(255).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp3/p1';
bio(255).sigName='';
bio(255).portIdx=0;
bio(255).dim=[1,1];
bio(255).sigWidth=1;
bio(255).sigAddress='&scoop_v3_B.AppendLatTimestamp3_o1';
bio(255).ndims=2;
bio(255).size=[];


bio(256).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp3/p2';
bio(256).sigName='';
bio(256).portIdx=1;
bio(256).dim=[1,1];
bio(256).sigWidth=1;
bio(256).sigAddress='&scoop_v3_B.AppendLatTimestamp3_o2';
bio(256).ndims=2;
bio(256).size=[];


bio(257).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp4/p1';
bio(257).sigName='';
bio(257).portIdx=0;
bio(257).dim=[1,1];
bio(257).sigWidth=1;
bio(257).sigAddress='&scoop_v3_B.AppendLatTimestamp4_o1';
bio(257).ndims=2;
bio(257).size=[];


bio(258).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp4/p2';
bio(258).sigName='';
bio(258).portIdx=1;
bio(258).dim=[1,1];
bio(258).sigWidth=1;
bio(258).sigAddress='&scoop_v3_B.AppendLatTimestamp4_o2';
bio(258).ndims=2;
bio(258).size=[];


bio(259).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp5/p1';
bio(259).sigName='';
bio(259).portIdx=0;
bio(259).dim=[1,1];
bio(259).sigWidth=1;
bio(259).sigAddress='&scoop_v3_B.AppendLatTimestamp5_o1';
bio(259).ndims=2;
bio(259).size=[];


bio(260).blkName='CAN IN (Platform)/Truck/AppendLatTimestamp5/p2';
bio(260).sigName='IMU_TimeStamp';
bio(260).portIdx=1;
bio(260).dim=[1,1];
bio(260).sigWidth=1;
bio(260).sigAddress='&scoop_v3_B.IMU_TimeStamp';
bio(260).ndims=2;
bio(260).size=[];


bio(261).blkName='CAN OUT (Platform)/Truck/Signal light logic/p1';
bio(261).sigName='red';
bio(261).portIdx=0;
bio(261).dim=[1,1];
bio(261).sigWidth=1;
bio(261).sigAddress='&scoop_v3_B.red';
bio(261).ndims=2;
bio(261).size=[];


bio(262).blkName='CAN OUT (Platform)/Truck/Signal light logic/p2';
bio(262).sigName='yellow';
bio(262).portIdx=1;
bio(262).dim=[1,1];
bio(262).sigWidth=1;
bio(262).sigAddress='&scoop_v3_B.yellow';
bio(262).ndims=2;
bio(262).size=[];


bio(263).blkName='CAN OUT (Platform)/Truck/Signal light logic/p3';
bio(263).sigName='green';
bio(263).portIdx=2;
bio(263).dim=[1,1];
bio(263).sigWidth=1;
bio(263).sigAddress='&scoop_v3_B.green';
bio(263).ndims=2;
bio(263).size=[];


bio(264).blkName='CAN OUT (Platform)/Truck/Signal light logic/p4';
bio(264).sigName='blue';
bio(264).portIdx=3;
bio(264).dim=[1,1];
bio(264).sigWidth=1;
bio(264).sigAddress='&scoop_v3_B.blue';
bio(264).ndims=2;
bio(264).size=[];


bio(265).blkName='CAN OUT (Platform)/Truck/Signal light logic/p5';
bio(265).sigName='white';
bio(265).portIdx=4;
bio(265).dim=[1,1];
bio(265).sigWidth=1;
bio(265).sigAddress='&scoop_v3_B.white';
bio(265).ndims=2;
bio(265).size=[];


bio(266).blkName='CAN OUT (Platform)/Truck/Signal light logic/p6';
bio(266).sigName='redOut';
bio(266).portIdx=5;
bio(266).dim=[1,1];
bio(266).sigWidth=1;
bio(266).sigAddress='&scoop_v3_B.redOut';
bio(266).ndims=2;
bio(266).size=[];


bio(267).blkName='CAN OUT (Platform)/Truck/Signal light logic/p7';
bio(267).sigName='greenOut';
bio(267).portIdx=6;
bio(267).dim=[1,1];
bio(267).sigWidth=1;
bio(267).sigAddress='&scoop_v3_B.greenOut';
bio(267).ndims=2;
bio(267).size=[];


bio(268).blkName='CAN OUT (Platform)/Truck/Constant';
bio(268).sigName='';
bio(268).portIdx=0;
bio(268).dim=[1,1];
bio(268).sigWidth=1;
bio(268).sigAddress='&scoop_v3_B.Constant';
bio(268).ndims=2;
bio(268).size=[];


bio(269).blkName='CAN OUT (Platform)/Truck/Data Type Conversion';
bio(269).sigName='';
bio(269).portIdx=0;
bio(269).dim=[1,1];
bio(269).sigWidth=1;
bio(269).sigAddress='&scoop_v3_B.DataTypeConversion';
bio(269).ndims=2;
bio(269).size=[];


bio(270).blkName='CAN OUT (Platform)/Truck/Data Type Conversion1';
bio(270).sigName='';
bio(270).portIdx=0;
bio(270).dim=[1,1];
bio(270).sigWidth=1;
bio(270).sigAddress='&scoop_v3_B.DataTypeConversion1';
bio(270).ndims=2;
bio(270).size=[];


bio(271).blkName='CAN OUT (Platform)/Truck/AccRequestRateTransmition';
bio(271).sigName='';
bio(271).portIdx=0;
bio(271).dim=[1,1];
bio(271).sigWidth=1;
bio(271).sigAddress='&scoop_v3_B.AccRequestRateTransmition';
bio(271).ndims=2;
bio(271).size=[];


bio(272).blkName='CAN OUT (Platform)/Truck/Rate Transition1';
bio(272).sigName='';
bio(272).portIdx=0;
bio(272).dim=[1,1];
bio(272).sigWidth=1;
bio(272).sigAddress='&scoop_v3_B.RateTransition1_c';
bio(272).ndims=2;
bio(272).size=[];


bio(273).blkName='CAN OUT (Platform)/Truck/Rate Transition10';
bio(273).sigName='';
bio(273).portIdx=0;
bio(273).dim=[1,1];
bio(273).sigWidth=1;
bio(273).sigAddress='&scoop_v3_B.RateTransition10_k';
bio(273).ndims=2;
bio(273).size=[];


bio(274).blkName='CAN OUT (Platform)/Truck/Rate Transition2';
bio(274).sigName='';
bio(274).portIdx=0;
bio(274).dim=[1,1];
bio(274).sigWidth=1;
bio(274).sigAddress='&scoop_v3_B.RateTransition2_g';
bio(274).ndims=2;
bio(274).size=[];


bio(275).blkName='CAN OUT (Platform)/Truck/Rate Transition3';
bio(275).sigName='';
bio(275).portIdx=0;
bio(275).dim=[1,1];
bio(275).sigWidth=1;
bio(275).sigAddress='&scoop_v3_B.RateTransition3_n';
bio(275).ndims=2;
bio(275).size=[];


bio(276).blkName='CAN OUT (Platform)/Truck/Rate Transition5';
bio(276).sigName='';
bio(276).portIdx=0;
bio(276).dim=[1,1];
bio(276).sigWidth=1;
bio(276).sigAddress='&scoop_v3_B.RateTransition5';
bio(276).ndims=2;
bio(276).size=[];


bio(277).blkName='CAN OUT (Platform)/Truck/Rate Transition6';
bio(277).sigName='';
bio(277).portIdx=0;
bio(277).dim=[1,1];
bio(277).sigWidth=1;
bio(277).sigAddress='&scoop_v3_B.RateTransition6_e';
bio(277).ndims=2;
bio(277).size=[];


bio(278).blkName='CAN OUT (Platform)/Truck/Rate Transition7';
bio(278).sigName='';
bio(278).portIdx=0;
bio(278).dim=[1,1];
bio(278).sigWidth=1;
bio(278).sigAddress='&scoop_v3_B.RateTransition7_b';
bio(278).ndims=2;
bio(278).size=[];


bio(279).blkName='CAN OUT (Platform)/Truck/Rate Transition9';
bio(279).sigName='';
bio(279).portIdx=0;
bio(279).dim=[1,1];
bio(279).sigWidth=1;
bio(279).sigAddress='&scoop_v3_B.RateTransition9';
bio(279).ndims=2;
bio(279).size=[];


bio(280).blkName='CAN OUT (Platform)/Truck/VxRequestRateTransmition';
bio(280).sigName='';
bio(280).portIdx=0;
bio(280).dim=[1,1];
bio(280).sigWidth=1;
bio(280).sigAddress='&scoop_v3_B.VxRequestRateTransmition';
bio(280).ndims=2;
bio(280).size=[];


bio(281).blkName='CAN OUT (Platform)/Truck/CAN Pack1';
bio(281).sigName='';
bio(281).portIdx=0;
bio(281).dim=[1,1];
bio(281).sigWidth=1;
bio(281).sigAddress='&scoop_v3_B.CANPack1';
bio(281).ndims=2;
bio(281).size=[];


bio(282).blkName='MonitoringAndLogging /Log Received Data/DefaultValue1';
bio(282).sigName='';
bio(282).portIdx=0;
bio(282).dim=[1,1];
bio(282).sigWidth=1;
bio(282).sigAddress='&scoop_v3_B.DefaultValue1';
bio(282).ndims=2;
bio(282).size=[];


bio(283).blkName='MonitoringAndLogging /Log Received Data/DefaultValue2';
bio(283).sigName='Latitude_data';
bio(283).portIdx=0;
bio(283).dim=[1,1];
bio(283).sigWidth=1;
bio(283).sigAddress='&scoop_v3_B.Latitude_data_h';
bio(283).ndims=2;
bio(283).size=[];


bio(284).blkName='MonitoringAndLogging /Log Received Data/DefaultValue3';
bio(284).sigName='';
bio(284).portIdx=0;
bio(284).dim=[1,1];
bio(284).sigWidth=1;
bio(284).sigAddress='&scoop_v3_B.DefaultValue3_o';
bio(284).ndims=2;
bio(284).size=[];


bio(285).blkName='MonitoringAndLogging /Log Received Data/AcknowledgeFlag';
bio(285).sigName='';
bio(285).portIdx=0;
bio(285).dim=[1,1];
bio(285).sigWidth=1;
bio(285).sigAddress='&scoop_v3_B.AcknowledgeFlag_b';
bio(285).ndims=2;
bio(285).size=[];


bio(286).blkName='MonitoringAndLogging /Log Received Data/BackwardID';
bio(286).sigName='';
bio(286).portIdx=0;
bio(286).dim=[1,1];
bio(286).sigWidth=1;
bio(286).sigAddress='&scoop_v3_B.BackwardID_e';
bio(286).ndims=2;
bio(286).size=[];


bio(287).blkName='MonitoringAndLogging /Log Received Data/CAM_GenerationTime';
bio(287).sigName='';
bio(287).portIdx=0;
bio(287).dim=[1,1];
bio(287).sigWidth=1;
bio(287).sigAddress='&scoop_v3_B.CAM_GenerationTime_e';
bio(287).ndims=2;
bio(287).size=[];


bio(288).blkName='MonitoringAndLogging /Log Received Data/CounterIntersection';
bio(288).sigName='';
bio(288).portIdx=0;
bio(288).dim=[1,1];
bio(288).sigWidth=1;
bio(288).sigAddress='&scoop_v3_B.CounterIntersection_c';
bio(288).ndims=2;
bio(288).size=[];


bio(289).blkName='MonitoringAndLogging /Log Received Data/CruiseSpeed';
bio(289).sigName='';
bio(289).portIdx=0;
bio(289).dim=[1,1];
bio(289).sigWidth=1;
bio(289).sigAddress='&scoop_v3_B.CruiseSpeed_d';
bio(289).ndims=2;
bio(289).size=[];


bio(290).blkName='MonitoringAndLogging /Log Received Data/DENM_GenerationTime';
bio(290).sigName='';
bio(290).portIdx=0;
bio(290).dim=[1,1];
bio(290).sigWidth=1;
bio(290).sigAddress='&scoop_v3_B.DENM_GenerationTime_k';
bio(290).ndims=2;
bio(290).size=[];


bio(291).blkName='MonitoringAndLogging /Log Received Data/DistanceTravelledCZ';
bio(291).sigName='';
bio(291).portIdx=0;
bio(291).dim=[1,1];
bio(291).sigWidth=1;
bio(291).sigAddress='&scoop_v3_B.DistanceTravelledCZ_d';
bio(291).ndims=2;
bio(291).size=[];


bio(292).blkName='MonitoringAndLogging /Log Received Data/EndOfScenario';
bio(292).sigName='';
bio(292).portIdx=0;
bio(292).dim=[1,1];
bio(292).sigWidth=1;
bio(292).sigAddress='&scoop_v3_B.EndOfScenario_p';
bio(292).ndims=2;
bio(292).size=[];


bio(293).blkName='MonitoringAndLogging /Log Received Data/EventType_CauseCode';
bio(293).sigName='';
bio(293).portIdx=0;
bio(293).dim=[1,1];
bio(293).sigWidth=1;
bio(293).sigAddress='&scoop_v3_B.EventType_CauseCode_n';
bio(293).ndims=2;
bio(293).size=[];


bio(294).blkName='MonitoringAndLogging /Log Received Data/ForwardID';
bio(294).sigName='';
bio(294).portIdx=0;
bio(294).dim=[1,1];
bio(294).sigWidth=1;
bio(294).sigAddress='&scoop_v3_B.ForwardID_g';
bio(294).ndims=2;
bio(294).size=[];


bio(295).blkName='MonitoringAndLogging /Log Received Data/Heading';
bio(295).sigName='';
bio(295).portIdx=0;
bio(295).dim=[1,1];
bio(295).sigWidth=1;
bio(295).sigAddress='&scoop_v3_B.Heading_c';
bio(295).ndims=2;
bio(295).size=[];


bio(296).blkName='MonitoringAndLogging /Log Received Data/Intention';
bio(296).sigName='';
bio(296).portIdx=0;
bio(296).dim=[1,1];
bio(296).sigWidth=1;
bio(296).sigAddress='&scoop_v3_B.Intention_o';
bio(296).ndims=2;
bio(296).size=[];


bio(297).blkName='MonitoringAndLogging /Log Received Data/Lane';
bio(297).sigName='';
bio(297).portIdx=0;
bio(297).dim=[1,1];
bio(297).sigWidth=1;
bio(297).sigAddress='&scoop_v3_B.Lane_o';
bio(297).ndims=2;
bio(297).size=[];


bio(298).blkName='MonitoringAndLogging /Log Received Data/Latitude';
bio(298).sigName='';
bio(298).portIdx=0;
bio(298).dim=[1,1];
bio(298).sigWidth=1;
bio(298).sigAddress='&scoop_v3_B.Latitude_e';
bio(298).ndims=2;
bio(298).size=[];


bio(299).blkName='MonitoringAndLogging /Log Received Data/Longitude';
bio(299).sigName='';
bio(299).portIdx=0;
bio(299).dim=[1,1];
bio(299).sigWidth=1;
bio(299).sigAddress='&scoop_v3_B.Longitude_o';
bio(299).ndims=2;
bio(299).size=[];


bio(300).blkName='MonitoringAndLogging /Log Received Data/LongitudinalAcceleration';
bio(300).sigName='';
bio(300).portIdx=0;
bio(300).dim=[1,1];
bio(300).sigWidth=1;
bio(300).sigAddress='&scoop_v3_B.LongitudinalAcceleration_a';
bio(300).ndims=2;
bio(300).size=[];


bio(301).blkName='MonitoringAndLogging /Log Received Data/MergeFlag';
bio(301).sigName='';
bio(301).portIdx=0;
bio(301).dim=[1,1];
bio(301).sigWidth=1;
bio(301).sigAddress='&scoop_v3_B.MergeFlag_a';
bio(301).ndims=2;
bio(301).size=[];


bio(302).blkName='MonitoringAndLogging /Log Received Data/MergeFlagHead';
bio(302).sigName='';
bio(302).portIdx=0;
bio(302).dim=[1,1];
bio(302).sigWidth=1;
bio(302).sigAddress='&scoop_v3_B.MergeFlagHead_b';
bio(302).ndims=2;
bio(302).size=[];


bio(303).blkName='MonitoringAndLogging /Log Received Data/MergeFlagTail';
bio(303).sigName='';
bio(303).portIdx=0;
bio(303).dim=[1,1];
bio(303).sigWidth=1;
bio(303).sigAddress='&scoop_v3_B.MergeFlagTail_c';
bio(303).ndims=2;
bio(303).size=[];


bio(304).blkName='MonitoringAndLogging /Log Received Data/MergeRequest';
bio(304).sigName='';
bio(304).portIdx=0;
bio(304).dim=[1,1];
bio(304).sigWidth=1;
bio(304).sigAddress='&scoop_v3_B.MergeRequest_b';
bio(304).ndims=2;
bio(304).size=[];


bio(305).blkName='MonitoringAndLogging /Log Received Data/MergeSafeToMerge';
bio(305).sigName='';
bio(305).portIdx=0;
bio(305).dim=[1,1];
bio(305).sigWidth=1;
bio(305).sigAddress='&scoop_v3_B.MergeSafeToMerge_j';
bio(305).ndims=2;
bio(305).size=[];


bio(306).blkName='MonitoringAndLogging /Log Received Data/MioBearing';
bio(306).sigName='';
bio(306).portIdx=0;
bio(306).dim=[1,1];
bio(306).sigWidth=1;
bio(306).sigAddress='&scoop_v3_B.MioBearing_c';
bio(306).ndims=2;
bio(306).size=[];


bio(307).blkName='MonitoringAndLogging /Log Received Data/MioID';
bio(307).sigName='';
bio(307).portIdx=0;
bio(307).dim=[1,1];
bio(307).sigWidth=1;
bio(307).sigAddress='&scoop_v3_B.MioID_n';
bio(307).ndims=2;
bio(307).size=[];


bio(308).blkName='MonitoringAndLogging /Log Received Data/MioRange';
bio(308).sigName='';
bio(308).portIdx=0;
bio(308).dim=[1,1];
bio(308).sigWidth=1;
bio(308).sigAddress='&scoop_v3_B.MioRange_a';
bio(308).ndims=2;
bio(308).size=[];


bio(309).blkName='MonitoringAndLogging /Log Received Data/MioRangeRate';
bio(309).sigName='';
bio(309).portIdx=0;
bio(309).dim=[1,1];
bio(309).sigWidth=1;
bio(309).sigAddress='&scoop_v3_B.MioRangeRate_d';
bio(309).ndims=2;
bio(309).size=[];


bio(310).blkName='MonitoringAndLogging /Log Received Data/PlatoonID';
bio(310).sigName='';
bio(310).portIdx=0;
bio(310).dim=[1,1];
bio(310).sigWidth=1;
bio(310).sigAddress='&scoop_v3_B.PlatoonID_c';
bio(310).ndims=2;
bio(310).size=[];


bio(311).blkName='MonitoringAndLogging /Log Received Data/ReferenceTime';
bio(311).sigName='';
bio(311).portIdx=0;
bio(311).dim=[1,1];
bio(311).sigWidth=1;
bio(311).sigAddress='&scoop_v3_B.ReferenceTime_e';
bio(311).ndims=2;
bio(311).size=[];


bio(312).blkName='MonitoringAndLogging /Log Received Data/Speed';
bio(312).sigName='';
bio(312).portIdx=0;
bio(312).dim=[1,1];
bio(312).sigWidth=1;
bio(312).sigAddress='&scoop_v3_B.Speed_i';
bio(312).ndims=2;
bio(312).size=[];


bio(313).blkName='MonitoringAndLogging /Log Received Data/StartPlatoon';
bio(313).sigName='';
bio(313).portIdx=0;
bio(313).dim=[1,1];
bio(313).sigWidth=1;
bio(313).sigAddress='&scoop_v3_B.StartPlatoon_h';
bio(313).ndims=2;
bio(313).size=[];


bio(314).blkName='MonitoringAndLogging /Log Received Data/TimeHeadway';
bio(314).sigName='';
bio(314).portIdx=0;
bio(314).dim=[1,1];
bio(314).sigWidth=1;
bio(314).sigAddress='&scoop_v3_B.TimeHeadway_l';
bio(314).ndims=2;
bio(314).size=[];


bio(315).blkName='MonitoringAndLogging /Log Received Data/VehicleLength';
bio(315).sigName='';
bio(315).portIdx=0;
bio(315).dim=[1,1];
bio(315).sigWidth=1;
bio(315).sigAddress='&scoop_v3_B.VehicleLength_d';
bio(315).ndims=2;
bio(315).size=[];


bio(316).blkName='MonitoringAndLogging /Log Received Data/VehicleRearAxleLocation';
bio(316).sigName='';
bio(316).portIdx=0;
bio(316).dim=[1,1];
bio(316).sigWidth=1;
bio(316).sigAddress='&scoop_v3_B.VehicleRearAxleLocation_g';
bio(316).ndims=2;
bio(316).size=[];


bio(317).blkName='MonitoringAndLogging /Log Received Data/VehicleWidth';
bio(317).sigName='';
bio(317).portIdx=0;
bio(317).dim=[1,1];
bio(317).sigWidth=1;
bio(317).sigAddress='&scoop_v3_B.VehicleWidth_e';
bio(317).ndims=2;
bio(317).size=[];


bio(318).blkName='MonitoringAndLogging /Log Received Data/iCLCL_GenerationTime';
bio(318).sigName='';
bio(318).portIdx=0;
bio(318).dim=[1,1];
bio(318).sigWidth=1;
bio(318).sigAddress='&scoop_v3_B.iCLCL_GenerationTime_h';
bio(318).ndims=2;
bio(318).size=[];


bio(319).blkName='MonitoringAndLogging /Log Received Data/stationID';
bio(319).sigName='';
bio(319).portIdx=0;
bio(319).dim=[1,1];
bio(319).sigWidth=1;
bio(319).sigAddress='&scoop_v3_B.stationID_h';
bio(319).ndims=2;
bio(319).size=[];


bio(320).blkName='MonitoringAndLogging /Log Received Data/stationType';
bio(320).sigName='';
bio(320).portIdx=0;
bio(320).dim=[1,1];
bio(320).sigWidth=1;
bio(320).sigAddress='&scoop_v3_B.stationType_i';
bio(320).ndims=2;
bio(320).size=[];


bio(321).blkName='MonitoringAndLogging /Log Received Data/Logical Operator';
bio(321).sigName='';
bio(321).portIdx=0;
bio(321).dim=[1,1];
bio(321).sigWidth=1;
bio(321).sigAddress='&scoop_v3_B.LogicalOperator';
bio(321).ndims=2;
bio(321).size=[];


bio(322).blkName='MonitoringAndLogging /Log Received Data/AppendLonTimestamp1/p1';
bio(322).sigName='';
bio(322).portIdx=0;
bio(322).dim=[1,1];
bio(322).sigWidth=1;
bio(322).sigAddress='&scoop_v3_B.AppendLonTimestamp1_o1_a';
bio(322).ndims=2;
bio(322).size=[];


bio(323).blkName='MonitoringAndLogging /Log Received Data/AppendLonTimestamp1/p2';
bio(323).sigName='';
bio(323).portIdx=1;
bio(323).dim=[1,1];
bio(323).sigWidth=1;
bio(323).sigAddress='&scoop_v3_B.AppendLonTimestamp1_o2_i';
bio(323).ndims=2;
bio(323).size=[];


bio(324).blkName='MonitoringAndLogging /Log Received Data/AppendLonTimestamp2/p1';
bio(324).sigName='';
bio(324).portIdx=0;
bio(324).dim=[1,1];
bio(324).sigWidth=1;
bio(324).sigAddress='&scoop_v3_B.AppendLonTimestamp2_o1';
bio(324).ndims=2;
bio(324).size=[];


bio(325).blkName='MonitoringAndLogging /Log Received Data/AppendLonTimestamp2/p2';
bio(325).sigName='';
bio(325).portIdx=1;
bio(325).dim=[1,1];
bio(325).sigWidth=1;
bio(325).sigAddress='&scoop_v3_B.AppendLonTimestamp2_o2';
bio(325).ndims=2;
bio(325).size=[];


bio(326).blkName='MonitoringAndLogging /Log Received Data/AppendLonTimestamp5/p1';
bio(326).sigName='Latitude_data';
bio(326).portIdx=0;
bio(326).dim=[1,1];
bio(326).sigWidth=1;
bio(326).sigAddress='&scoop_v3_B.Latitude_data_o';
bio(326).ndims=2;
bio(326).size=[];


bio(327).blkName='MonitoringAndLogging /Log Received Data/AppendLonTimestamp5/p2';
bio(327).sigName='';
bio(327).portIdx=1;
bio(327).dim=[1,1];
bio(327).sigWidth=1;
bio(327).sigAddress='&scoop_v3_B.AppendLonTimestamp5_o2_c';
bio(327).ndims=2;
bio(327).size=[];


bio(328).blkName='MonitoringAndLogging /Log Received Data/Switch1';
bio(328).sigName='';
bio(328).portIdx=0;
bio(328).dim=[1,1];
bio(328).sigWidth=1;
bio(328).sigAddress='&scoop_v3_B.Switch1_a';
bio(328).ndims=2;
bio(328).size=[];


bio(329).blkName='MonitoringAndLogging /Log Received Data/Switch10';
bio(329).sigName='';
bio(329).portIdx=0;
bio(329).dim=[1,1];
bio(329).sigWidth=1;
bio(329).sigAddress='&scoop_v3_B.Switch10_o';
bio(329).ndims=2;
bio(329).size=[];


bio(330).blkName='MonitoringAndLogging /Log Received Data/Switch11';
bio(330).sigName='';
bio(330).portIdx=0;
bio(330).dim=[1,1];
bio(330).sigWidth=1;
bio(330).sigAddress='&scoop_v3_B.Switch11_b';
bio(330).ndims=2;
bio(330).size=[];


bio(331).blkName='MonitoringAndLogging /Log Received Data/Switch12';
bio(331).sigName='';
bio(331).portIdx=0;
bio(331).dim=[1,1];
bio(331).sigWidth=1;
bio(331).sigAddress='&scoop_v3_B.Switch12_h';
bio(331).ndims=2;
bio(331).size=[];


bio(332).blkName='MonitoringAndLogging /Log Received Data/Switch13';
bio(332).sigName='';
bio(332).portIdx=0;
bio(332).dim=[1,1];
bio(332).sigWidth=1;
bio(332).sigAddress='&scoop_v3_B.Switch13_g';
bio(332).ndims=2;
bio(332).size=[];


bio(333).blkName='MonitoringAndLogging /Log Received Data/Switch14';
bio(333).sigName='';
bio(333).portIdx=0;
bio(333).dim=[1,1];
bio(333).sigWidth=1;
bio(333).sigAddress='&scoop_v3_B.Switch14_i';
bio(333).ndims=2;
bio(333).size=[];


bio(334).blkName='MonitoringAndLogging /Log Received Data/Switch15';
bio(334).sigName='';
bio(334).portIdx=0;
bio(334).dim=[1,1];
bio(334).sigWidth=1;
bio(334).sigAddress='&scoop_v3_B.Switch15_j';
bio(334).ndims=2;
bio(334).size=[];


bio(335).blkName='MonitoringAndLogging /Log Received Data/Switch16';
bio(335).sigName='';
bio(335).portIdx=0;
bio(335).dim=[1,1];
bio(335).sigWidth=1;
bio(335).sigAddress='&scoop_v3_B.Switch16_d';
bio(335).ndims=2;
bio(335).size=[];


bio(336).blkName='MonitoringAndLogging /Log Received Data/Switch17';
bio(336).sigName='';
bio(336).portIdx=0;
bio(336).dim=[1,1];
bio(336).sigWidth=1;
bio(336).sigAddress='&scoop_v3_B.Switch17_f';
bio(336).ndims=2;
bio(336).size=[];


bio(337).blkName='MonitoringAndLogging /Log Received Data/Switch18';
bio(337).sigName='';
bio(337).portIdx=0;
bio(337).dim=[1,1];
bio(337).sigWidth=1;
bio(337).sigAddress='&scoop_v3_B.Switch18_i';
bio(337).ndims=2;
bio(337).size=[];


bio(338).blkName='MonitoringAndLogging /Log Received Data/Switch19';
bio(338).sigName='';
bio(338).portIdx=0;
bio(338).dim=[1,1];
bio(338).sigWidth=1;
bio(338).sigAddress='&scoop_v3_B.Switch19_j';
bio(338).ndims=2;
bio(338).size=[];


bio(339).blkName='MonitoringAndLogging /Log Received Data/Switch2';
bio(339).sigName='';
bio(339).portIdx=0;
bio(339).dim=[1,1];
bio(339).sigWidth=1;
bio(339).sigAddress='&scoop_v3_B.Switch2_d';
bio(339).ndims=2;
bio(339).size=[];


bio(340).blkName='MonitoringAndLogging /Log Received Data/Switch20';
bio(340).sigName='';
bio(340).portIdx=0;
bio(340).dim=[1,1];
bio(340).sigWidth=1;
bio(340).sigAddress='&scoop_v3_B.Switch20_e';
bio(340).ndims=2;
bio(340).size=[];


bio(341).blkName='MonitoringAndLogging /Log Received Data/Switch21';
bio(341).sigName='';
bio(341).portIdx=0;
bio(341).dim=[1,1];
bio(341).sigWidth=1;
bio(341).sigAddress='&scoop_v3_B.Switch21_p';
bio(341).ndims=2;
bio(341).size=[];


bio(342).blkName='MonitoringAndLogging /Log Received Data/Switch22';
bio(342).sigName='';
bio(342).portIdx=0;
bio(342).dim=[1,1];
bio(342).sigWidth=1;
bio(342).sigAddress='&scoop_v3_B.Switch22_a';
bio(342).ndims=2;
bio(342).size=[];


bio(343).blkName='MonitoringAndLogging /Log Received Data/Switch23';
bio(343).sigName='';
bio(343).portIdx=0;
bio(343).dim=[1,1];
bio(343).sigWidth=1;
bio(343).sigAddress='&scoop_v3_B.Switch23_k';
bio(343).ndims=2;
bio(343).size=[];


bio(344).blkName='MonitoringAndLogging /Log Received Data/Switch24';
bio(344).sigName='';
bio(344).portIdx=0;
bio(344).dim=[1,1];
bio(344).sigWidth=1;
bio(344).sigAddress='&scoop_v3_B.Switch24_c';
bio(344).ndims=2;
bio(344).size=[];


bio(345).blkName='MonitoringAndLogging /Log Received Data/Switch25';
bio(345).sigName='';
bio(345).portIdx=0;
bio(345).dim=[1,1];
bio(345).sigWidth=1;
bio(345).sigAddress='&scoop_v3_B.Switch25_p';
bio(345).ndims=2;
bio(345).size=[];


bio(346).blkName='MonitoringAndLogging /Log Received Data/Switch26';
bio(346).sigName='';
bio(346).portIdx=0;
bio(346).dim=[1,1];
bio(346).sigWidth=1;
bio(346).sigAddress='&scoop_v3_B.Switch26_m';
bio(346).ndims=2;
bio(346).size=[];


bio(347).blkName='MonitoringAndLogging /Log Received Data/Switch27';
bio(347).sigName='';
bio(347).portIdx=0;
bio(347).dim=[1,1];
bio(347).sigWidth=1;
bio(347).sigAddress='&scoop_v3_B.Switch27_k';
bio(347).ndims=2;
bio(347).size=[];


bio(348).blkName='MonitoringAndLogging /Log Received Data/Switch28';
bio(348).sigName='';
bio(348).portIdx=0;
bio(348).dim=[1,1];
bio(348).sigWidth=1;
bio(348).sigAddress='&scoop_v3_B.Switch28_a';
bio(348).ndims=2;
bio(348).size=[];


bio(349).blkName='MonitoringAndLogging /Log Received Data/Switch29';
bio(349).sigName='';
bio(349).portIdx=0;
bio(349).dim=[1,1];
bio(349).sigWidth=1;
bio(349).sigAddress='&scoop_v3_B.Switch29_k';
bio(349).ndims=2;
bio(349).size=[];


bio(350).blkName='MonitoringAndLogging /Log Received Data/Switch3';
bio(350).sigName='';
bio(350).portIdx=0;
bio(350).dim=[1,1];
bio(350).sigWidth=1;
bio(350).sigAddress='&scoop_v3_B.Switch3_c';
bio(350).ndims=2;
bio(350).size=[];


bio(351).blkName='MonitoringAndLogging /Log Received Data/Switch30';
bio(351).sigName='';
bio(351).portIdx=0;
bio(351).dim=[1,1];
bio(351).sigWidth=1;
bio(351).sigAddress='&scoop_v3_B.Switch30_j';
bio(351).ndims=2;
bio(351).size=[];


bio(352).blkName='MonitoringAndLogging /Log Received Data/Switch31';
bio(352).sigName='';
bio(352).portIdx=0;
bio(352).dim=[1,1];
bio(352).sigWidth=1;
bio(352).sigAddress='&scoop_v3_B.Switch31_k';
bio(352).ndims=2;
bio(352).size=[];


bio(353).blkName='MonitoringAndLogging /Log Received Data/Switch32';
bio(353).sigName='';
bio(353).portIdx=0;
bio(353).dim=[1,1];
bio(353).sigWidth=1;
bio(353).sigAddress='&scoop_v3_B.Switch32_l';
bio(353).ndims=2;
bio(353).size=[];


bio(354).blkName='MonitoringAndLogging /Log Received Data/Switch33';
bio(354).sigName='';
bio(354).portIdx=0;
bio(354).dim=[1,1];
bio(354).sigWidth=1;
bio(354).sigAddress='&scoop_v3_B.Switch33_b';
bio(354).ndims=2;
bio(354).size=[];


bio(355).blkName='MonitoringAndLogging /Log Received Data/Switch34';
bio(355).sigName='';
bio(355).portIdx=0;
bio(355).dim=[1,1];
bio(355).sigWidth=1;
bio(355).sigAddress='&scoop_v3_B.Switch34';
bio(355).ndims=2;
bio(355).size=[];


bio(356).blkName='MonitoringAndLogging /Log Received Data/Switch35';
bio(356).sigName='';
bio(356).portIdx=0;
bio(356).dim=[1,1];
bio(356).sigWidth=1;
bio(356).sigAddress='&scoop_v3_B.Switch35';
bio(356).ndims=2;
bio(356).size=[];


bio(357).blkName='MonitoringAndLogging /Log Received Data/Switch36';
bio(357).sigName='';
bio(357).portIdx=0;
bio(357).dim=[1,1];
bio(357).sigWidth=1;
bio(357).sigAddress='&scoop_v3_B.Switch36';
bio(357).ndims=2;
bio(357).size=[];


bio(358).blkName='MonitoringAndLogging /Log Received Data/Switch4';
bio(358).sigName='';
bio(358).portIdx=0;
bio(358).dim=[1,1];
bio(358).sigWidth=1;
bio(358).sigAddress='&scoop_v3_B.Switch4_h';
bio(358).ndims=2;
bio(358).size=[];


bio(359).blkName='MonitoringAndLogging /Log Received Data/Switch5';
bio(359).sigName='';
bio(359).portIdx=0;
bio(359).dim=[1,1];
bio(359).sigWidth=1;
bio(359).sigAddress='&scoop_v3_B.Switch5_a';
bio(359).ndims=2;
bio(359).size=[];


bio(360).blkName='MonitoringAndLogging /Log Received Data/Switch6';
bio(360).sigName='';
bio(360).portIdx=0;
bio(360).dim=[1,1];
bio(360).sigWidth=1;
bio(360).sigAddress='&scoop_v3_B.Switch6_p';
bio(360).ndims=2;
bio(360).size=[];


bio(361).blkName='MonitoringAndLogging /Log Received Data/Switch7';
bio(361).sigName='';
bio(361).portIdx=0;
bio(361).dim=[1,1];
bio(361).sigWidth=1;
bio(361).sigAddress='&scoop_v3_B.Switch7_j';
bio(361).ndims=2;
bio(361).size=[];


bio(362).blkName='MonitoringAndLogging /Log Received Data/Switch8';
bio(362).sigName='';
bio(362).portIdx=0;
bio(362).dim=[1,1];
bio(362).sigWidth=1;
bio(362).sigAddress='&scoop_v3_B.Switch8_c';
bio(362).ndims=2;
bio(362).size=[];


bio(363).blkName='MonitoringAndLogging /Log Received Data/Switch9';
bio(363).sigName='';
bio(363).portIdx=0;
bio(363).dim=[1,1];
bio(363).sigWidth=1;
bio(363).sigAddress='&scoop_v3_B.Switch9_f';
bio(363).ndims=2;
bio(363).size=[];


bio(364).blkName='MonitoringAndLogging /Log Transmitted Data/DefaultValue2';
bio(364).sigName='Latitude_data';
bio(364).portIdx=0;
bio(364).dim=[1,1];
bio(364).sigWidth=1;
bio(364).sigAddress='&scoop_v3_B.Latitude_data_m';
bio(364).ndims=2;
bio(364).size=[];


bio(365).blkName='MonitoringAndLogging /Log Transmitted Data/DefaultValue3';
bio(365).sigName='';
bio(365).portIdx=0;
bio(365).dim=[1,1];
bio(365).sigWidth=1;
bio(365).sigAddress='&scoop_v3_B.DefaultValue3';
bio(365).ndims=2;
bio(365).size=[];


bio(366).blkName='MonitoringAndLogging /Log Transmitted Data/Pulse Generator';
bio(366).sigName='';
bio(366).portIdx=0;
bio(366).dim=[1,1];
bio(366).sigWidth=1;
bio(366).sigAddress='&scoop_v3_B.PulseGenerator';
bio(366).ndims=2;
bio(366).size=[];


bio(367).blkName='MonitoringAndLogging /Log Transmitted Data/AcknowledgeFlag';
bio(367).sigName='';
bio(367).portIdx=0;
bio(367).dim=[1,1];
bio(367).sigWidth=1;
bio(367).sigAddress='&scoop_v3_B.AcknowledgeFlag';
bio(367).ndims=2;
bio(367).size=[];


bio(368).blkName='MonitoringAndLogging /Log Transmitted Data/BackwardID';
bio(368).sigName='';
bio(368).portIdx=0;
bio(368).dim=[1,1];
bio(368).sigWidth=1;
bio(368).sigAddress='&scoop_v3_B.BackwardID';
bio(368).ndims=2;
bio(368).size=[];


bio(369).blkName='MonitoringAndLogging /Log Transmitted Data/CAM_GenerationTime';
bio(369).sigName='';
bio(369).portIdx=0;
bio(369).dim=[1,1];
bio(369).sigWidth=1;
bio(369).sigAddress='&scoop_v3_B.CAM_GenerationTime';
bio(369).ndims=2;
bio(369).size=[];


bio(370).blkName='MonitoringAndLogging /Log Transmitted Data/CounterIntersection';
bio(370).sigName='';
bio(370).portIdx=0;
bio(370).dim=[1,1];
bio(370).sigWidth=1;
bio(370).sigAddress='&scoop_v3_B.CounterIntersection';
bio(370).ndims=2;
bio(370).size=[];


bio(371).blkName='MonitoringAndLogging /Log Transmitted Data/CruiseSpeed';
bio(371).sigName='';
bio(371).portIdx=0;
bio(371).dim=[1,1];
bio(371).sigWidth=1;
bio(371).sigAddress='&scoop_v3_B.CruiseSpeed';
bio(371).ndims=2;
bio(371).size=[];


bio(372).blkName='MonitoringAndLogging /Log Transmitted Data/DENM_GenerationTime';
bio(372).sigName='';
bio(372).portIdx=0;
bio(372).dim=[1,1];
bio(372).sigWidth=1;
bio(372).sigAddress='&scoop_v3_B.DENM_GenerationTime';
bio(372).ndims=2;
bio(372).size=[];


bio(373).blkName='MonitoringAndLogging /Log Transmitted Data/DistanceTravelledCZ';
bio(373).sigName='';
bio(373).portIdx=0;
bio(373).dim=[1,1];
bio(373).sigWidth=1;
bio(373).sigAddress='&scoop_v3_B.DistanceTravelledCZ';
bio(373).ndims=2;
bio(373).size=[];


bio(374).blkName='MonitoringAndLogging /Log Transmitted Data/EndOfScenario';
bio(374).sigName='';
bio(374).portIdx=0;
bio(374).dim=[1,1];
bio(374).sigWidth=1;
bio(374).sigAddress='&scoop_v3_B.EndOfScenario';
bio(374).ndims=2;
bio(374).size=[];


bio(375).blkName='MonitoringAndLogging /Log Transmitted Data/EventType_CauseCode';
bio(375).sigName='';
bio(375).portIdx=0;
bio(375).dim=[1,1];
bio(375).sigWidth=1;
bio(375).sigAddress='&scoop_v3_B.EventType_CauseCode';
bio(375).ndims=2;
bio(375).size=[];


bio(376).blkName='MonitoringAndLogging /Log Transmitted Data/ForwardID';
bio(376).sigName='';
bio(376).portIdx=0;
bio(376).dim=[1,1];
bio(376).sigWidth=1;
bio(376).sigAddress='&scoop_v3_B.ForwardID';
bio(376).ndims=2;
bio(376).size=[];


bio(377).blkName='MonitoringAndLogging /Log Transmitted Data/Heading';
bio(377).sigName='';
bio(377).portIdx=0;
bio(377).dim=[1,1];
bio(377).sigWidth=1;
bio(377).sigAddress='&scoop_v3_B.Heading';
bio(377).ndims=2;
bio(377).size=[];


bio(378).blkName='MonitoringAndLogging /Log Transmitted Data/Intention';
bio(378).sigName='';
bio(378).portIdx=0;
bio(378).dim=[1,1];
bio(378).sigWidth=1;
bio(378).sigAddress='&scoop_v3_B.Intention';
bio(378).ndims=2;
bio(378).size=[];


bio(379).blkName='MonitoringAndLogging /Log Transmitted Data/Lane';
bio(379).sigName='';
bio(379).portIdx=0;
bio(379).dim=[1,1];
bio(379).sigWidth=1;
bio(379).sigAddress='&scoop_v3_B.Lane';
bio(379).ndims=2;
bio(379).size=[];


bio(380).blkName='MonitoringAndLogging /Log Transmitted Data/Latitude';
bio(380).sigName='';
bio(380).portIdx=0;
bio(380).dim=[1,1];
bio(380).sigWidth=1;
bio(380).sigAddress='&scoop_v3_B.Latitude_a';
bio(380).ndims=2;
bio(380).size=[];


bio(381).blkName='MonitoringAndLogging /Log Transmitted Data/Longitude';
bio(381).sigName='';
bio(381).portIdx=0;
bio(381).dim=[1,1];
bio(381).sigWidth=1;
bio(381).sigAddress='&scoop_v3_B.Longitude_c';
bio(381).ndims=2;
bio(381).size=[];


bio(382).blkName='MonitoringAndLogging /Log Transmitted Data/LongitudinalAcceleration';
bio(382).sigName='';
bio(382).portIdx=0;
bio(382).dim=[1,1];
bio(382).sigWidth=1;
bio(382).sigAddress='&scoop_v3_B.LongitudinalAcceleration';
bio(382).ndims=2;
bio(382).size=[];


bio(383).blkName='MonitoringAndLogging /Log Transmitted Data/MergeFlag';
bio(383).sigName='';
bio(383).portIdx=0;
bio(383).dim=[1,1];
bio(383).sigWidth=1;
bio(383).sigAddress='&scoop_v3_B.MergeFlag';
bio(383).ndims=2;
bio(383).size=[];


bio(384).blkName='MonitoringAndLogging /Log Transmitted Data/MergeFlagHead';
bio(384).sigName='';
bio(384).portIdx=0;
bio(384).dim=[1,1];
bio(384).sigWidth=1;
bio(384).sigAddress='&scoop_v3_B.MergeFlagHead';
bio(384).ndims=2;
bio(384).size=[];


bio(385).blkName='MonitoringAndLogging /Log Transmitted Data/MergeFlagTail';
bio(385).sigName='';
bio(385).portIdx=0;
bio(385).dim=[1,1];
bio(385).sigWidth=1;
bio(385).sigAddress='&scoop_v3_B.MergeFlagTail';
bio(385).ndims=2;
bio(385).size=[];


bio(386).blkName='MonitoringAndLogging /Log Transmitted Data/MergeRequest';
bio(386).sigName='';
bio(386).portIdx=0;
bio(386).dim=[1,1];
bio(386).sigWidth=1;
bio(386).sigAddress='&scoop_v3_B.MergeRequest';
bio(386).ndims=2;
bio(386).size=[];


bio(387).blkName='MonitoringAndLogging /Log Transmitted Data/MergeSafeToMerge';
bio(387).sigName='';
bio(387).portIdx=0;
bio(387).dim=[1,1];
bio(387).sigWidth=1;
bio(387).sigAddress='&scoop_v3_B.MergeSafeToMerge';
bio(387).ndims=2;
bio(387).size=[];


bio(388).blkName='MonitoringAndLogging /Log Transmitted Data/MioBearing';
bio(388).sigName='';
bio(388).portIdx=0;
bio(388).dim=[1,1];
bio(388).sigWidth=1;
bio(388).sigAddress='&scoop_v3_B.MioBearing';
bio(388).ndims=2;
bio(388).size=[];


bio(389).blkName='MonitoringAndLogging /Log Transmitted Data/MioID';
bio(389).sigName='';
bio(389).portIdx=0;
bio(389).dim=[1,1];
bio(389).sigWidth=1;
bio(389).sigAddress='&scoop_v3_B.MioID';
bio(389).ndims=2;
bio(389).size=[];


bio(390).blkName='MonitoringAndLogging /Log Transmitted Data/MioRange';
bio(390).sigName='';
bio(390).portIdx=0;
bio(390).dim=[1,1];
bio(390).sigWidth=1;
bio(390).sigAddress='&scoop_v3_B.MioRange';
bio(390).ndims=2;
bio(390).size=[];


bio(391).blkName='MonitoringAndLogging /Log Transmitted Data/MioRangeRate';
bio(391).sigName='';
bio(391).portIdx=0;
bio(391).dim=[1,1];
bio(391).sigWidth=1;
bio(391).sigAddress='&scoop_v3_B.MioRangeRate';
bio(391).ndims=2;
bio(391).size=[];


bio(392).blkName='MonitoringAndLogging /Log Transmitted Data/PlatoonID';
bio(392).sigName='';
bio(392).portIdx=0;
bio(392).dim=[1,1];
bio(392).sigWidth=1;
bio(392).sigAddress='&scoop_v3_B.PlatoonID';
bio(392).ndims=2;
bio(392).size=[];


bio(393).blkName='MonitoringAndLogging /Log Transmitted Data/ReferenceTime';
bio(393).sigName='';
bio(393).portIdx=0;
bio(393).dim=[1,1];
bio(393).sigWidth=1;
bio(393).sigAddress='&scoop_v3_B.ReferenceTime';
bio(393).ndims=2;
bio(393).size=[];


bio(394).blkName='MonitoringAndLogging /Log Transmitted Data/Speed';
bio(394).sigName='';
bio(394).portIdx=0;
bio(394).dim=[1,1];
bio(394).sigWidth=1;
bio(394).sigAddress='&scoop_v3_B.Speed';
bio(394).ndims=2;
bio(394).size=[];


bio(395).blkName='MonitoringAndLogging /Log Transmitted Data/StartPlatoon';
bio(395).sigName='';
bio(395).portIdx=0;
bio(395).dim=[1,1];
bio(395).sigWidth=1;
bio(395).sigAddress='&scoop_v3_B.StartPlatoon';
bio(395).ndims=2;
bio(395).size=[];


bio(396).blkName='MonitoringAndLogging /Log Transmitted Data/TimeHeadway';
bio(396).sigName='';
bio(396).portIdx=0;
bio(396).dim=[1,1];
bio(396).sigWidth=1;
bio(396).sigAddress='&scoop_v3_B.TimeHeadway';
bio(396).ndims=2;
bio(396).size=[];


bio(397).blkName='MonitoringAndLogging /Log Transmitted Data/VehicleLength';
bio(397).sigName='';
bio(397).portIdx=0;
bio(397).dim=[1,1];
bio(397).sigWidth=1;
bio(397).sigAddress='&scoop_v3_B.VehicleLength';
bio(397).ndims=2;
bio(397).size=[];


bio(398).blkName='MonitoringAndLogging /Log Transmitted Data/VehicleRearAxleLocation';
bio(398).sigName='';
bio(398).portIdx=0;
bio(398).dim=[1,1];
bio(398).sigWidth=1;
bio(398).sigAddress='&scoop_v3_B.VehicleRearAxleLocation';
bio(398).ndims=2;
bio(398).size=[];


bio(399).blkName='MonitoringAndLogging /Log Transmitted Data/VehicleWidth';
bio(399).sigName='';
bio(399).portIdx=0;
bio(399).dim=[1,1];
bio(399).sigWidth=1;
bio(399).sigAddress='&scoop_v3_B.VehicleWidth';
bio(399).ndims=2;
bio(399).size=[];


bio(400).blkName='MonitoringAndLogging /Log Transmitted Data/iCLCL_GenerationTime';
bio(400).sigName='';
bio(400).portIdx=0;
bio(400).dim=[1,1];
bio(400).sigWidth=1;
bio(400).sigAddress='&scoop_v3_B.iCLCL_GenerationTime';
bio(400).ndims=2;
bio(400).size=[];


bio(401).blkName='MonitoringAndLogging /Log Transmitted Data/stationID';
bio(401).sigName='';
bio(401).portIdx=0;
bio(401).dim=[1,1];
bio(401).sigWidth=1;
bio(401).sigAddress='&scoop_v3_B.stationID';
bio(401).ndims=2;
bio(401).size=[];


bio(402).blkName='MonitoringAndLogging /Log Transmitted Data/stationType';
bio(402).sigName='';
bio(402).portIdx=0;
bio(402).dim=[1,1];
bio(402).sigWidth=1;
bio(402).sigAddress='&scoop_v3_B.stationType';
bio(402).ndims=2;
bio(402).size=[];


bio(403).blkName='MonitoringAndLogging /Log Transmitted Data/AppendLonTimestamp1/p1';
bio(403).sigName='';
bio(403).portIdx=0;
bio(403).dim=[1,1];
bio(403).sigWidth=1;
bio(403).sigAddress='&scoop_v3_B.AppendLonTimestamp1_o1';
bio(403).ndims=2;
bio(403).size=[];


bio(404).blkName='MonitoringAndLogging /Log Transmitted Data/AppendLonTimestamp1/p2';
bio(404).sigName='';
bio(404).portIdx=1;
bio(404).dim=[1,1];
bio(404).sigWidth=1;
bio(404).sigAddress='&scoop_v3_B.AppendLonTimestamp1_o2';
bio(404).ndims=2;
bio(404).size=[];


bio(405).blkName='MonitoringAndLogging /Log Transmitted Data/AppendLonTimestamp5/p1';
bio(405).sigName='Latitude_data';
bio(405).portIdx=0;
bio(405).dim=[1,1];
bio(405).sigWidth=1;
bio(405).sigAddress='&scoop_v3_B.Latitude_data_k';
bio(405).ndims=2;
bio(405).size=[];


bio(406).blkName='MonitoringAndLogging /Log Transmitted Data/AppendLonTimestamp5/p2';
bio(406).sigName='';
bio(406).portIdx=1;
bio(406).dim=[1,1];
bio(406).sigWidth=1;
bio(406).sigAddress='&scoop_v3_B.AppendLonTimestamp5_o2';
bio(406).ndims=2;
bio(406).size=[];


bio(407).blkName='MonitoringAndLogging /Log Transmitted Data/Switch1';
bio(407).sigName='';
bio(407).portIdx=0;
bio(407).dim=[1,1];
bio(407).sigWidth=1;
bio(407).sigAddress='&scoop_v3_B.Switch1';
bio(407).ndims=2;
bio(407).size=[];


bio(408).blkName='MonitoringAndLogging /Log Transmitted Data/Switch10';
bio(408).sigName='';
bio(408).portIdx=0;
bio(408).dim=[1,1];
bio(408).sigWidth=1;
bio(408).sigAddress='&scoop_v3_B.Switch10';
bio(408).ndims=2;
bio(408).size=[];


bio(409).blkName='MonitoringAndLogging /Log Transmitted Data/Switch11';
bio(409).sigName='';
bio(409).portIdx=0;
bio(409).dim=[1,1];
bio(409).sigWidth=1;
bio(409).sigAddress='&scoop_v3_B.Switch11';
bio(409).ndims=2;
bio(409).size=[];


bio(410).blkName='MonitoringAndLogging /Log Transmitted Data/Switch12';
bio(410).sigName='';
bio(410).portIdx=0;
bio(410).dim=[1,1];
bio(410).sigWidth=1;
bio(410).sigAddress='&scoop_v3_B.Switch12';
bio(410).ndims=2;
bio(410).size=[];


bio(411).blkName='MonitoringAndLogging /Log Transmitted Data/Switch13';
bio(411).sigName='';
bio(411).portIdx=0;
bio(411).dim=[1,1];
bio(411).sigWidth=1;
bio(411).sigAddress='&scoop_v3_B.Switch13';
bio(411).ndims=2;
bio(411).size=[];


bio(412).blkName='MonitoringAndLogging /Log Transmitted Data/Switch14';
bio(412).sigName='';
bio(412).portIdx=0;
bio(412).dim=[1,1];
bio(412).sigWidth=1;
bio(412).sigAddress='&scoop_v3_B.Switch14';
bio(412).ndims=2;
bio(412).size=[];


bio(413).blkName='MonitoringAndLogging /Log Transmitted Data/Switch15';
bio(413).sigName='';
bio(413).portIdx=0;
bio(413).dim=[1,1];
bio(413).sigWidth=1;
bio(413).sigAddress='&scoop_v3_B.Switch15';
bio(413).ndims=2;
bio(413).size=[];


bio(414).blkName='MonitoringAndLogging /Log Transmitted Data/Switch16';
bio(414).sigName='';
bio(414).portIdx=0;
bio(414).dim=[1,1];
bio(414).sigWidth=1;
bio(414).sigAddress='&scoop_v3_B.Switch16';
bio(414).ndims=2;
bio(414).size=[];


bio(415).blkName='MonitoringAndLogging /Log Transmitted Data/Switch17';
bio(415).sigName='';
bio(415).portIdx=0;
bio(415).dim=[1,1];
bio(415).sigWidth=1;
bio(415).sigAddress='&scoop_v3_B.Switch17';
bio(415).ndims=2;
bio(415).size=[];


bio(416).blkName='MonitoringAndLogging /Log Transmitted Data/Switch18';
bio(416).sigName='';
bio(416).portIdx=0;
bio(416).dim=[1,1];
bio(416).sigWidth=1;
bio(416).sigAddress='&scoop_v3_B.Switch18';
bio(416).ndims=2;
bio(416).size=[];


bio(417).blkName='MonitoringAndLogging /Log Transmitted Data/Switch19';
bio(417).sigName='';
bio(417).portIdx=0;
bio(417).dim=[1,1];
bio(417).sigWidth=1;
bio(417).sigAddress='&scoop_v3_B.Switch19';
bio(417).ndims=2;
bio(417).size=[];


bio(418).blkName='MonitoringAndLogging /Log Transmitted Data/Switch2';
bio(418).sigName='';
bio(418).portIdx=0;
bio(418).dim=[1,1];
bio(418).sigWidth=1;
bio(418).sigAddress='&scoop_v3_B.Switch2';
bio(418).ndims=2;
bio(418).size=[];


bio(419).blkName='MonitoringAndLogging /Log Transmitted Data/Switch20';
bio(419).sigName='';
bio(419).portIdx=0;
bio(419).dim=[1,1];
bio(419).sigWidth=1;
bio(419).sigAddress='&scoop_v3_B.Switch20';
bio(419).ndims=2;
bio(419).size=[];


bio(420).blkName='MonitoringAndLogging /Log Transmitted Data/Switch21';
bio(420).sigName='';
bio(420).portIdx=0;
bio(420).dim=[1,1];
bio(420).sigWidth=1;
bio(420).sigAddress='&scoop_v3_B.Switch21';
bio(420).ndims=2;
bio(420).size=[];


bio(421).blkName='MonitoringAndLogging /Log Transmitted Data/Switch22';
bio(421).sigName='';
bio(421).portIdx=0;
bio(421).dim=[1,1];
bio(421).sigWidth=1;
bio(421).sigAddress='&scoop_v3_B.Switch22';
bio(421).ndims=2;
bio(421).size=[];


bio(422).blkName='MonitoringAndLogging /Log Transmitted Data/Switch23';
bio(422).sigName='';
bio(422).portIdx=0;
bio(422).dim=[1,1];
bio(422).sigWidth=1;
bio(422).sigAddress='&scoop_v3_B.Switch23';
bio(422).ndims=2;
bio(422).size=[];


bio(423).blkName='MonitoringAndLogging /Log Transmitted Data/Switch24';
bio(423).sigName='';
bio(423).portIdx=0;
bio(423).dim=[1,1];
bio(423).sigWidth=1;
bio(423).sigAddress='&scoop_v3_B.Switch24';
bio(423).ndims=2;
bio(423).size=[];


bio(424).blkName='MonitoringAndLogging /Log Transmitted Data/Switch25';
bio(424).sigName='';
bio(424).portIdx=0;
bio(424).dim=[1,1];
bio(424).sigWidth=1;
bio(424).sigAddress='&scoop_v3_B.Switch25';
bio(424).ndims=2;
bio(424).size=[];


bio(425).blkName='MonitoringAndLogging /Log Transmitted Data/Switch26';
bio(425).sigName='';
bio(425).portIdx=0;
bio(425).dim=[1,1];
bio(425).sigWidth=1;
bio(425).sigAddress='&scoop_v3_B.Switch26';
bio(425).ndims=2;
bio(425).size=[];


bio(426).blkName='MonitoringAndLogging /Log Transmitted Data/Switch27';
bio(426).sigName='';
bio(426).portIdx=0;
bio(426).dim=[1,1];
bio(426).sigWidth=1;
bio(426).sigAddress='&scoop_v3_B.Switch27';
bio(426).ndims=2;
bio(426).size=[];


bio(427).blkName='MonitoringAndLogging /Log Transmitted Data/Switch28';
bio(427).sigName='';
bio(427).portIdx=0;
bio(427).dim=[1,1];
bio(427).sigWidth=1;
bio(427).sigAddress='&scoop_v3_B.Switch28';
bio(427).ndims=2;
bio(427).size=[];


bio(428).blkName='MonitoringAndLogging /Log Transmitted Data/Switch29';
bio(428).sigName='';
bio(428).portIdx=0;
bio(428).dim=[1,1];
bio(428).sigWidth=1;
bio(428).sigAddress='&scoop_v3_B.Switch29';
bio(428).ndims=2;
bio(428).size=[];


bio(429).blkName='MonitoringAndLogging /Log Transmitted Data/Switch3';
bio(429).sigName='';
bio(429).portIdx=0;
bio(429).dim=[1,1];
bio(429).sigWidth=1;
bio(429).sigAddress='&scoop_v3_B.Switch3_d';
bio(429).ndims=2;
bio(429).size=[];


bio(430).blkName='MonitoringAndLogging /Log Transmitted Data/Switch30';
bio(430).sigName='';
bio(430).portIdx=0;
bio(430).dim=[1,1];
bio(430).sigWidth=1;
bio(430).sigAddress='&scoop_v3_B.Switch30';
bio(430).ndims=2;
bio(430).size=[];


bio(431).blkName='MonitoringAndLogging /Log Transmitted Data/Switch31';
bio(431).sigName='';
bio(431).portIdx=0;
bio(431).dim=[1,1];
bio(431).sigWidth=1;
bio(431).sigAddress='&scoop_v3_B.Switch31';
bio(431).ndims=2;
bio(431).size=[];


bio(432).blkName='MonitoringAndLogging /Log Transmitted Data/Switch32';
bio(432).sigName='';
bio(432).portIdx=0;
bio(432).dim=[1,1];
bio(432).sigWidth=1;
bio(432).sigAddress='&scoop_v3_B.Switch32';
bio(432).ndims=2;
bio(432).size=[];


bio(433).blkName='MonitoringAndLogging /Log Transmitted Data/Switch33';
bio(433).sigName='';
bio(433).portIdx=0;
bio(433).dim=[1,1];
bio(433).sigWidth=1;
bio(433).sigAddress='&scoop_v3_B.Switch33';
bio(433).ndims=2;
bio(433).size=[];


bio(434).blkName='MonitoringAndLogging /Log Transmitted Data/Switch4';
bio(434).sigName='';
bio(434).portIdx=0;
bio(434).dim=[1,1];
bio(434).sigWidth=1;
bio(434).sigAddress='&scoop_v3_B.Switch4';
bio(434).ndims=2;
bio(434).size=[];


bio(435).blkName='MonitoringAndLogging /Log Transmitted Data/Switch5';
bio(435).sigName='';
bio(435).portIdx=0;
bio(435).dim=[1,1];
bio(435).sigWidth=1;
bio(435).sigAddress='&scoop_v3_B.Switch5';
bio(435).ndims=2;
bio(435).size=[];


bio(436).blkName='MonitoringAndLogging /Log Transmitted Data/Switch6';
bio(436).sigName='';
bio(436).portIdx=0;
bio(436).dim=[1,1];
bio(436).sigWidth=1;
bio(436).sigAddress='&scoop_v3_B.Switch6';
bio(436).ndims=2;
bio(436).size=[];


bio(437).blkName='MonitoringAndLogging /Log Transmitted Data/Switch7';
bio(437).sigName='';
bio(437).portIdx=0;
bio(437).dim=[1,1];
bio(437).sigWidth=1;
bio(437).sigAddress='&scoop_v3_B.Switch7';
bio(437).ndims=2;
bio(437).size=[];


bio(438).blkName='MonitoringAndLogging /Log Transmitted Data/Switch8';
bio(438).sigName='';
bio(438).portIdx=0;
bio(438).dim=[1,1];
bio(438).sigWidth=1;
bio(438).sigAddress='&scoop_v3_B.Switch8';
bio(438).ndims=2;
bio(438).size=[];


bio(439).blkName='MonitoringAndLogging /Log Transmitted Data/Switch9';
bio(439).sigName='';
bio(439).portIdx=0;
bio(439).dim=[1,1];
bio(439).sigWidth=1;
bio(439).sigAddress='&scoop_v3_B.Switch9';
bio(439).ndims=2;
bio(439).size=[];


bio(440).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition1';
bio(440).sigName='lon';
bio(440).portIdx=0;
bio(440).dim=[2,1];
bio(440).sigWidth=2;
bio(440).sigAddress='&scoop_v3_B.lon[0]';
bio(440).ndims=2;
bio(440).size=[];


bio(441).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition10';
bio(441).sigName='nrOfSats';
bio(441).portIdx=0;
bio(441).dim=[1,1];
bio(441).sigWidth=1;
bio(441).sigAddress='&scoop_v3_B.nrOfSats';
bio(441).ndims=2;
bio(441).size=[];


bio(442).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition2';
bio(442).sigName='alt';
bio(442).portIdx=0;
bio(442).dim=[2,1];
bio(442).sigWidth=2;
bio(442).sigAddress='&scoop_v3_B.alt[0]';
bio(442).ndims=2;
bio(442).size=[];


bio(443).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition3';
bio(443).sigName='hdng';
bio(443).portIdx=0;
bio(443).dim=[2,1];
bio(443).sigWidth=2;
bio(443).sigAddress='&scoop_v3_B.hdng[0]';
bio(443).ndims=2;
bio(443).size=[];


bio(444).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition4';
bio(444).sigName='GPSLongVel';
bio(444).portIdx=0;
bio(444).dim=[2,1];
bio(444).sigWidth=2;
bio(444).sigAddress='&scoop_v3_B.GPSLongVel[0]';
bio(444).ndims=2;
bio(444).size=[];


bio(445).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition5';
bio(445).sigName='GPSStatus';
bio(445).portIdx=0;
bio(445).dim=[2,1];
bio(445).sigWidth=2;
bio(445).sigAddress='&scoop_v3_B.GPSStatus[0]';
bio(445).ndims=2;
bio(445).size=[];


bio(446).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition6';
bio(446).sigName='UTCtime_hhmmsscc';
bio(446).portIdx=0;
bio(446).dim=[1,1];
bio(446).sigWidth=1;
bio(446).sigAddress='&scoop_v3_B.UTCtime_hhmmsscc';
bio(446).ndims=2;
bio(446).size=[];


bio(447).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition7';
bio(447).sigName='lat';
bio(447).portIdx=0;
bio(447).dim=[2,1];
bio(447).sigWidth=2;
bio(447).sigAddress='&scoop_v3_B.lat[0]';
bio(447).ndims=2;
bio(447).size=[];


bio(448).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition8';
bio(448).sigName='UTCtime_ddmmyy';
bio(448).portIdx=0;
bio(448).dim=[1,1];
bio(448).sigWidth=1;
bio(448).sigAddress='&scoop_v3_B.UTCtime_ddmmyy';
bio(448).ndims=2;
bio(448).size=[];


bio(449).blkName='Serial IN (Trimble)/Append Timestamp to GPS/Rate Transition9';
bio(449).sigName='GPSQuality';
bio(449).portIdx=0;
bio(449).dim=[1,1];
bio(449).sigWidth=1;
bio(449).sigAddress='&scoop_v3_B.GPSQuality';
bio(449).ndims=2;
bio(449).size=[];


bio(450).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp/p1';
bio(450).sigName='Longitude_data';
bio(450).portIdx=0;
bio(450).dim=[1,1];
bio(450).sigWidth=1;
bio(450).sigAddress='&scoop_v3_B.Longitude_data';
bio(450).ndims=2;
bio(450).size=[];


bio(451).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp/p2';
bio(451).sigName='Longitude_time';
bio(451).portIdx=1;
bio(451).dim=[1,1];
bio(451).sigWidth=1;
bio(451).sigAddress='&scoop_v3_B.Longitude_time';
bio(451).ndims=2;
bio(451).size=[];


bio(452).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp1/p1';
bio(452).sigName='Altitude_data';
bio(452).portIdx=0;
bio(452).dim=[1,1];
bio(452).sigWidth=1;
bio(452).sigAddress='&scoop_v3_B.Altitude_data';
bio(452).ndims=2;
bio(452).size=[];


bio(453).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp1/p2';
bio(453).sigName='Altitude_time';
bio(453).portIdx=1;
bio(453).dim=[1,1];
bio(453).sigWidth=1;
bio(453).sigAddress='&scoop_v3_B.Altitude_time';
bio(453).ndims=2;
bio(453).size=[];


bio(454).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp2/p1';
bio(454).sigName='Heading_data';
bio(454).portIdx=0;
bio(454).dim=[1,1];
bio(454).sigWidth=1;
bio(454).sigAddress='&scoop_v3_B.Heading_data';
bio(454).ndims=2;
bio(454).size=[];


bio(455).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp2/p2';
bio(455).sigName='Heading_time';
bio(455).portIdx=1;
bio(455).dim=[1,1];
bio(455).sigWidth=1;
bio(455).sigAddress='&scoop_v3_B.Heading_time';
bio(455).ndims=2;
bio(455).size=[];


bio(456).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp3/p1';
bio(456).sigName='VehicleSpeed_data';
bio(456).portIdx=0;
bio(456).dim=[1,1];
bio(456).sigWidth=1;
bio(456).sigAddress='&scoop_v3_B.VehicleSpeed_data';
bio(456).ndims=2;
bio(456).size=[];


bio(457).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp3/p2';
bio(457).sigName='VehicleSpeed_time';
bio(457).portIdx=1;
bio(457).dim=[1,1];
bio(457).sigWidth=1;
bio(457).sigAddress='&scoop_v3_B.VehicleSpeed_time';
bio(457).ndims=2;
bio(457).size=[];


bio(458).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp4/p1';
bio(458).sigName='GPS_Status';
bio(458).portIdx=0;
bio(458).dim=[1,1];
bio(458).sigWidth=1;
bio(458).sigAddress='&scoop_v3_B.GPS_Status';
bio(458).ndims=2;
bio(458).size=[];


bio(459).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp4/p2';
bio(459).sigName='GPS_Status_time';
bio(459).portIdx=1;
bio(459).dim=[1,1];
bio(459).sigWidth=1;
bio(459).sigAddress='&scoop_v3_B.GPS_Status_time';
bio(459).ndims=2;
bio(459).size=[];


bio(460).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp5/p1';
bio(460).sigName='Latitude_data';
bio(460).portIdx=0;
bio(460).dim=[1,1];
bio(460).sigWidth=1;
bio(460).sigAddress='&scoop_v3_B.Latitude_data';
bio(460).ndims=2;
bio(460).size=[];


bio(461).blkName='Serial IN (Trimble)/Append Timestamp to GPS/AppendLonTimestamp5/p2';
bio(461).sigName='Latitude_time';
bio(461).portIdx=1;
bio(461).dim=[1,1];
bio(461).sigWidth=1;
bio(461).sigAddress='&scoop_v3_B.Latitude_time';
bio(461).ndims=2;
bio(461).size=[];


bio(462).blkName='Serial IN (Trimble)/Baseboard Serial F1/Rate Transition';
bio(462).sigName='';
bio(462).portIdx=0;
bio(462).dim=[1,1];
bio(462).sigWidth=1;
bio(462).sigAddress='&scoop_v3_B.RateTransition';
bio(462).ndims=2;
bio(462).size=[];


bio(463).blkName='Serial IN (Trimble)/Baseboard Serial F1/Rate Transition1';
bio(463).sigName='';
bio(463).portIdx=0;
bio(463).dim=[1,1];
bio(463).sigWidth=1;
bio(463).sigAddress='&scoop_v3_B.RateTransition1';
bio(463).ndims=2;
bio(463).size=[];


bio(464).blkName='Serial IN (Trimble)/Baseboard Serial F1/Rate Transition2';
bio(464).sigName='';
bio(464).portIdx=0;
bio(464).dim=[1,1];
bio(464).sigWidth=1;
bio(464).sigAddress='&scoop_v3_B.RateTransition2';
bio(464).ndims=2;
bio(464).size=[];


bio(465).blkName='Serial IN (Trimble)/Baseboard Serial F1/Rate Transition3';
bio(465).sigName='';
bio(465).portIdx=0;
bio(465).dim=[1,1];
bio(465).sigWidth=1;
bio(465).sigAddress='&scoop_v3_B.RateTransition3';
bio(465).ndims=2;
bio(465).size=[];


bio(466).blkName='Serial IN (Trimble)/Baseboard Serial F1/FIFO write 1/p1';
bio(466).sigName='';
bio(466).portIdx=0;
bio(466).dim=[1,1];
bio(466).sigWidth=1;
bio(466).sigAddress='&scoop_v3_B.FIFOwrite1_o1';
bio(466).ndims=2;
bio(466).size=[];


bio(467).blkName='Serial IN (Trimble)/Baseboard Serial F1/FIFO write 1/p2';
bio(467).sigName='';
bio(467).portIdx=1;
bio(467).dim=[1,1];
bio(467).sigWidth=1;
bio(467).sigAddress='&scoop_v3_B.FIFOwrite1_o2';
bio(467).ndims=2;
bio(467).size=[];


bio(468).blkName='Serial IN (Trimble)/Baseboard Serial F1/FIFO write 2/p1';
bio(468).sigName='';
bio(468).portIdx=0;
bio(468).dim=[1,1];
bio(468).sigWidth=1;
bio(468).sigAddress='&scoop_v3_B.FIFOwrite2_o1';
bio(468).ndims=2;
bio(468).size=[];


bio(469).blkName='Serial IN (Trimble)/Baseboard Serial F1/FIFO write 2/p2';
bio(469).sigName='';
bio(469).portIdx=1;
bio(469).dim=[1,1];
bio(469).sigWidth=1;
bio(469).sigAddress='&scoop_v3_B.FIFOwrite2_o2';
bio(469).ndims=2;
bio(469).size=[];


bio(470).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/MATLAB Function/p1';
bio(470).sigName='gpsMsgType';
bio(470).portIdx=0;
bio(470).dim=[1,1];
bio(470).sigWidth=1;
bio(470).sigAddress='&scoop_v3_B.gpsMsgType';
bio(470).ndims=2;
bio(470).size=[];


bio(471).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/MATLAB Function/p2';
bio(471).sigName='gpsPayload';
bio(471).portIdx=1;
bio(471).dim=[1,249];
bio(471).sigWidth=249;
bio(471).sigAddress='&scoop_v3_B.gpsPayload[0]';
bio(471).ndims=2;
bio(471).size=[];


bio(472).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p1';
bio(472).sigName='stationID';
bio(472).portIdx=0;
bio(472).dim=[1,1];
bio(472).sigWidth=1;
bio(472).sigAddress='&scoop_v3_B.stationID_a';
bio(472).ndims=2;
bio(472).size=[];


bio(473).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p2';
bio(473).sigName='generationDeltaTime';
bio(473).portIdx=1;
bio(473).dim=[1,1];
bio(473).sigWidth=1;
bio(473).sigAddress='&scoop_v3_B.generationDeltaTime';
bio(473).ndims=2;
bio(473).size=[];


bio(474).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p3';
bio(474).sigName='stationType';
bio(474).portIdx=2;
bio(474).dim=[1,1];
bio(474).sigWidth=1;
bio(474).sigAddress='&scoop_v3_B.stationType_p';
bio(474).ndims=2;
bio(474).size=[];


bio(475).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p4';
bio(475).sigName='vehicleLengthValue';
bio(475).portIdx=3;
bio(475).dim=[1,1];
bio(475).sigWidth=1;
bio(475).sigAddress='&scoop_v3_B.vehicleLengthValue';
bio(475).ndims=2;
bio(475).size=[];


bio(476).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p5';
bio(476).sigName='vehicleLengthConfidenceIndication';
bio(476).portIdx=4;
bio(476).dim=[1,1];
bio(476).sigWidth=1;
bio(476).sigAddress='&scoop_v3_B.vehicleLengthConfidenceIndicati';
bio(476).ndims=2;
bio(476).size=[];


bio(477).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p6';
bio(477).sigName='vehicleWidth';
bio(477).portIdx=5;
bio(477).dim=[1,1];
bio(477).sigWidth=1;
bio(477).sigAddress='&scoop_v3_B.vehicleWidth';
bio(477).ndims=2;
bio(477).size=[];


bio(478).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p7';
bio(478).sigName='latitude';
bio(478).portIdx=6;
bio(478).dim=[1,1];
bio(478).sigWidth=1;
bio(478).sigAddress='&scoop_v3_B.latitude';
bio(478).ndims=2;
bio(478).size=[];


bio(479).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p8';
bio(479).sigName='longitude';
bio(479).portIdx=7;
bio(479).dim=[1,1];
bio(479).sigWidth=1;
bio(479).sigAddress='&scoop_v3_B.longitude';
bio(479).ndims=2;
bio(479).size=[];


bio(480).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p9';
bio(480).sigName='semiMajorConfidence';
bio(480).portIdx=8;
bio(480).dim=[1,1];
bio(480).sigWidth=1;
bio(480).sigAddress='&scoop_v3_B.semiMajorConfidence_f';
bio(480).ndims=2;
bio(480).size=[];


bio(481).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p10';
bio(481).sigName='semiMinorConfidence';
bio(481).portIdx=9;
bio(481).dim=[1,1];
bio(481).sigWidth=1;
bio(481).sigAddress='&scoop_v3_B.semiMinorConfidence_p';
bio(481).ndims=2;
bio(481).size=[];


bio(482).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p11';
bio(482).sigName='semiMajorOrientation';
bio(482).portIdx=10;
bio(482).dim=[1,1];
bio(482).sigWidth=1;
bio(482).sigAddress='&scoop_v3_B.semiMajorOrientation_g';
bio(482).ndims=2;
bio(482).size=[];


bio(483).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p12';
bio(483).sigName='headingValue';
bio(483).portIdx=11;
bio(483).dim=[1,1];
bio(483).sigWidth=1;
bio(483).sigAddress='&scoop_v3_B.headingValue';
bio(483).ndims=2;
bio(483).size=[];


bio(484).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p13';
bio(484).sigName='headingConfidence';
bio(484).portIdx=12;
bio(484).dim=[1,1];
bio(484).sigWidth=1;
bio(484).sigAddress='&scoop_v3_B.headingConfidence';
bio(484).ndims=2;
bio(484).size=[];


bio(485).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p14';
bio(485).sigName='speedValue';
bio(485).portIdx=13;
bio(485).dim=[1,1];
bio(485).sigWidth=1;
bio(485).sigAddress='&scoop_v3_B.speedValue';
bio(485).ndims=2;
bio(485).size=[];


bio(486).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p15';
bio(486).sigName='speedConfidence';
bio(486).portIdx=14;
bio(486).dim=[1,1];
bio(486).sigWidth=1;
bio(486).sigAddress='&scoop_v3_B.speedConfidence';
bio(486).ndims=2;
bio(486).size=[];


bio(487).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p16';
bio(487).sigName='yawRateValue';
bio(487).portIdx=15;
bio(487).dim=[1,1];
bio(487).sigWidth=1;
bio(487).sigAddress='&scoop_v3_B.yawRateValue';
bio(487).ndims=2;
bio(487).size=[];


bio(488).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p17';
bio(488).sigName='yawRateConfidence';
bio(488).portIdx=16;
bio(488).dim=[1,1];
bio(488).sigWidth=1;
bio(488).sigAddress='&scoop_v3_B.yawRateConfidence';
bio(488).ndims=2;
bio(488).size=[];


bio(489).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p18';
bio(489).sigName='longitudinalAccelerationValue';
bio(489).portIdx=17;
bio(489).dim=[1,1];
bio(489).sigWidth=1;
bio(489).sigAddress='&scoop_v3_B.longitudinalAccelerationValue';
bio(489).ndims=2;
bio(489).size=[];


bio(490).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p19';
bio(490).sigName='longitudinalAccelerationConfidence';
bio(490).portIdx=18;
bio(490).dim=[1,1];
bio(490).sigWidth=1;
bio(490).sigAddress='&scoop_v3_B.longitudinalAccelerationConfide';
bio(490).ndims=2;
bio(490).size=[];


bio(491).blkName='UDP IN (Cohda)1/CAM_Proc/S-Function Builder1/p20';
bio(491).sigName='vehicleRole';
bio(491).portIdx=19;
bio(491).dim=[1,1];
bio(491).sigWidth=1;
bio(491).sigAddress='&scoop_v3_B.vehicleRole';
bio(491).ndims=2;
bio(491).size=[];


bio(492).blkName='UDP IN (Cohda)1/Detect Change/FixPt Relational Operator';
bio(492).sigName='';
bio(492).portIdx=0;
bio(492).dim=[1,1];
bio(492).sigWidth=1;
bio(492).sigAddress='&scoop_v3_B.FixPtRelationalOperator';
bio(492).ndims=2;
bio(492).size=[];


bio(493).blkName='UDP IN (Cohda)1/Detect Change/Delay Input1';
bio(493).sigName='U(k-1)';
bio(493).portIdx=0;
bio(493).dim=[1,1];
bio(493).sigWidth=1;
bio(493).sigAddress='&scoop_v3_B.Uk1_k';
bio(493).ndims=2;
bio(493).size=[];


bio(494).blkName='UDP IN (Cohda)1/Detect Change1/FixPt Relational Operator';
bio(494).sigName='';
bio(494).portIdx=0;
bio(494).dim=[1,1];
bio(494).sigWidth=1;
bio(494).sigAddress='&scoop_v3_B.FixPtRelationalOperator_e';
bio(494).ndims=2;
bio(494).size=[];


bio(495).blkName='UDP IN (Cohda)1/Detect Change1/Delay Input1';
bio(495).sigName='U(k-1)';
bio(495).portIdx=0;
bio(495).dim=[1,1];
bio(495).sigWidth=1;
bio(495).sigAddress='&scoop_v3_B.Uk1_d';
bio(495).ndims=2;
bio(495).size=[];


bio(496).blkName='UDP IN (Cohda)1/Detect Change2/FixPt Relational Operator';
bio(496).sigName='';
bio(496).portIdx=0;
bio(496).dim=[1,1];
bio(496).sigWidth=1;
bio(496).sigAddress='&scoop_v3_B.FixPtRelationalOperator_b';
bio(496).ndims=2;
bio(496).size=[];


bio(497).blkName='UDP IN (Cohda)1/Detect Change2/Delay Input1';
bio(497).sigName='U(k-1)';
bio(497).portIdx=0;
bio(497).dim=[1,1];
bio(497).sigWidth=1;
bio(497).sigAddress='&scoop_v3_B.Uk1_i';
bio(497).ndims=2;
bio(497).size=[];


bio(498).blkName='UDP IN (Cohda)1/V2V convert/MATLAB Function1';
bio(498).sigName='StationIDXpc';
bio(498).portIdx=0;
bio(498).dim=[1,1];
bio(498).sigWidth=1;
bio(498).sigAddress='&scoop_v3_B.sf_MATLABFunction1.StationIDXpc';
bio(498).ndims=2;
bio(498).size=[];


bio(499).blkName='UDP IN (Cohda)1/V2V convert/MATLAB Function35';
bio(499).sigName='StationIDXpc';
bio(499).portIdx=0;
bio(499).dim=[1,1];
bio(499).sigWidth=1;
bio(499).sigAddress='&scoop_v3_B.sf_MATLABFunction35.StationIDXpc';
bio(499).ndims=2;
bio(499).size=[];


bio(500).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition1';
bio(500).sigName='VehicleWidthETSI';
bio(500).portIdx=0;
bio(500).dim=[1,1];
bio(500).sigWidth=1;
bio(500).sigAddress='&scoop_v3_B.VehicleWidthETSI_e';
bio(500).ndims=2;
bio(500).size=[];


bio(501).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition10';
bio(501).sigName='';
bio(501).portIdx=0;
bio(501).dim=[1,1];
bio(501).sigWidth=1;
bio(501).sigAddress='&scoop_v3_B.RateTransition10_j';
bio(501).ndims=2;
bio(501).size=[];


bio(502).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition11';
bio(502).sigName='';
bio(502).portIdx=0;
bio(502).dim=[1,1];
bio(502).sigWidth=1;
bio(502).sigAddress='&scoop_v3_B.RateTransition11_e';
bio(502).ndims=2;
bio(502).size=[];


bio(503).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition2';
bio(503).sigName='CtrlTypeETSI';
bio(503).portIdx=0;
bio(503).dim=[1,1];
bio(503).sigWidth=1;
bio(503).sigAddress='&scoop_v3_B.CtrlTypeETSI_ll';
bio(503).ndims=2;
bio(503).size=[];


bio(504).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition3';
bio(504).sigName='VehRespoTimeCnstETSI';
bio(504).portIdx=0;
bio(504).dim=[1,1];
bio(504).sigWidth=1;
bio(504).sigAddress='&scoop_v3_B.VehRespoTimeCnstETSI_o';
bio(504).ndims=2;
bio(504).size=[];


bio(505).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition4';
bio(505).sigName='VehRespoTimeDelayETSI';
bio(505).portIdx=0;
bio(505).dim=[1,1];
bio(505).sigWidth=1;
bio(505).sigAddress='&scoop_v3_B.VehRespoTimeDelayETSI_j0';
bio(505).ndims=2;
bio(505).size=[];


bio(506).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition5';
bio(506).sigName='PositionGenerationDeltaTimeETSI';
bio(506).portIdx=0;
bio(506).dim=[1,1];
bio(506).sigWidth=1;
bio(506).sigAddress='&scoop_v3_B.PositionGenerationDeltaTimeET_m';
bio(506).ndims=2;
bio(506).size=[];


bio(507).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition52';
bio(507).sigName='MioBearingETSI';
bio(507).portIdx=0;
bio(507).dim=[1,1];
bio(507).sigWidth=1;
bio(507).sigAddress='&scoop_v3_B.MioBearingETSI_h';
bio(507).ndims=2;
bio(507).size=[];


bio(508).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition53';
bio(508).sigName='MioRangeRateETSI';
bio(508).portIdx=0;
bio(508).dim=[1,1];
bio(508).sigWidth=1;
bio(508).sigAddress='&scoop_v3_B.MioRangeRateETSI_h';
bio(508).ndims=2;
bio(508).size=[];


bio(509).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition55';
bio(509).sigName='VehHeadingValETSI';
bio(509).portIdx=0;
bio(509).dim=[1,1];
bio(509).sigWidth=1;
bio(509).sigAddress='&scoop_v3_B.VehHeadingValETSI_a';
bio(509).ndims=2;
bio(509).size=[];


bio(510).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition56';
bio(510).sigName='VehHeadingConfETSI';
bio(510).portIdx=0;
bio(510).dim=[1,1];
bio(510).sigWidth=1;
bio(510).sigAddress='&scoop_v3_B.VehHeadingConfETSI_j';
bio(510).ndims=2;
bio(510).size=[];


bio(511).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition57';
bio(511).sigName='VehSpeedValETSI';
bio(511).portIdx=0;
bio(511).dim=[1,1];
bio(511).sigWidth=1;
bio(511).sigAddress='&scoop_v3_B.VehSpeedValETSI_m';
bio(511).ndims=2;
bio(511).size=[];


bio(512).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition58';
bio(512).sigName='VehSpeedConfETSI';
bio(512).portIdx=0;
bio(512).dim=[1,1];
bio(512).sigWidth=1;
bio(512).sigAddress='&scoop_v3_B.VehSpeedConfETSI_o';
bio(512).ndims=2;
bio(512).size=[];


bio(513).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition59';
bio(513).sigName='VehYawRateValETSI';
bio(513).portIdx=0;
bio(513).dim=[1,1];
bio(513).sigWidth=1;
bio(513).sigAddress='&scoop_v3_B.VehYawRateValETSI_m';
bio(513).ndims=2;
bio(513).size=[];


bio(514).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition6';
bio(514).sigName='DesLongAccETSI';
bio(514).portIdx=0;
bio(514).dim=[1,1];
bio(514).sigWidth=1;
bio(514).sigAddress='&scoop_v3_B.DesLongAccETSI';
bio(514).ndims=2;
bio(514).size=[];


bio(515).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition60';
bio(515).sigName='VehYawRateConfETSI';
bio(515).portIdx=0;
bio(515).dim=[1,1];
bio(515).sigWidth=1;
bio(515).sigAddress='&scoop_v3_B.VehYawRateConfETSI_o';
bio(515).ndims=2;
bio(515).size=[];


bio(516).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition61';
bio(516).sigName='LongAccValETSI';
bio(516).portIdx=0;
bio(516).dim=[1,1];
bio(516).sigWidth=1;
bio(516).sigAddress='&scoop_v3_B.LongAccValETSI_e';
bio(516).ndims=2;
bio(516).size=[];


bio(517).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition62';
bio(517).sigName='LongAccConfETSI';
bio(517).portIdx=0;
bio(517).dim=[1,1];
bio(517).sigWidth=1;
bio(517).sigAddress='&scoop_v3_B.LongAccConfETSI_g';
bio(517).ndims=2;
bio(517).size=[];


bio(518).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition63';
bio(518).sigName='TimeDelayETSI';
bio(518).portIdx=0;
bio(518).dim=[1,1];
bio(518).sigWidth=1;
bio(518).sigAddress='&scoop_v3_B.TimeDelayETSI_k';
bio(518).ndims=2;
bio(518).size=[];


bio(519).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition64';
bio(519).sigName='CruiseSpeedETSI';
bio(519).portIdx=0;
bio(519).dim=[1,1];
bio(519).sigWidth=1;
bio(519).sigAddress='&scoop_v3_B.CruiseSpeedETSI_k';
bio(519).ndims=2;
bio(519).size=[];


bio(520).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition65';
bio(520).sigName='MergeReqFlagETSI';
bio(520).portIdx=0;
bio(520).dim=[1,1];
bio(520).sigWidth=1;
bio(520).sigAddress='&scoop_v3_B.MergeReqFlagETSI_g';
bio(520).ndims=2;
bio(520).size=[];


bio(521).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition66';
bio(521).sigName='STOMETSI';
bio(521).portIdx=0;
bio(521).dim=[1,1];
bio(521).sigWidth=1;
bio(521).sigAddress='&scoop_v3_B.STOMETSI_jl';
bio(521).ndims=2;
bio(521).size=[];


bio(522).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition67';
bio(522).sigName='MergingFlagETSI';
bio(522).portIdx=0;
bio(522).dim=[1,1];
bio(522).sigWidth=1;
bio(522).sigAddress='&scoop_v3_B.MergingFlagETSI_m';
bio(522).ndims=2;
bio(522).size=[];


bio(523).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition68';
bio(523).sigName='ForwardIdETSI';
bio(523).portIdx=0;
bio(523).dim=[1,1];
bio(523).sigWidth=1;
bio(523).sigAddress='&scoop_v3_B.ForwardIdETSI_mj';
bio(523).ndims=2;
bio(523).size=[];


bio(524).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition69';
bio(524).sigName='BackwardIdETSI';
bio(524).portIdx=0;
bio(524).dim=[1,1];
bio(524).sigWidth=1;
bio(524).sigAddress='&scoop_v3_B.BackwardIdETSI_d';
bio(524).ndims=2;
bio(524).size=[];


bio(525).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition7';
bio(525).sigName='StationIDETSI';
bio(525).portIdx=0;
bio(525).dim=[1,1];
bio(525).sigWidth=1;
bio(525).sigAddress='&scoop_v3_B.StationIDETSI';
bio(525).ndims=2;
bio(525).size=[];


bio(526).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition70';
bio(526).sigName='TailVehFlagETSI';
bio(526).portIdx=0;
bio(526).dim=[1,1];
bio(526).sigWidth=1;
bio(526).sigAddress='&scoop_v3_B.TailVehFlagETSI_a';
bio(526).ndims=2;
bio(526).size=[];


bio(527).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition71';
bio(527).sigName='HeadVehFlagETSI';
bio(527).portIdx=0;
bio(527).dim=[1,1];
bio(527).sigWidth=1;
bio(527).sigAddress='&scoop_v3_B.HeadVehFlagETSI_h';
bio(527).ndims=2;
bio(527).size=[];


bio(528).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition72';
bio(528).sigName='PlatoonIdETSI';
bio(528).portIdx=0;
bio(528).dim=[1,1];
bio(528).sigWidth=1;
bio(528).sigAddress='&scoop_v3_B.PlatoonIdETSI_h';
bio(528).ndims=2;
bio(528).size=[];


bio(529).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition73';
bio(529).sigName='TraveledDistCZETSI';
bio(529).portIdx=0;
bio(529).dim=[1,1];
bio(529).sigWidth=1;
bio(529).sigAddress='&scoop_v3_B.TraveledDistCZETSI_e';
bio(529).ndims=2;
bio(529).size=[];


bio(530).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition74';
bio(530).sigName='IntentionETSI';
bio(530).portIdx=0;
bio(530).dim=[1,1];
bio(530).sigWidth=1;
bio(530).sigAddress='&scoop_v3_B.IntentionETSI_f';
bio(530).ndims=2;
bio(530).size=[];


bio(531).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition76';
bio(531).sigName='IntersVehCntrETSI';
bio(531).portIdx=0;
bio(531).dim=[1,1];
bio(531).sigWidth=1;
bio(531).sigAddress='&scoop_v3_B.IntersVehCntrETSI_i';
bio(531).ndims=2;
bio(531).size=[];


bio(532).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition77';
bio(532).sigName='PairAckFlagETSI';
bio(532).portIdx=0;
bio(532).dim=[1,1];
bio(532).sigWidth=1;
bio(532).sigAddress='&scoop_v3_B.PairAckFlagETSI_n';
bio(532).ndims=2;
bio(532).size=[];


bio(533).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition8';
bio(533).sigName='VehicleLengthConfETSI';
bio(533).portIdx=0;
bio(533).dim=[1,1];
bio(533).sigWidth=1;
bio(533).sigAddress='&scoop_v3_B.VehicleLengthConfETSI';
bio(533).ndims=2;
bio(533).size=[];


bio(534).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition82';
bio(534).sigName='LanePosETSI';
bio(534).portIdx=0;
bio(534).dim=[1,1];
bio(534).sigWidth=1;
bio(534).sigAddress='&scoop_v3_B.LanePosETSI_p';
bio(534).ndims=2;
bio(534).size=[];


bio(535).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition83';
bio(535).sigName='ParticipantsReadyETSI';
bio(535).portIdx=0;
bio(535).dim=[1,1];
bio(535).sigWidth=1;
bio(535).sigAddress='&scoop_v3_B.ParticipantsReadyETSI_m';
bio(535).ndims=2;
bio(535).size=[];


bio(536).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition84';
bio(536).sigName='StartScenarioETSI';
bio(536).portIdx=0;
bio(536).dim=[1,1];
bio(536).sigWidth=1;
bio(536).sigAddress='&scoop_v3_B.StartScenarioETSI_m';
bio(536).ndims=2;
bio(536).size=[];


bio(537).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition85';
bio(537).sigName='EoSETSI';
bio(537).portIdx=0;
bio(537).dim=[1,1];
bio(537).sigWidth=1;
bio(537).sigAddress='&scoop_v3_B.EoSETSI_a';
bio(537).ndims=2;
bio(537).size=[];


bio(538).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition86';
bio(538).sigName='VehicleRoleETSI';
bio(538).portIdx=0;
bio(538).dim=[1,1];
bio(538).sigWidth=1;
bio(538).sigAddress='&scoop_v3_B.VehicleRoleETSI_f';
bio(538).ndims=2;
bio(538).size=[];


bio(539).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition87';
bio(539).sigName='MioRangeETSI';
bio(539).portIdx=0;
bio(539).dim=[1,1];
bio(539).sigWidth=1;
bio(539).sigAddress='&scoop_v3_B.MioRangeETSI_j';
bio(539).ndims=2;
bio(539).size=[];


bio(540).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition88';
bio(540).sigName='LatRefPositionETSI';
bio(540).portIdx=0;
bio(540).dim=[1,1];
bio(540).sigWidth=1;
bio(540).sigAddress='&scoop_v3_B.LatRefPositionETSI_m';
bio(540).ndims=2;
bio(540).size=[];


bio(541).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition89';
bio(541).sigName='VehicleLengthETSI';
bio(541).portIdx=0;
bio(541).dim=[1,1];
bio(541).sigWidth=1;
bio(541).sigAddress='&scoop_v3_B.VehicleLengthETSI_e';
bio(541).ndims=2;
bio(541).size=[];


bio(542).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition9';
bio(542).sigName='';
bio(542).portIdx=0;
bio(542).dim=[1,1];
bio(542).sigWidth=1;
bio(542).sigAddress='&scoop_v3_B.RateTransition9_m';
bio(542).ndims=2;
bio(542).size=[];


bio(543).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition90';
bio(543).sigName='VehicleRearAxleLocationETSI';
bio(543).portIdx=0;
bio(543).dim=[1,1];
bio(543).sigWidth=1;
bio(543).sigAddress='&scoop_v3_B.VehicleRearAxleLocationETSI_j';
bio(543).ndims=2;
bio(543).size=[];


bio(544).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition91';
bio(544).sigName='StationIDETSI';
bio(544).portIdx=0;
bio(544).dim=[1,1];
bio(544).sigWidth=1;
bio(544).sigAddress='&scoop_v3_B.StationIDETSI_e';
bio(544).ndims=2;
bio(544).size=[];


bio(545).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition92';
bio(545).sigName='LonRefPositionETSI';
bio(545).portIdx=0;
bio(545).dim=[1,1];
bio(545).sigWidth=1;
bio(545).sigAddress='&scoop_v3_B.LonRefPositionETSI_p';
bio(545).ndims=2;
bio(545).size=[];


bio(546).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition93';
bio(546).sigName='MioIdETSI';
bio(546).portIdx=0;
bio(546).dim=[1,1];
bio(546).sigWidth=1;
bio(546).sigAddress='&scoop_v3_B.MioIdETSI_j';
bio(546).ndims=2;
bio(546).size=[];


bio(547).blkName='UDP IN (Cohda)1/V2V convert/Rate Transition94';
bio(547).sigName='StatioTypeETSI';
bio(547).portIdx=0;
bio(547).dim=[1,1];
bio(547).sigWidth=1;
bio(547).sigAddress='&scoop_v3_B.StatioTypeETSI_j';
bio(547).ndims=2;
bio(547).size=[];


bio(548).blkName='UDP IN (Cohda)1/V2V convert/-1-14';
bio(548).sigName='';
bio(548).portIdx=0;
bio(548).dim=[1,1];
bio(548).sigWidth=1;
bio(548).sigAddress='&scoop_v3_B.u14';
bio(548).ndims=2;
bio(548).size=[];


bio(549).blkName='UDP IN (Cohda)1/V2V convert/0-3';
bio(549).sigName='';
bio(549).portIdx=0;
bio(549).dim=[1,1];
bio(549).sigWidth=1;
bio(549).sigAddress='&scoop_v3_B.u';
bio(549).ndims=2;
bio(549).size=[];


bio(550).blkName='UDP IN (Cohda)1/V2V convert/0-3 ';
bio(550).sigName='';
bio(550).portIdx=0;
bio(550).dim=[1,1];
bio(550).sigWidth=1;
bio(550).sigAddress='&scoop_v3_B.u_k';
bio(550).ndims=2;
bio(550).size=[];


bio(551).blkName='UDP IN (Cohda)1/V2V convert/1-3';
bio(551).sigName='';
bio(551).portIdx=0;
bio(551).dim=[1,1];
bio(551).sigWidth=1;
bio(551).sigAddress='&scoop_v3_B.u_d';
bio(551).ndims=2;
bio(551).size=[];


bio(552).blkName='UDP IN (Cohda)1/V2V convert/boolean';
bio(552).sigName='';
bio(552).portIdx=0;
bio(552).dim=[1,1];
bio(552).sigWidth=1;
bio(552).sigAddress='&scoop_v3_B.boolean';
bio(552).ndims=2;
bio(552).size=[];


bio(553).blkName='UDP IN (Cohda)1/V2V convert/boolean sat14';
bio(553).sigName='';
bio(553).portIdx=0;
bio(553).dim=[1,1];
bio(553).sigWidth=1;
bio(553).sigAddress='&scoop_v3_B.booleansat14';
bio(553).ndims=2;
bio(553).size=[];


bio(554).blkName='UDP IN (Cohda)1/V2V convert/boolean sat15';
bio(554).sigName='';
bio(554).portIdx=0;
bio(554).dim=[1,1];
bio(554).sigWidth=1;
bio(554).sigAddress='&scoop_v3_B.booleansat15';
bio(554).ndims=2;
bio(554).size=[];


bio(555).blkName='UDP IN (Cohda)1/V2V convert/boolean sat16';
bio(555).sigName='';
bio(555).portIdx=0;
bio(555).dim=[1,1];
bio(555).sigWidth=1;
bio(555).sigAddress='&scoop_v3_B.booleansat16';
bio(555).ndims=2;
bio(555).size=[];


bio(556).blkName='UDP IN (Cohda)1/V2V convert/boolean sat17';
bio(556).sigName='';
bio(556).portIdx=0;
bio(556).dim=[1,1];
bio(556).sigWidth=1;
bio(556).sigAddress='&scoop_v3_B.booleansat17';
bio(556).ndims=2;
bio(556).size=[];


bio(557).blkName='UDP IN (Cohda)1/V2V convert/boolean1';
bio(557).sigName='';
bio(557).portIdx=0;
bio(557).dim=[1,1];
bio(557).sigWidth=1;
bio(557).sigAddress='&scoop_v3_B.boolean1';
bio(557).ndims=2;
bio(557).size=[];


bio(558).blkName='UDP IN (Cohda)1/V2V convert/boolean2';
bio(558).sigName='';
bio(558).portIdx=0;
bio(558).dim=[1,1];
bio(558).sigWidth=1;
bio(558).sigAddress='&scoop_v3_B.boolean2';
bio(558).ndims=2;
bio(558).size=[];


bio(559).blkName='UDP IN (Cohda)1/V2V convert/boolean3';
bio(559).sigName='';
bio(559).portIdx=0;
bio(559).dim=[1,1];
bio(559).sigWidth=1;
bio(559).sigAddress='&scoop_v3_B.boolean3';
bio(559).ndims=2;
bio(559).size=[];


bio(560).blkName='UDP IN (Cohda)1/V2V convert/boolean4';
bio(560).sigName='';
bio(560).portIdx=0;
bio(560).dim=[1,1];
bio(560).sigWidth=1;
bio(560).sigAddress='&scoop_v3_B.boolean4';
bio(560).ndims=2;
bio(560).size=[];


bio(561).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p1';
bio(561).sigName='stationID';
bio(561).portIdx=0;
bio(561).dim=[1,1];
bio(561).sigWidth=1;
bio(561).sigAddress='&scoop_v3_B.stationID_n';
bio(561).ndims=2;
bio(561).size=[];


bio(562).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p2';
bio(562).sigName='vehicleRearAxleLocation';
bio(562).portIdx=1;
bio(562).dim=[1,1];
bio(562).sigWidth=1;
bio(562).sigAddress='&scoop_v3_B.vehicleRearAxleLocation';
bio(562).ndims=2;
bio(562).size=[];


bio(563).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p3';
bio(563).sigName='controllerType';
bio(563).portIdx=2;
bio(563).dim=[1,1];
bio(563).sigWidth=1;
bio(563).sigAddress='&scoop_v3_B.controllerType';
bio(563).ndims=2;
bio(563).size=[];


bio(564).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p4';
bio(564).sigName='targetLongitudinalAcceleration';
bio(564).portIdx=3;
bio(564).dim=[1,1];
bio(564).sigWidth=1;
bio(564).sigAddress='&scoop_v3_B.targetLongitudinalAcceleration';
bio(564).ndims=2;
bio(564).size=[];


bio(565).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p5';
bio(565).sigName='timeHeadway';
bio(565).portIdx=4;
bio(565).dim=[1,1];
bio(565).sigWidth=1;
bio(565).sigAddress='&scoop_v3_B.timeHeadway';
bio(565).ndims=2;
bio(565).size=[];


bio(566).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p6';
bio(566).sigName='cruiseSpeed';
bio(566).portIdx=5;
bio(566).dim=[1,1];
bio(566).sigWidth=1;
bio(566).sigAddress='&scoop_v3_B.cruiseSpeed';
bio(566).ndims=2;
bio(566).size=[];


bio(567).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p7';
bio(567).sigName='vehicleResponseTimeConstant';
bio(567).portIdx=6;
bio(567).dim=[1,1];
bio(567).sigWidth=1;
bio(567).sigAddress='&scoop_v3_B.vehicleResponseTimeConstant';
bio(567).ndims=2;
bio(567).size=[];


bio(568).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p8';
bio(568).sigName='vehicleResponseTimeDelay';
bio(568).portIdx=7;
bio(568).dim=[1,1];
bio(568).sigWidth=1;
bio(568).sigAddress='&scoop_v3_B.vehicleResponseTimeDelay';
bio(568).ndims=2;
bio(568).size=[];


bio(569).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p9';
bio(569).sigName='participantsReady';
bio(569).portIdx=8;
bio(569).dim=[1,1];
bio(569).sigWidth=1;
bio(569).sigAddress='&scoop_v3_B.participantsReady';
bio(569).ndims=2;
bio(569).size=[];


bio(570).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p10';
bio(570).sigName='startPlatoon';
bio(570).portIdx=9;
bio(570).dim=[1,1];
bio(570).sigWidth=1;
bio(570).sigAddress='&scoop_v3_B.startPlatoon';
bio(570).ndims=2;
bio(570).size=[];


bio(571).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p11';
bio(571).sigName='endOfScenario';
bio(571).portIdx=10;
bio(571).dim=[1,1];
bio(571).sigWidth=1;
bio(571).sigAddress='&scoop_v3_B.endOfScenario';
bio(571).ndims=2;
bio(571).size=[];


bio(572).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p12';
bio(572).sigName='mioID';
bio(572).portIdx=11;
bio(572).dim=[1,1];
bio(572).sigWidth=1;
bio(572).sigAddress='&scoop_v3_B.mioID';
bio(572).ndims=2;
bio(572).size=[];


bio(573).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p13';
bio(573).sigName='mioRange';
bio(573).portIdx=12;
bio(573).dim=[1,1];
bio(573).sigWidth=1;
bio(573).sigAddress='&scoop_v3_B.mioRange';
bio(573).ndims=2;
bio(573).size=[];


bio(574).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p14';
bio(574).sigName='mioBearing';
bio(574).portIdx=13;
bio(574).dim=[1,1];
bio(574).sigWidth=1;
bio(574).sigAddress='&scoop_v3_B.mioBearing';
bio(574).ndims=2;
bio(574).size=[];


bio(575).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p15';
bio(575).sigName='mioRangeRate';
bio(575).portIdx=14;
bio(575).dim=[1,1];
bio(575).sigWidth=1;
bio(575).sigAddress='&scoop_v3_B.mioRangeRate';
bio(575).ndims=2;
bio(575).size=[];


bio(576).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p16';
bio(576).sigName='lane';
bio(576).portIdx=15;
bio(576).dim=[1,1];
bio(576).sigWidth=1;
bio(576).sigAddress='&scoop_v3_B.lane';
bio(576).ndims=2;
bio(576).size=[];


bio(577).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p17';
bio(577).sigName='forwardID';
bio(577).portIdx=16;
bio(577).dim=[1,1];
bio(577).sigWidth=1;
bio(577).sigAddress='&scoop_v3_B.forwardID';
bio(577).ndims=2;
bio(577).size=[];


bio(578).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p18';
bio(578).sigName='backwardID';
bio(578).portIdx=17;
bio(578).dim=[1,1];
bio(578).sigWidth=1;
bio(578).sigAddress='&scoop_v3_B.backwardID';
bio(578).ndims=2;
bio(578).size=[];


bio(579).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p19';
bio(579).sigName='acknowledgeFlag';
bio(579).portIdx=18;
bio(579).dim=[1,1];
bio(579).sigWidth=1;
bio(579).sigAddress='&scoop_v3_B.acknowledgeFlag';
bio(579).ndims=2;
bio(579).size=[];


bio(580).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p20';
bio(580).sigName='mergeRequest';
bio(580).portIdx=19;
bio(580).dim=[1,1];
bio(580).sigWidth=1;
bio(580).sigAddress='&scoop_v3_B.mergeRequest';
bio(580).ndims=2;
bio(580).size=[];


bio(581).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p21';
bio(581).sigName='mergeSafeToMerge';
bio(581).portIdx=20;
bio(581).dim=[1,1];
bio(581).sigWidth=1;
bio(581).sigAddress='&scoop_v3_B.mergeSafeToMerge';
bio(581).ndims=2;
bio(581).size=[];


bio(582).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p22';
bio(582).sigName='mergeFlag';
bio(582).portIdx=21;
bio(582).dim=[1,1];
bio(582).sigWidth=1;
bio(582).sigAddress='&scoop_v3_B.mergeFlag';
bio(582).ndims=2;
bio(582).size=[];


bio(583).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p23';
bio(583).sigName='mergeFlagTail';
bio(583).portIdx=22;
bio(583).dim=[1,1];
bio(583).sigWidth=1;
bio(583).sigAddress='&scoop_v3_B.mergeFlagTail';
bio(583).ndims=2;
bio(583).size=[];


bio(584).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p24';
bio(584).sigName='mergeFlagHead';
bio(584).portIdx=23;
bio(584).dim=[1,1];
bio(584).sigWidth=1;
bio(584).sigAddress='&scoop_v3_B.mergeFlagHead';
bio(584).ndims=2;
bio(584).size=[];


bio(585).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p25';
bio(585).sigName='platoonID';
bio(585).portIdx=24;
bio(585).dim=[1,1];
bio(585).sigWidth=1;
bio(585).sigAddress='&scoop_v3_B.platoonID';
bio(585).ndims=2;
bio(585).size=[];


bio(586).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p26';
bio(586).sigName='distanceTravelledCZ';
bio(586).portIdx=25;
bio(586).dim=[1,1];
bio(586).sigWidth=1;
bio(586).sigAddress='&scoop_v3_B.distanceTravelledCZ';
bio(586).ndims=2;
bio(586).size=[];


bio(587).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p27';
bio(587).sigName='intention';
bio(587).portIdx=26;
bio(587).dim=[1,1];
bio(587).sigWidth=1;
bio(587).sigAddress='&scoop_v3_B.intention';
bio(587).ndims=2;
bio(587).size=[];


bio(588).blkName='UDP IN (Cohda)1/iCLCM_Proc/S-Function iCLCM/p28';
bio(588).sigName='counterIntersection';
bio(588).portIdx=27;
bio(588).dim=[1,1];
bio(588).sigWidth=1;
bio(588).sigAddress='&scoop_v3_B.counterIntersection';
bio(588).ndims=2;
bio(588).size=[];


bio(589).blkName='UDP OUT (Cohda)1/CAM_iCLCM_Proc/S-Function iCLCM';
bio(589).sigName='';
bio(589).portIdx=0;
bio(589).dim=[268,1];
bio(589).sigWidth=268;
bio(589).sigAddress='&scoop_v3_B.SFunctioniCLCM[0]';
bio(589).ndims=2;
bio(589).size=[];


bio(590).blkName='reference signal routing/Signal Builder/FromWs';
bio(590).sigName='';
bio(590).portIdx=0;
bio(590).dim=[1,1];
bio(590).sigWidth=1;
bio(590).sigAddress='&scoop_v3_B.FromWs';
bio(590).ndims=2;
bio(590).size=[];


bio(591).blkName='referenceRouting/Discrete Derivative/Diff';
bio(591).sigName='';
bio(591).portIdx=0;
bio(591).dim=[1,1];
bio(591).sigWidth=1;
bio(591).sigAddress='&scoop_v3_B.Diff_m';
bio(591).ndims=2;
bio(591).size=[];


bio(592).blkName='referenceRouting/Discrete Derivative/TSamp';
bio(592).sigName='';
bio(592).portIdx=0;
bio(592).dim=[1,1];
bio(592).sigWidth=1;
bio(592).sigAddress='&scoop_v3_B.TSamp_n';
bio(592).ndims=2;
bio(592).size=[];


bio(593).blkName='referenceRouting/Discrete Derivative/UD';
bio(593).sigName='U(k-1)';
bio(593).portIdx=0;
bio(593).dim=[1,1];
bio(593).sigWidth=1;
bio(593).sigAddress='&scoop_v3_B.Uk1_b';
bio(593).ndims=2;
bio(593).size=[];


bio(594).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5800: GCDC00/CAN Unpack/p1';
bio(594).sigName='';
bio(594).portIdx=0;
bio(594).dim=[1,1];
bio(594).sigWidth=1;
bio(594).sigAddress='&scoop_v3_B.CANUnpack_o1';
bio(594).ndims=2;
bio(594).size=[];


bio(595).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5800: GCDC00/CAN Unpack/p2';
bio(595).sigName='';
bio(595).portIdx=1;
bio(595).dim=[1,1];
bio(595).sigWidth=1;
bio(595).sigAddress='&scoop_v3_B.CANUnpack_o2';
bio(595).ndims=2;
bio(595).size=[];


bio(596).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5800: GCDC00/CAN Unpack/p3';
bio(596).sigName='';
bio(596).portIdx=2;
bio(596).dim=[1,1];
bio(596).sigWidth=1;
bio(596).sigAddress='&scoop_v3_B.CANUnpack_o3';
bio(596).ndims=2;
bio(596).size=[];


bio(597).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5800: GCDC00/CAN Unpack/p4';
bio(597).sigName='';
bio(597).portIdx=3;
bio(597).dim=[1,1];
bio(597).sigWidth=1;
bio(597).sigAddress='&scoop_v3_B.CANUnpack_o4';
bio(597).ndims=2;
bio(597).size=[];


bio(598).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5801: GCDC01/CAN Unpack/p1';
bio(598).sigName='ActualEnginePercentTorque';
bio(598).portIdx=0;
bio(598).dim=[1,1];
bio(598).sigWidth=1;
bio(598).sigAddress='&scoop_v3_B.ActualEnginePercentTorque';
bio(598).ndims=2;
bio(598).size=[];


bio(599).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5801: GCDC01/CAN Unpack/p2';
bio(599).sigName='DriverDemand';
bio(599).portIdx=1;
bio(599).dim=[1,1];
bio(599).sigWidth=1;
bio(599).sigAddress='&scoop_v3_B.DriverDemand';
bio(599).ndims=2;
bio(599).size=[];


bio(600).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5801: GCDC01/CAN Unpack/p3';
bio(600).sigName='EngineSpeed';
bio(600).portIdx=2;
bio(600).dim=[1,1];
bio(600).sigWidth=1;
bio(600).sigAddress='&scoop_v3_B.EngineSpeed';
bio(600).ndims=2;
bio(600).size=[];


bio(601).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5801: GCDC01/CAN Unpack/p4';
bio(601).sigName='Mode';
bio(601).portIdx=3;
bio(601).dim=[1,1];
bio(601).sigWidth=1;
bio(601).sigAddress='&scoop_v3_B.Mode';
bio(601).ndims=2;
bio(601).size=[];


bio(602).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5801: GCDC01/CAN Unpack/p5';
bio(602).sigName='SourceAdsressOfControl';
bio(602).portIdx=4;
bio(602).dim=[1,1];
bio(602).sigWidth=1;
bio(602).sigAddress='&scoop_v3_B.SourceAdsressOfControl';
bio(602).ndims=2;
bio(602).size=[];


bio(603).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5801: GCDC01/CAN Unpack/p6';
bio(603).sigName='VehicleSpeed';
bio(603).portIdx=5;
bio(603).dim=[1,1];
bio(603).sigWidth=1;
bio(603).sigAddress='&scoop_v3_B.VehicleSpeed';
bio(603).ndims=2;
bio(603).size=[];


bio(604).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5805: GCDC05/CAN Unpack/p1';
bio(604).sigName='lateralAcceleration';
bio(604).portIdx=0;
bio(604).dim=[1,1];
bio(604).sigWidth=1;
bio(604).sigAddress='&scoop_v3_B.lateralAcceleration';
bio(604).ndims=2;
bio(604).size=[];


bio(605).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5805: GCDC05/CAN Unpack/p2';
bio(605).sigName='longitudalAcceleration';
bio(605).portIdx=1;
bio(605).dim=[1,1];
bio(605).sigWidth=1;
bio(605).sigAddress='&scoop_v3_B.longitudalAcceleration';
bio(605).ndims=2;
bio(605).size=[];


bio(606).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5805: GCDC05/CAN Unpack/p3';
bio(606).sigName='';
bio(606).portIdx=2;
bio(606).dim=[1,1];
bio(606).sigWidth=1;
bio(606).sigAddress='&scoop_v3_B.CANUnpack_o3_j';
bio(606).ndims=2;
bio(606).size=[];


bio(607).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5805: GCDC05/CAN Unpack/p4';
bio(607).sigName='';
bio(607).portIdx=3;
bio(607).dim=[1,1];
bio(607).sigWidth=1;
bio(607).sigAddress='&scoop_v3_B.CANUnpack_o4_n';
bio(607).ndims=2;
bio(607).size=[];


bio(608).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5805: GCDC05/CAN Unpack/p5';
bio(608).sigName='';
bio(608).portIdx=4;
bio(608).dim=[1,1];
bio(608).sigWidth=1;
bio(608).sigAddress='&scoop_v3_B.CANUnpack_o5';
bio(608).ndims=2;
bio(608).size=[];


bio(609).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5805: GCDC05/CAN Unpack/p6';
bio(609).sigName='yawRate';
bio(609).portIdx=5;
bio(609).dim=[1,1];
bio(609).sigWidth=1;
bio(609).sigAddress='&scoop_v3_B.yawRate';
bio(609).ndims=2;
bio(609).size=[];


bio(610).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5806: GCDC06/CAN Unpack/p1';
bio(610).sigName='AcceleratorPedalPosition';
bio(610).portIdx=0;
bio(610).dim=[1,1];
bio(610).sigWidth=1;
bio(610).sigAddress='&scoop_v3_B.AcceleratorPedalPosition';
bio(610).ndims=2;
bio(610).size=[];


bio(611).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5806: GCDC06/CAN Unpack/p2';
bio(611).sigName='BrakePedalPosition';
bio(611).portIdx=1;
bio(611).dim=[1,1];
bio(611).sigWidth=1;
bio(611).sigAddress='&scoop_v3_B.BrakePedalPosition';
bio(611).ndims=2;
bio(611).size=[];


bio(612).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5806: GCDC06/CAN Unpack/p3';
bio(612).sigName='';
bio(612).portIdx=2;
bio(612).dim=[1,1];
bio(612).sigWidth=1;
bio(612).sigAddress='&scoop_v3_B.CANUnpack_o3_k';
bio(612).ndims=2;
bio(612).size=[];


bio(613).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5806: GCDC06/CAN Unpack/p4';
bio(613).sigName='';
bio(613).portIdx=3;
bio(613).dim=[1,1];
bio(613).sigWidth=1;
bio(613).sigAddress='&scoop_v3_B.CANUnpack_o4_f';
bio(613).ndims=2;
bio(613).size=[];


bio(614).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5806: GCDC06/CAN Unpack/p5';
bio(614).sigName='';
bio(614).portIdx=4;
bio(614).dim=[1,1];
bio(614).sigWidth=1;
bio(614).sigAddress='&scoop_v3_B.CANUnpack_o5_h';
bio(614).ndims=2;
bio(614).size=[];


bio(615).blkName='CAN IN (Platform)/Truck/CAN1 Recieve: 0x18FF5806: GCDC06/CAN Unpack/p6';
bio(615).sigName='';
bio(615).portIdx=5;
bio(615).dim=[1,1];
bio(615).sigWidth=1;
bio(615).sigAddress='&scoop_v3_B.CANUnpack_o6';
bio(615).ndims=2;
bio(615).size=[];


bio(616).blkName='CAN IN (Platform)/Truck/Discrete Derivative/Diff';
bio(616).sigName='';
bio(616).portIdx=0;
bio(616).dim=[1,1];
bio(616).sigWidth=1;
bio(616).sigAddress='&scoop_v3_B.Diff';
bio(616).ndims=2;
bio(616).size=[];


bio(617).blkName='CAN IN (Platform)/Truck/Discrete Derivative/TSamp';
bio(617).sigName='';
bio(617).portIdx=0;
bio(617).dim=[1,1];
bio(617).sigWidth=1;
bio(617).sigAddress='&scoop_v3_B.TSamp';
bio(617).ndims=2;
bio(617).size=[];


bio(618).blkName='CAN IN (Platform)/Truck/Discrete Derivative/UD';
bio(618).sigName='U(k-1)';
bio(618).portIdx=0;
bio(618).dim=[1,1];
bio(618).sigWidth=1;
bio(618).sigAddress='&scoop_v3_B.Uk1';
bio(618).ndims=2;
bio(618).size=[];


bio(619).blkName='CAN IN (Platform)/Truck/GeneralInfoDISProp_DIS1/Rate Transition20';
bio(619).sigName='Tracks_Oncoming';
bio(619).portIdx=0;
bio(619).dim=[1,1];
bio(619).sigWidth=1;
bio(619).sigAddress='&scoop_v3_B.Tracks_Oncoming_a';
bio(619).ndims=2;
bio(619).size=[];


bio(620).blkName='CAN IN (Platform)/Truck/GeneralInfoDISProp_DIS1/Rate Transition21';
bio(620).sigName='RADAR_Status';
bio(620).portIdx=0;
bio(620).dim=[1,1];
bio(620).sigWidth=1;
bio(620).sigAddress='&scoop_v3_B.RADAR_Status_p';
bio(620).ndims=2;
bio(620).size=[];


bio(621).blkName='CAN IN (Platform)/Truck/GeneralInfoDISProp_DIS1/Rate Transition22';
bio(621).sigName='EstHostVehicleSpeed';
bio(621).portIdx=0;
bio(621).dim=[1,1];
bio(621).sigWidth=1;
bio(621).sigAddress='&scoop_v3_B.EstHostVehicleSpeed_c';
bio(621).ndims=2;
bio(621).size=[];


bio(622).blkName='CAN IN (Platform)/Truck/GeneralInfoDISProp_DIS1/Rate Transition23';
bio(622).sigName='TrafficDirection';
bio(622).portIdx=0;
bio(622).dim=[1,1];
bio(622).sigWidth=1;
bio(622).sigAddress='&scoop_v3_B.TrafficDirection_l';
bio(622).ndims=2;
bio(622).size=[];


bio(623).blkName='CAN IN (Platform)/Truck/GeneralInfoDISProp_DIS1/GeneralInfoDISProp_DIS/p1';
bio(623).sigName='Tracks_Oncoming';
bio(623).portIdx=0;
bio(623).dim=[1,1];
bio(623).sigWidth=1;
bio(623).sigAddress='&scoop_v3_B.Tracks_Oncoming';
bio(623).ndims=2;
bio(623).size=[];


bio(624).blkName='CAN IN (Platform)/Truck/GeneralInfoDISProp_DIS1/GeneralInfoDISProp_DIS/p2';
bio(624).sigName='RADAR_Status';
bio(624).portIdx=1;
bio(624).dim=[1,1];
bio(624).sigWidth=1;
bio(624).sigAddress='&scoop_v3_B.RADAR_Status';
bio(624).ndims=2;
bio(624).size=[];


bio(625).blkName='CAN IN (Platform)/Truck/GeneralInfoDISProp_DIS1/GeneralInfoDISProp_DIS/p3';
bio(625).sigName='EstHostVehicleSpeed';
bio(625).portIdx=2;
bio(625).dim=[1,1];
bio(625).sigWidth=1;
bio(625).sigAddress='&scoop_v3_B.EstHostVehicleSpeed';
bio(625).ndims=2;
bio(625).size=[];


bio(626).blkName='CAN IN (Platform)/Truck/GeneralInfoDISProp_DIS1/GeneralInfoDISProp_DIS/p4';
bio(626).sigName='TrafficDirection';
bio(626).portIdx=3;
bio(626).dim=[1,1];
bio(626).sigWidth=1;
bio(626).sigAddress='&scoop_v3_B.TrafficDirection';
bio(626).ndims=2;
bio(626).size=[];


bio(627).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition43';
bio(627).sigName='RT4_RelativeLateralSpeed';
bio(627).portIdx=0;
bio(627).dim=[1,1];
bio(627).sigWidth=1;
bio(627).sigAddress='&scoop_v3_B.RT4_RelativeLateralSpeed_m';
bio(627).ndims=2;
bio(627).size=[];


bio(628).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition44';
bio(628).sigName='RT4_Type';
bio(628).portIdx=0;
bio(628).dim=[1,1];
bio(628).sigWidth=1;
bio(628).sigAddress='&scoop_v3_B.RT4_Type_i';
bio(628).ndims=2;
bio(628).size=[];


bio(629).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition45';
bio(629).sigName='RT4_RelativeLongSpeed';
bio(629).portIdx=0;
bio(629).dim=[1,1];
bio(629).sigWidth=1;
bio(629).sigAddress='&scoop_v3_B.RT4_RelativeLongSpeed_d';
bio(629).ndims=2;
bio(629).size=[];


bio(630).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition46';
bio(630).sigName='RT4B_MsgCounter';
bio(630).portIdx=0;
bio(630).dim=[1,1];
bio(630).sigWidth=1;
bio(630).sigAddress='&scoop_v3_B.RT4B_MsgCounter_k';
bio(630).ndims=2;
bio(630).size=[];


bio(631).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition47';
bio(631).sigName='RT4_Lane';
bio(631).portIdx=0;
bio(631).dim=[1,1];
bio(631).sigWidth=1;
bio(631).sigAddress='&scoop_v3_B.RT4_Lane_e';
bio(631).ndims=2;
bio(631).size=[];


bio(632).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition48';
bio(632).sigName='RT4_LaneConfidence';
bio(632).portIdx=0;
bio(632).dim=[1,1];
bio(632).sigWidth=1;
bio(632).sigAddress='&scoop_v3_B.RT4_LaneConfidence_i';
bio(632).ndims=2;
bio(632).size=[];


bio(633).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition49';
bio(633).sigName='RT4_Movement';
bio(633).portIdx=0;
bio(633).dim=[1,1];
bio(633).sigWidth=1;
bio(633).sigAddress='&scoop_v3_B.RT4_Movement_m';
bio(633).ndims=2;
bio(633).size=[];


bio(634).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition50';
bio(634).sigName='RT4_TrackIDNumber';
bio(634).portIdx=0;
bio(634).dim=[1,1];
bio(634).sigWidth=1;
bio(634).sigAddress='&scoop_v3_B.RT4_TrackIDNumber_k';
bio(634).ndims=2;
bio(634).size=[];


bio(635).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition51';
bio(635).sigName='RT4_TrackStatus';
bio(635).portIdx=0;
bio(635).dim=[1,1];
bio(635).sigWidth=1;
bio(635).sigAddress='&scoop_v3_B.RT4_TrackStatus_k';
bio(635).ndims=2;
bio(635).size=[];


bio(636).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition52';
bio(636).sigName='RT4_LateralDistance';
bio(636).portIdx=0;
bio(636).dim=[1,1];
bio(636).sigWidth=1;
bio(636).sigAddress='&scoop_v3_B.RT4_LateralDistance_o';
bio(636).ndims=2;
bio(636).size=[];


bio(637).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition53';
bio(637).sigName='RT4_LongDistance';
bio(637).portIdx=0;
bio(637).dim=[1,1];
bio(637).sigWidth=1;
bio(637).sigAddress='&scoop_v3_B.RT4_LongDistance_j';
bio(637).ndims=2;
bio(637).size=[];


bio(638).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition54';
bio(638).sigName='RT4_RelLongAcceleration';
bio(638).portIdx=0;
bio(638).dim=[1,1];
bio(638).sigWidth=1;
bio(638).sigAddress='&scoop_v3_B.RT4_RelLongAcceleration_e';
bio(638).ndims=2;
bio(638).size=[];


bio(639).blkName='CAN IN (Platform)/Truck/RT10/Rate Transition55';
bio(639).sigName='RT4A_MsgCounter';
bio(639).portIdx=0;
bio(639).dim=[1,1];
bio(639).sigWidth=1;
bio(639).sigAddress='&scoop_v3_B.RT4A_MsgCounter_f';
bio(639).ndims=2;
bio(639).size=[];


bio(640).blkName='CAN IN (Platform)/Truck/RT10/RT4A_DIS_Unpack/p1';
bio(640).sigName='RT4A_MsgCounter';
bio(640).portIdx=0;
bio(640).dim=[1,1];
bio(640).sigWidth=1;
bio(640).sigAddress='&scoop_v3_B.RT4A_MsgCounter';
bio(640).ndims=2;
bio(640).size=[];


bio(641).blkName='CAN IN (Platform)/Truck/RT10/RT4A_DIS_Unpack/p2';
bio(641).sigName='RT4_LateralDistance';
bio(641).portIdx=1;
bio(641).dim=[1,1];
bio(641).sigWidth=1;
bio(641).sigAddress='&scoop_v3_B.RT4_LateralDistance';
bio(641).ndims=2;
bio(641).size=[];


bio(642).blkName='CAN IN (Platform)/Truck/RT10/RT4A_DIS_Unpack/p3';
bio(642).sigName='RT4_LongDistance';
bio(642).portIdx=2;
bio(642).dim=[1,1];
bio(642).sigWidth=1;
bio(642).sigAddress='&scoop_v3_B.RT4_LongDistance';
bio(642).ndims=2;
bio(642).size=[];


bio(643).blkName='CAN IN (Platform)/Truck/RT10/RT4A_DIS_Unpack/p4';
bio(643).sigName='RT4_RelLongAcceleration';
bio(643).portIdx=3;
bio(643).dim=[1,1];
bio(643).sigWidth=1;
bio(643).sigAddress='&scoop_v3_B.RT4_RelLongAcceleration';
bio(643).ndims=2;
bio(643).size=[];


bio(644).blkName='CAN IN (Platform)/Truck/RT10/RT4A_DIS_Unpack/p5';
bio(644).sigName='RT4_RelativeLateralSpeed';
bio(644).portIdx=4;
bio(644).dim=[1,1];
bio(644).sigWidth=1;
bio(644).sigAddress='&scoop_v3_B.RT4_RelativeLateralSpeed';
bio(644).ndims=2;
bio(644).size=[];


bio(645).blkName='CAN IN (Platform)/Truck/RT10/RT4A_DIS_Unpack/p6';
bio(645).sigName='RT4_RelativeLongSpeed';
bio(645).portIdx=5;
bio(645).dim=[1,1];
bio(645).sigWidth=1;
bio(645).sigAddress='&scoop_v3_B.RT4_RelativeLongSpeed';
bio(645).ndims=2;
bio(645).size=[];


bio(646).blkName='CAN IN (Platform)/Truck/RT10/RT4A_DIS_Unpack/p7';
bio(646).sigName='RT4_Type';
bio(646).portIdx=6;
bio(646).dim=[1,1];
bio(646).sigWidth=1;
bio(646).sigAddress='&scoop_v3_B.RT4_Type';
bio(646).ndims=2;
bio(646).size=[];


bio(647).blkName='CAN IN (Platform)/Truck/RT10/RT4B_DIS_Unpack/p1';
bio(647).sigName='RT4B_MsgCounter';
bio(647).portIdx=0;
bio(647).dim=[1,1];
bio(647).sigWidth=1;
bio(647).sigAddress='&scoop_v3_B.RT4B_MsgCounter';
bio(647).ndims=2;
bio(647).size=[];


bio(648).blkName='CAN IN (Platform)/Truck/RT10/RT4B_DIS_Unpack/p2';
bio(648).sigName='RT4_Lane';
bio(648).portIdx=1;
bio(648).dim=[1,1];
bio(648).sigWidth=1;
bio(648).sigAddress='&scoop_v3_B.RT4_Lane';
bio(648).ndims=2;
bio(648).size=[];


bio(649).blkName='CAN IN (Platform)/Truck/RT10/RT4B_DIS_Unpack/p3';
bio(649).sigName='RT4_LaneConfidence';
bio(649).portIdx=2;
bio(649).dim=[1,1];
bio(649).sigWidth=1;
bio(649).sigAddress='&scoop_v3_B.RT4_LaneConfidence';
bio(649).ndims=2;
bio(649).size=[];


bio(650).blkName='CAN IN (Platform)/Truck/RT10/RT4B_DIS_Unpack/p4';
bio(650).sigName='RT4_Movement';
bio(650).portIdx=3;
bio(650).dim=[1,1];
bio(650).sigWidth=1;
bio(650).sigAddress='&scoop_v3_B.RT4_Movement';
bio(650).ndims=2;
bio(650).size=[];


bio(651).blkName='CAN IN (Platform)/Truck/RT10/RT4B_DIS_Unpack/p5';
bio(651).sigName='RT4_TrackIDNumber';
bio(651).portIdx=4;
bio(651).dim=[1,1];
bio(651).sigWidth=1;
bio(651).sigAddress='&scoop_v3_B.RT4_TrackIDNumber';
bio(651).ndims=2;
bio(651).size=[];


bio(652).blkName='CAN IN (Platform)/Truck/RT10/RT4B_DIS_Unpack/p6';
bio(652).sigName='RT4_TrackStatus';
bio(652).portIdx=5;
bio(652).dim=[1,1];
bio(652).sigWidth=1;
bio(652).sigAddress='&scoop_v3_B.RT4_TrackStatus';
bio(652).ndims=2;
bio(652).size=[];


bio(653).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition43';
bio(653).sigName='RT5_RelativeLateralSpeed';
bio(653).portIdx=0;
bio(653).dim=[1,1];
bio(653).sigWidth=1;
bio(653).sigAddress='&scoop_v3_B.RT5_RelativeLateralSpeed_d';
bio(653).ndims=2;
bio(653).size=[];


bio(654).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition44';
bio(654).sigName='RT5_Type';
bio(654).portIdx=0;
bio(654).dim=[1,1];
bio(654).sigWidth=1;
bio(654).sigAddress='&scoop_v3_B.RT5_Type_d';
bio(654).ndims=2;
bio(654).size=[];


bio(655).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition45';
bio(655).sigName='RT5_RelativeLongSpeed';
bio(655).portIdx=0;
bio(655).dim=[1,1];
bio(655).sigWidth=1;
bio(655).sigAddress='&scoop_v3_B.RT5_RelativeLongSpeed_c';
bio(655).ndims=2;
bio(655).size=[];


bio(656).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition46';
bio(656).sigName='RT5B_MsgCounter';
bio(656).portIdx=0;
bio(656).dim=[1,1];
bio(656).sigWidth=1;
bio(656).sigAddress='&scoop_v3_B.RT5B_MsgCounter_g';
bio(656).ndims=2;
bio(656).size=[];


bio(657).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition47';
bio(657).sigName='RT5_Lane';
bio(657).portIdx=0;
bio(657).dim=[1,1];
bio(657).sigWidth=1;
bio(657).sigAddress='&scoop_v3_B.RT5_Lane_c';
bio(657).ndims=2;
bio(657).size=[];


bio(658).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition48';
bio(658).sigName='RT5_LaneConfidence';
bio(658).portIdx=0;
bio(658).dim=[1,1];
bio(658).sigWidth=1;
bio(658).sigAddress='&scoop_v3_B.RT5_LaneConfidence_l';
bio(658).ndims=2;
bio(658).size=[];


bio(659).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition49';
bio(659).sigName='RT5_Movement';
bio(659).portIdx=0;
bio(659).dim=[1,1];
bio(659).sigWidth=1;
bio(659).sigAddress='&scoop_v3_B.RT5_Movement_a';
bio(659).ndims=2;
bio(659).size=[];


bio(660).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition50';
bio(660).sigName='RT5_TrackIDNumber';
bio(660).portIdx=0;
bio(660).dim=[1,1];
bio(660).sigWidth=1;
bio(660).sigAddress='&scoop_v3_B.RT5_TrackIDNumber_d';
bio(660).ndims=2;
bio(660).size=[];


bio(661).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition51';
bio(661).sigName='RT5_TrackStatus';
bio(661).portIdx=0;
bio(661).dim=[1,1];
bio(661).sigWidth=1;
bio(661).sigAddress='&scoop_v3_B.RT5_TrackStatus_n';
bio(661).ndims=2;
bio(661).size=[];


bio(662).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition52';
bio(662).sigName='RT5_LateralDistance';
bio(662).portIdx=0;
bio(662).dim=[1,1];
bio(662).sigWidth=1;
bio(662).sigAddress='&scoop_v3_B.RT5_LateralDistance_n';
bio(662).ndims=2;
bio(662).size=[];


bio(663).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition53';
bio(663).sigName='RT5_LongDistance';
bio(663).portIdx=0;
bio(663).dim=[1,1];
bio(663).sigWidth=1;
bio(663).sigAddress='&scoop_v3_B.RT5_LongDistance_g';
bio(663).ndims=2;
bio(663).size=[];


bio(664).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition54';
bio(664).sigName='RT5_RelLongAcceleration';
bio(664).portIdx=0;
bio(664).dim=[1,1];
bio(664).sigWidth=1;
bio(664).sigAddress='&scoop_v3_B.RT5_RelLongAcceleration_m';
bio(664).ndims=2;
bio(664).size=[];


bio(665).blkName='CAN IN (Platform)/Truck/RT11/Rate Transition55';
bio(665).sigName='RT5A_MsgCounter';
bio(665).portIdx=0;
bio(665).dim=[1,1];
bio(665).sigWidth=1;
bio(665).sigAddress='&scoop_v3_B.RT5A_MsgCounter_h';
bio(665).ndims=2;
bio(665).size=[];


bio(666).blkName='CAN IN (Platform)/Truck/RT11/RT3A_DIS_Unpack/p1';
bio(666).sigName='RT5A_MsgCounter';
bio(666).portIdx=0;
bio(666).dim=[1,1];
bio(666).sigWidth=1;
bio(666).sigAddress='&scoop_v3_B.RT5A_MsgCounter';
bio(666).ndims=2;
bio(666).size=[];


bio(667).blkName='CAN IN (Platform)/Truck/RT11/RT3A_DIS_Unpack/p2';
bio(667).sigName='RT5_LateralDistance';
bio(667).portIdx=1;
bio(667).dim=[1,1];
bio(667).sigWidth=1;
bio(667).sigAddress='&scoop_v3_B.RT5_LateralDistance';
bio(667).ndims=2;
bio(667).size=[];


bio(668).blkName='CAN IN (Platform)/Truck/RT11/RT3A_DIS_Unpack/p3';
bio(668).sigName='RT5_LongDistance';
bio(668).portIdx=2;
bio(668).dim=[1,1];
bio(668).sigWidth=1;
bio(668).sigAddress='&scoop_v3_B.RT5_LongDistance';
bio(668).ndims=2;
bio(668).size=[];


bio(669).blkName='CAN IN (Platform)/Truck/RT11/RT3A_DIS_Unpack/p4';
bio(669).sigName='RT5_RelLongAcceleration';
bio(669).portIdx=3;
bio(669).dim=[1,1];
bio(669).sigWidth=1;
bio(669).sigAddress='&scoop_v3_B.RT5_RelLongAcceleration';
bio(669).ndims=2;
bio(669).size=[];


bio(670).blkName='CAN IN (Platform)/Truck/RT11/RT3A_DIS_Unpack/p5';
bio(670).sigName='RT5_RelativeLateralSpeed';
bio(670).portIdx=4;
bio(670).dim=[1,1];
bio(670).sigWidth=1;
bio(670).sigAddress='&scoop_v3_B.RT5_RelativeLateralSpeed';
bio(670).ndims=2;
bio(670).size=[];


bio(671).blkName='CAN IN (Platform)/Truck/RT11/RT3A_DIS_Unpack/p6';
bio(671).sigName='RT5_RelativeLongSpeed';
bio(671).portIdx=5;
bio(671).dim=[1,1];
bio(671).sigWidth=1;
bio(671).sigAddress='&scoop_v3_B.RT5_RelativeLongSpeed';
bio(671).ndims=2;
bio(671).size=[];


bio(672).blkName='CAN IN (Platform)/Truck/RT11/RT3A_DIS_Unpack/p7';
bio(672).sigName='RT5_Type';
bio(672).portIdx=6;
bio(672).dim=[1,1];
bio(672).sigWidth=1;
bio(672).sigAddress='&scoop_v3_B.RT5_Type';
bio(672).ndims=2;
bio(672).size=[];


bio(673).blkName='CAN IN (Platform)/Truck/RT11/RT3B_DIS_Unpack/p1';
bio(673).sigName='RT5B_MsgCounter';
bio(673).portIdx=0;
bio(673).dim=[1,1];
bio(673).sigWidth=1;
bio(673).sigAddress='&scoop_v3_B.RT5B_MsgCounter';
bio(673).ndims=2;
bio(673).size=[];


bio(674).blkName='CAN IN (Platform)/Truck/RT11/RT3B_DIS_Unpack/p2';
bio(674).sigName='RT5_Lane';
bio(674).portIdx=1;
bio(674).dim=[1,1];
bio(674).sigWidth=1;
bio(674).sigAddress='&scoop_v3_B.RT5_Lane';
bio(674).ndims=2;
bio(674).size=[];


bio(675).blkName='CAN IN (Platform)/Truck/RT11/RT3B_DIS_Unpack/p3';
bio(675).sigName='RT5_LaneConfidence';
bio(675).portIdx=2;
bio(675).dim=[1,1];
bio(675).sigWidth=1;
bio(675).sigAddress='&scoop_v3_B.RT5_LaneConfidence';
bio(675).ndims=2;
bio(675).size=[];


bio(676).blkName='CAN IN (Platform)/Truck/RT11/RT3B_DIS_Unpack/p4';
bio(676).sigName='RT5_Movement';
bio(676).portIdx=3;
bio(676).dim=[1,1];
bio(676).sigWidth=1;
bio(676).sigAddress='&scoop_v3_B.RT5_Movement';
bio(676).ndims=2;
bio(676).size=[];


bio(677).blkName='CAN IN (Platform)/Truck/RT11/RT3B_DIS_Unpack/p5';
bio(677).sigName='RT5_TrackIDNumber';
bio(677).portIdx=4;
bio(677).dim=[1,1];
bio(677).sigWidth=1;
bio(677).sigAddress='&scoop_v3_B.RT5_TrackIDNumber';
bio(677).ndims=2;
bio(677).size=[];


bio(678).blkName='CAN IN (Platform)/Truck/RT11/RT3B_DIS_Unpack/p6';
bio(678).sigName='RT5_TrackStatus';
bio(678).portIdx=5;
bio(678).dim=[1,1];
bio(678).sigWidth=1;
bio(678).sigAddress='&scoop_v3_B.RT5_TrackStatus';
bio(678).ndims=2;
bio(678).size=[];


bio(679).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition43';
bio(679).sigName='RT6_RelativeLateralSpeed';
bio(679).portIdx=0;
bio(679).dim=[1,1];
bio(679).sigWidth=1;
bio(679).sigAddress='&scoop_v3_B.RT6_RelativeLateralSpeed_j';
bio(679).ndims=2;
bio(679).size=[];


bio(680).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition44';
bio(680).sigName='RT6_Type';
bio(680).portIdx=0;
bio(680).dim=[1,1];
bio(680).sigWidth=1;
bio(680).sigAddress='&scoop_v3_B.RT6_Type_f';
bio(680).ndims=2;
bio(680).size=[];


bio(681).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition45';
bio(681).sigName='RT6_RelativeLongSpeed';
bio(681).portIdx=0;
bio(681).dim=[1,1];
bio(681).sigWidth=1;
bio(681).sigAddress='&scoop_v3_B.RT6_RelativeLongSpeed_k';
bio(681).ndims=2;
bio(681).size=[];


bio(682).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition46';
bio(682).sigName='RT6B_MsgCounter';
bio(682).portIdx=0;
bio(682).dim=[1,1];
bio(682).sigWidth=1;
bio(682).sigAddress='&scoop_v3_B.RT6B_MsgCounter_n';
bio(682).ndims=2;
bio(682).size=[];


bio(683).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition47';
bio(683).sigName='RT6_Lane';
bio(683).portIdx=0;
bio(683).dim=[1,1];
bio(683).sigWidth=1;
bio(683).sigAddress='&scoop_v3_B.RT6_Lane_h';
bio(683).ndims=2;
bio(683).size=[];


bio(684).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition48';
bio(684).sigName='RT6_LaneConfidence';
bio(684).portIdx=0;
bio(684).dim=[1,1];
bio(684).sigWidth=1;
bio(684).sigAddress='&scoop_v3_B.RT6_LaneConfidence_i';
bio(684).ndims=2;
bio(684).size=[];


bio(685).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition49';
bio(685).sigName='RT6_Movement';
bio(685).portIdx=0;
bio(685).dim=[1,1];
bio(685).sigWidth=1;
bio(685).sigAddress='&scoop_v3_B.RT6_Movement_o';
bio(685).ndims=2;
bio(685).size=[];


bio(686).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition50';
bio(686).sigName='RT6_TrackIDNumber';
bio(686).portIdx=0;
bio(686).dim=[1,1];
bio(686).sigWidth=1;
bio(686).sigAddress='&scoop_v3_B.RT6_TrackIDNumber_e';
bio(686).ndims=2;
bio(686).size=[];


bio(687).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition51';
bio(687).sigName='RT6_TrackStatus';
bio(687).portIdx=0;
bio(687).dim=[1,1];
bio(687).sigWidth=1;
bio(687).sigAddress='&scoop_v3_B.RT6_TrackStatus_m';
bio(687).ndims=2;
bio(687).size=[];


bio(688).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition52';
bio(688).sigName='RT6_LateralDistance';
bio(688).portIdx=0;
bio(688).dim=[1,1];
bio(688).sigWidth=1;
bio(688).sigAddress='&scoop_v3_B.RT6_LateralDistance_d';
bio(688).ndims=2;
bio(688).size=[];


bio(689).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition53';
bio(689).sigName='RT6_LongDistance';
bio(689).portIdx=0;
bio(689).dim=[1,1];
bio(689).sigWidth=1;
bio(689).sigAddress='&scoop_v3_B.RT6_LongDistance_h';
bio(689).ndims=2;
bio(689).size=[];


bio(690).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition54';
bio(690).sigName='RT6_RelLongAcceleration';
bio(690).portIdx=0;
bio(690).dim=[1,1];
bio(690).sigWidth=1;
bio(690).sigAddress='&scoop_v3_B.RT6_RelLongAcceleration_c';
bio(690).ndims=2;
bio(690).size=[];


bio(691).blkName='CAN IN (Platform)/Truck/RT12/Rate Transition55';
bio(691).sigName='RT36_MsgCounter';
bio(691).portIdx=0;
bio(691).dim=[1,1];
bio(691).sigWidth=1;
bio(691).sigAddress='&scoop_v3_B.RT36_MsgCounter';
bio(691).ndims=2;
bio(691).size=[];


bio(692).blkName='CAN IN (Platform)/Truck/RT12/RT3A_DIS_Unpack/p1';
bio(692).sigName='RT6A_MsgCounter';
bio(692).portIdx=0;
bio(692).dim=[1,1];
bio(692).sigWidth=1;
bio(692).sigAddress='&scoop_v3_B.RT6A_MsgCounter';
bio(692).ndims=2;
bio(692).size=[];


bio(693).blkName='CAN IN (Platform)/Truck/RT12/RT3A_DIS_Unpack/p2';
bio(693).sigName='RT6_LateralDistance';
bio(693).portIdx=1;
bio(693).dim=[1,1];
bio(693).sigWidth=1;
bio(693).sigAddress='&scoop_v3_B.RT6_LateralDistance';
bio(693).ndims=2;
bio(693).size=[];


bio(694).blkName='CAN IN (Platform)/Truck/RT12/RT3A_DIS_Unpack/p3';
bio(694).sigName='RT6_LongDistance';
bio(694).portIdx=2;
bio(694).dim=[1,1];
bio(694).sigWidth=1;
bio(694).sigAddress='&scoop_v3_B.RT6_LongDistance';
bio(694).ndims=2;
bio(694).size=[];


bio(695).blkName='CAN IN (Platform)/Truck/RT12/RT3A_DIS_Unpack/p4';
bio(695).sigName='RT6_RelLongAcceleration';
bio(695).portIdx=3;
bio(695).dim=[1,1];
bio(695).sigWidth=1;
bio(695).sigAddress='&scoop_v3_B.RT6_RelLongAcceleration';
bio(695).ndims=2;
bio(695).size=[];


bio(696).blkName='CAN IN (Platform)/Truck/RT12/RT3A_DIS_Unpack/p5';
bio(696).sigName='RT6_RelativeLateralSpeed';
bio(696).portIdx=4;
bio(696).dim=[1,1];
bio(696).sigWidth=1;
bio(696).sigAddress='&scoop_v3_B.RT6_RelativeLateralSpeed';
bio(696).ndims=2;
bio(696).size=[];


bio(697).blkName='CAN IN (Platform)/Truck/RT12/RT3A_DIS_Unpack/p6';
bio(697).sigName='RT6_RelativeLongSpeed';
bio(697).portIdx=5;
bio(697).dim=[1,1];
bio(697).sigWidth=1;
bio(697).sigAddress='&scoop_v3_B.RT6_RelativeLongSpeed';
bio(697).ndims=2;
bio(697).size=[];


bio(698).blkName='CAN IN (Platform)/Truck/RT12/RT3A_DIS_Unpack/p7';
bio(698).sigName='RT6_Type';
bio(698).portIdx=6;
bio(698).dim=[1,1];
bio(698).sigWidth=1;
bio(698).sigAddress='&scoop_v3_B.RT6_Type';
bio(698).ndims=2;
bio(698).size=[];


bio(699).blkName='CAN IN (Platform)/Truck/RT12/RT3B_DIS_Unpack/p1';
bio(699).sigName='RT6B_MsgCounter';
bio(699).portIdx=0;
bio(699).dim=[1,1];
bio(699).sigWidth=1;
bio(699).sigAddress='&scoop_v3_B.RT6B_MsgCounter';
bio(699).ndims=2;
bio(699).size=[];


bio(700).blkName='CAN IN (Platform)/Truck/RT12/RT3B_DIS_Unpack/p2';
bio(700).sigName='RT6_Lane';
bio(700).portIdx=1;
bio(700).dim=[1,1];
bio(700).sigWidth=1;
bio(700).sigAddress='&scoop_v3_B.RT6_Lane';
bio(700).ndims=2;
bio(700).size=[];


bio(701).blkName='CAN IN (Platform)/Truck/RT12/RT3B_DIS_Unpack/p3';
bio(701).sigName='RT6_LaneConfidence';
bio(701).portIdx=2;
bio(701).dim=[1,1];
bio(701).sigWidth=1;
bio(701).sigAddress='&scoop_v3_B.RT6_LaneConfidence';
bio(701).ndims=2;
bio(701).size=[];


bio(702).blkName='CAN IN (Platform)/Truck/RT12/RT3B_DIS_Unpack/p4';
bio(702).sigName='RT6_Movement';
bio(702).portIdx=3;
bio(702).dim=[1,1];
bio(702).sigWidth=1;
bio(702).sigAddress='&scoop_v3_B.RT6_Movement';
bio(702).ndims=2;
bio(702).size=[];


bio(703).blkName='CAN IN (Platform)/Truck/RT12/RT3B_DIS_Unpack/p5';
bio(703).sigName='RT6_TrackIDNumber';
bio(703).portIdx=4;
bio(703).dim=[1,1];
bio(703).sigWidth=1;
bio(703).sigAddress='&scoop_v3_B.RT6_TrackIDNumber';
bio(703).ndims=2;
bio(703).size=[];


bio(704).blkName='CAN IN (Platform)/Truck/RT12/RT3B_DIS_Unpack/p6';
bio(704).sigName='RT6_TrackStatus';
bio(704).portIdx=5;
bio(704).dim=[1,1];
bio(704).sigWidth=1;
bio(704).sigAddress='&scoop_v3_B.RT6_TrackStatus';
bio(704).ndims=2;
bio(704).size=[];


bio(705).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition1';
bio(705).sigName='RT1_LongDistance';
bio(705).portIdx=0;
bio(705).dim=[1,1];
bio(705).sigWidth=1;
bio(705).sigAddress='&scoop_v3_B.RT1_LongDistance_m';
bio(705).ndims=2;
bio(705).size=[];


bio(706).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition19';
bio(706).sigName='RT1_RelativeLongSpeed';
bio(706).portIdx=0;
bio(706).dim=[1,1];
bio(706).sigWidth=1;
bio(706).sigAddress='&scoop_v3_B.RT1_RelativeLongSpeed_a';
bio(706).ndims=2;
bio(706).size=[];


bio(707).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition2';
bio(707).sigName='RT1B_MsgCounter';
bio(707).portIdx=0;
bio(707).dim=[1,1];
bio(707).sigWidth=1;
bio(707).sigAddress='&scoop_v3_B.RT1B_MsgCounter_m';
bio(707).ndims=2;
bio(707).size=[];


bio(708).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition25';
bio(708).sigName='RT1_Lane';
bio(708).portIdx=0;
bio(708).dim=[1,1];
bio(708).sigWidth=1;
bio(708).sigAddress='&scoop_v3_B.RT1_Lane_d';
bio(708).ndims=2;
bio(708).size=[];


bio(709).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition26';
bio(709).sigName='RT1_LaneConfidence';
bio(709).portIdx=0;
bio(709).dim=[1,1];
bio(709).sigWidth=1;
bio(709).sigAddress='&scoop_v3_B.RT1_LaneConfidence_b';
bio(709).ndims=2;
bio(709).size=[];


bio(710).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition27';
bio(710).sigName='RT1_Movement';
bio(710).portIdx=0;
bio(710).dim=[1,1];
bio(710).sigWidth=1;
bio(710).sigAddress='&scoop_v3_B.RT1_Movement_d';
bio(710).ndims=2;
bio(710).size=[];


bio(711).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition28';
bio(711).sigName='RT1_TrackIDNumber';
bio(711).portIdx=0;
bio(711).dim=[1,1];
bio(711).sigWidth=1;
bio(711).sigAddress='&scoop_v3_B.RT1_TrackIDNumber_h';
bio(711).ndims=2;
bio(711).size=[];


bio(712).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition3';
bio(712).sigName='RT1_RelativeLateralSpeed';
bio(712).portIdx=0;
bio(712).dim=[1,1];
bio(712).sigWidth=1;
bio(712).sigAddress='&scoop_v3_B.RT1_RelativeLateralSpeed_e';
bio(712).ndims=2;
bio(712).size=[];


bio(713).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition4';
bio(713).sigName='RT1_Type';
bio(713).portIdx=0;
bio(713).dim=[1,1];
bio(713).sigWidth=1;
bio(713).sigAddress='&scoop_v3_B.RT1_Type_j';
bio(713).ndims=2;
bio(713).size=[];


bio(714).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition41';
bio(714).sigName='RT1_TrackStatus';
bio(714).portIdx=0;
bio(714).dim=[1,1];
bio(714).sigWidth=1;
bio(714).sigAddress='&scoop_v3_B.RT1_TrackStatus_n';
bio(714).ndims=2;
bio(714).size=[];


bio(715).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition5';
bio(715).sigName='RT1_LateralDistance';
bio(715).portIdx=0;
bio(715).dim=[1,1];
bio(715).sigWidth=1;
bio(715).sigAddress='&scoop_v3_B.RT1_LateralDistance_k';
bio(715).ndims=2;
bio(715).size=[];


bio(716).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition6';
bio(716).sigName='RT1_RelLongAcceleration';
bio(716).portIdx=0;
bio(716).dim=[1,1];
bio(716).sigWidth=1;
bio(716).sigAddress='&scoop_v3_B.RT1_RelLongAcceleration_d';
bio(716).ndims=2;
bio(716).size=[];


bio(717).blkName='CAN IN (Platform)/Truck/RT7/Rate Transition7';
bio(717).sigName='RT1A_MsgCounter';
bio(717).portIdx=0;
bio(717).dim=[1,1];
bio(717).sigWidth=1;
bio(717).sigAddress='&scoop_v3_B.RT1A_MsgCounter_i';
bio(717).ndims=2;
bio(717).size=[];


bio(718).blkName='CAN IN (Platform)/Truck/RT7/RT1A_DIS_Unpack/p1';
bio(718).sigName='RT1A_MsgCounter';
bio(718).portIdx=0;
bio(718).dim=[1,1];
bio(718).sigWidth=1;
bio(718).sigAddress='&scoop_v3_B.RT1A_MsgCounter';
bio(718).ndims=2;
bio(718).size=[];


bio(719).blkName='CAN IN (Platform)/Truck/RT7/RT1A_DIS_Unpack/p2';
bio(719).sigName='RT1_LateralDistance';
bio(719).portIdx=1;
bio(719).dim=[1,1];
bio(719).sigWidth=1;
bio(719).sigAddress='&scoop_v3_B.RT1_LateralDistance';
bio(719).ndims=2;
bio(719).size=[];


bio(720).blkName='CAN IN (Platform)/Truck/RT7/RT1A_DIS_Unpack/p3';
bio(720).sigName='RT1_LongDistance';
bio(720).portIdx=2;
bio(720).dim=[1,1];
bio(720).sigWidth=1;
bio(720).sigAddress='&scoop_v3_B.RT1_LongDistance';
bio(720).ndims=2;
bio(720).size=[];


bio(721).blkName='CAN IN (Platform)/Truck/RT7/RT1A_DIS_Unpack/p4';
bio(721).sigName='RT1_RelLongAcceleration';
bio(721).portIdx=3;
bio(721).dim=[1,1];
bio(721).sigWidth=1;
bio(721).sigAddress='&scoop_v3_B.RT1_RelLongAcceleration';
bio(721).ndims=2;
bio(721).size=[];


bio(722).blkName='CAN IN (Platform)/Truck/RT7/RT1A_DIS_Unpack/p5';
bio(722).sigName='RT1_RelativeLateralSpeed';
bio(722).portIdx=4;
bio(722).dim=[1,1];
bio(722).sigWidth=1;
bio(722).sigAddress='&scoop_v3_B.RT1_RelativeLateralSpeed';
bio(722).ndims=2;
bio(722).size=[];


bio(723).blkName='CAN IN (Platform)/Truck/RT7/RT1A_DIS_Unpack/p6';
bio(723).sigName='RT1_RelativeLongSpeed';
bio(723).portIdx=5;
bio(723).dim=[1,1];
bio(723).sigWidth=1;
bio(723).sigAddress='&scoop_v3_B.RT1_RelativeLongSpeed';
bio(723).ndims=2;
bio(723).size=[];


bio(724).blkName='CAN IN (Platform)/Truck/RT7/RT1A_DIS_Unpack/p7';
bio(724).sigName='RT1_Type';
bio(724).portIdx=6;
bio(724).dim=[1,1];
bio(724).sigWidth=1;
bio(724).sigAddress='&scoop_v3_B.RT1_Type';
bio(724).ndims=2;
bio(724).size=[];


bio(725).blkName='CAN IN (Platform)/Truck/RT7/RT1B_DIS_Unpack/p1';
bio(725).sigName='RT1B_MsgCounter';
bio(725).portIdx=0;
bio(725).dim=[1,1];
bio(725).sigWidth=1;
bio(725).sigAddress='&scoop_v3_B.RT1B_MsgCounter';
bio(725).ndims=2;
bio(725).size=[];


bio(726).blkName='CAN IN (Platform)/Truck/RT7/RT1B_DIS_Unpack/p2';
bio(726).sigName='RT1_Lane';
bio(726).portIdx=1;
bio(726).dim=[1,1];
bio(726).sigWidth=1;
bio(726).sigAddress='&scoop_v3_B.RT1_Lane';
bio(726).ndims=2;
bio(726).size=[];


bio(727).blkName='CAN IN (Platform)/Truck/RT7/RT1B_DIS_Unpack/p3';
bio(727).sigName='RT1_LaneConfidence';
bio(727).portIdx=2;
bio(727).dim=[1,1];
bio(727).sigWidth=1;
bio(727).sigAddress='&scoop_v3_B.RT1_LaneConfidence';
bio(727).ndims=2;
bio(727).size=[];


bio(728).blkName='CAN IN (Platform)/Truck/RT7/RT1B_DIS_Unpack/p4';
bio(728).sigName='RT1_Movement';
bio(728).portIdx=3;
bio(728).dim=[1,1];
bio(728).sigWidth=1;
bio(728).sigAddress='&scoop_v3_B.RT1_Movement';
bio(728).ndims=2;
bio(728).size=[];


bio(729).blkName='CAN IN (Platform)/Truck/RT7/RT1B_DIS_Unpack/p5';
bio(729).sigName='RT1_TrackIDNumber';
bio(729).portIdx=4;
bio(729).dim=[1,1];
bio(729).sigWidth=1;
bio(729).sigAddress='&scoop_v3_B.RT1_TrackIDNumber';
bio(729).ndims=2;
bio(729).size=[];


bio(730).blkName='CAN IN (Platform)/Truck/RT7/RT1B_DIS_Unpack/p6';
bio(730).sigName='RT1_TrackStatus';
bio(730).portIdx=5;
bio(730).dim=[1,1];
bio(730).sigWidth=1;
bio(730).sigAddress='&scoop_v3_B.RT1_TrackStatus';
bio(730).ndims=2;
bio(730).size=[];


bio(731).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition29';
bio(731).sigName='RT2_LongDistance';
bio(731).portIdx=0;
bio(731).dim=[1,1];
bio(731).sigWidth=1;
bio(731).sigAddress='&scoop_v3_B.RT2_LongDistance_p';
bio(731).ndims=2;
bio(731).size=[];


bio(732).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition30';
bio(732).sigName='RT2A_MsgCounter';
bio(732).portIdx=0;
bio(732).dim=[1,1];
bio(732).sigWidth=1;
bio(732).sigAddress='&scoop_v3_B.RT2A_MsgCounter_m';
bio(732).ndims=2;
bio(732).size=[];


bio(733).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition31';
bio(733).sigName='RT2_RelativeLateralSpeed';
bio(733).portIdx=0;
bio(733).dim=[1,1];
bio(733).sigWidth=1;
bio(733).sigAddress='&scoop_v3_B.RT2_RelativeLateralSpeed_j';
bio(733).ndims=2;
bio(733).size=[];


bio(734).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition32';
bio(734).sigName='RT2_Type';
bio(734).portIdx=0;
bio(734).dim=[1,1];
bio(734).sigWidth=1;
bio(734).sigAddress='&scoop_v3_B.RT2_Type_l';
bio(734).ndims=2;
bio(734).size=[];


bio(735).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition33';
bio(735).sigName='RT2_RelativeLongSpeed';
bio(735).portIdx=0;
bio(735).dim=[1,1];
bio(735).sigWidth=1;
bio(735).sigAddress='&scoop_v3_B.RT2_RelativeLongSpeed_a';
bio(735).ndims=2;
bio(735).size=[];


bio(736).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition34';
bio(736).sigName='RT2B_MsgCounter';
bio(736).portIdx=0;
bio(736).dim=[1,1];
bio(736).sigWidth=1;
bio(736).sigAddress='&scoop_v3_B.RT2B_MsgCounter_i';
bio(736).ndims=2;
bio(736).size=[];


bio(737).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition35';
bio(737).sigName='RT2_Lane';
bio(737).portIdx=0;
bio(737).dim=[1,1];
bio(737).sigWidth=1;
bio(737).sigAddress='&scoop_v3_B.RT2_Lane_g';
bio(737).ndims=2;
bio(737).size=[];


bio(738).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition36';
bio(738).sigName='RT2_LaneConfidence';
bio(738).portIdx=0;
bio(738).dim=[1,1];
bio(738).sigWidth=1;
bio(738).sigAddress='&scoop_v3_B.RT2_LaneConfidence_p';
bio(738).ndims=2;
bio(738).size=[];


bio(739).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition37';
bio(739).sigName='RT2_Movement';
bio(739).portIdx=0;
bio(739).dim=[1,1];
bio(739).sigWidth=1;
bio(739).sigAddress='&scoop_v3_B.RT2_Movement_p';
bio(739).ndims=2;
bio(739).size=[];


bio(740).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition38';
bio(740).sigName='RT2_TrackIDNumber';
bio(740).portIdx=0;
bio(740).dim=[1,1];
bio(740).sigWidth=1;
bio(740).sigAddress='&scoop_v3_B.RT2_TrackIDNumber_h';
bio(740).ndims=2;
bio(740).size=[];


bio(741).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition39';
bio(741).sigName='RT2_TrackStatus';
bio(741).portIdx=0;
bio(741).dim=[1,1];
bio(741).sigWidth=1;
bio(741).sigAddress='&scoop_v3_B.RT2_TrackStatus_a';
bio(741).ndims=2;
bio(741).size=[];


bio(742).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition40';
bio(742).sigName='RT2_LateralDistance';
bio(742).portIdx=0;
bio(742).dim=[1,1];
bio(742).sigWidth=1;
bio(742).sigAddress='&scoop_v3_B.RT2_LateralDistance_j';
bio(742).ndims=2;
bio(742).size=[];


bio(743).blkName='CAN IN (Platform)/Truck/RT8/Rate Transition42';
bio(743).sigName='RT2_RelLongAcceleration';
bio(743).portIdx=0;
bio(743).dim=[1,1];
bio(743).sigWidth=1;
bio(743).sigAddress='&scoop_v3_B.RT2_RelLongAcceleration_d';
bio(743).ndims=2;
bio(743).size=[];


bio(744).blkName='CAN IN (Platform)/Truck/RT8/RT2A_DIS_Unpack/p1';
bio(744).sigName='RT2A_MsgCounter';
bio(744).portIdx=0;
bio(744).dim=[1,1];
bio(744).sigWidth=1;
bio(744).sigAddress='&scoop_v3_B.RT2A_MsgCounter';
bio(744).ndims=2;
bio(744).size=[];


bio(745).blkName='CAN IN (Platform)/Truck/RT8/RT2A_DIS_Unpack/p2';
bio(745).sigName='RT2_LateralDistance';
bio(745).portIdx=1;
bio(745).dim=[1,1];
bio(745).sigWidth=1;
bio(745).sigAddress='&scoop_v3_B.RT2_LateralDistance';
bio(745).ndims=2;
bio(745).size=[];


bio(746).blkName='CAN IN (Platform)/Truck/RT8/RT2A_DIS_Unpack/p3';
bio(746).sigName='RT2_LongDistance';
bio(746).portIdx=2;
bio(746).dim=[1,1];
bio(746).sigWidth=1;
bio(746).sigAddress='&scoop_v3_B.RT2_LongDistance';
bio(746).ndims=2;
bio(746).size=[];


bio(747).blkName='CAN IN (Platform)/Truck/RT8/RT2A_DIS_Unpack/p4';
bio(747).sigName='RT2_RelLongAcceleration';
bio(747).portIdx=3;
bio(747).dim=[1,1];
bio(747).sigWidth=1;
bio(747).sigAddress='&scoop_v3_B.RT2_RelLongAcceleration';
bio(747).ndims=2;
bio(747).size=[];


bio(748).blkName='CAN IN (Platform)/Truck/RT8/RT2A_DIS_Unpack/p5';
bio(748).sigName='RT2_RelativeLateralSpeed';
bio(748).portIdx=4;
bio(748).dim=[1,1];
bio(748).sigWidth=1;
bio(748).sigAddress='&scoop_v3_B.RT2_RelativeLateralSpeed';
bio(748).ndims=2;
bio(748).size=[];


bio(749).blkName='CAN IN (Platform)/Truck/RT8/RT2A_DIS_Unpack/p6';
bio(749).sigName='RT2_RelativeLongSpeed';
bio(749).portIdx=5;
bio(749).dim=[1,1];
bio(749).sigWidth=1;
bio(749).sigAddress='&scoop_v3_B.RT2_RelativeLongSpeed';
bio(749).ndims=2;
bio(749).size=[];


bio(750).blkName='CAN IN (Platform)/Truck/RT8/RT2A_DIS_Unpack/p7';
bio(750).sigName='RT2_Type';
bio(750).portIdx=6;
bio(750).dim=[1,1];
bio(750).sigWidth=1;
bio(750).sigAddress='&scoop_v3_B.RT2_Type';
bio(750).ndims=2;
bio(750).size=[];


bio(751).blkName='CAN IN (Platform)/Truck/RT8/RT2B_DIS_Unpack/p1';
bio(751).sigName='RT2B_MsgCounter';
bio(751).portIdx=0;
bio(751).dim=[1,1];
bio(751).sigWidth=1;
bio(751).sigAddress='&scoop_v3_B.RT2B_MsgCounter';
bio(751).ndims=2;
bio(751).size=[];


bio(752).blkName='CAN IN (Platform)/Truck/RT8/RT2B_DIS_Unpack/p2';
bio(752).sigName='RT2_Lane';
bio(752).portIdx=1;
bio(752).dim=[1,1];
bio(752).sigWidth=1;
bio(752).sigAddress='&scoop_v3_B.RT2_Lane';
bio(752).ndims=2;
bio(752).size=[];


bio(753).blkName='CAN IN (Platform)/Truck/RT8/RT2B_DIS_Unpack/p3';
bio(753).sigName='RT2_LaneConfidence';
bio(753).portIdx=2;
bio(753).dim=[1,1];
bio(753).sigWidth=1;
bio(753).sigAddress='&scoop_v3_B.RT2_LaneConfidence';
bio(753).ndims=2;
bio(753).size=[];


bio(754).blkName='CAN IN (Platform)/Truck/RT8/RT2B_DIS_Unpack/p4';
bio(754).sigName='RT2_Movement';
bio(754).portIdx=3;
bio(754).dim=[1,1];
bio(754).sigWidth=1;
bio(754).sigAddress='&scoop_v3_B.RT2_Movement';
bio(754).ndims=2;
bio(754).size=[];


bio(755).blkName='CAN IN (Platform)/Truck/RT8/RT2B_DIS_Unpack/p5';
bio(755).sigName='RT2_TrackIDNumber';
bio(755).portIdx=4;
bio(755).dim=[1,1];
bio(755).sigWidth=1;
bio(755).sigAddress='&scoop_v3_B.RT2_TrackIDNumber';
bio(755).ndims=2;
bio(755).size=[];


bio(756).blkName='CAN IN (Platform)/Truck/RT8/RT2B_DIS_Unpack/p6';
bio(756).sigName='RT2_TrackStatus';
bio(756).portIdx=5;
bio(756).dim=[1,1];
bio(756).sigWidth=1;
bio(756).sigAddress='&scoop_v3_B.RT2_TrackStatus';
bio(756).ndims=2;
bio(756).size=[];


bio(757).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition43';
bio(757).sigName='RT3_RelativeLateralSpeed';
bio(757).portIdx=0;
bio(757).dim=[1,1];
bio(757).sigWidth=1;
bio(757).sigAddress='&scoop_v3_B.RT3_RelativeLateralSpeed_i';
bio(757).ndims=2;
bio(757).size=[];


bio(758).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition44';
bio(758).sigName='RT3_Type';
bio(758).portIdx=0;
bio(758).dim=[1,1];
bio(758).sigWidth=1;
bio(758).sigAddress='&scoop_v3_B.RT3_Type_o';
bio(758).ndims=2;
bio(758).size=[];


bio(759).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition45';
bio(759).sigName='RT3_RelativeLongSpeed';
bio(759).portIdx=0;
bio(759).dim=[1,1];
bio(759).sigWidth=1;
bio(759).sigAddress='&scoop_v3_B.RT3_RelativeLongSpeed_j';
bio(759).ndims=2;
bio(759).size=[];


bio(760).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition46';
bio(760).sigName='RT3B_MsgCounter';
bio(760).portIdx=0;
bio(760).dim=[1,1];
bio(760).sigWidth=1;
bio(760).sigAddress='&scoop_v3_B.RT3B_MsgCounter_n';
bio(760).ndims=2;
bio(760).size=[];


bio(761).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition47';
bio(761).sigName='RT3_Lane';
bio(761).portIdx=0;
bio(761).dim=[1,1];
bio(761).sigWidth=1;
bio(761).sigAddress='&scoop_v3_B.RT3_Lane_d';
bio(761).ndims=2;
bio(761).size=[];


bio(762).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition48';
bio(762).sigName='RT3_LaneConfidence';
bio(762).portIdx=0;
bio(762).dim=[1,1];
bio(762).sigWidth=1;
bio(762).sigAddress='&scoop_v3_B.RT3_LaneConfidence_j';
bio(762).ndims=2;
bio(762).size=[];


bio(763).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition49';
bio(763).sigName='RT3_Movement';
bio(763).portIdx=0;
bio(763).dim=[1,1];
bio(763).sigWidth=1;
bio(763).sigAddress='&scoop_v3_B.RT3_Movement_g';
bio(763).ndims=2;
bio(763).size=[];


bio(764).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition50';
bio(764).sigName='RT3_TrackIDNumber';
bio(764).portIdx=0;
bio(764).dim=[1,1];
bio(764).sigWidth=1;
bio(764).sigAddress='&scoop_v3_B.RT3_TrackIDNumber_c';
bio(764).ndims=2;
bio(764).size=[];


bio(765).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition51';
bio(765).sigName='RT3_TrackStatus';
bio(765).portIdx=0;
bio(765).dim=[1,1];
bio(765).sigWidth=1;
bio(765).sigAddress='&scoop_v3_B.RT3_TrackStatus_n';
bio(765).ndims=2;
bio(765).size=[];


bio(766).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition52';
bio(766).sigName='RT3_LateralDistance';
bio(766).portIdx=0;
bio(766).dim=[1,1];
bio(766).sigWidth=1;
bio(766).sigAddress='&scoop_v3_B.RT3_LateralDistance_f';
bio(766).ndims=2;
bio(766).size=[];


bio(767).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition53';
bio(767).sigName='RT3_LongDistance';
bio(767).portIdx=0;
bio(767).dim=[1,1];
bio(767).sigWidth=1;
bio(767).sigAddress='&scoop_v3_B.RT3_LongDistance_h';
bio(767).ndims=2;
bio(767).size=[];


bio(768).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition54';
bio(768).sigName='RT3_RelLongAcceleration';
bio(768).portIdx=0;
bio(768).dim=[1,1];
bio(768).sigWidth=1;
bio(768).sigAddress='&scoop_v3_B.RT3_RelLongAcceleration_i';
bio(768).ndims=2;
bio(768).size=[];


bio(769).blkName='CAN IN (Platform)/Truck/RT9/Rate Transition55';
bio(769).sigName='RT3A_MsgCounter';
bio(769).portIdx=0;
bio(769).dim=[1,1];
bio(769).sigWidth=1;
bio(769).sigAddress='&scoop_v3_B.RT3A_MsgCounter_n';
bio(769).ndims=2;
bio(769).size=[];


bio(770).blkName='CAN IN (Platform)/Truck/RT9/RT3A_DIS_Unpack/p1';
bio(770).sigName='RT3A_MsgCounter';
bio(770).portIdx=0;
bio(770).dim=[1,1];
bio(770).sigWidth=1;
bio(770).sigAddress='&scoop_v3_B.RT3A_MsgCounter';
bio(770).ndims=2;
bio(770).size=[];


bio(771).blkName='CAN IN (Platform)/Truck/RT9/RT3A_DIS_Unpack/p2';
bio(771).sigName='RT3_LateralDistance';
bio(771).portIdx=1;
bio(771).dim=[1,1];
bio(771).sigWidth=1;
bio(771).sigAddress='&scoop_v3_B.RT3_LateralDistance';
bio(771).ndims=2;
bio(771).size=[];


bio(772).blkName='CAN IN (Platform)/Truck/RT9/RT3A_DIS_Unpack/p3';
bio(772).sigName='RT3_LongDistance';
bio(772).portIdx=2;
bio(772).dim=[1,1];
bio(772).sigWidth=1;
bio(772).sigAddress='&scoop_v3_B.RT3_LongDistance';
bio(772).ndims=2;
bio(772).size=[];


bio(773).blkName='CAN IN (Platform)/Truck/RT9/RT3A_DIS_Unpack/p4';
bio(773).sigName='RT3_RelLongAcceleration';
bio(773).portIdx=3;
bio(773).dim=[1,1];
bio(773).sigWidth=1;
bio(773).sigAddress='&scoop_v3_B.RT3_RelLongAcceleration';
bio(773).ndims=2;
bio(773).size=[];


bio(774).blkName='CAN IN (Platform)/Truck/RT9/RT3A_DIS_Unpack/p5';
bio(774).sigName='RT3_RelativeLateralSpeed';
bio(774).portIdx=4;
bio(774).dim=[1,1];
bio(774).sigWidth=1;
bio(774).sigAddress='&scoop_v3_B.RT3_RelativeLateralSpeed';
bio(774).ndims=2;
bio(774).size=[];


bio(775).blkName='CAN IN (Platform)/Truck/RT9/RT3A_DIS_Unpack/p6';
bio(775).sigName='RT3_RelativeLongSpeed';
bio(775).portIdx=5;
bio(775).dim=[1,1];
bio(775).sigWidth=1;
bio(775).sigAddress='&scoop_v3_B.RT3_RelativeLongSpeed';
bio(775).ndims=2;
bio(775).size=[];


bio(776).blkName='CAN IN (Platform)/Truck/RT9/RT3A_DIS_Unpack/p7';
bio(776).sigName='RT3_Type';
bio(776).portIdx=6;
bio(776).dim=[1,1];
bio(776).sigWidth=1;
bio(776).sigAddress='&scoop_v3_B.RT3_Type';
bio(776).ndims=2;
bio(776).size=[];


bio(777).blkName='CAN IN (Platform)/Truck/RT9/RT3B_DIS_Unpack/p1';
bio(777).sigName='RT3B_MsgCounter';
bio(777).portIdx=0;
bio(777).dim=[1,1];
bio(777).sigWidth=1;
bio(777).sigAddress='&scoop_v3_B.RT3B_MsgCounter';
bio(777).ndims=2;
bio(777).size=[];


bio(778).blkName='CAN IN (Platform)/Truck/RT9/RT3B_DIS_Unpack/p2';
bio(778).sigName='RT3_Lane';
bio(778).portIdx=1;
bio(778).dim=[1,1];
bio(778).sigWidth=1;
bio(778).sigAddress='&scoop_v3_B.RT3_Lane';
bio(778).ndims=2;
bio(778).size=[];


bio(779).blkName='CAN IN (Platform)/Truck/RT9/RT3B_DIS_Unpack/p3';
bio(779).sigName='RT3_LaneConfidence';
bio(779).portIdx=2;
bio(779).dim=[1,1];
bio(779).sigWidth=1;
bio(779).sigAddress='&scoop_v3_B.RT3_LaneConfidence';
bio(779).ndims=2;
bio(779).size=[];


bio(780).blkName='CAN IN (Platform)/Truck/RT9/RT3B_DIS_Unpack/p4';
bio(780).sigName='RT3_Movement';
bio(780).portIdx=3;
bio(780).dim=[1,1];
bio(780).sigWidth=1;
bio(780).sigAddress='&scoop_v3_B.RT3_Movement';
bio(780).ndims=2;
bio(780).size=[];


bio(781).blkName='CAN IN (Platform)/Truck/RT9/RT3B_DIS_Unpack/p5';
bio(781).sigName='RT3_TrackIDNumber';
bio(781).portIdx=4;
bio(781).dim=[1,1];
bio(781).sigWidth=1;
bio(781).sigAddress='&scoop_v3_B.RT3_TrackIDNumber';
bio(781).ndims=2;
bio(781).size=[];


bio(782).blkName='CAN IN (Platform)/Truck/RT9/RT3B_DIS_Unpack/p6';
bio(782).sigName='RT3_TrackStatus';
bio(782).portIdx=5;
bio(782).dim=[1,1];
bio(782).sigWidth=1;
bio(782).sigAddress='&scoop_v3_B.RT3_TrackStatus';
bio(782).ndims=2;
bio(782).size=[];


bio(783).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/Rate Transition1';
bio(783).sigName='RTS1_Lane';
bio(783).portIdx=0;
bio(783).dim=[1,1];
bio(783).sigWidth=1;
bio(783).sigAddress='&scoop_v3_B.RTS1_Lane_i';
bio(783).ndims=2;
bio(783).size=[];


bio(784).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/Rate Transition2';
bio(784).sigName='RTS1_MsgCounter';
bio(784).portIdx=0;
bio(784).dim=[1,1];
bio(784).sigWidth=1;
bio(784).sigAddress='&scoop_v3_B.RTS1_MsgCounter_i';
bio(784).ndims=2;
bio(784).size=[];


bio(785).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/Rate Transition3';
bio(785).sigName='RTS1_TrackStatus';
bio(785).portIdx=0;
bio(785).dim=[1,1];
bio(785).sigWidth=1;
bio(785).sigAddress='&scoop_v3_B.RTS1_TrackStatus_n';
bio(785).ndims=2;
bio(785).size=[];


bio(786).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/Rate Transition4';
bio(786).sigName='RTS1_Type';
bio(786).portIdx=0;
bio(786).dim=[1,1];
bio(786).sigWidth=1;
bio(786).sigAddress='&scoop_v3_B.RTS1_Type_c';
bio(786).ndims=2;
bio(786).size=[];


bio(787).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/Rate Transition45';
bio(787).sigName='RTS1_TrackIDNumber';
bio(787).portIdx=0;
bio(787).dim=[1,1];
bio(787).sigWidth=1;
bio(787).sigAddress='&scoop_v3_B.RTS1_TrackIDNumber';
bio(787).ndims=2;
bio(787).size=[];


bio(788).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/Rate Transition52';
bio(788).sigName='RTS1_LaneConfidence';
bio(788).portIdx=0;
bio(788).dim=[1,1];
bio(788).sigWidth=1;
bio(788).sigAddress='&scoop_v3_B.RTS1_LaneConfidence_o';
bio(788).ndims=2;
bio(788).size=[];


bio(789).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/Rate Transition53';
bio(789).sigName='RTS1_LateralDistance';
bio(789).portIdx=0;
bio(789).dim=[1,1];
bio(789).sigWidth=1;
bio(789).sigAddress='&scoop_v3_B.RTS1_LateralDistance_a';
bio(789).ndims=2;
bio(789).size=[];


bio(790).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/Rate Transition54';
bio(790).sigName='RTS1_LongDistance';
bio(790).portIdx=0;
bio(790).dim=[1,1];
bio(790).sigWidth=1;
bio(790).sigAddress='&scoop_v3_B.RTS1_LongDistance_b';
bio(790).ndims=2;
bio(790).size=[];


bio(791).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/RT3A_DIS_Unpack/p1';
bio(791).sigName='RTS1_Lane';
bio(791).portIdx=0;
bio(791).dim=[1,1];
bio(791).sigWidth=1;
bio(791).sigAddress='&scoop_v3_B.RTS1_Lane';
bio(791).ndims=2;
bio(791).size=[];


bio(792).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/RT3A_DIS_Unpack/p2';
bio(792).sigName='RTS1_LaneConfidence';
bio(792).portIdx=1;
bio(792).dim=[1,1];
bio(792).sigWidth=1;
bio(792).sigAddress='&scoop_v3_B.RTS1_LaneConfidence';
bio(792).ndims=2;
bio(792).size=[];


bio(793).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/RT3A_DIS_Unpack/p3';
bio(793).sigName='RTS1_LateralDistance';
bio(793).portIdx=2;
bio(793).dim=[1,1];
bio(793).sigWidth=1;
bio(793).sigAddress='&scoop_v3_B.RTS1_LateralDistance';
bio(793).ndims=2;
bio(793).size=[];


bio(794).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/RT3A_DIS_Unpack/p4';
bio(794).sigName='RTS1_LongDistance';
bio(794).portIdx=3;
bio(794).dim=[1,1];
bio(794).sigWidth=1;
bio(794).sigAddress='&scoop_v3_B.RTS1_LongDistance';
bio(794).ndims=2;
bio(794).size=[];


bio(795).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/RT3A_DIS_Unpack/p5';
bio(795).sigName='RTS1_MsgCounter';
bio(795).portIdx=4;
bio(795).dim=[1,1];
bio(795).sigWidth=1;
bio(795).sigAddress='&scoop_v3_B.RTS1_MsgCounter';
bio(795).ndims=2;
bio(795).size=[];


bio(796).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/RT3A_DIS_Unpack/p6';
bio(796).sigName='RTS1_TrackNumber';
bio(796).portIdx=5;
bio(796).dim=[1,1];
bio(796).sigWidth=1;
bio(796).sigAddress='&scoop_v3_B.RTS1_TrackNumber';
bio(796).ndims=2;
bio(796).size=[];


bio(797).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/RT3A_DIS_Unpack/p7';
bio(797).sigName='RTS1_TrackStatus';
bio(797).portIdx=6;
bio(797).dim=[1,1];
bio(797).sigWidth=1;
bio(797).sigAddress='&scoop_v3_B.RTS1_TrackStatus';
bio(797).ndims=2;
bio(797).size=[];


bio(798).blkName='CAN IN (Platform)/Truck/RTS1_Unpack1/RT3A_DIS_Unpack/p8';
bio(798).sigName='RTS1_Type';
bio(798).portIdx=7;
bio(798).dim=[1,1];
bio(798).sigWidth=1;
bio(798).sigAddress='&scoop_v3_B.RTS1_Type';
bio(798).ndims=2;
bio(798).size=[];


bio(799).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/Rate Transition1';
bio(799).sigName='RTS2_Lane';
bio(799).portIdx=0;
bio(799).dim=[1,1];
bio(799).sigWidth=1;
bio(799).sigAddress='&scoop_v3_B.RTS2_Lane_h';
bio(799).ndims=2;
bio(799).size=[];


bio(800).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/Rate Transition2';
bio(800).sigName='RTS2_MsgCounter';
bio(800).portIdx=0;
bio(800).dim=[1,1];
bio(800).sigWidth=1;
bio(800).sigAddress='&scoop_v3_B.RTS2_MsgCounter_o';
bio(800).ndims=2;
bio(800).size=[];


bio(801).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/Rate Transition3';
bio(801).sigName='RTS2_TrackStatus';
bio(801).portIdx=0;
bio(801).dim=[1,1];
bio(801).sigWidth=1;
bio(801).sigAddress='&scoop_v3_B.RTS2_TrackStatus_k';
bio(801).ndims=2;
bio(801).size=[];


bio(802).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/Rate Transition4';
bio(802).sigName='RTS2_Type';
bio(802).portIdx=0;
bio(802).dim=[1,1];
bio(802).sigWidth=1;
bio(802).sigAddress='&scoop_v3_B.RTS2_Type_c';
bio(802).ndims=2;
bio(802).size=[];


bio(803).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/Rate Transition45';
bio(803).sigName='RTS2_TrackIDNumber';
bio(803).portIdx=0;
bio(803).dim=[1,1];
bio(803).sigWidth=1;
bio(803).sigAddress='&scoop_v3_B.RTS2_TrackIDNumber';
bio(803).ndims=2;
bio(803).size=[];


bio(804).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/Rate Transition52';
bio(804).sigName='RTS2_LaneConfidence';
bio(804).portIdx=0;
bio(804).dim=[1,1];
bio(804).sigWidth=1;
bio(804).sigAddress='&scoop_v3_B.RTS2_LaneConfidence_o';
bio(804).ndims=2;
bio(804).size=[];


bio(805).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/Rate Transition53';
bio(805).sigName='RTS2_LateralDistance';
bio(805).portIdx=0;
bio(805).dim=[1,1];
bio(805).sigWidth=1;
bio(805).sigAddress='&scoop_v3_B.RTS2_LateralDistance_h';
bio(805).ndims=2;
bio(805).size=[];


bio(806).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/Rate Transition54';
bio(806).sigName='RTS2_LongDistance';
bio(806).portIdx=0;
bio(806).dim=[1,1];
bio(806).sigWidth=1;
bio(806).sigAddress='&scoop_v3_B.RTS2_LongDistance_l';
bio(806).ndims=2;
bio(806).size=[];


bio(807).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/RT3A_DIS_Unpack/p1';
bio(807).sigName='RTS2_Lane';
bio(807).portIdx=0;
bio(807).dim=[1,1];
bio(807).sigWidth=1;
bio(807).sigAddress='&scoop_v3_B.RTS2_Lane';
bio(807).ndims=2;
bio(807).size=[];


bio(808).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/RT3A_DIS_Unpack/p2';
bio(808).sigName='RTS2_LaneConfidence';
bio(808).portIdx=1;
bio(808).dim=[1,1];
bio(808).sigWidth=1;
bio(808).sigAddress='&scoop_v3_B.RTS2_LaneConfidence';
bio(808).ndims=2;
bio(808).size=[];


bio(809).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/RT3A_DIS_Unpack/p3';
bio(809).sigName='RTS2_LateralDistance';
bio(809).portIdx=2;
bio(809).dim=[1,1];
bio(809).sigWidth=1;
bio(809).sigAddress='&scoop_v3_B.RTS2_LateralDistance';
bio(809).ndims=2;
bio(809).size=[];


bio(810).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/RT3A_DIS_Unpack/p4';
bio(810).sigName='RTS2_LongDistance';
bio(810).portIdx=3;
bio(810).dim=[1,1];
bio(810).sigWidth=1;
bio(810).sigAddress='&scoop_v3_B.RTS2_LongDistance';
bio(810).ndims=2;
bio(810).size=[];


bio(811).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/RT3A_DIS_Unpack/p5';
bio(811).sigName='RTS2_MsgCounter';
bio(811).portIdx=4;
bio(811).dim=[1,1];
bio(811).sigWidth=1;
bio(811).sigAddress='&scoop_v3_B.RTS2_MsgCounter';
bio(811).ndims=2;
bio(811).size=[];


bio(812).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/RT3A_DIS_Unpack/p6';
bio(812).sigName='RTS2_TrackNumber';
bio(812).portIdx=5;
bio(812).dim=[1,1];
bio(812).sigWidth=1;
bio(812).sigAddress='&scoop_v3_B.RTS2_TrackNumber';
bio(812).ndims=2;
bio(812).size=[];


bio(813).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/RT3A_DIS_Unpack/p7';
bio(813).sigName='RTS2_TrackStatus';
bio(813).portIdx=6;
bio(813).dim=[1,1];
bio(813).sigWidth=1;
bio(813).sigAddress='&scoop_v3_B.RTS2_TrackStatus';
bio(813).ndims=2;
bio(813).size=[];


bio(814).blkName='CAN IN (Platform)/Truck/RTS2_Unpack1/RT3A_DIS_Unpack/p8';
bio(814).sigName='RTS2_Type';
bio(814).portIdx=7;
bio(814).dim=[1,1];
bio(814).sigWidth=1;
bio(814).sigAddress='&scoop_v3_B.RTS2_Type';
bio(814).ndims=2;
bio(814).size=[];


bio(815).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/Rate Transition1';
bio(815).sigName='RTS3_Lane';
bio(815).portIdx=0;
bio(815).dim=[1,1];
bio(815).sigWidth=1;
bio(815).sigAddress='&scoop_v3_B.RTS3_Lane_n';
bio(815).ndims=2;
bio(815).size=[];


bio(816).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/Rate Transition2';
bio(816).sigName='RTS3_MsgCounter';
bio(816).portIdx=0;
bio(816).dim=[1,1];
bio(816).sigWidth=1;
bio(816).sigAddress='&scoop_v3_B.RTS3_MsgCounter_n';
bio(816).ndims=2;
bio(816).size=[];


bio(817).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/Rate Transition3';
bio(817).sigName='RTS3_TrackStatus';
bio(817).portIdx=0;
bio(817).dim=[1,1];
bio(817).sigWidth=1;
bio(817).sigAddress='&scoop_v3_B.RTS3_TrackStatus_b';
bio(817).ndims=2;
bio(817).size=[];


bio(818).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/Rate Transition4';
bio(818).sigName='RTS2_Type';
bio(818).portIdx=0;
bio(818).dim=[1,1];
bio(818).sigWidth=1;
bio(818).sigAddress='&scoop_v3_B.RTS2_Type_e';
bio(818).ndims=2;
bio(818).size=[];


bio(819).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/Rate Transition45';
bio(819).sigName='RTS3_TrackIDNumber';
bio(819).portIdx=0;
bio(819).dim=[1,1];
bio(819).sigWidth=1;
bio(819).sigAddress='&scoop_v3_B.RTS3_TrackIDNumber';
bio(819).ndims=2;
bio(819).size=[];


bio(820).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/Rate Transition52';
bio(820).sigName='RTS3_LaneConfidence';
bio(820).portIdx=0;
bio(820).dim=[1,1];
bio(820).sigWidth=1;
bio(820).sigAddress='&scoop_v3_B.RTS3_LaneConfidence_c';
bio(820).ndims=2;
bio(820).size=[];


bio(821).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/Rate Transition53';
bio(821).sigName='RTS3_LateralDistance';
bio(821).portIdx=0;
bio(821).dim=[1,1];
bio(821).sigWidth=1;
bio(821).sigAddress='&scoop_v3_B.RTS3_LateralDistance_a';
bio(821).ndims=2;
bio(821).size=[];


bio(822).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/Rate Transition54';
bio(822).sigName='RTS3_LongDistance';
bio(822).portIdx=0;
bio(822).dim=[1,1];
bio(822).sigWidth=1;
bio(822).sigAddress='&scoop_v3_B.RTS3_LongDistance_i';
bio(822).ndims=2;
bio(822).size=[];


bio(823).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/RT3A_DIS_Unpack/p1';
bio(823).sigName='RTS3_Lane';
bio(823).portIdx=0;
bio(823).dim=[1,1];
bio(823).sigWidth=1;
bio(823).sigAddress='&scoop_v3_B.RTS3_Lane';
bio(823).ndims=2;
bio(823).size=[];


bio(824).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/RT3A_DIS_Unpack/p2';
bio(824).sigName='RTS3_LaneConfidence';
bio(824).portIdx=1;
bio(824).dim=[1,1];
bio(824).sigWidth=1;
bio(824).sigAddress='&scoop_v3_B.RTS3_LaneConfidence';
bio(824).ndims=2;
bio(824).size=[];


bio(825).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/RT3A_DIS_Unpack/p3';
bio(825).sigName='RTS3_LateralDistance';
bio(825).portIdx=2;
bio(825).dim=[1,1];
bio(825).sigWidth=1;
bio(825).sigAddress='&scoop_v3_B.RTS3_LateralDistance';
bio(825).ndims=2;
bio(825).size=[];


bio(826).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/RT3A_DIS_Unpack/p4';
bio(826).sigName='RTS3_LongDistance';
bio(826).portIdx=3;
bio(826).dim=[1,1];
bio(826).sigWidth=1;
bio(826).sigAddress='&scoop_v3_B.RTS3_LongDistance';
bio(826).ndims=2;
bio(826).size=[];


bio(827).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/RT3A_DIS_Unpack/p5';
bio(827).sigName='RTS3_MsgCounter';
bio(827).portIdx=4;
bio(827).dim=[1,1];
bio(827).sigWidth=1;
bio(827).sigAddress='&scoop_v3_B.RTS3_MsgCounter';
bio(827).ndims=2;
bio(827).size=[];


bio(828).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/RT3A_DIS_Unpack/p6';
bio(828).sigName='RTS3_TrackNumber';
bio(828).portIdx=5;
bio(828).dim=[1,1];
bio(828).sigWidth=1;
bio(828).sigAddress='&scoop_v3_B.RTS3_TrackNumber';
bio(828).ndims=2;
bio(828).size=[];


bio(829).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/RT3A_DIS_Unpack/p7';
bio(829).sigName='RTS3_TrackStatus';
bio(829).portIdx=6;
bio(829).dim=[1,1];
bio(829).sigWidth=1;
bio(829).sigAddress='&scoop_v3_B.RTS3_TrackStatus';
bio(829).ndims=2;
bio(829).size=[];


bio(830).blkName='CAN IN (Platform)/Truck/RTS3_Unpack1/RT3A_DIS_Unpack/p8';
bio(830).sigName='RTS3_Type';
bio(830).portIdx=7;
bio(830).dim=[1,1];
bio(830).sigWidth=1;
bio(830).sigAddress='&scoop_v3_B.RTS3_Type';
bio(830).ndims=2;
bio(830).size=[];


bio(831).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/Rate Transition1';
bio(831).sigName='RTS4_Lane';
bio(831).portIdx=0;
bio(831).dim=[1,1];
bio(831).sigWidth=1;
bio(831).sigAddress='&scoop_v3_B.RTS4_Lane_n';
bio(831).ndims=2;
bio(831).size=[];


bio(832).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/Rate Transition2';
bio(832).sigName='RTS4_MsgCounter';
bio(832).portIdx=0;
bio(832).dim=[1,1];
bio(832).sigWidth=1;
bio(832).sigAddress='&scoop_v3_B.RTS4_MsgCounter_h';
bio(832).ndims=2;
bio(832).size=[];


bio(833).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/Rate Transition3';
bio(833).sigName='RTS4_TrackStatus';
bio(833).portIdx=0;
bio(833).dim=[1,1];
bio(833).sigWidth=1;
bio(833).sigAddress='&scoop_v3_B.RTS4_TrackStatus_p';
bio(833).ndims=2;
bio(833).size=[];


bio(834).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/Rate Transition4';
bio(834).sigName='RTS4_Type';
bio(834).portIdx=0;
bio(834).dim=[1,1];
bio(834).sigWidth=1;
bio(834).sigAddress='&scoop_v3_B.RTS4_Type_j';
bio(834).ndims=2;
bio(834).size=[];


bio(835).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/Rate Transition45';
bio(835).sigName='RTS4_TrackIDNumber';
bio(835).portIdx=0;
bio(835).dim=[1,1];
bio(835).sigWidth=1;
bio(835).sigAddress='&scoop_v3_B.RTS4_TrackIDNumber';
bio(835).ndims=2;
bio(835).size=[];


bio(836).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/Rate Transition52';
bio(836).sigName='RTS4_LaneConfidence';
bio(836).portIdx=0;
bio(836).dim=[1,1];
bio(836).sigWidth=1;
bio(836).sigAddress='&scoop_v3_B.RTS4_LaneConfidence_d';
bio(836).ndims=2;
bio(836).size=[];


bio(837).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/Rate Transition53';
bio(837).sigName='RTS4_LateralDistance';
bio(837).portIdx=0;
bio(837).dim=[1,1];
bio(837).sigWidth=1;
bio(837).sigAddress='&scoop_v3_B.RTS4_LateralDistance_b';
bio(837).ndims=2;
bio(837).size=[];


bio(838).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/Rate Transition54';
bio(838).sigName='RTS4_LongDistance';
bio(838).portIdx=0;
bio(838).dim=[1,1];
bio(838).sigWidth=1;
bio(838).sigAddress='&scoop_v3_B.RTS4_LongDistance_a';
bio(838).ndims=2;
bio(838).size=[];


bio(839).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/RT3A_DIS_Unpack/p1';
bio(839).sigName='RTS4_Lane';
bio(839).portIdx=0;
bio(839).dim=[1,1];
bio(839).sigWidth=1;
bio(839).sigAddress='&scoop_v3_B.RTS4_Lane';
bio(839).ndims=2;
bio(839).size=[];


bio(840).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/RT3A_DIS_Unpack/p2';
bio(840).sigName='RTS4_LaneConfidence';
bio(840).portIdx=1;
bio(840).dim=[1,1];
bio(840).sigWidth=1;
bio(840).sigAddress='&scoop_v3_B.RTS4_LaneConfidence';
bio(840).ndims=2;
bio(840).size=[];


bio(841).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/RT3A_DIS_Unpack/p3';
bio(841).sigName='RTS4_LateralDistance';
bio(841).portIdx=2;
bio(841).dim=[1,1];
bio(841).sigWidth=1;
bio(841).sigAddress='&scoop_v3_B.RTS4_LateralDistance';
bio(841).ndims=2;
bio(841).size=[];


bio(842).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/RT3A_DIS_Unpack/p4';
bio(842).sigName='RTS4_LongDistance';
bio(842).portIdx=3;
bio(842).dim=[1,1];
bio(842).sigWidth=1;
bio(842).sigAddress='&scoop_v3_B.RTS4_LongDistance';
bio(842).ndims=2;
bio(842).size=[];


bio(843).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/RT3A_DIS_Unpack/p5';
bio(843).sigName='RTS4_MsgCounter';
bio(843).portIdx=4;
bio(843).dim=[1,1];
bio(843).sigWidth=1;
bio(843).sigAddress='&scoop_v3_B.RTS4_MsgCounter';
bio(843).ndims=2;
bio(843).size=[];


bio(844).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/RT3A_DIS_Unpack/p6';
bio(844).sigName='RTS4_TrackNumber';
bio(844).portIdx=5;
bio(844).dim=[1,1];
bio(844).sigWidth=1;
bio(844).sigAddress='&scoop_v3_B.RTS4_TrackNumber';
bio(844).ndims=2;
bio(844).size=[];


bio(845).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/RT3A_DIS_Unpack/p7';
bio(845).sigName='RTS4_TrackStatus';
bio(845).portIdx=6;
bio(845).dim=[1,1];
bio(845).sigWidth=1;
bio(845).sigAddress='&scoop_v3_B.RTS4_TrackStatus';
bio(845).ndims=2;
bio(845).size=[];


bio(846).blkName='CAN IN (Platform)/Truck/RTS4_Unpack1/RT3A_DIS_Unpack/p8';
bio(846).sigName='RTS4_Type';
bio(846).portIdx=7;
bio(846).dim=[1,1];
bio(846).sigWidth=1;
bio(846).sigAddress='&scoop_v3_B.RTS4_Type';
bio(846).ndims=2;
bio(846).size=[];


bio(847).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/Rate Transition1';
bio(847).sigName='RTS5_Lane';
bio(847).portIdx=0;
bio(847).dim=[1,1];
bio(847).sigWidth=1;
bio(847).sigAddress='&scoop_v3_B.RTS5_Lane_i';
bio(847).ndims=2;
bio(847).size=[];


bio(848).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/Rate Transition2';
bio(848).sigName='RTS5_MsgCounter';
bio(848).portIdx=0;
bio(848).dim=[1,1];
bio(848).sigWidth=1;
bio(848).sigAddress='&scoop_v3_B.RTS5_MsgCounter_g';
bio(848).ndims=2;
bio(848).size=[];


bio(849).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/Rate Transition3';
bio(849).sigName='RTS5_TrackStatus';
bio(849).portIdx=0;
bio(849).dim=[1,1];
bio(849).sigWidth=1;
bio(849).sigAddress='&scoop_v3_B.RTS5_TrackStatus_g';
bio(849).ndims=2;
bio(849).size=[];


bio(850).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/Rate Transition4';
bio(850).sigName='RTS5_Type';
bio(850).portIdx=0;
bio(850).dim=[1,1];
bio(850).sigWidth=1;
bio(850).sigAddress='&scoop_v3_B.RTS5_Type_g';
bio(850).ndims=2;
bio(850).size=[];


bio(851).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/Rate Transition45';
bio(851).sigName='RTS5_TrackIDNumber';
bio(851).portIdx=0;
bio(851).dim=[1,1];
bio(851).sigWidth=1;
bio(851).sigAddress='&scoop_v3_B.RTS5_TrackIDNumber';
bio(851).ndims=2;
bio(851).size=[];


bio(852).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/Rate Transition52';
bio(852).sigName='RTS5_LaneConfidence';
bio(852).portIdx=0;
bio(852).dim=[1,1];
bio(852).sigWidth=1;
bio(852).sigAddress='&scoop_v3_B.RTS5_LaneConfidence_j';
bio(852).ndims=2;
bio(852).size=[];


bio(853).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/Rate Transition53';
bio(853).sigName='RTS5_LateralDistance';
bio(853).portIdx=0;
bio(853).dim=[1,1];
bio(853).sigWidth=1;
bio(853).sigAddress='&scoop_v3_B.RTS5_LateralDistance_l';
bio(853).ndims=2;
bio(853).size=[];


bio(854).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/Rate Transition54';
bio(854).sigName='RTS5_LongDistance';
bio(854).portIdx=0;
bio(854).dim=[1,1];
bio(854).sigWidth=1;
bio(854).sigAddress='&scoop_v3_B.RTS5_LongDistance_l';
bio(854).ndims=2;
bio(854).size=[];


bio(855).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/RT3A_DIS_Unpack/p1';
bio(855).sigName='RTS5_Lane';
bio(855).portIdx=0;
bio(855).dim=[1,1];
bio(855).sigWidth=1;
bio(855).sigAddress='&scoop_v3_B.RTS5_Lane';
bio(855).ndims=2;
bio(855).size=[];


bio(856).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/RT3A_DIS_Unpack/p2';
bio(856).sigName='RTS5_LaneConfidence';
bio(856).portIdx=1;
bio(856).dim=[1,1];
bio(856).sigWidth=1;
bio(856).sigAddress='&scoop_v3_B.RTS5_LaneConfidence';
bio(856).ndims=2;
bio(856).size=[];


bio(857).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/RT3A_DIS_Unpack/p3';
bio(857).sigName='RTS5_LateralDistance';
bio(857).portIdx=2;
bio(857).dim=[1,1];
bio(857).sigWidth=1;
bio(857).sigAddress='&scoop_v3_B.RTS5_LateralDistance';
bio(857).ndims=2;
bio(857).size=[];


bio(858).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/RT3A_DIS_Unpack/p4';
bio(858).sigName='RTS5_LongDistance';
bio(858).portIdx=3;
bio(858).dim=[1,1];
bio(858).sigWidth=1;
bio(858).sigAddress='&scoop_v3_B.RTS5_LongDistance';
bio(858).ndims=2;
bio(858).size=[];


bio(859).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/RT3A_DIS_Unpack/p5';
bio(859).sigName='RTS5_MsgCounter';
bio(859).portIdx=4;
bio(859).dim=[1,1];
bio(859).sigWidth=1;
bio(859).sigAddress='&scoop_v3_B.RTS5_MsgCounter';
bio(859).ndims=2;
bio(859).size=[];


bio(860).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/RT3A_DIS_Unpack/p6';
bio(860).sigName='RTS5_TrackNumber';
bio(860).portIdx=5;
bio(860).dim=[1,1];
bio(860).sigWidth=1;
bio(860).sigAddress='&scoop_v3_B.RTS5_TrackNumber';
bio(860).ndims=2;
bio(860).size=[];


bio(861).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/RT3A_DIS_Unpack/p7';
bio(861).sigName='RTS5_TrackStatus';
bio(861).portIdx=6;
bio(861).dim=[1,1];
bio(861).sigWidth=1;
bio(861).sigAddress='&scoop_v3_B.RTS5_TrackStatus';
bio(861).ndims=2;
bio(861).size=[];


bio(862).blkName='CAN IN (Platform)/Truck/RTS5_Unpack1/RT3A_DIS_Unpack/p8';
bio(862).sigName='RTS5_Type';
bio(862).portIdx=7;
bio(862).dim=[1,1];
bio(862).sigWidth=1;
bio(862).sigAddress='&scoop_v3_B.RTS5_Type';
bio(862).ndims=2;
bio(862).size=[];


bio(863).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/Rate Transition1';
bio(863).sigName='RTS6_Lane';
bio(863).portIdx=0;
bio(863).dim=[1,1];
bio(863).sigWidth=1;
bio(863).sigAddress='&scoop_v3_B.RTS6_Lane_d';
bio(863).ndims=2;
bio(863).size=[];


bio(864).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/Rate Transition2';
bio(864).sigName='RTS6_MsgCounter';
bio(864).portIdx=0;
bio(864).dim=[1,1];
bio(864).sigWidth=1;
bio(864).sigAddress='&scoop_v3_B.RTS6_MsgCounter_l';
bio(864).ndims=2;
bio(864).size=[];


bio(865).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/Rate Transition3';
bio(865).sigName='RTS6_TrackStatus';
bio(865).portIdx=0;
bio(865).dim=[1,1];
bio(865).sigWidth=1;
bio(865).sigAddress='&scoop_v3_B.RTS6_TrackStatus_j';
bio(865).ndims=2;
bio(865).size=[];


bio(866).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/Rate Transition4';
bio(866).sigName='RTS6_Type';
bio(866).portIdx=0;
bio(866).dim=[1,1];
bio(866).sigWidth=1;
bio(866).sigAddress='&scoop_v3_B.RTS6_Type_b';
bio(866).ndims=2;
bio(866).size=[];


bio(867).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/Rate Transition45';
bio(867).sigName='RTS6_TrackIDNumber';
bio(867).portIdx=0;
bio(867).dim=[1,1];
bio(867).sigWidth=1;
bio(867).sigAddress='&scoop_v3_B.RTS6_TrackIDNumber';
bio(867).ndims=2;
bio(867).size=[];


bio(868).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/Rate Transition52';
bio(868).sigName='RTS6_LaneConfidence';
bio(868).portIdx=0;
bio(868).dim=[1,1];
bio(868).sigWidth=1;
bio(868).sigAddress='&scoop_v3_B.RTS6_LaneConfidence_p';
bio(868).ndims=2;
bio(868).size=[];


bio(869).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/Rate Transition53';
bio(869).sigName='RTS6_LateralDistance';
bio(869).portIdx=0;
bio(869).dim=[1,1];
bio(869).sigWidth=1;
bio(869).sigAddress='&scoop_v3_B.RTS6_LateralDistance_l';
bio(869).ndims=2;
bio(869).size=[];


bio(870).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/Rate Transition54';
bio(870).sigName='RTS6_LongDistance';
bio(870).portIdx=0;
bio(870).dim=[1,1];
bio(870).sigWidth=1;
bio(870).sigAddress='&scoop_v3_B.RTS6_LongDistance_p';
bio(870).ndims=2;
bio(870).size=[];


bio(871).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/RT3A_DIS_Unpack/p1';
bio(871).sigName='RTS6_Lane';
bio(871).portIdx=0;
bio(871).dim=[1,1];
bio(871).sigWidth=1;
bio(871).sigAddress='&scoop_v3_B.RTS6_Lane';
bio(871).ndims=2;
bio(871).size=[];


bio(872).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/RT3A_DIS_Unpack/p2';
bio(872).sigName='RTS6_LaneConfidence';
bio(872).portIdx=1;
bio(872).dim=[1,1];
bio(872).sigWidth=1;
bio(872).sigAddress='&scoop_v3_B.RTS6_LaneConfidence';
bio(872).ndims=2;
bio(872).size=[];


bio(873).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/RT3A_DIS_Unpack/p3';
bio(873).sigName='RTS6_LateralDistance';
bio(873).portIdx=2;
bio(873).dim=[1,1];
bio(873).sigWidth=1;
bio(873).sigAddress='&scoop_v3_B.RTS6_LateralDistance';
bio(873).ndims=2;
bio(873).size=[];


bio(874).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/RT3A_DIS_Unpack/p4';
bio(874).sigName='RTS6_LongDistance';
bio(874).portIdx=3;
bio(874).dim=[1,1];
bio(874).sigWidth=1;
bio(874).sigAddress='&scoop_v3_B.RTS6_LongDistance';
bio(874).ndims=2;
bio(874).size=[];


bio(875).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/RT3A_DIS_Unpack/p5';
bio(875).sigName='RTS6_MsgCounter';
bio(875).portIdx=4;
bio(875).dim=[1,1];
bio(875).sigWidth=1;
bio(875).sigAddress='&scoop_v3_B.RTS6_MsgCounter';
bio(875).ndims=2;
bio(875).size=[];


bio(876).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/RT3A_DIS_Unpack/p6';
bio(876).sigName='RTS6_TrackNumber';
bio(876).portIdx=5;
bio(876).dim=[1,1];
bio(876).sigWidth=1;
bio(876).sigAddress='&scoop_v3_B.RTS6_TrackNumber';
bio(876).ndims=2;
bio(876).size=[];


bio(877).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/RT3A_DIS_Unpack/p7';
bio(877).sigName='RTS6_TrackStatus';
bio(877).portIdx=6;
bio(877).dim=[1,1];
bio(877).sigWidth=1;
bio(877).sigAddress='&scoop_v3_B.RTS6_TrackStatus';
bio(877).ndims=2;
bio(877).size=[];


bio(878).blkName='CAN IN (Platform)/Truck/RTS6_Unpack1/RT3A_DIS_Unpack/p8';
bio(878).sigName='RTS6_Type';
bio(878).portIdx=7;
bio(878).dim=[1,1];
bio(878).sigWidth=1;
bio(878).sigAddress='&scoop_v3_B.RTS6_Type';
bio(878).ndims=2;
bio(878).size=[];


bio(879).blkName='MonitoringAndLogging /Log Received Data/CAM Compare/Compare';
bio(879).sigName='';
bio(879).portIdx=0;
bio(879).dim=[1,1];
bio(879).sigWidth=1;
bio(879).sigAddress='&scoop_v3_B.Compare_o';
bio(879).ndims=2;
bio(879).size=[];


bio(880).blkName='MonitoringAndLogging /Log Received Data/CAM Compare1/Compare';
bio(880).sigName='';
bio(880).portIdx=0;
bio(880).dim=[1,1];
bio(880).sigWidth=1;
bio(880).sigAddress='&scoop_v3_B.Compare_l';
bio(880).ndims=2;
bio(880).size=[];


bio(881).blkName='MonitoringAndLogging /Log Received Data/CAM Compare2/Compare';
bio(881).sigName='';
bio(881).portIdx=0;
bio(881).dim=[1,1];
bio(881).sigWidth=1;
bio(881).sigAddress='&scoop_v3_B.Compare_n';
bio(881).ndims=2;
bio(881).size=[];


bio(882).blkName='MonitoringAndLogging /Log Received Data/CAM Compare3/Compare';
bio(882).sigName='';
bio(882).portIdx=0;
bio(882).dim=[1,1];
bio(882).sigWidth=1;
bio(882).sigAddress='&scoop_v3_B.Compare_ow';
bio(882).ndims=2;
bio(882).size=[];


bio(883).blkName='MonitoringAndLogging /Log Received Data/CAM Compare4/Compare';
bio(883).sigName='';
bio(883).portIdx=0;
bio(883).dim=[1,1];
bio(883).sigWidth=1;
bio(883).sigAddress='&scoop_v3_B.Compare_e';
bio(883).ndims=2;
bio(883).size=[];


bio(884).blkName='MonitoringAndLogging /Log Received Data/CAM Compare5/Compare';
bio(884).sigName='';
bio(884).portIdx=0;
bio(884).dim=[1,1];
bio(884).sigWidth=1;
bio(884).sigAddress='&scoop_v3_B.Compare_g';
bio(884).ndims=2;
bio(884).size=[];


bio(885).blkName='MonitoringAndLogging /Log Received Data/CAM Compare6/Compare';
bio(885).sigName='';
bio(885).portIdx=0;
bio(885).dim=[1,1];
bio(885).sigWidth=1;
bio(885).sigAddress='&scoop_v3_B.Compare_j';
bio(885).ndims=2;
bio(885).size=[];


bio(886).blkName='MonitoringAndLogging /Log Received Data/CAM Compare7/Compare';
bio(886).sigName='';
bio(886).portIdx=0;
bio(886).dim=[1,1];
bio(886).sigWidth=1;
bio(886).sigAddress='&scoop_v3_B.Compare_g2';
bio(886).ndims=2;
bio(886).size=[];


bio(887).blkName='MonitoringAndLogging /Log Received Data/CAM Compare8/Compare';
bio(887).sigName='';
bio(887).portIdx=0;
bio(887).dim=[1,1];
bio(887).sigWidth=1;
bio(887).sigAddress='&scoop_v3_B.Compare_m';
bio(887).ndims=2;
bio(887).size=[];


bio(888).blkName='MonitoringAndLogging /Log Received Data/CAM Compare9/Compare';
bio(888).sigName='';
bio(888).portIdx=0;
bio(888).dim=[1,1];
bio(888).sigWidth=1;
bio(888).sigAddress='&scoop_v3_B.Compare_p';
bio(888).ndims=2;
bio(888).size=[];


bio(889).blkName='MonitoringAndLogging /Log Received Data/DEMN Compare/Compare';
bio(889).sigName='';
bio(889).portIdx=0;
bio(889).dim=[1,1];
bio(889).sigWidth=1;
bio(889).sigAddress='&scoop_v3_B.Compare_c';
bio(889).ndims=2;
bio(889).size=[];


bio(890).blkName='MonitoringAndLogging /Log Received Data/DEMN Compare1/Compare';
bio(890).sigName='';
bio(890).portIdx=0;
bio(890).dim=[1,1];
bio(890).sigWidth=1;
bio(890).sigAddress='&scoop_v3_B.Compare_ot';
bio(890).ndims=2;
bio(890).size=[];


bio(891).blkName='MonitoringAndLogging /Log Received Data/DEMN Compare2/Compare';
bio(891).sigName='';
bio(891).portIdx=0;
bio(891).dim=[1,1];
bio(891).sigWidth=1;
bio(891).sigAddress='&scoop_v3_B.Compare_a';
bio(891).ndims=2;
bio(891).size=[];


bio(892).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare/Compare';
bio(892).sigName='';
bio(892).portIdx=0;
bio(892).dim=[1,1];
bio(892).sigWidth=1;
bio(892).sigAddress='&scoop_v3_B.Compare_mr';
bio(892).ndims=2;
bio(892).size=[];


bio(893).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare1/Compare';
bio(893).sigName='';
bio(893).portIdx=0;
bio(893).dim=[1,1];
bio(893).sigWidth=1;
bio(893).sigAddress='&scoop_v3_B.Compare_d';
bio(893).ndims=2;
bio(893).size=[];


bio(894).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare10/Compare';
bio(894).sigName='';
bio(894).portIdx=0;
bio(894).dim=[1,1];
bio(894).sigWidth=1;
bio(894).sigAddress='&scoop_v3_B.Compare_oij';
bio(894).ndims=2;
bio(894).size=[];


bio(895).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare11/Compare';
bio(895).sigName='';
bio(895).portIdx=0;
bio(895).dim=[1,1];
bio(895).sigWidth=1;
bio(895).sigAddress='&scoop_v3_B.Compare_nf';
bio(895).ndims=2;
bio(895).size=[];


bio(896).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare12/Compare';
bio(896).sigName='';
bio(896).portIdx=0;
bio(896).dim=[1,1];
bio(896).sigWidth=1;
bio(896).sigAddress='&scoop_v3_B.Compare_gw';
bio(896).ndims=2;
bio(896).size=[];


bio(897).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare13/Compare';
bio(897).sigName='';
bio(897).portIdx=0;
bio(897).dim=[1,1];
bio(897).sigWidth=1;
bio(897).sigAddress='&scoop_v3_B.Compare';
bio(897).ndims=2;
bio(897).size=[];


bio(898).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare14/Compare';
bio(898).sigName='';
bio(898).portIdx=0;
bio(898).dim=[1,1];
bio(898).sigWidth=1;
bio(898).sigAddress='&scoop_v3_B.Compare_o2';
bio(898).ndims=2;
bio(898).size=[];


bio(899).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare15/Compare';
bio(899).sigName='';
bio(899).portIdx=0;
bio(899).dim=[1,1];
bio(899).sigWidth=1;
bio(899).sigAddress='&scoop_v3_B.Compare_f';
bio(899).ndims=2;
bio(899).size=[];


bio(900).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare16/Compare';
bio(900).sigName='';
bio(900).portIdx=0;
bio(900).dim=[1,1];
bio(900).sigWidth=1;
bio(900).sigAddress='&scoop_v3_B.Compare_b';
bio(900).ndims=2;
bio(900).size=[];


bio(901).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare17/Compare';
bio(901).sigName='';
bio(901).portIdx=0;
bio(901).dim=[1,1];
bio(901).sigWidth=1;
bio(901).sigAddress='&scoop_v3_B.Compare_k';
bio(901).ndims=2;
bio(901).size=[];


bio(902).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare18/Compare';
bio(902).sigName='';
bio(902).portIdx=0;
bio(902).dim=[1,1];
bio(902).sigWidth=1;
bio(902).sigAddress='&scoop_v3_B.Compare_js';
bio(902).ndims=2;
bio(902).size=[];


bio(903).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare19/Compare';
bio(903).sigName='';
bio(903).portIdx=0;
bio(903).dim=[1,1];
bio(903).sigWidth=1;
bio(903).sigAddress='&scoop_v3_B.Compare_av';
bio(903).ndims=2;
bio(903).size=[];


bio(904).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare2/Compare';
bio(904).sigName='';
bio(904).portIdx=0;
bio(904).dim=[1,1];
bio(904).sigWidth=1;
bio(904).sigAddress='&scoop_v3_B.Compare_gd';
bio(904).ndims=2;
bio(904).size=[];


bio(905).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare20/Compare';
bio(905).sigName='';
bio(905).portIdx=0;
bio(905).dim=[1,1];
bio(905).sigWidth=1;
bio(905).sigAddress='&scoop_v3_B.Compare_eu';
bio(905).ndims=2;
bio(905).size=[];


bio(906).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare21/Compare';
bio(906).sigName='';
bio(906).portIdx=0;
bio(906).dim=[1,1];
bio(906).sigWidth=1;
bio(906).sigAddress='&scoop_v3_B.Compare_eo';
bio(906).ndims=2;
bio(906).size=[];


bio(907).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare22/Compare';
bio(907).sigName='';
bio(907).portIdx=0;
bio(907).dim=[1,1];
bio(907).sigWidth=1;
bio(907).sigAddress='&scoop_v3_B.Compare_ko';
bio(907).ndims=2;
bio(907).size=[];


bio(908).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare23/Compare';
bio(908).sigName='';
bio(908).portIdx=0;
bio(908).dim=[1,1];
bio(908).sigWidth=1;
bio(908).sigAddress='&scoop_v3_B.Compare_i';
bio(908).ndims=2;
bio(908).size=[];


bio(909).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare3/Compare';
bio(909).sigName='';
bio(909).portIdx=0;
bio(909).dim=[1,1];
bio(909).sigWidth=1;
bio(909).sigAddress='&scoop_v3_B.Compare_g4';
bio(909).ndims=2;
bio(909).size=[];


bio(910).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare4/Compare';
bio(910).sigName='';
bio(910).portIdx=0;
bio(910).dim=[1,1];
bio(910).sigWidth=1;
bio(910).sigAddress='&scoop_v3_B.Compare_o2p';
bio(910).ndims=2;
bio(910).size=[];


bio(911).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare5/Compare';
bio(911).sigName='';
bio(911).portIdx=0;
bio(911).dim=[1,1];
bio(911).sigWidth=1;
bio(911).sigAddress='&scoop_v3_B.Compare_lt';
bio(911).ndims=2;
bio(911).size=[];


bio(912).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare6/Compare';
bio(912).sigName='';
bio(912).portIdx=0;
bio(912).dim=[1,1];
bio(912).sigWidth=1;
bio(912).sigAddress='&scoop_v3_B.Compare_ni';
bio(912).ndims=2;
bio(912).size=[];


bio(913).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare7/Compare';
bio(913).sigName='';
bio(913).portIdx=0;
bio(913).dim=[1,1];
bio(913).sigWidth=1;
bio(913).sigAddress='&scoop_v3_B.Compare_oi';
bio(913).ndims=2;
bio(913).size=[];


bio(914).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare8/Compare';
bio(914).sigName='';
bio(914).portIdx=0;
bio(914).dim=[1,1];
bio(914).sigWidth=1;
bio(914).sigAddress='&scoop_v3_B.Compare_h';
bio(914).ndims=2;
bio(914).size=[];


bio(915).blkName='MonitoringAndLogging /Log Received Data/iCLCM Compare9/Compare';
bio(915).sigName='';
bio(915).portIdx=0;
bio(915).dim=[1,1];
bio(915).sigWidth=1;
bio(915).sigAddress='&scoop_v3_B.Compare_aj';
bio(915).ndims=2;
bio(915).size=[];


bio(916).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Read Int Status FC1/p1';
bio(916).sigName='';
bio(916).portIdx=0;
bio(916).dim=[1,1];
bio(916).sigWidth=1;
bio(916).sigAddress='&scoop_v3_B.ReadIntStatusFC1_o2';
bio(916).ndims=2;
bio(916).size=[];


bio(917).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p1';
bio(917).sigName='UTCposition';
bio(917).portIdx=0;
bio(917).dim=[1,1];
bio(917).sigWidth=1;
bio(917).sigAddress='&scoop_v3_B.UTCposition';
bio(917).ndims=2;
bio(917).size=[];


bio(918).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p2';
bio(918).sigName='Latitude';
bio(918).portIdx=1;
bio(918).dim=[1,1];
bio(918).sigWidth=1;
bio(918).sigAddress='&scoop_v3_B.Latitude';
bio(918).ndims=2;
bio(918).size=[];


bio(919).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p3';
bio(919).sigName='LatitudeDirection';
bio(919).portIdx=2;
bio(919).dim=[1,1];
bio(919).sigWidth=1;
bio(919).sigAddress='&scoop_v3_B.LatitudeDirection';
bio(919).ndims=2;
bio(919).size=[];


bio(920).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p4';
bio(920).sigName='Longitude';
bio(920).portIdx=3;
bio(920).dim=[1,1];
bio(920).sigWidth=1;
bio(920).sigAddress='&scoop_v3_B.Longitude';
bio(920).ndims=2;
bio(920).size=[];


bio(921).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p5';
bio(921).sigName='LongitudeDirection';
bio(921).portIdx=4;
bio(921).dim=[1,1];
bio(921).sigWidth=1;
bio(921).sigAddress='&scoop_v3_B.LongitudeDirection';
bio(921).ndims=2;
bio(921).size=[];


bio(922).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p6';
bio(922).sigName='GPSQuality';
bio(922).portIdx=5;
bio(922).dim=[1,1];
bio(922).sigWidth=1;
bio(922).sigAddress='&scoop_v3_B.GPSQuality_j';
bio(922).ndims=2;
bio(922).size=[];


bio(923).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p7';
bio(923).sigName='Satellites';
bio(923).portIdx=6;
bio(923).dim=[1,1];
bio(923).sigWidth=1;
bio(923).sigAddress='&scoop_v3_B.Satellites';
bio(923).ndims=2;
bio(923).size=[];


bio(924).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p8';
bio(924).sigName='HDOP';
bio(924).portIdx=7;
bio(924).dim=[1,1];
bio(924).sigWidth=1;
bio(924).sigAddress='&scoop_v3_B.HDOP';
bio(924).ndims=2;
bio(924).size=[];


bio(925).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p9';
bio(925).sigName='HeightMSL';
bio(925).portIdx=8;
bio(925).dim=[1,1];
bio(925).sigWidth=1;
bio(925).sigAddress='&scoop_v3_B.HeightMSL';
bio(925).ndims=2;
bio(925).size=[];


bio(926).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p10';
bio(926).sigName='HeightUnit';
bio(926).portIdx=9;
bio(926).dim=[1,1];
bio(926).sigWidth=1;
bio(926).sigAddress='&scoop_v3_B.HeightUnit';
bio(926).ndims=2;
bio(926).size=[];


bio(927).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p11';
bio(927).sigName='GeoidSeperation';
bio(927).portIdx=10;
bio(927).dim=[1,1];
bio(927).sigWidth=1;
bio(927).sigAddress='&scoop_v3_B.GeoidSeperation';
bio(927).ndims=2;
bio(927).size=[];


bio(928).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p12';
bio(928).sigName='GeoidUnit';
bio(928).portIdx=11;
bio(928).dim=[1,1];
bio(928).sigWidth=1;
bio(928).sigAddress='&scoop_v3_B.GeoidUnit';
bio(928).ndims=2;
bio(928).size=[];


bio(929).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p13';
bio(929).sigName='DGPS';
bio(929).portIdx=12;
bio(929).dim=[1,1];
bio(929).sigWidth=1;
bio(929).sigAddress='&scoop_v3_B.DGPS';
bio(929).ndims=2;
bio(929).size=[];


bio(930).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGGA/GGAparser/p14';
bio(930).sigName='ReferenceID';
bio(930).portIdx=13;
bio(930).dim=[1,1];
bio(930).sigWidth=1;
bio(930).sigAddress='&scoop_v3_B.ReferenceID';
bio(930).ndims=2;
bio(930).size=[];


bio(931).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGST/GSTparser/p1';
bio(931).sigName='';
bio(931).portIdx=0;
bio(931).dim=[1,1];
bio(931).sigWidth=1;
bio(931).sigAddress='&scoop_v3_B.GSTparser_o1';
bio(931).ndims=2;
bio(931).size=[];


bio(932).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGST/GSTparser/p2';
bio(932).sigName='';
bio(932).portIdx=1;
bio(932).dim=[1,1];
bio(932).sigWidth=1;
bio(932).sigAddress='&scoop_v3_B.GSTparser_o2';
bio(932).ndims=2;
bio(932).size=[];


bio(933).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGST/GSTparser/p3';
bio(933).sigName='';
bio(933).portIdx=2;
bio(933).dim=[1,1];
bio(933).sigWidth=1;
bio(933).sigAddress='&scoop_v3_B.GSTparser_o3';
bio(933).ndims=2;
bio(933).size=[];


bio(934).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGST/GSTparser/p4';
bio(934).sigName='';
bio(934).portIdx=3;
bio(934).dim=[1,1];
bio(934).sigWidth=1;
bio(934).sigAddress='&scoop_v3_B.GSTparser_o4';
bio(934).ndims=2;
bio(934).size=[];


bio(935).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGST/GSTparser/p5';
bio(935).sigName='';
bio(935).portIdx=4;
bio(935).dim=[1,1];
bio(935).sigWidth=1;
bio(935).sigAddress='&scoop_v3_B.GSTparser_o5';
bio(935).ndims=2;
bio(935).size=[];


bio(936).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGST/GSTparser/p6';
bio(936).sigName='';
bio(936).portIdx=5;
bio(936).dim=[1,1];
bio(936).sigWidth=1;
bio(936).sigAddress='&scoop_v3_B.GSTparser_o6';
bio(936).ndims=2;
bio(936).size=[];


bio(937).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGST/GSTparser/p7';
bio(937).sigName='';
bio(937).portIdx=6;
bio(937).dim=[1,1];
bio(937).sigWidth=1;
bio(937).sigAddress='&scoop_v3_B.GSTparser_o7';
bio(937).ndims=2;
bio(937).size=[];


bio(938).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPGST/GSTparser/p8';
bio(938).sigName='';
bio(938).portIdx=7;
bio(938).dim=[1,1];
bio(938).sigWidth=1;
bio(938).sigAddress='&scoop_v3_B.GSTparser_o8';
bio(938).ndims=2;
bio(938).size=[];


bio(939).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p1';
bio(939).sigName='positionUTC';
bio(939).portIdx=0;
bio(939).dim=[1,1];
bio(939).sigWidth=1;
bio(939).sigAddress='&scoop_v3_B.positionUTC';
bio(939).ndims=2;
bio(939).size=[];


bio(940).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p2';
bio(940).sigName='status';
bio(940).portIdx=1;
bio(940).dim=[1,1];
bio(940).sigWidth=1;
bio(940).sigAddress='&scoop_v3_B.status';
bio(940).ndims=2;
bio(940).size=[];


bio(941).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p3';
bio(941).sigName='positionLatitude';
bio(941).portIdx=2;
bio(941).dim=[1,1];
bio(941).sigWidth=1;
bio(941).sigAddress='&scoop_v3_B.positionLatitude';
bio(941).ndims=2;
bio(941).size=[];


bio(942).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p4';
bio(942).sigName='North';
bio(942).portIdx=3;
bio(942).dim=[1,1];
bio(942).sigWidth=1;
bio(942).sigAddress='&scoop_v3_B.North';
bio(942).ndims=2;
bio(942).size=[];


bio(943).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p5';
bio(943).sigName='positionLongitude';
bio(943).portIdx=4;
bio(943).dim=[1,1];
bio(943).sigWidth=1;
bio(943).sigAddress='&scoop_v3_B.positionLongitude';
bio(943).ndims=2;
bio(943).size=[];


bio(944).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p6';
bio(944).sigName='East';
bio(944).portIdx=5;
bio(944).dim=[1,1];
bio(944).sigWidth=1;
bio(944).sigAddress='&scoop_v3_B.East';
bio(944).ndims=2;
bio(944).size=[];


bio(945).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p7';
bio(945).sigName='vehicleSpeed';
bio(945).portIdx=6;
bio(945).dim=[1,1];
bio(945).sigWidth=1;
bio(945).sigAddress='&scoop_v3_B.vehicleSpeed';
bio(945).ndims=2;
bio(945).size=[];


bio(946).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p8';
bio(946).sigName='trackAngle';
bio(946).portIdx=7;
bio(946).dim=[1,1];
bio(946).sigWidth=1;
bio(946).sigAddress='&scoop_v3_B.trackAngle';
bio(946).ndims=2;
bio(946).size=[];


bio(947).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p9';
bio(947).sigName='date';
bio(947).portIdx=8;
bio(947).dim=[1,1];
bio(947).sigWidth=1;
bio(947).sigAddress='&scoop_v3_B.date';
bio(947).ndims=2;
bio(947).size=[];


bio(948).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p10';
bio(948).sigName='magneticVariation';
bio(948).portIdx=9;
bio(948).dim=[1,1];
bio(948).sigWidth=1;
bio(948).sigAddress='&scoop_v3_B.magneticVariation';
bio(948).ndims=2;
bio(948).size=[];


bio(949).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/RMCparser/p11';
bio(949).sigName='West';
bio(949).portIdx=10;
bio(949).dim=[1,1];
bio(949).sigWidth=1;
bio(949).sigAddress='&scoop_v3_B.West';
bio(949).ndims=2;
bio(949).size=[];


bio(950).blkName='Serial IN (Trimble)/Data conversion/GGA Conversion/Latitude Conversion Min -> Degree ';
bio(950).sigName='lat_deg';
bio(950).portIdx=0;
bio(950).dim=[1,1];
bio(950).sigWidth=1;
bio(950).sigAddress='&scoop_v3_B.sf_LatitudeConversionMinDegre_c.lat_deg';
bio(950).ndims=2;
bio(950).size=[];


bio(951).blkName='Serial IN (Trimble)/Data conversion/GGA Conversion/Longitude Conversion Min -> Degree ';
bio(951).sigName='lon_deg';
bio(951).portIdx=0;
bio(951).dim=[1,1];
bio(951).sigWidth=1;
bio(951).sigAddress='&scoop_v3_B.sf_LongitudeConversionMinDegr_d.lon_deg';
bio(951).ndims=2;
bio(951).size=[];


bio(952).blkName='Serial IN (Trimble)/Data conversion/RMC Conversion/Latitude Conversion Min -> Degree ';
bio(952).sigName='lat_deg';
bio(952).portIdx=0;
bio(952).dim=[1,1];
bio(952).sigWidth=1;
bio(952).sigAddress='&scoop_v3_B.sf_LatitudeConversionMinDegree.lat_deg';
bio(952).ndims=2;
bio(952).size=[];


bio(953).blkName='Serial IN (Trimble)/Data conversion/RMC Conversion/Longitude Conversion Min -> Degree ';
bio(953).sigName='lon_deg';
bio(953).portIdx=0;
bio(953).dim=[1,1];
bio(953).sigWidth=1;
bio(953).sigAddress='&scoop_v3_B.sf_LongitudeConversionMinDegree.lon_deg';
bio(953).ndims=2;
bio(953).size=[];


bio(954).blkName='Serial IN (Trimble)/Data conversion/RMC Conversion/Speed Conversion knot -> m//s';
bio(954).sigName='speed_ms';
bio(954).portIdx=0;
bio(954).dim=[1,1];
bio(954).sigWidth=1;
bio(954).sigAddress='&scoop_v3_B.speed_ms';
bio(954).ndims=2;
bio(954).size=[];


bio(955).blkName='CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)/p1';
bio(955).sigName='';
bio(955).portIdx=0;
bio(955).dim=[1,1];
bio(955).sigWidth=1;
bio(955).sigAddress='&scoop_v3_B.Readv3_o1';
bio(955).ndims=2;
bio(955).size=[];


bio(956).blkName='CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)/p2';
bio(956).sigName='';
bio(956).portIdx=1;
bio(956).dim=[1,1];
bio(956).sigWidth=1;
bio(956).sigAddress='&scoop_v3_B.Readv3_o2';
bio(956).ndims=2;
bio(956).size=[];


bio(957).blkName='CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)/p3';
bio(957).sigName='';
bio(957).portIdx=2;
bio(957).dim=[1,1];
bio(957).sigWidth=1;
bio(957).sigAddress='&scoop_v3_B.Readv3_o3';
bio(957).ndims=2;
bio(957).size=[];


bio(958).blkName='CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)/p4';
bio(958).sigName='';
bio(958).portIdx=3;
bio(958).dim=[1,1];
bio(958).sigWidth=1;
bio(958).sigAddress='&scoop_v3_B.Readv3_o4';
bio(958).ndims=2;
bio(958).size=[];


bio(959).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 1/FIFO write 1';
bio(959).sigName='';
bio(959).portIdx=0;
bio(959).dim=[1,1];
bio(959).sigWidth=1;
bio(959).sigAddress='&scoop_v3_B.FIFOwrite1';
bio(959).ndims=2;
bio(959).size=[];


bio(960).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 1/Read HW FIFO1';
bio(960).sigName='';
bio(960).portIdx=0;
bio(960).dim=[65,1];
bio(960).sigWidth=65;
bio(960).sigAddress='&scoop_v3_B.ReadHWFIFO1[0]';
bio(960).ndims=2;
bio(960).size=[];


bio(961).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 2/FIFO write 2';
bio(961).sigName='';
bio(961).portIdx=0;
bio(961).dim=[1,1];
bio(961).sigWidth=1;
bio(961).sigAddress='&scoop_v3_B.FIFOwrite2';
bio(961).ndims=2;
bio(961).size=[];


bio(962).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 2/Read HW FIFO2';
bio(962).sigName='';
bio(962).portIdx=0;
bio(962).dim=[65,1];
bio(962).sigWidth=65;
bio(962).sigAddress='&scoop_v3_B.ReadHWFIFO2[0]';
bio(962).ndims=2;
bio(962).size=[];


bio(963).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/Constant1';
bio(963).sigName='';
bio(963).portIdx=0;
bio(963).dim=[1,1];
bio(963).sigWidth=1;
bio(963).sigAddress='&scoop_v3_B.Constant1_j';
bio(963).ndims=2;
bio(963).size=[];


bio(964).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1/p1';
bio(964).sigName='';
bio(964).portIdx=0;
bio(964).dim=[61,1];
bio(964).sigWidth=61;
bio(964).sigAddress='&scoop_v3_B.FIFOread1_o1[0]';
bio(964).ndims=2;
bio(964).size=[];


bio(965).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1/p2';
bio(965).sigName='';
bio(965).portIdx=1;
bio(965).dim=[1,1];
bio(965).sigWidth=1;
bio(965).sigAddress='&scoop_v3_B.FIFOread1_o2';
bio(965).ndims=2;
bio(965).size=[];


bio(966).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/Constant2';
bio(966).sigName='';
bio(966).portIdx=0;
bio(966).dim=[1,1];
bio(966).sigWidth=1;
bio(966).sigAddress='&scoop_v3_B.Constant2';
bio(966).ndims=2;
bio(966).size=[];


bio(967).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2/p1';
bio(967).sigName='';
bio(967).portIdx=0;
bio(967).dim=[61,1];
bio(967).sigWidth=61;
bio(967).sigAddress='&scoop_v3_B.FIFOread2_o1[0]';
bio(967).ndims=2;
bio(967).size=[];


bio(968).blkName='Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2/p2';
bio(968).sigName='';
bio(968).portIdx=1;
bio(968).dim=[1,1];
bio(968).sigWidth=1;
bio(968).sigAddress='&scoop_v3_B.FIFOread2_o2';
bio(968).ndims=2;
bio(968).size=[];


bio(969).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/TimeSynch/SynchronizeTime';
bio(969).sigName='';
bio(969).portIdx=0;
bio(969).dim=[8,1];
bio(969).sigWidth=8;
bio(969).sigAddress='&scoop_v3_B.SynchronizeTime[0]';
bio(969).ndims=2;
bio(969).size=[];


bio(970).blkName='Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/TimeSynch/Delay';
bio(970).sigName='';
bio(970).portIdx=0;
bio(970).dim=[1,1];
bio(970).sigWidth=1;
bio(970).sigAddress='&scoop_v3_B.Delay';
bio(970).ndims=2;
bio(970).size=[];


bio(971).blkName='UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/Extract /p1';
bio(971).sigName='';
bio(971).portIdx=0;
bio(971).dim=[268,1];
bio(971).sigWidth=268;
bio(971).sigAddress='&scoop_v3_B.Extract_o1[0]';
bio(971).ndims=2;
bio(971).size=[];


bio(972).blkName='UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/Extract /p2';
bio(972).sigName='';
bio(972).portIdx=1;
bio(972).dim=[1,1];
bio(972).sigWidth=1;
bio(972).sigAddress='&scoop_v3_B.Extract_o2';
bio(972).ndims=2;
bio(972).size=[];


bio(973).blkName='UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/Manage ';
bio(973).sigName='';
bio(973).portIdx=0;
bio(973).dim=[1,1];
bio(973).sigWidth=1;
bio(973).sigAddress='&scoop_v3_B.Manage';
bio(973).ndims=2;
bio(973).size=[];


bio(974).blkName='UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Consume /p1';
bio(974).sigName='';
bio(974).portIdx=0;
bio(974).dim=[1,1];
bio(974).sigWidth=1;
bio(974).sigAddress='&scoop_v3_B.UDPConsume_o1';
bio(974).ndims=2;
bio(974).size=[];


bio(975).blkName='UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Consume /p2';
bio(975).sigName='';
bio(975).portIdx=1;
bio(975).dim=[1,1];
bio(975).sigWidth=1;
bio(975).sigAddress='&scoop_v3_B.UDPConsume_o2';
bio(975).ndims=2;
bio(975).size=[];


bio(976).blkName='UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Rx ';
bio(976).sigName='';
bio(976).portIdx=0;
bio(976).dim=[1,1];
bio(976).sigWidth=1;
bio(976).sigAddress='&scoop_v3_B.UDPRx';
bio(976).ndims=2;
bio(976).size=[];


bio(977).blkName='UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/Compose ';
bio(977).sigName='';
bio(977).portIdx=0;
bio(977).dim=[1,1];
bio(977).sigWidth=1;
bio(977).sigAddress='&scoop_v3_B.Compose';
bio(977).ndims=2;
bio(977).size=[];


bio(978).blkName='UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
bio(978).sigName='';
bio(978).portIdx=0;
bio(978).dim=[1,1];
bio(978).sigWidth=1;
bio(978).sigAddress='&scoop_v3_B.UDPProduce';
bio(978).ndims=2;
bio(978).size=[];


function len = getlenBIO
len = 978;

