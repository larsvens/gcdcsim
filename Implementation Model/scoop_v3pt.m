function pt=scoop_v3pt
pt = [];
pt(1).blockname = 'Constant';
pt(1).paramname = 'Value';
pt(1).class     = 'scalar';
pt(1).nrows     = 1;
pt(1).ncols     = 1;
pt(1).subsource = 'SS_DOUBLE';
pt(1).ndims     = '2';
pt(1).size      = '[]';
pt(getlenPT) = pt(1);

pt(2).blockname = 'Constant3';
pt(2).paramname = 'Value';
pt(2).class     = 'scalar';
pt(2).nrows     = 1;
pt(2).ncols     = 1;
pt(2).subsource = 'SS_DOUBLE';
pt(2).ndims     = '2';
pt(2).size      = '[]';

pt(3).blockname = 'Constant4';
pt(3).paramname = 'Value';
pt(3).class     = 'scalar';
pt(3).nrows     = 1;
pt(3).ncols     = 1;
pt(3).subsource = 'SS_DOUBLE';
pt(3).ndims     = '2';
pt(3).size      = '[]';

pt(4).blockname = 'LightInfo';
pt(4).paramname = 'Value';
pt(4).class     = 'scalar';
pt(4).nrows     = 1;
pt(4).ncols     = 1;
pt(4).subsource = 'SS_DOUBLE';
pt(4).ndims     = '2';
pt(4).size      = '[]';

pt(5).blockname = 'ManualRefAcc';
pt(5).paramname = 'Value';
pt(5).class     = 'scalar';
pt(5).nrows     = 1;
pt(5).ncols     = 1;
pt(5).subsource = 'SS_DOUBLE';
pt(5).ndims     = '2';
pt(5).size      = '[]';

pt(6).blockname = 'ManualRefON';
pt(6).paramname = 'Value';
pt(6).class     = 'scalar';
pt(6).nrows     = 1;
pt(6).ncols     = 1;
pt(6).subsource = 'SS_DOUBLE';
pt(6).ndims     = '2';
pt(6).size      = '[]';

pt(7).blockname = 'ManualRefVel';
pt(7).paramname = 'Value';
pt(7).class     = 'scalar';
pt(7).nrows     = 1;
pt(7).ncols     = 1;
pt(7).subsource = 'SS_DOUBLE';
pt(7).ndims     = '2';
pt(7).size      = '[]';

pt(8).blockname = 'Rate Transition4';
pt(8).paramname = 'X0';
pt(8).class     = 'scalar';
pt(8).nrows     = 1;
pt(8).ncols     = 1;
pt(8).subsource = 'SS_DOUBLE';
pt(8).ndims     = '2';
pt(8).size      = '[]';

pt(9).blockname = 'Rate Transition6';
pt(9).paramname = 'X0';
pt(9).class     = 'scalar';
pt(9).nrows     = 1;
pt(9).ncols     = 1;
pt(9).subsource = 'SS_DOUBLE';
pt(9).ndims     = '2';
pt(9).size      = '[]';

pt(10).blockname = 'Unit Delay1';
pt(10).paramname = 'InitialCondition';
pt(10).class     = 'scalar';
pt(10).nrows     = 1;
pt(10).ncols     = 1;
pt(10).subsource = 'SS_DOUBLE';
pt(10).ndims     = '2';
pt(10).size      = '[]';

pt(11).blockname = 'Unit Delay3';
pt(11).paramname = 'InitialCondition';
pt(11).class     = 'scalar';
pt(11).nrows     = 1;
pt(11).ncols     = 1;
pt(11).subsource = 'SS_DOUBLE';
pt(11).ndims     = '2';
pt(11).size      = '[]';

pt(12).blockname = 'MonitoringAndLogging /IMULatAcc';
pt(12).paramname = 'Gain';
pt(12).class     = 'scalar';
pt(12).nrows     = 1;
pt(12).ncols     = 1;
pt(12).subsource = 'SS_DOUBLE';
pt(12).ndims     = '2';
pt(12).size      = '[]';

pt(13).blockname = 'MonitoringAndLogging /IMULatAccTS';
pt(13).paramname = 'Gain';
pt(13).class     = 'scalar';
pt(13).nrows     = 1;
pt(13).ncols     = 1;
pt(13).subsource = 'SS_DOUBLE';
pt(13).ndims     = '2';
pt(13).size      = '[]';

pt(14).blockname = 'MonitoringAndLogging /IMULongAcc';
pt(14).paramname = 'Gain';
pt(14).class     = 'scalar';
pt(14).nrows     = 1;
pt(14).ncols     = 1;
pt(14).subsource = 'SS_DOUBLE';
pt(14).ndims     = '2';
pt(14).size      = '[]';

pt(15).blockname = 'MonitoringAndLogging /IMULongAccTS';
pt(15).paramname = 'Gain';
pt(15).class     = 'scalar';
pt(15).nrows     = 1;
pt(15).ncols     = 1;
pt(15).subsource = 'SS_DOUBLE';
pt(15).ndims     = '2';
pt(15).size      = '[]';

pt(16).blockname = 'MonitoringAndLogging /IMUTS';
pt(16).paramname = 'Gain';
pt(16).class     = 'scalar';
pt(16).nrows     = 1;
pt(16).ncols     = 1;
pt(16).subsource = 'SS_DOUBLE';
pt(16).ndims     = '2';
pt(16).size      = '[]';

pt(17).blockname = 'MonitoringAndLogging /IMUYawrate';
pt(17).paramname = 'Gain';
pt(17).class     = 'scalar';
pt(17).nrows     = 1;
pt(17).ncols     = 1;
pt(17).subsource = 'SS_DOUBLE';
pt(17).ndims     = '2';
pt(17).size      = '[]';

pt(18).blockname = 'MonitoringAndLogging /IMUYawrateTS';
pt(18).paramname = 'Gain';
pt(18).class     = 'scalar';
pt(18).nrows     = 1;
pt(18).ncols     = 1;
pt(18).subsource = 'SS_DOUBLE';
pt(18).ndims     = '2';
pt(18).size      = '[]';

pt(19).blockname = 'MonitoringAndLogging /MtrLongAcc';
pt(19).paramname = 'Gain';
pt(19).class     = 'scalar';
pt(19).nrows     = 1;
pt(19).ncols     = 1;
pt(19).subsource = 'SS_DOUBLE';
pt(19).ndims     = '2';
pt(19).size      = '[]';

pt(20).blockname = 'MonitoringAndLogging /MtrLongAccTS';
pt(20).paramname = 'Gain';
pt(20).class     = 'scalar';
pt(20).nrows     = 1;
pt(20).ncols     = 1;
pt(20).subsource = 'SS_DOUBLE';
pt(20).ndims     = '2';
pt(20).size      = '[]';

pt(21).blockname = 'MonitoringAndLogging /MtrLongVel';
pt(21).paramname = 'Gain';
pt(21).class     = 'scalar';
pt(21).nrows     = 1;
pt(21).ncols     = 1;
pt(21).subsource = 'SS_DOUBLE';
pt(21).ndims     = '2';
pt(21).size      = '[]';

pt(22).blockname = 'MonitoringAndLogging /MtrLongVelTS';
pt(22).paramname = 'Gain';
pt(22).class     = 'scalar';
pt(22).nrows     = 1;
pt(22).ncols     = 1;
pt(22).subsource = 'SS_DOUBLE';
pt(22).ndims     = '2';
pt(22).size      = '[]';

pt(23).blockname = 'MonitoringAndLogging /gpsAltitude';
pt(23).paramname = 'Gain';
pt(23).class     = 'scalar';
pt(23).nrows     = 1;
pt(23).ncols     = 1;
pt(23).subsource = 'SS_DOUBLE';
pt(23).ndims     = '2';
pt(23).size      = '[]';

pt(24).blockname = 'MonitoringAndLogging /gpsAltitudeTS';
pt(24).paramname = 'Gain';
pt(24).class     = 'scalar';
pt(24).nrows     = 1;
pt(24).ncols     = 1;
pt(24).subsource = 'SS_DOUBLE';
pt(24).ndims     = '2';
pt(24).size      = '[]';

pt(25).blockname = 'MonitoringAndLogging /gpsHdng';
pt(25).paramname = 'Gain';
pt(25).class     = 'scalar';
pt(25).nrows     = 1;
pt(25).ncols     = 1;
pt(25).subsource = 'SS_DOUBLE';
pt(25).ndims     = '2';
pt(25).size      = '[]';

pt(26).blockname = 'MonitoringAndLogging /gpsHdngTS';
pt(26).paramname = 'Gain';
pt(26).class     = 'scalar';
pt(26).nrows     = 1;
pt(26).ncols     = 1;
pt(26).subsource = 'SS_DOUBLE';
pt(26).ndims     = '2';
pt(26).size      = '[]';

pt(27).blockname = 'MonitoringAndLogging /gpsLongVel';
pt(27).paramname = 'Gain';
pt(27).class     = 'scalar';
pt(27).nrows     = 1;
pt(27).ncols     = 1;
pt(27).subsource = 'SS_DOUBLE';
pt(27).ndims     = '2';
pt(27).size      = '[]';

pt(28).blockname = 'MonitoringAndLogging /gpsLongVelTS';
pt(28).paramname = 'Gain';
pt(28).class     = 'scalar';
pt(28).nrows     = 1;
pt(28).ncols     = 1;
pt(28).subsource = 'SS_DOUBLE';
pt(28).ndims     = '2';
pt(28).size      = '[]';

pt(29).blockname = 'MonitoringAndLogging /gpsNrOfSats';
pt(29).paramname = 'Gain';
pt(29).class     = 'scalar';
pt(29).nrows     = 1;
pt(29).ncols     = 1;
pt(29).subsource = 'SS_DOUBLE';
pt(29).ndims     = '2';
pt(29).size      = '[]';

pt(30).blockname = 'MonitoringAndLogging /gpsPosLat';
pt(30).paramname = 'Gain';
pt(30).class     = 'scalar';
pt(30).nrows     = 1;
pt(30).ncols     = 1;
pt(30).subsource = 'SS_DOUBLE';
pt(30).ndims     = '2';
pt(30).size      = '[]';

pt(31).blockname = 'MonitoringAndLogging /gpsPosLatTS';
pt(31).paramname = 'Gain';
pt(31).class     = 'scalar';
pt(31).nrows     = 1;
pt(31).ncols     = 1;
pt(31).subsource = 'SS_DOUBLE';
pt(31).ndims     = '2';
pt(31).size      = '[]';

pt(32).blockname = 'MonitoringAndLogging /gpsPosLon';
pt(32).paramname = 'Gain';
pt(32).class     = 'scalar';
pt(32).nrows     = 1;
pt(32).ncols     = 1;
pt(32).subsource = 'SS_DOUBLE';
pt(32).ndims     = '2';
pt(32).size      = '[]';

pt(33).blockname = 'MonitoringAndLogging /gpsPosLonTS';
pt(33).paramname = 'Gain';
pt(33).class     = 'scalar';
pt(33).nrows     = 1;
pt(33).ncols     = 1;
pt(33).subsource = 'SS_DOUBLE';
pt(33).ndims     = '2';
pt(33).size      = '[]';

pt(34).blockname = 'MonitoringAndLogging /gpsQuality';
pt(34).paramname = 'Gain';
pt(34).class     = 'scalar';
pt(34).nrows     = 1;
pt(34).ncols     = 1;
pt(34).subsource = 'SS_DOUBLE';
pt(34).ndims     = '2';
pt(34).size      = '[]';

pt(35).blockname = 'MonitoringAndLogging /gpsStatus';
pt(35).paramname = 'Gain';
pt(35).class     = 'scalar';
pt(35).nrows     = 1;
pt(35).ncols     = 1;
pt(35).subsource = 'SS_DOUBLE';
pt(35).ndims     = '2';
pt(35).size      = '[]';

pt(36).blockname = 'MonitoringAndLogging /gpsStatusTS';
pt(36).paramname = 'Gain';
pt(36).class     = 'scalar';
pt(36).nrows     = 1;
pt(36).ncols     = 1;
pt(36).subsource = 'SS_DOUBLE';
pt(36).ndims     = '2';
pt(36).size      = '[]';

pt(37).blockname = 'MonitoringAndLogging /gpsUTCtime_ddmmyy';
pt(37).paramname = 'Gain';
pt(37).class     = 'scalar';
pt(37).nrows     = 1;
pt(37).ncols     = 1;
pt(37).subsource = 'SS_DOUBLE';
pt(37).ndims     = '2';
pt(37).size      = '[]';

pt(38).blockname = 'MonitoringAndLogging /gpsUTCtime_hhmmsscc';
pt(38).paramname = 'Gain';
pt(38).class     = 'scalar';
pt(38).nrows     = 1;
pt(38).ncols     = 1;
pt(38).subsource = 'SS_DOUBLE';
pt(38).ndims     = '2';
pt(38).size      = '[]';

pt(39).blockname = 'Serial IN (Trimble)/FIFO read';
pt(39).paramname = 'P1';
pt(39).class     = 'scalar';
pt(39).nrows     = 1;
pt(39).ncols     = 1;
pt(39).subsource = 'SS_DOUBLE';
pt(39).ndims     = '2';
pt(39).size      = '[]';

pt(40).blockname = 'Serial IN (Trimble)/FIFO read';
pt(40).paramname = 'P2';
pt(40).class     = 'scalar';
pt(40).nrows     = 1;
pt(40).ncols     = 1;
pt(40).subsource = 'SS_DOUBLE';
pt(40).ndims     = '2';
pt(40).size      = '[]';

pt(41).blockname = 'Serial IN (Trimble)/FIFO read';
pt(41).paramname = 'P3';
pt(41).class     = 'scalar';
pt(41).nrows     = 1;
pt(41).ncols     = 1;
pt(41).subsource = 'SS_DOUBLE';
pt(41).ndims     = '2';
pt(41).size      = '[]';

pt(42).blockname = 'Serial IN (Trimble)/FIFO read';
pt(42).paramname = 'P4';
pt(42).class     = 'scalar';
pt(42).nrows     = 1;
pt(42).ncols     = 1;
pt(42).subsource = 'SS_DOUBLE';
pt(42).ndims     = '2';
pt(42).size      = '[]';

pt(43).blockname = 'Serial IN (Trimble)/FIFO read';
pt(43).paramname = 'P5';
pt(43).class     = 'scalar';
pt(43).nrows     = 1;
pt(43).ncols     = 1;
pt(43).subsource = 'SS_DOUBLE';
pt(43).ndims     = '2';
pt(43).size      = '[]';

pt(44).blockname = 'Serial IN (Trimble)/FIFO read';
pt(44).paramname = 'P6';
pt(44).class     = 'scalar';
pt(44).nrows     = 1;
pt(44).ncols     = 1;
pt(44).subsource = 'SS_DOUBLE';
pt(44).ndims     = '2';
pt(44).size      = '[]';

pt(45).blockname = 'Serial IN (Trimble)/FIFO read';
pt(45).paramname = 'P7';
pt(45).class     = 'scalar';
pt(45).nrows     = 1;
pt(45).ncols     = 1;
pt(45).subsource = 'SS_DOUBLE';
pt(45).ndims     = '2';
pt(45).size      = '[]';

pt(46).blockname = 'Serial IN (Trimble)/FIFO read';
pt(46).paramname = 'P8';
pt(46).class     = 'scalar';
pt(46).nrows     = 1;
pt(46).ncols     = 1;
pt(46).subsource = 'SS_DOUBLE';
pt(46).ndims     = '2';
pt(46).size      = '[]';

pt(47).blockname = 'StaticVehicleParams/length';
pt(47).paramname = 'Value';
pt(47).class     = 'scalar';
pt(47).nrows     = 1;
pt(47).ncols     = 1;
pt(47).subsource = 'SS_DOUBLE';
pt(47).ndims     = '2';
pt(47).size      = '[]';

pt(48).blockname = 'StaticVehicleParams/rearAxleLocation';
pt(48).paramname = 'Value';
pt(48).class     = 'scalar';
pt(48).nrows     = 1;
pt(48).ncols     = 1;
pt(48).subsource = 'SS_DOUBLE';
pt(48).ndims     = '2';
pt(48).size      = '[]';

pt(49).blockname = 'StaticVehicleParams/width';
pt(49).paramname = 'Value';
pt(49).class     = 'scalar';
pt(49).nrows     = 1;
pt(49).ncols     = 1;
pt(49).subsource = 'SS_DOUBLE';
pt(49).ndims     = '2';
pt(49).size      = '[]';

pt(50).blockname = 'StaticVehicleParams1/length';
pt(50).paramname = 'Value';
pt(50).class     = 'scalar';
pt(50).nrows     = 1;
pt(50).ncols     = 1;
pt(50).subsource = 'SS_DOUBLE';
pt(50).ndims     = '2';
pt(50).size      = '[]';

pt(51).blockname = 'StaticVehicleParams1/rearAxleLocation';
pt(51).paramname = 'Value';
pt(51).class     = 'scalar';
pt(51).nrows     = 1;
pt(51).ncols     = 1;
pt(51).subsource = 'SS_DOUBLE';
pt(51).ndims     = '2';
pt(51).size      = '[]';

pt(52).blockname = 'StaticVehicleParams1/width';
pt(52).paramname = 'Value';
pt(52).class     = 'scalar';
pt(52).nrows     = 1;
pt(52).ncols     = 1;
pt(52).subsource = 'SS_DOUBLE';
pt(52).ndims     = '2';
pt(52).size      = '[]';

pt(53).blockname = 'Subsystem/ECEFg0_';
pt(53).paramname = 'Gain';
pt(53).class     = 'scalar';
pt(53).nrows     = 1;
pt(53).ncols     = 1;
pt(53).subsource = 'SS_DOUBLE';
pt(53).ndims     = '2';
pt(53).size      = '[]';

pt(54).blockname = 'Subsystem/ECEFr0_';
pt(54).paramname = 'Gain';
pt(54).class     = 'scalar';
pt(54).nrows     = 1;
pt(54).ncols     = 1;
pt(54).subsource = 'SS_DOUBLE';
pt(54).ndims     = '2';
pt(54).size      = '[]';

pt(55).blockname = 'Subsystem/originInitialized_';
pt(55).paramname = 'Gain';
pt(55).class     = 'scalar';
pt(55).nrows     = 1;
pt(55).ncols     = 1;
pt(55).subsource = 'SS_DOUBLE';
pt(55).ndims     = '2';
pt(55).size      = '[]';

pt(56).blockname = 'Subsystem1/RCV_Speed';
pt(56).paramname = 'Value';
pt(56).class     = 'scalar';
pt(56).nrows     = 1;
pt(56).ncols     = 1;
pt(56).subsource = 'SS_DOUBLE';
pt(56).ndims     = '2';
pt(56).size      = '[]';

pt(57).blockname = 'Subsystem1/DistToFront';
pt(57).paramname = 'Gain';
pt(57).class     = 'scalar';
pt(57).nrows     = 1;
pt(57).ncols     = 1;
pt(57).subsource = 'SS_DOUBLE';
pt(57).ndims     = '2';
pt(57).size      = '[]';

pt(58).blockname = 'Subsystem1/FwdLeftMioLatDist';
pt(58).paramname = 'Gain';
pt(58).class     = 'scalar';
pt(58).nrows     = 1;
pt(58).ncols     = 1;
pt(58).subsource = 'SS_DOUBLE';
pt(58).ndims     = '2';
pt(58).size      = '[]';

pt(59).blockname = 'Subsystem1/FwdLeftMioLongDist';
pt(59).paramname = 'Gain';
pt(59).class     = 'scalar';
pt(59).nrows     = 1;
pt(59).ncols     = 1;
pt(59).subsource = 'SS_DOUBLE';
pt(59).ndims     = '2';
pt(59).size      = '[]';

pt(60).blockname = 'Subsystem1/FwdMioLaDist';
pt(60).paramname = 'Gain';
pt(60).class     = 'scalar';
pt(60).nrows     = 1;
pt(60).ncols     = 1;
pt(60).subsource = 'SS_DOUBLE';
pt(60).ndims     = '2';
pt(60).size      = '[]';

pt(61).blockname = 'Subsystem1/FwdMioLongDist';
pt(61).paramname = 'Gain';
pt(61).class     = 'scalar';
pt(61).nrows     = 1;
pt(61).ncols     = 1;
pt(61).subsource = 'SS_DOUBLE';
pt(61).ndims     = '2';
pt(61).size      = '[]';

pt(62).blockname = 'Subsystem1/FwdRightMioLatDist';
pt(62).paramname = 'Gain';
pt(62).class     = 'scalar';
pt(62).nrows     = 1;
pt(62).ncols     = 1;
pt(62).subsource = 'SS_DOUBLE';
pt(62).ndims     = '2';
pt(62).size      = '[]';

pt(63).blockname = 'Subsystem1/FwdRightMioLongDist';
pt(63).paramname = 'Gain';
pt(63).class     = 'scalar';
pt(63).nrows     = 1;
pt(63).ncols     = 1;
pt(63).subsource = 'SS_DOUBLE';
pt(63).ndims     = '2';
pt(63).size      = '[]';

pt(64).blockname = 'Subsystem1/RTS1_Long_dist';
pt(64).paramname = 'Gain';
pt(64).class     = 'scalar';
pt(64).nrows     = 1;
pt(64).ncols     = 1;
pt(64).subsource = 'SS_DOUBLE';
pt(64).ndims     = '2';
pt(64).size      = '[]';

pt(65).blockname = 'Subsystem1/RTS2_Long_dist';
pt(65).paramname = 'Gain';
pt(65).class     = 'scalar';
pt(65).nrows     = 1;
pt(65).ncols     = 1;
pt(65).subsource = 'SS_DOUBLE';
pt(65).ndims     = '2';
pt(65).size      = '[]';

pt(66).blockname = 'Subsystem1/RTS3_Long_dist';
pt(66).paramname = 'Gain';
pt(66).class     = 'scalar';
pt(66).nrows     = 1;
pt(66).ncols     = 1;
pt(66).subsource = 'SS_DOUBLE';
pt(66).ndims     = '2';
pt(66).size      = '[]';

pt(67).blockname = 'Subsystem1/RTS4_Long_dist';
pt(67).paramname = 'Gain';
pt(67).class     = 'scalar';
pt(67).nrows     = 1;
pt(67).ncols     = 1;
pt(67).subsource = 'SS_DOUBLE';
pt(67).ndims     = '2';
pt(67).size      = '[]';

pt(68).blockname = 'Subsystem1/RTS5_Long_dist';
pt(68).paramname = 'Gain';
pt(68).class     = 'scalar';
pt(68).nrows     = 1;
pt(68).ncols     = 1;
pt(68).subsource = 'SS_DOUBLE';
pt(68).ndims     = '2';
pt(68).size      = '[]';

pt(69).blockname = 'Subsystem1/RTS6_Long_dist';
pt(69).paramname = 'Gain';
pt(69).class     = 'scalar';
pt(69).nrows     = 1;
pt(69).ncols     = 1;
pt(69).subsource = 'SS_DOUBLE';
pt(69).ndims     = '2';
pt(69).size      = '[]';

pt(70).blockname = 'Subsystem1/is_valid_MIO';
pt(70).paramname = 'Gain';
pt(70).class     = 'scalar';
pt(70).nrows     = 1;
pt(70).ncols     = 1;
pt(70).subsource = 'SS_DOUBLE';
pt(70).ndims     = '2';
pt(70).size      = '[]';

pt(71).blockname = 'Subsystem1/is_valid_left_MIO';
pt(71).paramname = 'Gain';
pt(71).class     = 'scalar';
pt(71).nrows     = 1;
pt(71).ncols     = 1;
pt(71).subsource = 'SS_DOUBLE';
pt(71).ndims     = '2';
pt(71).size      = '[]';

pt(72).blockname = 'Subsystem1/is_valid_right_MIO';
pt(72).paramname = 'Gain';
pt(72).class     = 'scalar';
pt(72).nrows     = 1;
pt(72).ncols     = 1;
pt(72).subsource = 'SS_DOUBLE';
pt(72).ndims     = '2';
pt(72).size      = '[]';

pt(73).blockname = 'Subsystem1/long_dist_MIO1';
pt(73).paramname = 'Gain';
pt(73).class     = 'scalar';
pt(73).nrows     = 1;
pt(73).ncols     = 1;
pt(73).subsource = 'SS_DOUBLE';
pt(73).ndims     = '2';
pt(73).size      = '[]';

pt(74).blockname = 'UDP IN (Cohda)1/Detect Change';
pt(74).paramname = 'vinit';
pt(74).class     = 'scalar';
pt(74).nrows     = 1;
pt(74).ncols     = 1;
pt(74).subsource = 'SS_INT32';
pt(74).ndims     = '2';
pt(74).size      = '[]';

pt(75).blockname = 'UDP IN (Cohda)1/Detect Change1';
pt(75).paramname = 'vinit';
pt(75).class     = 'scalar';
pt(75).nrows     = 1;
pt(75).ncols     = 1;
pt(75).subsource = 'SS_UINT32';
pt(75).ndims     = '2';
pt(75).size      = '[]';

pt(76).blockname = 'UDP IN (Cohda)1/Detect Change2';
pt(76).paramname = 'vinit';
pt(76).class     = 'scalar';
pt(76).nrows     = 1;
pt(76).ncols     = 1;
pt(76).subsource = 'SS_UINT32';
pt(76).ndims     = '2';
pt(76).size      = '[]';

pt(77).blockname = 'V2V Out Convert/CtrlType';
pt(77).paramname = 'Value';
pt(77).class     = 'scalar';
pt(77).nrows     = 1;
pt(77).ncols     = 1;
pt(77).subsource = 'SS_DOUBLE';
pt(77).ndims     = '2';
pt(77).size      = '[]';

pt(78).blockname = 'V2V Out Convert/DrivLaneStatusXpc';
pt(78).paramname = 'Value';
pt(78).class     = 'scalar';
pt(78).nrows     = 1;
pt(78).ncols     = 1;
pt(78).subsource = 'SS_DOUBLE';
pt(78).ndims     = '2';
pt(78).size      = '[]';

pt(79).blockname = 'V2V Out Convert/EntranceLaneCZXpc';
pt(79).paramname = 'Value';
pt(79).class     = 'scalar';
pt(79).nrows     = 1;
pt(79).ncols     = 1;
pt(79).subsource = 'SS_DOUBLE';
pt(79).ndims     = '2';
pt(79).size      = '[]';

pt(80).blockname = 'V2V Out Convert/EoSXpc';
pt(80).paramname = 'Value';
pt(80).class     = 'scalar';
pt(80).nrows     = 1;
pt(80).ncols     = 1;
pt(80).subsource = 'SS_DOUBLE';
pt(80).ndims     = '2';
pt(80).size      = '[]';

pt(81).blockname = 'V2V Out Convert/IntentionXpc';
pt(81).paramname = 'Value';
pt(81).class     = 'scalar';
pt(81).nrows     = 1;
pt(81).ncols     = 1;
pt(81).subsource = 'SS_DOUBLE';
pt(81).ndims     = '2';
pt(81).size      = '[]';

pt(82).blockname = 'V2V Out Convert/LongAccConfXpc';
pt(82).paramname = 'Value';
pt(82).class     = 'scalar';
pt(82).nrows     = 1;
pt(82).ncols     = 1;
pt(82).subsource = 'SS_DOUBLE';
pt(82).ndims     = '2';
pt(82).size      = '[]';

pt(83).blockname = 'V2V Out Convert/MioBearingXpc';
pt(83).paramname = 'Value';
pt(83).class     = 'scalar';
pt(83).nrows     = 1;
pt(83).ncols     = 1;
pt(83).subsource = 'SS_DOUBLE';
pt(83).ndims     = '2';
pt(83).size      = '[]';

pt(84).blockname = 'V2V Out Convert/MioIdXpc';
pt(84).paramname = 'Value';
pt(84).class     = 'scalar';
pt(84).nrows     = 1;
pt(84).ncols     = 1;
pt(84).subsource = 'SS_DOUBLE';
pt(84).ndims     = '2';
pt(84).size      = '[]';

pt(85).blockname = 'V2V Out Convert/MioRangeRateXpc';
pt(85).paramname = 'Value';
pt(85).class     = 'scalar';
pt(85).nrows     = 1;
pt(85).ncols     = 1;
pt(85).subsource = 'SS_DOUBLE';
pt(85).ndims     = '2';
pt(85).size      = '[]';

pt(86).blockname = 'V2V Out Convert/MioRangeXpc';
pt(86).paramname = 'Value';
pt(86).class     = 'scalar';
pt(86).nrows     = 1;
pt(86).ncols     = 1;
pt(86).subsource = 'SS_DOUBLE';
pt(86).ndims     = '2';
pt(86).size      = '[]';

pt(87).blockname = 'V2V Out Convert/PosConfEllipseInterval95';
pt(87).paramname = 'Value';
pt(87).class     = 'scalar';
pt(87).nrows     = 1;
pt(87).ncols     = 1;
pt(87).subsource = 'SS_DOUBLE';
pt(87).ndims     = '2';
pt(87).size      = '[]';

pt(88).blockname = 'V2V Out Convert/RearAxlePos';
pt(88).paramname = 'Value';
pt(88).class     = 'scalar';
pt(88).nrows     = 1;
pt(88).ncols     = 1;
pt(88).subsource = 'SS_DOUBLE';
pt(88).ndims     = '2';
pt(88).size      = '[]';

pt(89).blockname = 'V2V Out Convert/StatioID';
pt(89).paramname = 'Value';
pt(89).class     = 'scalar';
pt(89).nrows     = 1;
pt(89).ncols     = 1;
pt(89).subsource = 'SS_DOUBLE';
pt(89).ndims     = '2';
pt(89).size      = '[]';

pt(90).blockname = 'V2V Out Convert/StationType';
pt(90).paramname = 'Value';
pt(90).class     = 'scalar';
pt(90).nrows     = 1;
pt(90).ncols     = 1;
pt(90).subsource = 'SS_DOUBLE';
pt(90).ndims     = '2';
pt(90).size      = '[]';

pt(91).blockname = 'V2V Out Convert/VehHeadingVal';
pt(91).paramname = 'Value';
pt(91).class     = 'scalar';
pt(91).nrows     = 1;
pt(91).ncols     = 1;
pt(91).subsource = 'SS_DOUBLE';
pt(91).ndims     = '2';
pt(91).size      = '[]';

pt(92).blockname = 'V2V Out Convert/VehLength';
pt(92).paramname = 'Value';
pt(92).class     = 'scalar';
pt(92).nrows     = 1;
pt(92).ncols     = 1;
pt(92).subsource = 'SS_DOUBLE';
pt(92).ndims     = '2';
pt(92).size      = '[]';

pt(93).blockname = 'V2V Out Convert/VehRespoTimeCnst';
pt(93).paramname = 'Value';
pt(93).class     = 'scalar';
pt(93).nrows     = 1;
pt(93).ncols     = 1;
pt(93).subsource = 'SS_DOUBLE';
pt(93).ndims     = '2';
pt(93).size      = '[]';

pt(94).blockname = 'V2V Out Convert/VehRespoTimeDelay';
pt(94).paramname = 'Value';
pt(94).class     = 'scalar';
pt(94).nrows     = 1;
pt(94).ncols     = 1;
pt(94).subsource = 'SS_DOUBLE';
pt(94).ndims     = '2';
pt(94).size      = '[]';

pt(95).blockname = 'V2V Out Convert/VehRole';
pt(95).paramname = 'Value';
pt(95).class     = 'scalar';
pt(95).nrows     = 1;
pt(95).ncols     = 1;
pt(95).subsource = 'SS_DOUBLE';
pt(95).ndims     = '2';
pt(95).size      = '[]';

pt(96).blockname = 'V2V Out Convert/VehSpeedConf';
pt(96).paramname = 'Value';
pt(96).class     = 'scalar';
pt(96).nrows     = 1;
pt(96).ncols     = 1;
pt(96).subsource = 'SS_DOUBLE';
pt(96).ndims     = '2';
pt(96).size      = '[]';

pt(97).blockname = 'V2V Out Convert/VehWidth';
pt(97).paramname = 'Value';
pt(97).class     = 'scalar';
pt(97).nrows     = 1;
pt(97).ncols     = 1;
pt(97).subsource = 'SS_DOUBLE';
pt(97).ndims     = '2';
pt(97).size      = '[]';

pt(98).blockname = 'V2V Out Convert/VehYawRateConf';
pt(98).paramname = 'Value';
pt(98).class     = 'scalar';
pt(98).nrows     = 1;
pt(98).ncols     = 1;
pt(98).subsource = 'SS_DOUBLE';
pt(98).ndims     = '2';
pt(98).size      = '[]';

pt(99).blockname = 'V2V Out Convert/Res';
pt(99).paramname = 'Gain';
pt(99).class     = 'scalar';
pt(99).nrows     = 1;
pt(99).ncols     = 1;
pt(99).subsource = 'SS_DOUBLE';
pt(99).ndims     = '2';
pt(99).size      = '[]';

pt(100).blockname = 'V2V Out Convert/Res 0.002';
pt(100).paramname = 'Gain';
pt(100).class     = 'scalar';
pt(100).nrows     = 1;
pt(100).ncols     = 1;
pt(100).subsource = 'SS_DOUBLE';
pt(100).ndims     = '2';
pt(100).size      = '[]';

pt(101).blockname = 'V2V Out Convert/Res 0.01';
pt(101).paramname = 'Gain';
pt(101).class     = 'scalar';
pt(101).nrows     = 1;
pt(101).ncols     = 1;
pt(101).subsource = 'SS_DOUBLE';
pt(101).ndims     = '2';
pt(101).size      = '[]';

pt(102).blockname = 'V2V Out Convert/Res 0.1';
pt(102).paramname = 'Gain';
pt(102).class     = 'scalar';
pt(102).nrows     = 1;
pt(102).ncols     = 1;
pt(102).subsource = 'SS_DOUBLE';
pt(102).ndims     = '2';
pt(102).size      = '[]';

pt(103).blockname = 'V2V Out Convert/Res 1';
pt(103).paramname = 'Gain';
pt(103).class     = 'scalar';
pt(103).nrows     = 1;
pt(103).ncols     = 1;
pt(103).subsource = 'SS_DOUBLE';
pt(103).ndims     = '2';
pt(103).size      = '[]';

pt(104).blockname = 'V2V Out Convert/Res 2';
pt(104).paramname = 'Gain';
pt(104).class     = 'scalar';
pt(104).nrows     = 1;
pt(104).ncols     = 1;
pt(104).subsource = 'SS_DOUBLE';
pt(104).ndims     = '2';
pt(104).size      = '[]';

pt(105).blockname = 'V2V Out Convert/Res 3';
pt(105).paramname = 'Gain';
pt(105).class     = 'scalar';
pt(105).nrows     = 1;
pt(105).ncols     = 1;
pt(105).subsource = 'SS_DOUBLE';
pt(105).ndims     = '2';
pt(105).size      = '[]';

pt(106).blockname = 'V2V Out Convert/Res1 0.01';
pt(106).paramname = 'Gain';
pt(106).class     = 'scalar';
pt(106).nrows     = 1;
pt(106).ncols     = 1;
pt(106).subsource = 'SS_DOUBLE';
pt(106).ndims     = '2';
pt(106).size      = '[]';

pt(107).blockname = 'V2V Out Convert/Res1 0.1';
pt(107).paramname = 'Gain';
pt(107).class     = 'scalar';
pt(107).nrows     = 1;
pt(107).ncols     = 1;
pt(107).subsource = 'SS_DOUBLE';
pt(107).ndims     = '2';
pt(107).size      = '[]';

pt(108).blockname = 'V2V Out Convert/Res2';
pt(108).paramname = 'Gain';
pt(108).class     = 'scalar';
pt(108).nrows     = 1;
pt(108).ncols     = 1;
pt(108).subsource = 'SS_DOUBLE';
pt(108).ndims     = '2';
pt(108).size      = '[]';

pt(109).blockname = 'V2V Out Convert/Res2 0.01';
pt(109).paramname = 'Gain';
pt(109).class     = 'scalar';
pt(109).nrows     = 1;
pt(109).ncols     = 1;
pt(109).subsource = 'SS_DOUBLE';
pt(109).ndims     = '2';
pt(109).size      = '[]';

pt(110).blockname = 'V2V Out Convert/Res2 0.2';
pt(110).paramname = 'Gain';
pt(110).class     = 'scalar';
pt(110).nrows     = 1;
pt(110).ncols     = 1;
pt(110).subsource = 'SS_DOUBLE';
pt(110).ndims     = '2';
pt(110).size      = '[]';

pt(111).blockname = 'V2V Out Convert/Sec2msec2';
pt(111).paramname = 'Gain';
pt(111).class     = 'scalar';
pt(111).nrows     = 1;
pt(111).ncols     = 1;
pt(111).subsource = 'SS_DOUBLE';
pt(111).ndims     = '2';
pt(111).size      = '[]';

pt(112).blockname = 'V2V Out Convert/-+1571';
pt(112).paramname = 'UpperLimit';
pt(112).class     = 'scalar';
pt(112).nrows     = 1;
pt(112).ncols     = 1;
pt(112).subsource = 'SS_DOUBLE';
pt(112).ndims     = '2';
pt(112).size      = '[]';

pt(113).blockname = 'V2V Out Convert/-+1571';
pt(113).paramname = 'LowerLimit';
pt(113).class     = 'scalar';
pt(113).nrows     = 1;
pt(113).ncols     = 1;
pt(113).subsource = 'SS_DOUBLE';
pt(113).ndims     = '2';
pt(113).size      = '[]';

pt(114).blockname = 'V2V Out Convert/-+32767';
pt(114).paramname = 'UpperLimit';
pt(114).class     = 'scalar';
pt(114).nrows     = 1;
pt(114).ncols     = 1;
pt(114).subsource = 'SS_DOUBLE';
pt(114).ndims     = '2';
pt(114).size      = '[]';

pt(115).blockname = 'V2V Out Convert/-+32767';
pt(115).paramname = 'LowerLimit';
pt(115).class     = 'scalar';
pt(115).nrows     = 1;
pt(115).ncols     = 1;
pt(115).subsource = 'SS_DOUBLE';
pt(115).ndims     = '2';
pt(115).size      = '[]';

pt(116).blockname = 'V2V Out Convert/-1-14';
pt(116).paramname = 'UpperLimit';
pt(116).class     = 'scalar';
pt(116).nrows     = 1;
pt(116).ncols     = 1;
pt(116).subsource = 'SS_DOUBLE';
pt(116).ndims     = '2';
pt(116).size      = '[]';

pt(117).blockname = 'V2V Out Convert/-1-14';
pt(117).paramname = 'LowerLimit';
pt(117).class     = 'scalar';
pt(117).nrows     = 1;
pt(117).ncols     = 1;
pt(117).subsource = 'SS_DOUBLE';
pt(117).ndims     = '2';
pt(117).size      = '[]';

pt(118).blockname = 'V2V Out Convert/0-10000';
pt(118).paramname = 'UpperLimit';
pt(118).class     = 'scalar';
pt(118).nrows     = 1;
pt(118).ncols     = 1;
pt(118).subsource = 'SS_DOUBLE';
pt(118).ndims     = '2';
pt(118).size      = '[]';

pt(119).blockname = 'V2V Out Convert/0-10000';
pt(119).paramname = 'LowerLimit';
pt(119).class     = 'scalar';
pt(119).nrows     = 1;
pt(119).ncols     = 1;
pt(119).subsource = 'SS_DOUBLE';
pt(119).ndims     = '2';
pt(119).size      = '[]';

pt(120).blockname = 'V2V Out Convert/0-3';
pt(120).paramname = 'UpperLimit';
pt(120).class     = 'scalar';
pt(120).nrows     = 1;
pt(120).ncols     = 1;
pt(120).subsource = 'SS_DOUBLE';
pt(120).ndims     = '2';
pt(120).size      = '[]';

pt(121).blockname = 'V2V Out Convert/0-3';
pt(121).paramname = 'LowerLimit';
pt(121).class     = 'scalar';
pt(121).nrows     = 1;
pt(121).ncols     = 1;
pt(121).subsource = 'SS_DOUBLE';
pt(121).ndims     = '2';
pt(121).size      = '[]';

pt(122).blockname = 'V2V Out Convert/0-3 ';
pt(122).paramname = 'UpperLimit';
pt(122).class     = 'scalar';
pt(122).nrows     = 1;
pt(122).ncols     = 1;
pt(122).subsource = 'SS_DOUBLE';
pt(122).ndims     = '2';
pt(122).size      = '[]';

pt(123).blockname = 'V2V Out Convert/0-3 ';
pt(123).paramname = 'LowerLimit';
pt(123).class     = 'scalar';
pt(123).nrows     = 1;
pt(123).ncols     = 1;
pt(123).subsource = 'SS_DOUBLE';
pt(123).ndims     = '2';
pt(123).size      = '[]';

pt(124).blockname = 'V2V Out Convert/0-361';
pt(124).paramname = 'UpperLimit';
pt(124).class     = 'scalar';
pt(124).nrows     = 1;
pt(124).ncols     = 1;
pt(124).subsource = 'SS_DOUBLE';
pt(124).ndims     = '2';
pt(124).size      = '[]';

pt(125).blockname = 'V2V Out Convert/0-361';
pt(125).paramname = 'LowerLimit';
pt(125).class     = 'scalar';
pt(125).nrows     = 1;
pt(125).ncols     = 1;
pt(125).subsource = 'SS_DOUBLE';
pt(125).ndims     = '2';
pt(125).size      = '[]';

pt(126).blockname = 'V2V Out Convert/0-5001';
pt(126).paramname = 'UpperLimit';
pt(126).class     = 'scalar';
pt(126).nrows     = 1;
pt(126).ncols     = 1;
pt(126).subsource = 'SS_DOUBLE';
pt(126).ndims     = '2';
pt(126).size      = '[]';

pt(127).blockname = 'V2V Out Convert/0-5001';
pt(127).paramname = 'LowerLimit';
pt(127).class     = 'scalar';
pt(127).nrows     = 1;
pt(127).ncols     = 1;
pt(127).subsource = 'SS_DOUBLE';
pt(127).ndims     = '2';
pt(127).size      = '[]';

pt(128).blockname = 'V2V Out Convert/0-65535';
pt(128).paramname = 'UpperLimit';
pt(128).class     = 'scalar';
pt(128).nrows     = 1;
pt(128).ncols     = 1;
pt(128).subsource = 'SS_DOUBLE';
pt(128).ndims     = '2';
pt(128).size      = '[]';

pt(129).blockname = 'V2V Out Convert/0-65535';
pt(129).paramname = 'LowerLimit';
pt(129).class     = 'scalar';
pt(129).nrows     = 1;
pt(129).ncols     = 1;
pt(129).subsource = 'SS_DOUBLE';
pt(129).ndims     = '2';
pt(129).size      = '[]';

pt(130).blockname = 'V2V Out Convert/1-14';
pt(130).paramname = 'UpperLimit';
pt(130).class     = 'scalar';
pt(130).nrows     = 1;
pt(130).ncols     = 1;
pt(130).subsource = 'SS_DOUBLE';
pt(130).ndims     = '2';
pt(130).size      = '[]';

pt(131).blockname = 'V2V Out Convert/1-14';
pt(131).paramname = 'LowerLimit';
pt(131).class     = 'scalar';
pt(131).nrows     = 1;
pt(131).ncols     = 1;
pt(131).subsource = 'SS_DOUBLE';
pt(131).ndims     = '2';
pt(131).size      = '[]';

pt(132).blockname = 'V2V Out Convert/1-3';
pt(132).paramname = 'UpperLimit';
pt(132).class     = 'scalar';
pt(132).nrows     = 1;
pt(132).ncols     = 1;
pt(132).subsource = 'SS_DOUBLE';
pt(132).ndims     = '2';
pt(132).size      = '[]';

pt(133).blockname = 'V2V Out Convert/1-3';
pt(133).paramname = 'LowerLimit';
pt(133).class     = 'scalar';
pt(133).nrows     = 1;
pt(133).ncols     = 1;
pt(133).subsource = 'SS_DOUBLE';
pt(133).ndims     = '2';
pt(133).size      = '[]';

pt(134).blockname = 'V2V Out Convert/1-4';
pt(134).paramname = 'UpperLimit';
pt(134).class     = 'scalar';
pt(134).nrows     = 1;
pt(134).ncols     = 1;
pt(134).subsource = 'SS_DOUBLE';
pt(134).ndims     = '2';
pt(134).size      = '[]';

pt(135).blockname = 'V2V Out Convert/1-4';
pt(135).paramname = 'LowerLimit';
pt(135).class     = 'scalar';
pt(135).nrows     = 1;
pt(135).ncols     = 1;
pt(135).subsource = 'SS_DOUBLE';
pt(135).ndims     = '2';
pt(135).size      = '[]';

pt(136).blockname = 'V2V Out Convert/8bit sat6';
pt(136).paramname = 'UpperLimit';
pt(136).class     = 'scalar';
pt(136).nrows     = 1;
pt(136).ncols     = 1;
pt(136).subsource = 'SS_DOUBLE';
pt(136).ndims     = '2';
pt(136).size      = '[]';

pt(137).blockname = 'V2V Out Convert/8bit sat6';
pt(137).paramname = 'LowerLimit';
pt(137).class     = 'scalar';
pt(137).nrows     = 1;
pt(137).ncols     = 1;
pt(137).subsource = 'SS_DOUBLE';
pt(137).ndims     = '2';
pt(137).size      = '[]';

pt(138).blockname = 'V2V Out Convert/8bit sat7';
pt(138).paramname = 'UpperLimit';
pt(138).class     = 'scalar';
pt(138).nrows     = 1;
pt(138).ncols     = 1;
pt(138).subsource = 'SS_DOUBLE';
pt(138).ndims     = '2';
pt(138).size      = '[]';

pt(139).blockname = 'V2V Out Convert/8bit sat7';
pt(139).paramname = 'LowerLimit';
pt(139).class     = 'scalar';
pt(139).nrows     = 1;
pt(139).ncols     = 1;
pt(139).subsource = 'SS_DOUBLE';
pt(139).ndims     = '2';
pt(139).size      = '[]';

pt(140).blockname = 'V2V Out Convert/boolean sat';
pt(140).paramname = 'UpperLimit';
pt(140).class     = 'scalar';
pt(140).nrows     = 1;
pt(140).ncols     = 1;
pt(140).subsource = 'SS_DOUBLE';
pt(140).ndims     = '2';
pt(140).size      = '[]';

pt(141).blockname = 'V2V Out Convert/boolean sat';
pt(141).paramname = 'LowerLimit';
pt(141).class     = 'scalar';
pt(141).nrows     = 1;
pt(141).ncols     = 1;
pt(141).subsource = 'SS_DOUBLE';
pt(141).ndims     = '2';
pt(141).size      = '[]';

pt(142).blockname = 'V2V Out Convert/boolean sat1';
pt(142).paramname = 'UpperLimit';
pt(142).class     = 'scalar';
pt(142).nrows     = 1;
pt(142).ncols     = 1;
pt(142).subsource = 'SS_DOUBLE';
pt(142).ndims     = '2';
pt(142).size      = '[]';

pt(143).blockname = 'V2V Out Convert/boolean sat1';
pt(143).paramname = 'LowerLimit';
pt(143).class     = 'scalar';
pt(143).nrows     = 1;
pt(143).ncols     = 1;
pt(143).subsource = 'SS_DOUBLE';
pt(143).ndims     = '2';
pt(143).size      = '[]';

pt(144).blockname = 'V2V Out Convert/boolean sat2';
pt(144).paramname = 'UpperLimit';
pt(144).class     = 'scalar';
pt(144).nrows     = 1;
pt(144).ncols     = 1;
pt(144).subsource = 'SS_DOUBLE';
pt(144).ndims     = '2';
pt(144).size      = '[]';

pt(145).blockname = 'V2V Out Convert/boolean sat2';
pt(145).paramname = 'LowerLimit';
pt(145).class     = 'scalar';
pt(145).nrows     = 1;
pt(145).ncols     = 1;
pt(145).subsource = 'SS_DOUBLE';
pt(145).ndims     = '2';
pt(145).size      = '[]';

pt(146).blockname = 'V2V Out Convert/boolean sat3';
pt(146).paramname = 'UpperLimit';
pt(146).class     = 'scalar';
pt(146).nrows     = 1;
pt(146).ncols     = 1;
pt(146).subsource = 'SS_DOUBLE';
pt(146).ndims     = '2';
pt(146).size      = '[]';

pt(147).blockname = 'V2V Out Convert/boolean sat3';
pt(147).paramname = 'LowerLimit';
pt(147).class     = 'scalar';
pt(147).nrows     = 1;
pt(147).ncols     = 1;
pt(147).subsource = 'SS_DOUBLE';
pt(147).ndims     = '2';
pt(147).size      = '[]';

pt(148).blockname = 'V2V Out Convert/boolean sat4';
pt(148).paramname = 'UpperLimit';
pt(148).class     = 'scalar';
pt(148).nrows     = 1;
pt(148).ncols     = 1;
pt(148).subsource = 'SS_DOUBLE';
pt(148).ndims     = '2';
pt(148).size      = '[]';

pt(149).blockname = 'V2V Out Convert/boolean sat4';
pt(149).paramname = 'LowerLimit';
pt(149).class     = 'scalar';
pt(149).nrows     = 1;
pt(149).ncols     = 1;
pt(149).subsource = 'SS_DOUBLE';
pt(149).ndims     = '2';
pt(149).size      = '[]';

pt(150).blockname = 'V2V Out Convert/boolean sat5';
pt(150).paramname = 'UpperLimit';
pt(150).class     = 'scalar';
pt(150).nrows     = 1;
pt(150).ncols     = 1;
pt(150).subsource = 'SS_DOUBLE';
pt(150).ndims     = '2';
pt(150).size      = '[]';

pt(151).blockname = 'V2V Out Convert/boolean sat5';
pt(151).paramname = 'LowerLimit';
pt(151).class     = 'scalar';
pt(151).nrows     = 1;
pt(151).ncols     = 1;
pt(151).subsource = 'SS_DOUBLE';
pt(151).ndims     = '2';
pt(151).size      = '[]';

pt(152).blockname = 'V2V Out Convert/boolean sat6';
pt(152).paramname = 'UpperLimit';
pt(152).class     = 'scalar';
pt(152).nrows     = 1;
pt(152).ncols     = 1;
pt(152).subsource = 'SS_DOUBLE';
pt(152).ndims     = '2';
pt(152).size      = '[]';

pt(153).blockname = 'V2V Out Convert/boolean sat6';
pt(153).paramname = 'LowerLimit';
pt(153).class     = 'scalar';
pt(153).nrows     = 1;
pt(153).ncols     = 1;
pt(153).subsource = 'SS_DOUBLE';
pt(153).ndims     = '2';
pt(153).size      = '[]';

pt(154).blockname = 'V2V Out Convert/boolean sat7';
pt(154).paramname = 'UpperLimit';
pt(154).class     = 'scalar';
pt(154).nrows     = 1;
pt(154).ncols     = 1;
pt(154).subsource = 'SS_DOUBLE';
pt(154).ndims     = '2';
pt(154).size      = '[]';

pt(155).blockname = 'V2V Out Convert/boolean sat7';
pt(155).paramname = 'LowerLimit';
pt(155).class     = 'scalar';
pt(155).nrows     = 1;
pt(155).ncols     = 1;
pt(155).subsource = 'SS_DOUBLE';
pt(155).ndims     = '2';
pt(155).size      = '[]';

pt(156).blockname = 'V2V Out Convert/boolean sat8';
pt(156).paramname = 'UpperLimit';
pt(156).class     = 'scalar';
pt(156).nrows     = 1;
pt(156).ncols     = 1;
pt(156).subsource = 'SS_DOUBLE';
pt(156).ndims     = '2';
pt(156).size      = '[]';

pt(157).blockname = 'V2V Out Convert/boolean sat8';
pt(157).paramname = 'LowerLimit';
pt(157).class     = 'scalar';
pt(157).nrows     = 1;
pt(157).ncols     = 1;
pt(157).subsource = 'SS_DOUBLE';
pt(157).ndims     = '2';
pt(157).size      = '[]';

pt(158).blockname = 'reference signal routing/RefBrake';
pt(158).paramname = 'Value';
pt(158).class     = 'scalar';
pt(158).nrows     = 1;
pt(158).ncols     = 1;
pt(158).subsource = 'SS_DOUBLE';
pt(158).ndims     = '2';
pt(158).size      = '[]';

pt(159).blockname = 'reference signal routing/RefSteering';
pt(159).paramname = 'Value';
pt(159).class     = 'scalar';
pt(159).nrows     = 1;
pt(159).ncols     = 1;
pt(159).subsource = 'SS_DOUBLE';
pt(159).ndims     = '2';
pt(159).size      = '[]';

pt(160).blockname = 'reference signal routing/RefSteering1';
pt(160).paramname = 'Value';
pt(160).class     = 'scalar';
pt(160).nrows     = 1;
pt(160).ncols     = 1;
pt(160).subsource = 'SS_DOUBLE';
pt(160).ndims     = '2';
pt(160).size      = '[]';

pt(161).blockname = 'reference signal routing/RefSteering2';
pt(161).paramname = 'Value';
pt(161).class     = 'scalar';
pt(161).nrows     = 1;
pt(161).ncols     = 1;
pt(161).subsource = 'SS_DOUBLE';
pt(161).ndims     = '2';
pt(161).size      = '[]';

pt(162).blockname = 'reference signal routing/RefTorque';
pt(162).paramname = 'Value';
pt(162).class     = 'scalar';
pt(162).nrows     = 1;
pt(162).ncols     = 1;
pt(162).subsource = 'SS_DOUBLE';
pt(162).ndims     = '2';
pt(162).size      = '[]';

pt(163).blockname = 'reference signal routing/RefVelTruck';
pt(163).paramname = 'Value';
pt(163).class     = 'scalar';
pt(163).nrows     = 1;
pt(163).ncols     = 1;
pt(163).subsource = 'SS_DOUBLE';
pt(163).ndims     = '2';
pt(163).size      = '[]';

pt(164).blockname = 'reference signal routing/RefVelTruck1';
pt(164).paramname = 'Value';
pt(164).class     = 'scalar';
pt(164).nrows     = 1;
pt(164).ncols     = 1;
pt(164).subsource = 'SS_DOUBLE';
pt(164).ndims     = '2';
pt(164).size      = '[]';

pt(165).blockname = 'reference signal routing/RefVelTruck2';
pt(165).paramname = 'Value';
pt(165).class     = 'scalar';
pt(165).nrows     = 1;
pt(165).ncols     = 1;
pt(165).subsource = 'SS_DOUBLE';
pt(165).ndims     = '2';
pt(165).size      = '[]';

pt(166).blockname = 'reference signal routing/trqRefMode';
pt(166).paramname = 'Value';
pt(166).class     = 'scalar';
pt(166).nrows     = 1;
pt(166).ncols     = 1;
pt(166).subsource = 'SS_DOUBLE';
pt(166).ndims     = '2';
pt(166).size      = '[]';

pt(167).blockname = 'reference signal routing/Gain';
pt(167).paramname = 'Gain';
pt(167).class     = 'scalar';
pt(167).nrows     = 1;
pt(167).ncols     = 1;
pt(167).subsource = 'SS_DOUBLE';
pt(167).ndims     = '2';
pt(167).size      = '[]';

pt(168).blockname = 'referenceRouting/Discrete Derivative';
pt(168).paramname = 'ICPrevScaledInput';
pt(168).class     = 'scalar';
pt(168).nrows     = 1;
pt(168).ncols     = 1;
pt(168).subsource = 'SS_DOUBLE';
pt(168).ndims     = '2';
pt(168).size      = '[]';

pt(169).blockname = 'referenceRouting/GhostInitDist';
pt(169).paramname = 'Value';
pt(169).class     = 'scalar';
pt(169).nrows     = 1;
pt(169).ncols     = 1;
pt(169).subsource = 'SS_DOUBLE';
pt(169).ndims     = '2';
pt(169).size      = '[]';

pt(170).blockname = 'referenceRouting/GhostVelocity';
pt(170).paramname = 'Value';
pt(170).class     = 'scalar';
pt(170).nrows     = 1;
pt(170).ncols     = 1;
pt(170).subsource = 'SS_DOUBLE';
pt(170).ndims     = '2';
pt(170).size      = '[]';

pt(171).blockname = 'referenceRouting/referenceSourceMode';
pt(171).paramname = 'Value';
pt(171).class     = 'scalar';
pt(171).nrows     = 1;
pt(171).ncols     = 1;
pt(171).subsource = 'SS_DOUBLE';
pt(171).ndims     = '2';
pt(171).size      = '[]';

pt(172).blockname = 'referenceRouting/Discrete-Time Integrator';
pt(172).paramname = 'gainval';
pt(172).class     = 'scalar';
pt(172).nrows     = 1;
pt(172).ncols     = 1;
pt(172).subsource = 'SS_DOUBLE';
pt(172).ndims     = '2';
pt(172).size      = '[]';

pt(173).blockname = 'referenceRouting/Discrete-Time Integrator1';
pt(173).paramname = 'gainval';
pt(173).class     = 'scalar';
pt(173).nrows     = 1;
pt(173).ncols     = 1;
pt(173).subsource = 'SS_DOUBLE';
pt(173).ndims     = '2';
pt(173).size      = '[]';

pt(174).blockname = 'referenceRouting/Discrete-Time Integrator1';
pt(174).paramname = 'InitialCondition';
pt(174).class     = 'scalar';
pt(174).nrows     = 1;
pt(174).ncols     = 1;
pt(174).subsource = 'SS_DOUBLE';
pt(174).ndims     = '2';
pt(174).size      = '[]';

pt(175).blockname = 'referenceRouting/kmh2ms1';
pt(175).paramname = 'Gain';
pt(175).class     = 'scalar';
pt(175).nrows     = 1;
pt(175).ncols     = 1;
pt(175).subsource = 'SS_DOUBLE';
pt(175).ndims     = '2';
pt(175).size      = '[]';

pt(176).blockname = 'referenceRouting/kmh2ms2';
pt(176).paramname = 'Gain';
pt(176).class     = 'scalar';
pt(176).nrows     = 1;
pt(176).ncols     = 1;
pt(176).subsource = 'SS_DOUBLE';
pt(176).ndims     = '2';
pt(176).size      = '[]';

pt(177).blockname = 'referenceRouting/kmh2ms3';
pt(177).paramname = 'Gain';
pt(177).class     = 'scalar';
pt(177).nrows     = 1;
pt(177).ncols     = 1;
pt(177).subsource = 'SS_DOUBLE';
pt(177).ndims     = '2';
pt(177).size      = '[]';

pt(178).blockname = 'CAN IN (Platform)/Truck/Discrete Derivative';
pt(178).paramname = 'ICPrevScaledInput';
pt(178).class     = 'scalar';
pt(178).nrows     = 1;
pt(178).ncols     = 1;
pt(178).subsource = 'SS_DOUBLE';
pt(178).ndims     = '2';
pt(178).size      = '[]';

pt(179).blockname = 'CAN IN (Platform)/Truck/IMULatAcc';
pt(179).paramname = 'Gain';
pt(179).class     = 'scalar';
pt(179).nrows     = 1;
pt(179).ncols     = 1;
pt(179).subsource = 'SS_DOUBLE';
pt(179).ndims     = '2';
pt(179).size      = '[]';

pt(180).blockname = 'CAN IN (Platform)/Truck/IMULongAcc';
pt(180).paramname = 'Gain';
pt(180).class     = 'scalar';
pt(180).nrows     = 1;
pt(180).ncols     = 1;
pt(180).subsource = 'SS_DOUBLE';
pt(180).ndims     = '2';
pt(180).size      = '[]';

pt(181).blockname = 'CAN IN (Platform)/Truck/IMUYawrate';
pt(181).paramname = 'Gain';
pt(181).class     = 'scalar';
pt(181).nrows     = 1;
pt(181).ncols     = 1;
pt(181).subsource = 'SS_DOUBLE';
pt(181).ndims     = '2';
pt(181).size      = '[]';

pt(182).blockname = 'CAN IN (Platform)/Truck/MtrLongAcc';
pt(182).paramname = 'Gain';
pt(182).class     = 'scalar';
pt(182).nrows     = 1;
pt(182).ncols     = 1;
pt(182).subsource = 'SS_DOUBLE';
pt(182).ndims     = '2';
pt(182).size      = '[]';

pt(183).blockname = 'CAN IN (Platform)/Truck/MtrLongVel';
pt(183).paramname = 'Gain';
pt(183).class     = 'scalar';
pt(183).nrows     = 1;
pt(183).ncols     = 1;
pt(183).subsource = 'SS_DOUBLE';
pt(183).ndims     = '2';
pt(183).size      = '[]';

pt(184).blockname = 'CAN IN (Platform)/Truck/Silent_On//Off';
pt(184).paramname = 'Gain';
pt(184).class     = 'scalar';
pt(184).nrows     = 1;
pt(184).ncols     = 1;
pt(184).subsource = 'SS_DOUBLE';
pt(184).ndims     = '2';
pt(184).size      = '[]';

pt(185).blockname = 'CAN IN (Platform)/Truck/Switch1_Off';
pt(185).paramname = 'Gain';
pt(185).class     = 'scalar';
pt(185).nrows     = 1;
pt(185).ncols     = 1;
pt(185).subsource = 'SS_DOUBLE';
pt(185).ndims     = '2';
pt(185).size      = '[]';

pt(186).blockname = 'CAN IN (Platform)/Truck/Switch1_On';
pt(186).paramname = 'Gain';
pt(186).class     = 'scalar';
pt(186).nrows     = 1;
pt(186).ncols     = 1;
pt(186).subsource = 'SS_DOUBLE';
pt(186).ndims     = '2';
pt(186).size      = '[]';

pt(187).blockname = 'CAN IN (Platform)/Truck/Switch1_Up';
pt(187).paramname = 'Gain';
pt(187).class     = 'scalar';
pt(187).nrows     = 1;
pt(187).ncols     = 1;
pt(187).subsource = 'SS_DOUBLE';
pt(187).ndims     = '2';
pt(187).size      = '[]';

pt(188).blockname = 'CAN IN (Platform)/Truck/accPedalPos';
pt(188).paramname = 'Gain';
pt(188).class     = 'scalar';
pt(188).nrows     = 1;
pt(188).ncols     = 1;
pt(188).subsource = 'SS_DOUBLE';
pt(188).ndims     = '2';
pt(188).size      = '[]';

pt(189).blockname = 'CAN IN (Platform)/Truck/brakePedalPos';
pt(189).paramname = 'Gain';
pt(189).class     = 'scalar';
pt(189).nrows     = 1;
pt(189).ncols     = 1;
pt(189).subsource = 'SS_DOUBLE';
pt(189).ndims     = '2';
pt(189).size      = '[]';

pt(190).blockname = 'CAN OUT (Platform)/Truck/Constant';
pt(190).paramname = 'Value';
pt(190).class     = 'scalar';
pt(190).nrows     = 1;
pt(190).ncols     = 1;
pt(190).subsource = 'SS_DOUBLE';
pt(190).ndims     = '2';
pt(190).size      = '[]';

pt(191).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare';
pt(191).paramname = 'const';
pt(191).class     = 'scalar';
pt(191).nrows     = 1;
pt(191).ncols     = 1;
pt(191).subsource = 'SS_INT32';
pt(191).ndims     = '2';
pt(191).size      = '[]';

pt(192).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare1';
pt(192).paramname = 'const';
pt(192).class     = 'scalar';
pt(192).nrows     = 1;
pt(192).ncols     = 1;
pt(192).subsource = 'SS_INT32';
pt(192).ndims     = '2';
pt(192).size      = '[]';

pt(193).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare2';
pt(193).paramname = 'const';
pt(193).class     = 'scalar';
pt(193).nrows     = 1;
pt(193).ncols     = 1;
pt(193).subsource = 'SS_INT32';
pt(193).ndims     = '2';
pt(193).size      = '[]';

pt(194).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare3';
pt(194).paramname = 'const';
pt(194).class     = 'scalar';
pt(194).nrows     = 1;
pt(194).ncols     = 1;
pt(194).subsource = 'SS_INT32';
pt(194).ndims     = '2';
pt(194).size      = '[]';

pt(195).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare4';
pt(195).paramname = 'const';
pt(195).class     = 'scalar';
pt(195).nrows     = 1;
pt(195).ncols     = 1;
pt(195).subsource = 'SS_INT32';
pt(195).ndims     = '2';
pt(195).size      = '[]';

pt(196).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare5';
pt(196).paramname = 'const';
pt(196).class     = 'scalar';
pt(196).nrows     = 1;
pt(196).ncols     = 1;
pt(196).subsource = 'SS_INT32';
pt(196).ndims     = '2';
pt(196).size      = '[]';

pt(197).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare6';
pt(197).paramname = 'const';
pt(197).class     = 'scalar';
pt(197).nrows     = 1;
pt(197).ncols     = 1;
pt(197).subsource = 'SS_INT32';
pt(197).ndims     = '2';
pt(197).size      = '[]';

pt(198).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare7';
pt(198).paramname = 'const';
pt(198).class     = 'scalar';
pt(198).nrows     = 1;
pt(198).ncols     = 1;
pt(198).subsource = 'SS_INT32';
pt(198).ndims     = '2';
pt(198).size      = '[]';

pt(199).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare8';
pt(199).paramname = 'const';
pt(199).class     = 'scalar';
pt(199).nrows     = 1;
pt(199).ncols     = 1;
pt(199).subsource = 'SS_INT32';
pt(199).ndims     = '2';
pt(199).size      = '[]';

pt(200).blockname = 'MonitoringAndLogging /Log Received Data/CAM Compare9';
pt(200).paramname = 'const';
pt(200).class     = 'scalar';
pt(200).nrows     = 1;
pt(200).ncols     = 1;
pt(200).subsource = 'SS_INT32';
pt(200).ndims     = '2';
pt(200).size      = '[]';

pt(201).blockname = 'MonitoringAndLogging /Log Received Data/DEMN Compare';
pt(201).paramname = 'const';
pt(201).class     = 'scalar';
pt(201).nrows     = 1;
pt(201).ncols     = 1;
pt(201).subsource = 'SS_INT32';
pt(201).ndims     = '2';
pt(201).size      = '[]';

pt(202).blockname = 'MonitoringAndLogging /Log Received Data/DEMN Compare1';
pt(202).paramname = 'const';
pt(202).class     = 'scalar';
pt(202).nrows     = 1;
pt(202).ncols     = 1;
pt(202).subsource = 'SS_INT32';
pt(202).ndims     = '2';
pt(202).size      = '[]';

pt(203).blockname = 'MonitoringAndLogging /Log Received Data/DEMN Compare2';
pt(203).paramname = 'const';
pt(203).class     = 'scalar';
pt(203).nrows     = 1;
pt(203).ncols     = 1;
pt(203).subsource = 'SS_INT32';
pt(203).ndims     = '2';
pt(203).size      = '[]';

pt(204).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare';
pt(204).paramname = 'const';
pt(204).class     = 'scalar';
pt(204).nrows     = 1;
pt(204).ncols     = 1;
pt(204).subsource = 'SS_INT32';
pt(204).ndims     = '2';
pt(204).size      = '[]';

pt(205).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare1';
pt(205).paramname = 'const';
pt(205).class     = 'scalar';
pt(205).nrows     = 1;
pt(205).ncols     = 1;
pt(205).subsource = 'SS_INT32';
pt(205).ndims     = '2';
pt(205).size      = '[]';

pt(206).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare10';
pt(206).paramname = 'const';
pt(206).class     = 'scalar';
pt(206).nrows     = 1;
pt(206).ncols     = 1;
pt(206).subsource = 'SS_INT32';
pt(206).ndims     = '2';
pt(206).size      = '[]';

pt(207).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare11';
pt(207).paramname = 'const';
pt(207).class     = 'scalar';
pt(207).nrows     = 1;
pt(207).ncols     = 1;
pt(207).subsource = 'SS_INT32';
pt(207).ndims     = '2';
pt(207).size      = '[]';

pt(208).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare12';
pt(208).paramname = 'const';
pt(208).class     = 'scalar';
pt(208).nrows     = 1;
pt(208).ncols     = 1;
pt(208).subsource = 'SS_INT32';
pt(208).ndims     = '2';
pt(208).size      = '[]';

pt(209).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare13';
pt(209).paramname = 'const';
pt(209).class     = 'scalar';
pt(209).nrows     = 1;
pt(209).ncols     = 1;
pt(209).subsource = 'SS_INT32';
pt(209).ndims     = '2';
pt(209).size      = '[]';

pt(210).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare14';
pt(210).paramname = 'const';
pt(210).class     = 'scalar';
pt(210).nrows     = 1;
pt(210).ncols     = 1;
pt(210).subsource = 'SS_INT32';
pt(210).ndims     = '2';
pt(210).size      = '[]';

pt(211).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare15';
pt(211).paramname = 'const';
pt(211).class     = 'scalar';
pt(211).nrows     = 1;
pt(211).ncols     = 1;
pt(211).subsource = 'SS_INT32';
pt(211).ndims     = '2';
pt(211).size      = '[]';

pt(212).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare16';
pt(212).paramname = 'const';
pt(212).class     = 'scalar';
pt(212).nrows     = 1;
pt(212).ncols     = 1;
pt(212).subsource = 'SS_INT32';
pt(212).ndims     = '2';
pt(212).size      = '[]';

pt(213).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare17';
pt(213).paramname = 'const';
pt(213).class     = 'scalar';
pt(213).nrows     = 1;
pt(213).ncols     = 1;
pt(213).subsource = 'SS_INT32';
pt(213).ndims     = '2';
pt(213).size      = '[]';

pt(214).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare18';
pt(214).paramname = 'const';
pt(214).class     = 'scalar';
pt(214).nrows     = 1;
pt(214).ncols     = 1;
pt(214).subsource = 'SS_INT32';
pt(214).ndims     = '2';
pt(214).size      = '[]';

pt(215).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare19';
pt(215).paramname = 'const';
pt(215).class     = 'scalar';
pt(215).nrows     = 1;
pt(215).ncols     = 1;
pt(215).subsource = 'SS_INT32';
pt(215).ndims     = '2';
pt(215).size      = '[]';

pt(216).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare2';
pt(216).paramname = 'const';
pt(216).class     = 'scalar';
pt(216).nrows     = 1;
pt(216).ncols     = 1;
pt(216).subsource = 'SS_INT32';
pt(216).ndims     = '2';
pt(216).size      = '[]';

pt(217).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare20';
pt(217).paramname = 'const';
pt(217).class     = 'scalar';
pt(217).nrows     = 1;
pt(217).ncols     = 1;
pt(217).subsource = 'SS_INT32';
pt(217).ndims     = '2';
pt(217).size      = '[]';

pt(218).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare21';
pt(218).paramname = 'const';
pt(218).class     = 'scalar';
pt(218).nrows     = 1;
pt(218).ncols     = 1;
pt(218).subsource = 'SS_INT32';
pt(218).ndims     = '2';
pt(218).size      = '[]';

pt(219).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare22';
pt(219).paramname = 'const';
pt(219).class     = 'scalar';
pt(219).nrows     = 1;
pt(219).ncols     = 1;
pt(219).subsource = 'SS_INT32';
pt(219).ndims     = '2';
pt(219).size      = '[]';

pt(220).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare23';
pt(220).paramname = 'const';
pt(220).class     = 'scalar';
pt(220).nrows     = 1;
pt(220).ncols     = 1;
pt(220).subsource = 'SS_INT32';
pt(220).ndims     = '2';
pt(220).size      = '[]';

pt(221).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare3';
pt(221).paramname = 'const';
pt(221).class     = 'scalar';
pt(221).nrows     = 1;
pt(221).ncols     = 1;
pt(221).subsource = 'SS_INT32';
pt(221).ndims     = '2';
pt(221).size      = '[]';

pt(222).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare4';
pt(222).paramname = 'const';
pt(222).class     = 'scalar';
pt(222).nrows     = 1;
pt(222).ncols     = 1;
pt(222).subsource = 'SS_INT32';
pt(222).ndims     = '2';
pt(222).size      = '[]';

pt(223).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare5';
pt(223).paramname = 'const';
pt(223).class     = 'scalar';
pt(223).nrows     = 1;
pt(223).ncols     = 1;
pt(223).subsource = 'SS_INT32';
pt(223).ndims     = '2';
pt(223).size      = '[]';

pt(224).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare6';
pt(224).paramname = 'const';
pt(224).class     = 'scalar';
pt(224).nrows     = 1;
pt(224).ncols     = 1;
pt(224).subsource = 'SS_INT32';
pt(224).ndims     = '2';
pt(224).size      = '[]';

pt(225).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare7';
pt(225).paramname = 'const';
pt(225).class     = 'scalar';
pt(225).nrows     = 1;
pt(225).ncols     = 1;
pt(225).subsource = 'SS_INT32';
pt(225).ndims     = '2';
pt(225).size      = '[]';

pt(226).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare8';
pt(226).paramname = 'const';
pt(226).class     = 'scalar';
pt(226).nrows     = 1;
pt(226).ncols     = 1;
pt(226).subsource = 'SS_INT32';
pt(226).ndims     = '2';
pt(226).size      = '[]';

pt(227).blockname = 'MonitoringAndLogging /Log Received Data/iCLCM Compare9';
pt(227).paramname = 'const';
pt(227).class     = 'scalar';
pt(227).nrows     = 1;
pt(227).ncols     = 1;
pt(227).subsource = 'SS_INT32';
pt(227).ndims     = '2';
pt(227).size      = '[]';

pt(228).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue';
pt(228).paramname = 'Value';
pt(228).class     = 'scalar';
pt(228).nrows     = 1;
pt(228).ncols     = 1;
pt(228).subsource = 'SS_DOUBLE';
pt(228).ndims     = '2';
pt(228).size      = '[]';

pt(229).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue1';
pt(229).paramname = 'Value';
pt(229).class     = 'scalar';
pt(229).nrows     = 1;
pt(229).ncols     = 1;
pt(229).subsource = 'SS_DOUBLE';
pt(229).ndims     = '2';
pt(229).size      = '[]';

pt(230).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue10';
pt(230).paramname = 'Value';
pt(230).class     = 'scalar';
pt(230).nrows     = 1;
pt(230).ncols     = 1;
pt(230).subsource = 'SS_DOUBLE';
pt(230).ndims     = '2';
pt(230).size      = '[]';

pt(231).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue11';
pt(231).paramname = 'Value';
pt(231).class     = 'scalar';
pt(231).nrows     = 1;
pt(231).ncols     = 1;
pt(231).subsource = 'SS_DOUBLE';
pt(231).ndims     = '2';
pt(231).size      = '[]';

pt(232).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue12';
pt(232).paramname = 'Value';
pt(232).class     = 'scalar';
pt(232).nrows     = 1;
pt(232).ncols     = 1;
pt(232).subsource = 'SS_DOUBLE';
pt(232).ndims     = '2';
pt(232).size      = '[]';

pt(233).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue13';
pt(233).paramname = 'Value';
pt(233).class     = 'scalar';
pt(233).nrows     = 1;
pt(233).ncols     = 1;
pt(233).subsource = 'SS_DOUBLE';
pt(233).ndims     = '2';
pt(233).size      = '[]';

pt(234).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue14';
pt(234).paramname = 'Value';
pt(234).class     = 'scalar';
pt(234).nrows     = 1;
pt(234).ncols     = 1;
pt(234).subsource = 'SS_DOUBLE';
pt(234).ndims     = '2';
pt(234).size      = '[]';

pt(235).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue15';
pt(235).paramname = 'Value';
pt(235).class     = 'scalar';
pt(235).nrows     = 1;
pt(235).ncols     = 1;
pt(235).subsource = 'SS_DOUBLE';
pt(235).ndims     = '2';
pt(235).size      = '[]';

pt(236).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue16';
pt(236).paramname = 'Value';
pt(236).class     = 'scalar';
pt(236).nrows     = 1;
pt(236).ncols     = 1;
pt(236).subsource = 'SS_DOUBLE';
pt(236).ndims     = '2';
pt(236).size      = '[]';

pt(237).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue17';
pt(237).paramname = 'Value';
pt(237).class     = 'scalar';
pt(237).nrows     = 1;
pt(237).ncols     = 1;
pt(237).subsource = 'SS_DOUBLE';
pt(237).ndims     = '2';
pt(237).size      = '[]';

pt(238).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue18';
pt(238).paramname = 'Value';
pt(238).class     = 'scalar';
pt(238).nrows     = 1;
pt(238).ncols     = 1;
pt(238).subsource = 'SS_DOUBLE';
pt(238).ndims     = '2';
pt(238).size      = '[]';

pt(239).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue19';
pt(239).paramname = 'Value';
pt(239).class     = 'scalar';
pt(239).nrows     = 1;
pt(239).ncols     = 1;
pt(239).subsource = 'SS_DOUBLE';
pt(239).ndims     = '2';
pt(239).size      = '[]';

pt(240).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue2';
pt(240).paramname = 'Value';
pt(240).class     = 'scalar';
pt(240).nrows     = 1;
pt(240).ncols     = 1;
pt(240).subsource = 'SS_DOUBLE';
pt(240).ndims     = '2';
pt(240).size      = '[]';

pt(241).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue20';
pt(241).paramname = 'Value';
pt(241).class     = 'scalar';
pt(241).nrows     = 1;
pt(241).ncols     = 1;
pt(241).subsource = 'SS_DOUBLE';
pt(241).ndims     = '2';
pt(241).size      = '[]';

pt(242).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue21';
pt(242).paramname = 'Value';
pt(242).class     = 'scalar';
pt(242).nrows     = 1;
pt(242).ncols     = 1;
pt(242).subsource = 'SS_DOUBLE';
pt(242).ndims     = '2';
pt(242).size      = '[]';

pt(243).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue22';
pt(243).paramname = 'Value';
pt(243).class     = 'scalar';
pt(243).nrows     = 1;
pt(243).ncols     = 1;
pt(243).subsource = 'SS_DOUBLE';
pt(243).ndims     = '2';
pt(243).size      = '[]';

pt(244).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue23';
pt(244).paramname = 'Value';
pt(244).class     = 'scalar';
pt(244).nrows     = 1;
pt(244).ncols     = 1;
pt(244).subsource = 'SS_DOUBLE';
pt(244).ndims     = '2';
pt(244).size      = '[]';

pt(245).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue24';
pt(245).paramname = 'Value';
pt(245).class     = 'scalar';
pt(245).nrows     = 1;
pt(245).ncols     = 1;
pt(245).subsource = 'SS_DOUBLE';
pt(245).ndims     = '2';
pt(245).size      = '[]';

pt(246).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue25';
pt(246).paramname = 'Value';
pt(246).class     = 'scalar';
pt(246).nrows     = 1;
pt(246).ncols     = 1;
pt(246).subsource = 'SS_DOUBLE';
pt(246).ndims     = '2';
pt(246).size      = '[]';

pt(247).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue26';
pt(247).paramname = 'Value';
pt(247).class     = 'scalar';
pt(247).nrows     = 1;
pt(247).ncols     = 1;
pt(247).subsource = 'SS_DOUBLE';
pt(247).ndims     = '2';
pt(247).size      = '[]';

pt(248).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue27';
pt(248).paramname = 'Value';
pt(248).class     = 'scalar';
pt(248).nrows     = 1;
pt(248).ncols     = 1;
pt(248).subsource = 'SS_DOUBLE';
pt(248).ndims     = '2';
pt(248).size      = '[]';

pt(249).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue28';
pt(249).paramname = 'Value';
pt(249).class     = 'scalar';
pt(249).nrows     = 1;
pt(249).ncols     = 1;
pt(249).subsource = 'SS_DOUBLE';
pt(249).ndims     = '2';
pt(249).size      = '[]';

pt(250).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue29';
pt(250).paramname = 'Value';
pt(250).class     = 'scalar';
pt(250).nrows     = 1;
pt(250).ncols     = 1;
pt(250).subsource = 'SS_DOUBLE';
pt(250).ndims     = '2';
pt(250).size      = '[]';

pt(251).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue3';
pt(251).paramname = 'Value';
pt(251).class     = 'scalar';
pt(251).nrows     = 1;
pt(251).ncols     = 1;
pt(251).subsource = 'SS_DOUBLE';
pt(251).ndims     = '2';
pt(251).size      = '[]';

pt(252).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue30';
pt(252).paramname = 'Value';
pt(252).class     = 'scalar';
pt(252).nrows     = 1;
pt(252).ncols     = 1;
pt(252).subsource = 'SS_DOUBLE';
pt(252).ndims     = '2';
pt(252).size      = '[]';

pt(253).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue31';
pt(253).paramname = 'Value';
pt(253).class     = 'scalar';
pt(253).nrows     = 1;
pt(253).ncols     = 1;
pt(253).subsource = 'SS_DOUBLE';
pt(253).ndims     = '2';
pt(253).size      = '[]';

pt(254).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue32';
pt(254).paramname = 'Value';
pt(254).class     = 'scalar';
pt(254).nrows     = 1;
pt(254).ncols     = 1;
pt(254).subsource = 'SS_DOUBLE';
pt(254).ndims     = '2';
pt(254).size      = '[]';

pt(255).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue33';
pt(255).paramname = 'Value';
pt(255).class     = 'scalar';
pt(255).nrows     = 1;
pt(255).ncols     = 1;
pt(255).subsource = 'SS_DOUBLE';
pt(255).ndims     = '2';
pt(255).size      = '[]';

pt(256).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue34';
pt(256).paramname = 'Value';
pt(256).class     = 'scalar';
pt(256).nrows     = 1;
pt(256).ncols     = 1;
pt(256).subsource = 'SS_DOUBLE';
pt(256).ndims     = '2';
pt(256).size      = '[]';

pt(257).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue35';
pt(257).paramname = 'Value';
pt(257).class     = 'scalar';
pt(257).nrows     = 1;
pt(257).ncols     = 1;
pt(257).subsource = 'SS_DOUBLE';
pt(257).ndims     = '2';
pt(257).size      = '[]';

pt(258).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue36';
pt(258).paramname = 'Value';
pt(258).class     = 'scalar';
pt(258).nrows     = 1;
pt(258).ncols     = 1;
pt(258).subsource = 'SS_DOUBLE';
pt(258).ndims     = '2';
pt(258).size      = '[]';

pt(259).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue37';
pt(259).paramname = 'Value';
pt(259).class     = 'scalar';
pt(259).nrows     = 1;
pt(259).ncols     = 1;
pt(259).subsource = 'SS_DOUBLE';
pt(259).ndims     = '2';
pt(259).size      = '[]';

pt(260).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue38';
pt(260).paramname = 'Value';
pt(260).class     = 'scalar';
pt(260).nrows     = 1;
pt(260).ncols     = 1;
pt(260).subsource = 'SS_DOUBLE';
pt(260).ndims     = '2';
pt(260).size      = '[]';

pt(261).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue4';
pt(261).paramname = 'Value';
pt(261).class     = 'scalar';
pt(261).nrows     = 1;
pt(261).ncols     = 1;
pt(261).subsource = 'SS_DOUBLE';
pt(261).ndims     = '2';
pt(261).size      = '[]';

pt(262).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue5';
pt(262).paramname = 'Value';
pt(262).class     = 'scalar';
pt(262).nrows     = 1;
pt(262).ncols     = 1;
pt(262).subsource = 'SS_DOUBLE';
pt(262).ndims     = '2';
pt(262).size      = '[]';

pt(263).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue6';
pt(263).paramname = 'Value';
pt(263).class     = 'scalar';
pt(263).nrows     = 1;
pt(263).ncols     = 1;
pt(263).subsource = 'SS_DOUBLE';
pt(263).ndims     = '2';
pt(263).size      = '[]';

pt(264).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue7';
pt(264).paramname = 'Value';
pt(264).class     = 'scalar';
pt(264).nrows     = 1;
pt(264).ncols     = 1;
pt(264).subsource = 'SS_DOUBLE';
pt(264).ndims     = '2';
pt(264).size      = '[]';

pt(265).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue8';
pt(265).paramname = 'Value';
pt(265).class     = 'scalar';
pt(265).nrows     = 1;
pt(265).ncols     = 1;
pt(265).subsource = 'SS_DOUBLE';
pt(265).ndims     = '2';
pt(265).size      = '[]';

pt(266).blockname = 'MonitoringAndLogging /Log Received Data/DefaultValue9';
pt(266).paramname = 'Value';
pt(266).class     = 'scalar';
pt(266).nrows     = 1;
pt(266).ncols     = 1;
pt(266).subsource = 'SS_DOUBLE';
pt(266).ndims     = '2';
pt(266).size      = '[]';

pt(267).blockname = 'MonitoringAndLogging /Log Received Data/AcknowledgeFlag';
pt(267).paramname = 'Gain';
pt(267).class     = 'scalar';
pt(267).nrows     = 1;
pt(267).ncols     = 1;
pt(267).subsource = 'SS_DOUBLE';
pt(267).ndims     = '2';
pt(267).size      = '[]';

pt(268).blockname = 'MonitoringAndLogging /Log Received Data/BackwardID';
pt(268).paramname = 'Gain';
pt(268).class     = 'scalar';
pt(268).nrows     = 1;
pt(268).ncols     = 1;
pt(268).subsource = 'SS_DOUBLE';
pt(268).ndims     = '2';
pt(268).size      = '[]';

pt(269).blockname = 'MonitoringAndLogging /Log Received Data/CAM_GenerationTime';
pt(269).paramname = 'Gain';
pt(269).class     = 'scalar';
pt(269).nrows     = 1;
pt(269).ncols     = 1;
pt(269).subsource = 'SS_DOUBLE';
pt(269).ndims     = '2';
pt(269).size      = '[]';

pt(270).blockname = 'MonitoringAndLogging /Log Received Data/CounterIntersection';
pt(270).paramname = 'Gain';
pt(270).class     = 'scalar';
pt(270).nrows     = 1;
pt(270).ncols     = 1;
pt(270).subsource = 'SS_DOUBLE';
pt(270).ndims     = '2';
pt(270).size      = '[]';

pt(271).blockname = 'MonitoringAndLogging /Log Received Data/CruiseSpeed';
pt(271).paramname = 'Gain';
pt(271).class     = 'scalar';
pt(271).nrows     = 1;
pt(271).ncols     = 1;
pt(271).subsource = 'SS_DOUBLE';
pt(271).ndims     = '2';
pt(271).size      = '[]';

pt(272).blockname = 'MonitoringAndLogging /Log Received Data/DENM_GenerationTime';
pt(272).paramname = 'Gain';
pt(272).class     = 'scalar';
pt(272).nrows     = 1;
pt(272).ncols     = 1;
pt(272).subsource = 'SS_DOUBLE';
pt(272).ndims     = '2';
pt(272).size      = '[]';

pt(273).blockname = 'MonitoringAndLogging /Log Received Data/DistanceTravelledCZ';
pt(273).paramname = 'Gain';
pt(273).class     = 'scalar';
pt(273).nrows     = 1;
pt(273).ncols     = 1;
pt(273).subsource = 'SS_DOUBLE';
pt(273).ndims     = '2';
pt(273).size      = '[]';

pt(274).blockname = 'MonitoringAndLogging /Log Received Data/EndOfScenario';
pt(274).paramname = 'Gain';
pt(274).class     = 'scalar';
pt(274).nrows     = 1;
pt(274).ncols     = 1;
pt(274).subsource = 'SS_DOUBLE';
pt(274).ndims     = '2';
pt(274).size      = '[]';

pt(275).blockname = 'MonitoringAndLogging /Log Received Data/EventType_CauseCode';
pt(275).paramname = 'Gain';
pt(275).class     = 'scalar';
pt(275).nrows     = 1;
pt(275).ncols     = 1;
pt(275).subsource = 'SS_DOUBLE';
pt(275).ndims     = '2';
pt(275).size      = '[]';

pt(276).blockname = 'MonitoringAndLogging /Log Received Data/ForwardID';
pt(276).paramname = 'Gain';
pt(276).class     = 'scalar';
pt(276).nrows     = 1;
pt(276).ncols     = 1;
pt(276).subsource = 'SS_DOUBLE';
pt(276).ndims     = '2';
pt(276).size      = '[]';

pt(277).blockname = 'MonitoringAndLogging /Log Received Data/Heading';
pt(277).paramname = 'Gain';
pt(277).class     = 'scalar';
pt(277).nrows     = 1;
pt(277).ncols     = 1;
pt(277).subsource = 'SS_DOUBLE';
pt(277).ndims     = '2';
pt(277).size      = '[]';

pt(278).blockname = 'MonitoringAndLogging /Log Received Data/Intention';
pt(278).paramname = 'Gain';
pt(278).class     = 'scalar';
pt(278).nrows     = 1;
pt(278).ncols     = 1;
pt(278).subsource = 'SS_DOUBLE';
pt(278).ndims     = '2';
pt(278).size      = '[]';

pt(279).blockname = 'MonitoringAndLogging /Log Received Data/Lane';
pt(279).paramname = 'Gain';
pt(279).class     = 'scalar';
pt(279).nrows     = 1;
pt(279).ncols     = 1;
pt(279).subsource = 'SS_DOUBLE';
pt(279).ndims     = '2';
pt(279).size      = '[]';

pt(280).blockname = 'MonitoringAndLogging /Log Received Data/Latitude';
pt(280).paramname = 'Gain';
pt(280).class     = 'scalar';
pt(280).nrows     = 1;
pt(280).ncols     = 1;
pt(280).subsource = 'SS_DOUBLE';
pt(280).ndims     = '2';
pt(280).size      = '[]';

pt(281).blockname = 'MonitoringAndLogging /Log Received Data/Longitude';
pt(281).paramname = 'Gain';
pt(281).class     = 'scalar';
pt(281).nrows     = 1;
pt(281).ncols     = 1;
pt(281).subsource = 'SS_DOUBLE';
pt(281).ndims     = '2';
pt(281).size      = '[]';

pt(282).blockname = 'MonitoringAndLogging /Log Received Data/LongitudinalAcceleration';
pt(282).paramname = 'Gain';
pt(282).class     = 'scalar';
pt(282).nrows     = 1;
pt(282).ncols     = 1;
pt(282).subsource = 'SS_DOUBLE';
pt(282).ndims     = '2';
pt(282).size      = '[]';

pt(283).blockname = 'MonitoringAndLogging /Log Received Data/MergeFlag';
pt(283).paramname = 'Gain';
pt(283).class     = 'scalar';
pt(283).nrows     = 1;
pt(283).ncols     = 1;
pt(283).subsource = 'SS_DOUBLE';
pt(283).ndims     = '2';
pt(283).size      = '[]';

pt(284).blockname = 'MonitoringAndLogging /Log Received Data/MergeFlagHead';
pt(284).paramname = 'Gain';
pt(284).class     = 'scalar';
pt(284).nrows     = 1;
pt(284).ncols     = 1;
pt(284).subsource = 'SS_DOUBLE';
pt(284).ndims     = '2';
pt(284).size      = '[]';

pt(285).blockname = 'MonitoringAndLogging /Log Received Data/MergeFlagTail';
pt(285).paramname = 'Gain';
pt(285).class     = 'scalar';
pt(285).nrows     = 1;
pt(285).ncols     = 1;
pt(285).subsource = 'SS_DOUBLE';
pt(285).ndims     = '2';
pt(285).size      = '[]';

pt(286).blockname = 'MonitoringAndLogging /Log Received Data/MergeRequest';
pt(286).paramname = 'Gain';
pt(286).class     = 'scalar';
pt(286).nrows     = 1;
pt(286).ncols     = 1;
pt(286).subsource = 'SS_DOUBLE';
pt(286).ndims     = '2';
pt(286).size      = '[]';

pt(287).blockname = 'MonitoringAndLogging /Log Received Data/MergeSafeToMerge';
pt(287).paramname = 'Gain';
pt(287).class     = 'scalar';
pt(287).nrows     = 1;
pt(287).ncols     = 1;
pt(287).subsource = 'SS_DOUBLE';
pt(287).ndims     = '2';
pt(287).size      = '[]';

pt(288).blockname = 'MonitoringAndLogging /Log Received Data/MioBearing';
pt(288).paramname = 'Gain';
pt(288).class     = 'scalar';
pt(288).nrows     = 1;
pt(288).ncols     = 1;
pt(288).subsource = 'SS_DOUBLE';
pt(288).ndims     = '2';
pt(288).size      = '[]';

pt(289).blockname = 'MonitoringAndLogging /Log Received Data/MioID';
pt(289).paramname = 'Gain';
pt(289).class     = 'scalar';
pt(289).nrows     = 1;
pt(289).ncols     = 1;
pt(289).subsource = 'SS_DOUBLE';
pt(289).ndims     = '2';
pt(289).size      = '[]';

pt(290).blockname = 'MonitoringAndLogging /Log Received Data/MioRange';
pt(290).paramname = 'Gain';
pt(290).class     = 'scalar';
pt(290).nrows     = 1;
pt(290).ncols     = 1;
pt(290).subsource = 'SS_DOUBLE';
pt(290).ndims     = '2';
pt(290).size      = '[]';

pt(291).blockname = 'MonitoringAndLogging /Log Received Data/MioRangeRate';
pt(291).paramname = 'Gain';
pt(291).class     = 'scalar';
pt(291).nrows     = 1;
pt(291).ncols     = 1;
pt(291).subsource = 'SS_DOUBLE';
pt(291).ndims     = '2';
pt(291).size      = '[]';

pt(292).blockname = 'MonitoringAndLogging /Log Received Data/PlatoonID';
pt(292).paramname = 'Gain';
pt(292).class     = 'scalar';
pt(292).nrows     = 1;
pt(292).ncols     = 1;
pt(292).subsource = 'SS_DOUBLE';
pt(292).ndims     = '2';
pt(292).size      = '[]';

pt(293).blockname = 'MonitoringAndLogging /Log Received Data/ReferenceTime';
pt(293).paramname = 'Gain';
pt(293).class     = 'scalar';
pt(293).nrows     = 1;
pt(293).ncols     = 1;
pt(293).subsource = 'SS_DOUBLE';
pt(293).ndims     = '2';
pt(293).size      = '[]';

pt(294).blockname = 'MonitoringAndLogging /Log Received Data/Speed';
pt(294).paramname = 'Gain';
pt(294).class     = 'scalar';
pt(294).nrows     = 1;
pt(294).ncols     = 1;
pt(294).subsource = 'SS_DOUBLE';
pt(294).ndims     = '2';
pt(294).size      = '[]';

pt(295).blockname = 'MonitoringAndLogging /Log Received Data/StartPlatoon';
pt(295).paramname = 'Gain';
pt(295).class     = 'scalar';
pt(295).nrows     = 1;
pt(295).ncols     = 1;
pt(295).subsource = 'SS_DOUBLE';
pt(295).ndims     = '2';
pt(295).size      = '[]';

pt(296).blockname = 'MonitoringAndLogging /Log Received Data/TimeHeadway';
pt(296).paramname = 'Gain';
pt(296).class     = 'scalar';
pt(296).nrows     = 1;
pt(296).ncols     = 1;
pt(296).subsource = 'SS_DOUBLE';
pt(296).ndims     = '2';
pt(296).size      = '[]';

pt(297).blockname = 'MonitoringAndLogging /Log Received Data/VehicleLength';
pt(297).paramname = 'Gain';
pt(297).class     = 'scalar';
pt(297).nrows     = 1;
pt(297).ncols     = 1;
pt(297).subsource = 'SS_DOUBLE';
pt(297).ndims     = '2';
pt(297).size      = '[]';

pt(298).blockname = 'MonitoringAndLogging /Log Received Data/VehicleRearAxleLocation';
pt(298).paramname = 'Gain';
pt(298).class     = 'scalar';
pt(298).nrows     = 1;
pt(298).ncols     = 1;
pt(298).subsource = 'SS_DOUBLE';
pt(298).ndims     = '2';
pt(298).size      = '[]';

pt(299).blockname = 'MonitoringAndLogging /Log Received Data/VehicleWidth';
pt(299).paramname = 'Gain';
pt(299).class     = 'scalar';
pt(299).nrows     = 1;
pt(299).ncols     = 1;
pt(299).subsource = 'SS_DOUBLE';
pt(299).ndims     = '2';
pt(299).size      = '[]';

pt(300).blockname = 'MonitoringAndLogging /Log Received Data/iCLCL_GenerationTime';
pt(300).paramname = 'Gain';
pt(300).class     = 'scalar';
pt(300).nrows     = 1;
pt(300).ncols     = 1;
pt(300).subsource = 'SS_DOUBLE';
pt(300).ndims     = '2';
pt(300).size      = '[]';

pt(301).blockname = 'MonitoringAndLogging /Log Received Data/stationID';
pt(301).paramname = 'Gain';
pt(301).class     = 'scalar';
pt(301).nrows     = 1;
pt(301).ncols     = 1;
pt(301).subsource = 'SS_DOUBLE';
pt(301).ndims     = '2';
pt(301).size      = '[]';

pt(302).blockname = 'MonitoringAndLogging /Log Received Data/stationType';
pt(302).paramname = 'Gain';
pt(302).class     = 'scalar';
pt(302).nrows     = 1;
pt(302).ncols     = 1;
pt(302).subsource = 'SS_DOUBLE';
pt(302).ndims     = '2';
pt(302).size      = '[]';

pt(303).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue';
pt(303).paramname = 'Value';
pt(303).class     = 'scalar';
pt(303).nrows     = 1;
pt(303).ncols     = 1;
pt(303).subsource = 'SS_DOUBLE';
pt(303).ndims     = '2';
pt(303).size      = '[]';

pt(304).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue1';
pt(304).paramname = 'Value';
pt(304).class     = 'scalar';
pt(304).nrows     = 1;
pt(304).ncols     = 1;
pt(304).subsource = 'SS_DOUBLE';
pt(304).ndims     = '2';
pt(304).size      = '[]';

pt(305).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue10';
pt(305).paramname = 'Value';
pt(305).class     = 'scalar';
pt(305).nrows     = 1;
pt(305).ncols     = 1;
pt(305).subsource = 'SS_DOUBLE';
pt(305).ndims     = '2';
pt(305).size      = '[]';

pt(306).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue11';
pt(306).paramname = 'Value';
pt(306).class     = 'scalar';
pt(306).nrows     = 1;
pt(306).ncols     = 1;
pt(306).subsource = 'SS_DOUBLE';
pt(306).ndims     = '2';
pt(306).size      = '[]';

pt(307).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue12';
pt(307).paramname = 'Value';
pt(307).class     = 'scalar';
pt(307).nrows     = 1;
pt(307).ncols     = 1;
pt(307).subsource = 'SS_DOUBLE';
pt(307).ndims     = '2';
pt(307).size      = '[]';

pt(308).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue13';
pt(308).paramname = 'Value';
pt(308).class     = 'scalar';
pt(308).nrows     = 1;
pt(308).ncols     = 1;
pt(308).subsource = 'SS_DOUBLE';
pt(308).ndims     = '2';
pt(308).size      = '[]';

pt(309).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue14';
pt(309).paramname = 'Value';
pt(309).class     = 'scalar';
pt(309).nrows     = 1;
pt(309).ncols     = 1;
pt(309).subsource = 'SS_DOUBLE';
pt(309).ndims     = '2';
pt(309).size      = '[]';

pt(310).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue15';
pt(310).paramname = 'Value';
pt(310).class     = 'scalar';
pt(310).nrows     = 1;
pt(310).ncols     = 1;
pt(310).subsource = 'SS_DOUBLE';
pt(310).ndims     = '2';
pt(310).size      = '[]';

pt(311).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue16';
pt(311).paramname = 'Value';
pt(311).class     = 'scalar';
pt(311).nrows     = 1;
pt(311).ncols     = 1;
pt(311).subsource = 'SS_DOUBLE';
pt(311).ndims     = '2';
pt(311).size      = '[]';

pt(312).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue17';
pt(312).paramname = 'Value';
pt(312).class     = 'scalar';
pt(312).nrows     = 1;
pt(312).ncols     = 1;
pt(312).subsource = 'SS_DOUBLE';
pt(312).ndims     = '2';
pt(312).size      = '[]';

pt(313).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue18';
pt(313).paramname = 'Value';
pt(313).class     = 'scalar';
pt(313).nrows     = 1;
pt(313).ncols     = 1;
pt(313).subsource = 'SS_DOUBLE';
pt(313).ndims     = '2';
pt(313).size      = '[]';

pt(314).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue19';
pt(314).paramname = 'Value';
pt(314).class     = 'scalar';
pt(314).nrows     = 1;
pt(314).ncols     = 1;
pt(314).subsource = 'SS_DOUBLE';
pt(314).ndims     = '2';
pt(314).size      = '[]';

pt(315).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue2';
pt(315).paramname = 'Value';
pt(315).class     = 'scalar';
pt(315).nrows     = 1;
pt(315).ncols     = 1;
pt(315).subsource = 'SS_DOUBLE';
pt(315).ndims     = '2';
pt(315).size      = '[]';

pt(316).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue20';
pt(316).paramname = 'Value';
pt(316).class     = 'scalar';
pt(316).nrows     = 1;
pt(316).ncols     = 1;
pt(316).subsource = 'SS_DOUBLE';
pt(316).ndims     = '2';
pt(316).size      = '[]';

pt(317).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue21';
pt(317).paramname = 'Value';
pt(317).class     = 'scalar';
pt(317).nrows     = 1;
pt(317).ncols     = 1;
pt(317).subsource = 'SS_DOUBLE';
pt(317).ndims     = '2';
pt(317).size      = '[]';

pt(318).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue22';
pt(318).paramname = 'Value';
pt(318).class     = 'scalar';
pt(318).nrows     = 1;
pt(318).ncols     = 1;
pt(318).subsource = 'SS_DOUBLE';
pt(318).ndims     = '2';
pt(318).size      = '[]';

pt(319).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue23';
pt(319).paramname = 'Value';
pt(319).class     = 'scalar';
pt(319).nrows     = 1;
pt(319).ncols     = 1;
pt(319).subsource = 'SS_DOUBLE';
pt(319).ndims     = '2';
pt(319).size      = '[]';

pt(320).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue24';
pt(320).paramname = 'Value';
pt(320).class     = 'scalar';
pt(320).nrows     = 1;
pt(320).ncols     = 1;
pt(320).subsource = 'SS_DOUBLE';
pt(320).ndims     = '2';
pt(320).size      = '[]';

pt(321).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue25';
pt(321).paramname = 'Value';
pt(321).class     = 'scalar';
pt(321).nrows     = 1;
pt(321).ncols     = 1;
pt(321).subsource = 'SS_DOUBLE';
pt(321).ndims     = '2';
pt(321).size      = '[]';

pt(322).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue26';
pt(322).paramname = 'Value';
pt(322).class     = 'scalar';
pt(322).nrows     = 1;
pt(322).ncols     = 1;
pt(322).subsource = 'SS_DOUBLE';
pt(322).ndims     = '2';
pt(322).size      = '[]';

pt(323).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue27';
pt(323).paramname = 'Value';
pt(323).class     = 'scalar';
pt(323).nrows     = 1;
pt(323).ncols     = 1;
pt(323).subsource = 'SS_DOUBLE';
pt(323).ndims     = '2';
pt(323).size      = '[]';

pt(324).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue28';
pt(324).paramname = 'Value';
pt(324).class     = 'scalar';
pt(324).nrows     = 1;
pt(324).ncols     = 1;
pt(324).subsource = 'SS_DOUBLE';
pt(324).ndims     = '2';
pt(324).size      = '[]';

pt(325).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue29';
pt(325).paramname = 'Value';
pt(325).class     = 'scalar';
pt(325).nrows     = 1;
pt(325).ncols     = 1;
pt(325).subsource = 'SS_DOUBLE';
pt(325).ndims     = '2';
pt(325).size      = '[]';

pt(326).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue3';
pt(326).paramname = 'Value';
pt(326).class     = 'scalar';
pt(326).nrows     = 1;
pt(326).ncols     = 1;
pt(326).subsource = 'SS_DOUBLE';
pt(326).ndims     = '2';
pt(326).size      = '[]';

pt(327).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue30';
pt(327).paramname = 'Value';
pt(327).class     = 'scalar';
pt(327).nrows     = 1;
pt(327).ncols     = 1;
pt(327).subsource = 'SS_DOUBLE';
pt(327).ndims     = '2';
pt(327).size      = '[]';

pt(328).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue31';
pt(328).paramname = 'Value';
pt(328).class     = 'scalar';
pt(328).nrows     = 1;
pt(328).ncols     = 1;
pt(328).subsource = 'SS_DOUBLE';
pt(328).ndims     = '2';
pt(328).size      = '[]';

pt(329).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue32';
pt(329).paramname = 'Value';
pt(329).class     = 'scalar';
pt(329).nrows     = 1;
pt(329).ncols     = 1;
pt(329).subsource = 'SS_DOUBLE';
pt(329).ndims     = '2';
pt(329).size      = '[]';

pt(330).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue33';
pt(330).paramname = 'Value';
pt(330).class     = 'scalar';
pt(330).nrows     = 1;
pt(330).ncols     = 1;
pt(330).subsource = 'SS_DOUBLE';
pt(330).ndims     = '2';
pt(330).size      = '[]';

pt(331).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue34';
pt(331).paramname = 'Value';
pt(331).class     = 'scalar';
pt(331).nrows     = 1;
pt(331).ncols     = 1;
pt(331).subsource = 'SS_DOUBLE';
pt(331).ndims     = '2';
pt(331).size      = '[]';

pt(332).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue35';
pt(332).paramname = 'Value';
pt(332).class     = 'scalar';
pt(332).nrows     = 1;
pt(332).ncols     = 1;
pt(332).subsource = 'SS_DOUBLE';
pt(332).ndims     = '2';
pt(332).size      = '[]';

pt(333).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue36';
pt(333).paramname = 'Value';
pt(333).class     = 'scalar';
pt(333).nrows     = 1;
pt(333).ncols     = 1;
pt(333).subsource = 'SS_DOUBLE';
pt(333).ndims     = '2';
pt(333).size      = '[]';

pt(334).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue37';
pt(334).paramname = 'Value';
pt(334).class     = 'scalar';
pt(334).nrows     = 1;
pt(334).ncols     = 1;
pt(334).subsource = 'SS_DOUBLE';
pt(334).ndims     = '2';
pt(334).size      = '[]';

pt(335).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue4';
pt(335).paramname = 'Value';
pt(335).class     = 'scalar';
pt(335).nrows     = 1;
pt(335).ncols     = 1;
pt(335).subsource = 'SS_DOUBLE';
pt(335).ndims     = '2';
pt(335).size      = '[]';

pt(336).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue5';
pt(336).paramname = 'Value';
pt(336).class     = 'scalar';
pt(336).nrows     = 1;
pt(336).ncols     = 1;
pt(336).subsource = 'SS_DOUBLE';
pt(336).ndims     = '2';
pt(336).size      = '[]';

pt(337).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue6';
pt(337).paramname = 'Value';
pt(337).class     = 'scalar';
pt(337).nrows     = 1;
pt(337).ncols     = 1;
pt(337).subsource = 'SS_DOUBLE';
pt(337).ndims     = '2';
pt(337).size      = '[]';

pt(338).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue7';
pt(338).paramname = 'Value';
pt(338).class     = 'scalar';
pt(338).nrows     = 1;
pt(338).ncols     = 1;
pt(338).subsource = 'SS_DOUBLE';
pt(338).ndims     = '2';
pt(338).size      = '[]';

pt(339).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue8';
pt(339).paramname = 'Value';
pt(339).class     = 'scalar';
pt(339).nrows     = 1;
pt(339).ncols     = 1;
pt(339).subsource = 'SS_DOUBLE';
pt(339).ndims     = '2';
pt(339).size      = '[]';

pt(340).blockname = 'MonitoringAndLogging /Log Transmitted Data/DefaultValue9';
pt(340).paramname = 'Value';
pt(340).class     = 'scalar';
pt(340).nrows     = 1;
pt(340).ncols     = 1;
pt(340).subsource = 'SS_DOUBLE';
pt(340).ndims     = '2';
pt(340).size      = '[]';

pt(341).blockname = 'MonitoringAndLogging /Log Transmitted Data/Pulse Generator';
pt(341).paramname = 'Amplitude';
pt(341).class     = 'scalar';
pt(341).nrows     = 1;
pt(341).ncols     = 1;
pt(341).subsource = 'SS_DOUBLE';
pt(341).ndims     = '2';
pt(341).size      = '[]';

pt(342).blockname = 'MonitoringAndLogging /Log Transmitted Data/Pulse Generator';
pt(342).paramname = 'Period';
pt(342).class     = 'scalar';
pt(342).nrows     = 1;
pt(342).ncols     = 1;
pt(342).subsource = 'SS_DOUBLE';
pt(342).ndims     = '2';
pt(342).size      = '[]';

pt(343).blockname = 'MonitoringAndLogging /Log Transmitted Data/Pulse Generator';
pt(343).paramname = 'PulseWidth';
pt(343).class     = 'scalar';
pt(343).nrows     = 1;
pt(343).ncols     = 1;
pt(343).subsource = 'SS_DOUBLE';
pt(343).ndims     = '2';
pt(343).size      = '[]';

pt(344).blockname = 'MonitoringAndLogging /Log Transmitted Data/Pulse Generator';
pt(344).paramname = 'PhaseDelay';
pt(344).class     = 'scalar';
pt(344).nrows     = 1;
pt(344).ncols     = 1;
pt(344).subsource = 'SS_DOUBLE';
pt(344).ndims     = '2';
pt(344).size      = '[]';

pt(345).blockname = 'MonitoringAndLogging /Log Transmitted Data/AcknowledgeFlag';
pt(345).paramname = 'Gain';
pt(345).class     = 'scalar';
pt(345).nrows     = 1;
pt(345).ncols     = 1;
pt(345).subsource = 'SS_DOUBLE';
pt(345).ndims     = '2';
pt(345).size      = '[]';

pt(346).blockname = 'MonitoringAndLogging /Log Transmitted Data/BackwardID';
pt(346).paramname = 'Gain';
pt(346).class     = 'scalar';
pt(346).nrows     = 1;
pt(346).ncols     = 1;
pt(346).subsource = 'SS_DOUBLE';
pt(346).ndims     = '2';
pt(346).size      = '[]';

pt(347).blockname = 'MonitoringAndLogging /Log Transmitted Data/CAM_GenerationTime';
pt(347).paramname = 'Gain';
pt(347).class     = 'scalar';
pt(347).nrows     = 1;
pt(347).ncols     = 1;
pt(347).subsource = 'SS_DOUBLE';
pt(347).ndims     = '2';
pt(347).size      = '[]';

pt(348).blockname = 'MonitoringAndLogging /Log Transmitted Data/CounterIntersection';
pt(348).paramname = 'Gain';
pt(348).class     = 'scalar';
pt(348).nrows     = 1;
pt(348).ncols     = 1;
pt(348).subsource = 'SS_DOUBLE';
pt(348).ndims     = '2';
pt(348).size      = '[]';

pt(349).blockname = 'MonitoringAndLogging /Log Transmitted Data/CruiseSpeed';
pt(349).paramname = 'Gain';
pt(349).class     = 'scalar';
pt(349).nrows     = 1;
pt(349).ncols     = 1;
pt(349).subsource = 'SS_DOUBLE';
pt(349).ndims     = '2';
pt(349).size      = '[]';

pt(350).blockname = 'MonitoringAndLogging /Log Transmitted Data/DENM_GenerationTime';
pt(350).paramname = 'Gain';
pt(350).class     = 'scalar';
pt(350).nrows     = 1;
pt(350).ncols     = 1;
pt(350).subsource = 'SS_DOUBLE';
pt(350).ndims     = '2';
pt(350).size      = '[]';

pt(351).blockname = 'MonitoringAndLogging /Log Transmitted Data/DistanceTravelledCZ';
pt(351).paramname = 'Gain';
pt(351).class     = 'scalar';
pt(351).nrows     = 1;
pt(351).ncols     = 1;
pt(351).subsource = 'SS_DOUBLE';
pt(351).ndims     = '2';
pt(351).size      = '[]';

pt(352).blockname = 'MonitoringAndLogging /Log Transmitted Data/EndOfScenario';
pt(352).paramname = 'Gain';
pt(352).class     = 'scalar';
pt(352).nrows     = 1;
pt(352).ncols     = 1;
pt(352).subsource = 'SS_DOUBLE';
pt(352).ndims     = '2';
pt(352).size      = '[]';

pt(353).blockname = 'MonitoringAndLogging /Log Transmitted Data/EventType_CauseCode';
pt(353).paramname = 'Gain';
pt(353).class     = 'scalar';
pt(353).nrows     = 1;
pt(353).ncols     = 1;
pt(353).subsource = 'SS_DOUBLE';
pt(353).ndims     = '2';
pt(353).size      = '[]';

pt(354).blockname = 'MonitoringAndLogging /Log Transmitted Data/ForwardID';
pt(354).paramname = 'Gain';
pt(354).class     = 'scalar';
pt(354).nrows     = 1;
pt(354).ncols     = 1;
pt(354).subsource = 'SS_DOUBLE';
pt(354).ndims     = '2';
pt(354).size      = '[]';

pt(355).blockname = 'MonitoringAndLogging /Log Transmitted Data/Heading';
pt(355).paramname = 'Gain';
pt(355).class     = 'scalar';
pt(355).nrows     = 1;
pt(355).ncols     = 1;
pt(355).subsource = 'SS_DOUBLE';
pt(355).ndims     = '2';
pt(355).size      = '[]';

pt(356).blockname = 'MonitoringAndLogging /Log Transmitted Data/Intention';
pt(356).paramname = 'Gain';
pt(356).class     = 'scalar';
pt(356).nrows     = 1;
pt(356).ncols     = 1;
pt(356).subsource = 'SS_DOUBLE';
pt(356).ndims     = '2';
pt(356).size      = '[]';

pt(357).blockname = 'MonitoringAndLogging /Log Transmitted Data/Lane';
pt(357).paramname = 'Gain';
pt(357).class     = 'scalar';
pt(357).nrows     = 1;
pt(357).ncols     = 1;
pt(357).subsource = 'SS_DOUBLE';
pt(357).ndims     = '2';
pt(357).size      = '[]';

pt(358).blockname = 'MonitoringAndLogging /Log Transmitted Data/Latitude';
pt(358).paramname = 'Gain';
pt(358).class     = 'scalar';
pt(358).nrows     = 1;
pt(358).ncols     = 1;
pt(358).subsource = 'SS_DOUBLE';
pt(358).ndims     = '2';
pt(358).size      = '[]';

pt(359).blockname = 'MonitoringAndLogging /Log Transmitted Data/Longitude';
pt(359).paramname = 'Gain';
pt(359).class     = 'scalar';
pt(359).nrows     = 1;
pt(359).ncols     = 1;
pt(359).subsource = 'SS_DOUBLE';
pt(359).ndims     = '2';
pt(359).size      = '[]';

pt(360).blockname = 'MonitoringAndLogging /Log Transmitted Data/LongitudinalAcceleration';
pt(360).paramname = 'Gain';
pt(360).class     = 'scalar';
pt(360).nrows     = 1;
pt(360).ncols     = 1;
pt(360).subsource = 'SS_DOUBLE';
pt(360).ndims     = '2';
pt(360).size      = '[]';

pt(361).blockname = 'MonitoringAndLogging /Log Transmitted Data/MergeFlag';
pt(361).paramname = 'Gain';
pt(361).class     = 'scalar';
pt(361).nrows     = 1;
pt(361).ncols     = 1;
pt(361).subsource = 'SS_DOUBLE';
pt(361).ndims     = '2';
pt(361).size      = '[]';

pt(362).blockname = 'MonitoringAndLogging /Log Transmitted Data/MergeFlagHead';
pt(362).paramname = 'Gain';
pt(362).class     = 'scalar';
pt(362).nrows     = 1;
pt(362).ncols     = 1;
pt(362).subsource = 'SS_DOUBLE';
pt(362).ndims     = '2';
pt(362).size      = '[]';

pt(363).blockname = 'MonitoringAndLogging /Log Transmitted Data/MergeFlagTail';
pt(363).paramname = 'Gain';
pt(363).class     = 'scalar';
pt(363).nrows     = 1;
pt(363).ncols     = 1;
pt(363).subsource = 'SS_DOUBLE';
pt(363).ndims     = '2';
pt(363).size      = '[]';

pt(364).blockname = 'MonitoringAndLogging /Log Transmitted Data/MergeRequest';
pt(364).paramname = 'Gain';
pt(364).class     = 'scalar';
pt(364).nrows     = 1;
pt(364).ncols     = 1;
pt(364).subsource = 'SS_DOUBLE';
pt(364).ndims     = '2';
pt(364).size      = '[]';

pt(365).blockname = 'MonitoringAndLogging /Log Transmitted Data/MergeSafeToMerge';
pt(365).paramname = 'Gain';
pt(365).class     = 'scalar';
pt(365).nrows     = 1;
pt(365).ncols     = 1;
pt(365).subsource = 'SS_DOUBLE';
pt(365).ndims     = '2';
pt(365).size      = '[]';

pt(366).blockname = 'MonitoringAndLogging /Log Transmitted Data/MioBearing';
pt(366).paramname = 'Gain';
pt(366).class     = 'scalar';
pt(366).nrows     = 1;
pt(366).ncols     = 1;
pt(366).subsource = 'SS_DOUBLE';
pt(366).ndims     = '2';
pt(366).size      = '[]';

pt(367).blockname = 'MonitoringAndLogging /Log Transmitted Data/MioID';
pt(367).paramname = 'Gain';
pt(367).class     = 'scalar';
pt(367).nrows     = 1;
pt(367).ncols     = 1;
pt(367).subsource = 'SS_DOUBLE';
pt(367).ndims     = '2';
pt(367).size      = '[]';

pt(368).blockname = 'MonitoringAndLogging /Log Transmitted Data/MioRange';
pt(368).paramname = 'Gain';
pt(368).class     = 'scalar';
pt(368).nrows     = 1;
pt(368).ncols     = 1;
pt(368).subsource = 'SS_DOUBLE';
pt(368).ndims     = '2';
pt(368).size      = '[]';

pt(369).blockname = 'MonitoringAndLogging /Log Transmitted Data/MioRangeRate';
pt(369).paramname = 'Gain';
pt(369).class     = 'scalar';
pt(369).nrows     = 1;
pt(369).ncols     = 1;
pt(369).subsource = 'SS_DOUBLE';
pt(369).ndims     = '2';
pt(369).size      = '[]';

pt(370).blockname = 'MonitoringAndLogging /Log Transmitted Data/PlatoonID';
pt(370).paramname = 'Gain';
pt(370).class     = 'scalar';
pt(370).nrows     = 1;
pt(370).ncols     = 1;
pt(370).subsource = 'SS_DOUBLE';
pt(370).ndims     = '2';
pt(370).size      = '[]';

pt(371).blockname = 'MonitoringAndLogging /Log Transmitted Data/ReferenceTime';
pt(371).paramname = 'Gain';
pt(371).class     = 'scalar';
pt(371).nrows     = 1;
pt(371).ncols     = 1;
pt(371).subsource = 'SS_DOUBLE';
pt(371).ndims     = '2';
pt(371).size      = '[]';

pt(372).blockname = 'MonitoringAndLogging /Log Transmitted Data/Speed';
pt(372).paramname = 'Gain';
pt(372).class     = 'scalar';
pt(372).nrows     = 1;
pt(372).ncols     = 1;
pt(372).subsource = 'SS_DOUBLE';
pt(372).ndims     = '2';
pt(372).size      = '[]';

pt(373).blockname = 'MonitoringAndLogging /Log Transmitted Data/StartPlatoon';
pt(373).paramname = 'Gain';
pt(373).class     = 'scalar';
pt(373).nrows     = 1;
pt(373).ncols     = 1;
pt(373).subsource = 'SS_DOUBLE';
pt(373).ndims     = '2';
pt(373).size      = '[]';

pt(374).blockname = 'MonitoringAndLogging /Log Transmitted Data/TimeHeadway';
pt(374).paramname = 'Gain';
pt(374).class     = 'scalar';
pt(374).nrows     = 1;
pt(374).ncols     = 1;
pt(374).subsource = 'SS_DOUBLE';
pt(374).ndims     = '2';
pt(374).size      = '[]';

pt(375).blockname = 'MonitoringAndLogging /Log Transmitted Data/VehicleLength';
pt(375).paramname = 'Gain';
pt(375).class     = 'scalar';
pt(375).nrows     = 1;
pt(375).ncols     = 1;
pt(375).subsource = 'SS_DOUBLE';
pt(375).ndims     = '2';
pt(375).size      = '[]';

pt(376).blockname = 'MonitoringAndLogging /Log Transmitted Data/VehicleRearAxleLocation';
pt(376).paramname = 'Gain';
pt(376).class     = 'scalar';
pt(376).nrows     = 1;
pt(376).ncols     = 1;
pt(376).subsource = 'SS_DOUBLE';
pt(376).ndims     = '2';
pt(376).size      = '[]';

pt(377).blockname = 'MonitoringAndLogging /Log Transmitted Data/VehicleWidth';
pt(377).paramname = 'Gain';
pt(377).class     = 'scalar';
pt(377).nrows     = 1;
pt(377).ncols     = 1;
pt(377).subsource = 'SS_DOUBLE';
pt(377).ndims     = '2';
pt(377).size      = '[]';

pt(378).blockname = 'MonitoringAndLogging /Log Transmitted Data/iCLCL_GenerationTime';
pt(378).paramname = 'Gain';
pt(378).class     = 'scalar';
pt(378).nrows     = 1;
pt(378).ncols     = 1;
pt(378).subsource = 'SS_DOUBLE';
pt(378).ndims     = '2';
pt(378).size      = '[]';

pt(379).blockname = 'MonitoringAndLogging /Log Transmitted Data/stationID';
pt(379).paramname = 'Gain';
pt(379).class     = 'scalar';
pt(379).nrows     = 1;
pt(379).ncols     = 1;
pt(379).subsource = 'SS_DOUBLE';
pt(379).ndims     = '2';
pt(379).size      = '[]';

pt(380).blockname = 'MonitoringAndLogging /Log Transmitted Data/stationType';
pt(380).paramname = 'Gain';
pt(380).class     = 'scalar';
pt(380).nrows     = 1;
pt(380).ncols     = 1;
pt(380).subsource = 'SS_DOUBLE';
pt(380).ndims     = '2';
pt(380).size      = '[]';

pt(381).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 1';
pt(381).paramname = 'P1';
pt(381).class     = 'scalar';
pt(381).nrows     = 1;
pt(381).ncols     = 1;
pt(381).subsource = 'SS_DOUBLE';
pt(381).ndims     = '2';
pt(381).size      = '[]';

pt(382).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 1';
pt(382).paramname = 'P2';
pt(382).class     = 'scalar';
pt(382).nrows     = 1;
pt(382).ncols     = 1;
pt(382).subsource = 'SS_DOUBLE';
pt(382).ndims     = '2';
pt(382).size      = '[]';

pt(383).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 1';
pt(383).paramname = 'P3';
pt(383).class     = 'scalar';
pt(383).nrows     = 1;
pt(383).ncols     = 1;
pt(383).subsource = 'SS_DOUBLE';
pt(383).ndims     = '2';
pt(383).size      = '[]';

pt(384).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 1';
pt(384).paramname = 'P4';
pt(384).class     = 'scalar';
pt(384).nrows     = 1;
pt(384).ncols     = 1;
pt(384).subsource = 'SS_DOUBLE';
pt(384).ndims     = '2';
pt(384).size      = '[]';

pt(385).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 1';
pt(385).paramname = 'P5';
pt(385).class     = 'vector';
pt(385).nrows     = 1;
pt(385).ncols     = 20;
pt(385).subsource = 'SS_DOUBLE';
pt(385).ndims     = '2';
pt(385).size      = '[]';

pt(386).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 2';
pt(386).paramname = 'P1';
pt(386).class     = 'scalar';
pt(386).nrows     = 1;
pt(386).ncols     = 1;
pt(386).subsource = 'SS_DOUBLE';
pt(386).ndims     = '2';
pt(386).size      = '[]';

pt(387).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 2';
pt(387).paramname = 'P2';
pt(387).class     = 'scalar';
pt(387).nrows     = 1;
pt(387).ncols     = 1;
pt(387).subsource = 'SS_DOUBLE';
pt(387).ndims     = '2';
pt(387).size      = '[]';

pt(388).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 2';
pt(388).paramname = 'P3';
pt(388).class     = 'scalar';
pt(388).nrows     = 1;
pt(388).ncols     = 1;
pt(388).subsource = 'SS_DOUBLE';
pt(388).ndims     = '2';
pt(388).size      = '[]';

pt(389).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 2';
pt(389).paramname = 'P4';
pt(389).class     = 'scalar';
pt(389).nrows     = 1;
pt(389).ncols     = 1;
pt(389).subsource = 'SS_DOUBLE';
pt(389).ndims     = '2';
pt(389).size      = '[]';

pt(390).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/FIFO write 2';
pt(390).paramname = 'P5';
pt(390).class     = 'vector';
pt(390).nrows     = 1;
pt(390).ncols     = 20;
pt(390).subsource = 'SS_DOUBLE';
pt(390).ndims     = '2';
pt(390).size      = '[]';

pt(391).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup1';
pt(391).paramname = 'P1';
pt(391).class     = 'scalar';
pt(391).nrows     = 1;
pt(391).ncols     = 1;
pt(391).subsource = 'SS_DOUBLE';
pt(391).ndims     = '2';
pt(391).size      = '[]';

pt(392).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup1';
pt(392).paramname = 'P2';
pt(392).class     = 'scalar';
pt(392).nrows     = 1;
pt(392).ncols     = 1;
pt(392).subsource = 'SS_DOUBLE';
pt(392).ndims     = '2';
pt(392).size      = '[]';

pt(393).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup1';
pt(393).paramname = 'P3';
pt(393).class     = 'scalar';
pt(393).nrows     = 1;
pt(393).ncols     = 1;
pt(393).subsource = 'SS_DOUBLE';
pt(393).ndims     = '2';
pt(393).size      = '[]';

pt(394).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup1';
pt(394).paramname = 'P4';
pt(394).class     = 'scalar';
pt(394).nrows     = 1;
pt(394).ncols     = 1;
pt(394).subsource = 'SS_DOUBLE';
pt(394).ndims     = '2';
pt(394).size      = '[]';

pt(395).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup1';
pt(395).paramname = 'P5';
pt(395).class     = 'scalar';
pt(395).nrows     = 1;
pt(395).ncols     = 1;
pt(395).subsource = 'SS_DOUBLE';
pt(395).ndims     = '2';
pt(395).size      = '[]';

pt(396).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup1';
pt(396).paramname = 'P6';
pt(396).class     = 'scalar';
pt(396).nrows     = 1;
pt(396).ncols     = 1;
pt(396).subsource = 'SS_DOUBLE';
pt(396).ndims     = '2';
pt(396).size      = '[]';

pt(397).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup1';
pt(397).paramname = 'P7';
pt(397).class     = 'scalar';
pt(397).nrows     = 1;
pt(397).ncols     = 1;
pt(397).subsource = 'SS_DOUBLE';
pt(397).ndims     = '2';
pt(397).size      = '[]';

pt(398).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup1';
pt(398).paramname = 'P8';
pt(398).class     = 'scalar';
pt(398).nrows     = 1;
pt(398).ncols     = 1;
pt(398).subsource = 'SS_DOUBLE';
pt(398).ndims     = '2';
pt(398).size      = '[]';

pt(399).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup2';
pt(399).paramname = 'P1';
pt(399).class     = 'scalar';
pt(399).nrows     = 1;
pt(399).ncols     = 1;
pt(399).subsource = 'SS_DOUBLE';
pt(399).ndims     = '2';
pt(399).size      = '[]';

pt(400).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup2';
pt(400).paramname = 'P2';
pt(400).class     = 'scalar';
pt(400).nrows     = 1;
pt(400).ncols     = 1;
pt(400).subsource = 'SS_DOUBLE';
pt(400).ndims     = '2';
pt(400).size      = '[]';

pt(401).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup2';
pt(401).paramname = 'P3';
pt(401).class     = 'scalar';
pt(401).nrows     = 1;
pt(401).ncols     = 1;
pt(401).subsource = 'SS_DOUBLE';
pt(401).ndims     = '2';
pt(401).size      = '[]';

pt(402).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup2';
pt(402).paramname = 'P4';
pt(402).class     = 'scalar';
pt(402).nrows     = 1;
pt(402).ncols     = 1;
pt(402).subsource = 'SS_DOUBLE';
pt(402).ndims     = '2';
pt(402).size      = '[]';

pt(403).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup2';
pt(403).paramname = 'P5';
pt(403).class     = 'scalar';
pt(403).nrows     = 1;
pt(403).ncols     = 1;
pt(403).subsource = 'SS_DOUBLE';
pt(403).ndims     = '2';
pt(403).size      = '[]';

pt(404).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup2';
pt(404).paramname = 'P6';
pt(404).class     = 'scalar';
pt(404).nrows     = 1;
pt(404).ncols     = 1;
pt(404).subsource = 'SS_DOUBLE';
pt(404).ndims     = '2';
pt(404).size      = '[]';

pt(405).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup2';
pt(405).paramname = 'P7';
pt(405).class     = 'scalar';
pt(405).nrows     = 1;
pt(405).ncols     = 1;
pt(405).subsource = 'SS_DOUBLE';
pt(405).ndims     = '2';
pt(405).size      = '[]';

pt(406).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/Setup2';
pt(406).paramname = 'P8';
pt(406).class     = 'scalar';
pt(406).nrows     = 1;
pt(406).ncols     = 1;
pt(406).subsource = 'SS_DOUBLE';
pt(406).ndims     = '2';
pt(406).size      = '[]';

pt(407).blockname = 'UDP IN (Cohda)1/V2V convert/Res 0.002';
pt(407).paramname = 'Gain';
pt(407).class     = 'scalar';
pt(407).nrows     = 1;
pt(407).ncols     = 1;
pt(407).subsource = 'SS_INT32';
pt(407).ndims     = '2';
pt(407).size      = '[]';

pt(408).blockname = 'UDP IN (Cohda)1/V2V convert/Res 0.01';
pt(408).paramname = 'Gain';
pt(408).class     = 'scalar';
pt(408).nrows     = 1;
pt(408).ncols     = 1;
pt(408).subsource = 'SS_INT32';
pt(408).ndims     = '2';
pt(408).size      = '[]';

pt(409).blockname = 'UDP IN (Cohda)1/V2V convert/Res 0.01 ';
pt(409).paramname = 'Gain';
pt(409).class     = 'scalar';
pt(409).nrows     = 1;
pt(409).ncols     = 1;
pt(409).subsource = 'SS_INT32';
pt(409).ndims     = '2';
pt(409).size      = '[]';

pt(410).blockname = 'UDP IN (Cohda)1/V2V convert/Res 0.1';
pt(410).paramname = 'Gain';
pt(410).class     = 'scalar';
pt(410).nrows     = 1;
pt(410).ncols     = 1;
pt(410).subsource = 'SS_INT32';
pt(410).ndims     = '2';
pt(410).size      = '[]';

pt(411).blockname = 'UDP IN (Cohda)1/V2V convert/Res1 0.01';
pt(411).paramname = 'Gain';
pt(411).class     = 'scalar';
pt(411).nrows     = 1;
pt(411).ncols     = 1;
pt(411).subsource = 'SS_INT32';
pt(411).ndims     = '2';
pt(411).size      = '[]';

pt(412).blockname = 'UDP IN (Cohda)1/V2V convert/Res1 0.1';
pt(412).paramname = 'Gain';
pt(412).class     = 'scalar';
pt(412).nrows     = 1;
pt(412).ncols     = 1;
pt(412).subsource = 'SS_INT32';
pt(412).ndims     = '2';
pt(412).size      = '[]';

pt(413).blockname = 'UDP IN (Cohda)1/V2V convert/Res2 0.01';
pt(413).paramname = 'Gain';
pt(413).class     = 'scalar';
pt(413).nrows     = 1;
pt(413).ncols     = 1;
pt(413).subsource = 'SS_INT32';
pt(413).ndims     = '2';
pt(413).size      = '[]';

pt(414).blockname = 'UDP IN (Cohda)1/V2V convert/-1-14';
pt(414).paramname = 'UpperLimit';
pt(414).class     = 'scalar';
pt(414).nrows     = 1;
pt(414).ncols     = 1;
pt(414).subsource = 'SS_INT32';
pt(414).ndims     = '2';
pt(414).size      = '[]';

pt(415).blockname = 'UDP IN (Cohda)1/V2V convert/-1-14';
pt(415).paramname = 'LowerLimit';
pt(415).class     = 'scalar';
pt(415).nrows     = 1;
pt(415).ncols     = 1;
pt(415).subsource = 'SS_INT32';
pt(415).ndims     = '2';
pt(415).size      = '[]';

pt(416).blockname = 'UDP IN (Cohda)1/V2V convert/0-3';
pt(416).paramname = 'UpperLimit';
pt(416).class     = 'scalar';
pt(416).nrows     = 1;
pt(416).ncols     = 1;
pt(416).subsource = 'SS_INT32';
pt(416).ndims     = '2';
pt(416).size      = '[]';

pt(417).blockname = 'UDP IN (Cohda)1/V2V convert/0-3';
pt(417).paramname = 'LowerLimit';
pt(417).class     = 'scalar';
pt(417).nrows     = 1;
pt(417).ncols     = 1;
pt(417).subsource = 'SS_INT32';
pt(417).ndims     = '2';
pt(417).size      = '[]';

pt(418).blockname = 'UDP IN (Cohda)1/V2V convert/0-3 ';
pt(418).paramname = 'UpperLimit';
pt(418).class     = 'scalar';
pt(418).nrows     = 1;
pt(418).ncols     = 1;
pt(418).subsource = 'SS_INT32';
pt(418).ndims     = '2';
pt(418).size      = '[]';

pt(419).blockname = 'UDP IN (Cohda)1/V2V convert/0-3 ';
pt(419).paramname = 'LowerLimit';
pt(419).class     = 'scalar';
pt(419).nrows     = 1;
pt(419).ncols     = 1;
pt(419).subsource = 'SS_INT32';
pt(419).ndims     = '2';
pt(419).size      = '[]';

pt(420).blockname = 'UDP IN (Cohda)1/V2V convert/1-3';
pt(420).paramname = 'UpperLimit';
pt(420).class     = 'scalar';
pt(420).nrows     = 1;
pt(420).ncols     = 1;
pt(420).subsource = 'SS_INT32';
pt(420).ndims     = '2';
pt(420).size      = '[]';

pt(421).blockname = 'UDP IN (Cohda)1/V2V convert/1-3';
pt(421).paramname = 'LowerLimit';
pt(421).class     = 'scalar';
pt(421).nrows     = 1;
pt(421).ncols     = 1;
pt(421).subsource = 'SS_INT32';
pt(421).ndims     = '2';
pt(421).size      = '[]';

pt(422).blockname = 'UDP IN (Cohda)1/V2V convert/boolean';
pt(422).paramname = 'UpperLimit';
pt(422).class     = 'scalar';
pt(422).nrows     = 1;
pt(422).ncols     = 1;
pt(422).subsource = 'SS_INT32';
pt(422).ndims     = '2';
pt(422).size      = '[]';

pt(423).blockname = 'UDP IN (Cohda)1/V2V convert/boolean';
pt(423).paramname = 'LowerLimit';
pt(423).class     = 'scalar';
pt(423).nrows     = 1;
pt(423).ncols     = 1;
pt(423).subsource = 'SS_INT32';
pt(423).ndims     = '2';
pt(423).size      = '[]';

pt(424).blockname = 'UDP IN (Cohda)1/V2V convert/boolean sat14';
pt(424).paramname = 'UpperLimit';
pt(424).class     = 'scalar';
pt(424).nrows     = 1;
pt(424).ncols     = 1;
pt(424).subsource = 'SS_INT32';
pt(424).ndims     = '2';
pt(424).size      = '[]';

pt(425).blockname = 'UDP IN (Cohda)1/V2V convert/boolean sat14';
pt(425).paramname = 'LowerLimit';
pt(425).class     = 'scalar';
pt(425).nrows     = 1;
pt(425).ncols     = 1;
pt(425).subsource = 'SS_INT32';
pt(425).ndims     = '2';
pt(425).size      = '[]';

pt(426).blockname = 'UDP IN (Cohda)1/V2V convert/boolean sat15';
pt(426).paramname = 'UpperLimit';
pt(426).class     = 'scalar';
pt(426).nrows     = 1;
pt(426).ncols     = 1;
pt(426).subsource = 'SS_INT32';
pt(426).ndims     = '2';
pt(426).size      = '[]';

pt(427).blockname = 'UDP IN (Cohda)1/V2V convert/boolean sat15';
pt(427).paramname = 'LowerLimit';
pt(427).class     = 'scalar';
pt(427).nrows     = 1;
pt(427).ncols     = 1;
pt(427).subsource = 'SS_INT32';
pt(427).ndims     = '2';
pt(427).size      = '[]';

pt(428).blockname = 'UDP IN (Cohda)1/V2V convert/boolean sat16';
pt(428).paramname = 'UpperLimit';
pt(428).class     = 'scalar';
pt(428).nrows     = 1;
pt(428).ncols     = 1;
pt(428).subsource = 'SS_INT32';
pt(428).ndims     = '2';
pt(428).size      = '[]';

pt(429).blockname = 'UDP IN (Cohda)1/V2V convert/boolean sat16';
pt(429).paramname = 'LowerLimit';
pt(429).class     = 'scalar';
pt(429).nrows     = 1;
pt(429).ncols     = 1;
pt(429).subsource = 'SS_INT32';
pt(429).ndims     = '2';
pt(429).size      = '[]';

pt(430).blockname = 'UDP IN (Cohda)1/V2V convert/boolean sat17';
pt(430).paramname = 'UpperLimit';
pt(430).class     = 'scalar';
pt(430).nrows     = 1;
pt(430).ncols     = 1;
pt(430).subsource = 'SS_INT32';
pt(430).ndims     = '2';
pt(430).size      = '[]';

pt(431).blockname = 'UDP IN (Cohda)1/V2V convert/boolean sat17';
pt(431).paramname = 'LowerLimit';
pt(431).class     = 'scalar';
pt(431).nrows     = 1;
pt(431).ncols     = 1;
pt(431).subsource = 'SS_INT32';
pt(431).ndims     = '2';
pt(431).size      = '[]';

pt(432).blockname = 'UDP IN (Cohda)1/V2V convert/boolean1';
pt(432).paramname = 'UpperLimit';
pt(432).class     = 'scalar';
pt(432).nrows     = 1;
pt(432).ncols     = 1;
pt(432).subsource = 'SS_INT32';
pt(432).ndims     = '2';
pt(432).size      = '[]';

pt(433).blockname = 'UDP IN (Cohda)1/V2V convert/boolean1';
pt(433).paramname = 'LowerLimit';
pt(433).class     = 'scalar';
pt(433).nrows     = 1;
pt(433).ncols     = 1;
pt(433).subsource = 'SS_INT32';
pt(433).ndims     = '2';
pt(433).size      = '[]';

pt(434).blockname = 'UDP IN (Cohda)1/V2V convert/boolean2';
pt(434).paramname = 'UpperLimit';
pt(434).class     = 'scalar';
pt(434).nrows     = 1;
pt(434).ncols     = 1;
pt(434).subsource = 'SS_INT32';
pt(434).ndims     = '2';
pt(434).size      = '[]';

pt(435).blockname = 'UDP IN (Cohda)1/V2V convert/boolean2';
pt(435).paramname = 'LowerLimit';
pt(435).class     = 'scalar';
pt(435).nrows     = 1;
pt(435).ncols     = 1;
pt(435).subsource = 'SS_INT32';
pt(435).ndims     = '2';
pt(435).size      = '[]';

pt(436).blockname = 'UDP IN (Cohda)1/V2V convert/boolean3';
pt(436).paramname = 'UpperLimit';
pt(436).class     = 'scalar';
pt(436).nrows     = 1;
pt(436).ncols     = 1;
pt(436).subsource = 'SS_INT32';
pt(436).ndims     = '2';
pt(436).size      = '[]';

pt(437).blockname = 'UDP IN (Cohda)1/V2V convert/boolean3';
pt(437).paramname = 'LowerLimit';
pt(437).class     = 'scalar';
pt(437).nrows     = 1;
pt(437).ncols     = 1;
pt(437).subsource = 'SS_INT32';
pt(437).ndims     = '2';
pt(437).size      = '[]';

pt(438).blockname = 'UDP IN (Cohda)1/V2V convert/boolean4';
pt(438).paramname = 'UpperLimit';
pt(438).class     = 'scalar';
pt(438).nrows     = 1;
pt(438).ncols     = 1;
pt(438).subsource = 'SS_INT32';
pt(438).ndims     = '2';
pt(438).size      = '[]';

pt(439).blockname = 'UDP IN (Cohda)1/V2V convert/boolean4';
pt(439).paramname = 'LowerLimit';
pt(439).class     = 'scalar';
pt(439).nrows     = 1;
pt(439).ncols     = 1;
pt(439).subsource = 'SS_INT32';
pt(439).ndims     = '2';
pt(439).size      = '[]';

pt(440).blockname = 'referenceRouting/Discrete Derivative/TSamp';
pt(440).paramname = 'WtEt';
pt(440).class     = 'scalar';
pt(440).nrows     = 1;
pt(440).ncols     = 1;
pt(440).subsource = 'SS_DOUBLE';
pt(440).ndims     = '2';
pt(440).size      = '[]';

pt(441).blockname = 'CAN IN (Platform)/Truck/Discrete Derivative/TSamp';
pt(441).paramname = 'WtEt';
pt(441).class     = 'scalar';
pt(441).nrows     = 1;
pt(441).ncols     = 1;
pt(441).subsource = 'SS_DOUBLE';
pt(441).ndims     = '2';
pt(441).size      = '[]';

pt(442).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)';
pt(442).paramname = 'P1';
pt(442).class     = 'scalar';
pt(442).nrows     = 1;
pt(442).ncols     = 1;
pt(442).subsource = 'SS_DOUBLE';
pt(442).ndims     = '2';
pt(442).size      = '[]';

pt(443).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)';
pt(443).paramname = 'P2';
pt(443).class     = 'scalar';
pt(443).nrows     = 1;
pt(443).ncols     = 1;
pt(443).subsource = 'SS_DOUBLE';
pt(443).ndims     = '2';
pt(443).size      = '[]';

pt(444).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)';
pt(444).paramname = 'P3';
pt(444).class     = 'scalar';
pt(444).nrows     = 1;
pt(444).ncols     = 1;
pt(444).subsource = 'SS_DOUBLE';
pt(444).ndims     = '2';
pt(444).size      = '[]';

pt(445).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)';
pt(445).paramname = 'P4';
pt(445).class     = 'scalar';
pt(445).nrows     = 1;
pt(445).ncols     = 1;
pt(445).subsource = 'SS_DOUBLE';
pt(445).ndims     = '2';
pt(445).size      = '[]';

pt(446).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)';
pt(446).paramname = 'P5';
pt(446).class     = 'scalar';
pt(446).nrows     = 1;
pt(446).ncols     = 1;
pt(446).subsource = 'SS_DOUBLE';
pt(446).ndims     = '2';
pt(446).size      = '[]';

pt(447).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)';
pt(447).paramname = 'P6';
pt(447).class     = 'scalar';
pt(447).nrows     = 1;
pt(447).ncols     = 1;
pt(447).subsource = 'SS_DOUBLE';
pt(447).ndims     = '2';
pt(447).size      = '[]';

pt(448).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)';
pt(448).paramname = 'P7';
pt(448).class     = 'scalar';
pt(448).nrows     = 1;
pt(448).ncols     = 1;
pt(448).subsource = 'SS_DOUBLE';
pt(448).ndims     = '2';
pt(448).size      = '[]';

pt(449).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Read (v3)';
pt(449).paramname = 'P8';
pt(449).class     = 'scalar';
pt(449).nrows     = 1;
pt(449).ncols     = 1;
pt(449).subsource = 'SS_DOUBLE';
pt(449).ndims     = '2';
pt(449).size      = '[]';

pt(450).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(450).paramname = 'P1';
pt(450).class     = 'scalar';
pt(450).nrows     = 1;
pt(450).ncols     = 1;
pt(450).subsource = 'SS_DOUBLE';
pt(450).ndims     = '2';
pt(450).size      = '[]';

pt(451).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(451).paramname = 'P2';
pt(451).class     = 'scalar';
pt(451).nrows     = 1;
pt(451).ncols     = 1;
pt(451).subsource = 'SS_DOUBLE';
pt(451).ndims     = '2';
pt(451).size      = '[]';

pt(452).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(452).paramname = 'P3';
pt(452).class     = 'scalar';
pt(452).nrows     = 1;
pt(452).ncols     = 1;
pt(452).subsource = 'SS_DOUBLE';
pt(452).ndims     = '2';
pt(452).size      = '[]';

pt(453).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(453).paramname = 'P4';
pt(453).class     = 'vector';
pt(453).nrows     = 1;
pt(453).ncols     = 4;
pt(453).subsource = 'SS_DOUBLE';
pt(453).ndims     = '2';
pt(453).size      = '[]';

pt(454).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(454).paramname = 'P5';
pt(454).class     = 'vector';
pt(454).nrows     = 1;
pt(454).ncols     = 4;
pt(454).subsource = 'SS_DOUBLE';
pt(454).ndims     = '2';
pt(454).size      = '[]';

pt(455).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(455).paramname = 'P6';
pt(455).class     = 'vector';
pt(455).nrows     = 1;
pt(455).ncols     = 4;
pt(455).subsource = 'SS_DOUBLE';
pt(455).ndims     = '2';
pt(455).size      = '[]';

pt(456).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(456).paramname = 'P7';
pt(456).class     = 'scalar';
pt(456).nrows     = 1;
pt(456).ncols     = 1;
pt(456).subsource = 'SS_DOUBLE';
pt(456).ndims     = '2';
pt(456).size      = '[]';

pt(457).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(457).paramname = 'P8';
pt(457).class     = 'scalar';
pt(457).nrows     = 1;
pt(457).ncols     = 1;
pt(457).subsource = 'SS_DOUBLE';
pt(457).ndims     = '2';
pt(457).size      = '[]';

pt(458).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(458).paramname = 'P9';
pt(458).class     = 'vector';
pt(458).nrows     = 1;
pt(458).ncols     = 4;
pt(458).subsource = 'SS_DOUBLE';
pt(458).ndims     = '2';
pt(458).size      = '[]';

pt(459).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(459).paramname = 'P10';
pt(459).class     = 'vector';
pt(459).nrows     = 1;
pt(459).ncols     = 4;
pt(459).subsource = 'SS_DOUBLE';
pt(459).ndims     = '2';
pt(459).size      = '[]';

pt(460).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(460).paramname = 'P11';
pt(460).class     = 'vector';
pt(460).nrows     = 1;
pt(460).ncols     = 4;
pt(460).subsource = 'SS_DOUBLE';
pt(460).ndims     = '2';
pt(460).size      = '[]';

pt(461).blockname = 'CAN IN (Platform)/Truck/CanTruckVariant/SpeedgoatCanTruckVariant/Setup (v3)';
pt(461).paramname = 'P12';
pt(461).class     = 'scalar';
pt(461).nrows     = 1;
pt(461).ncols     = 1;
pt(461).subsource = 'SS_DOUBLE';
pt(461).ndims     = '2';
pt(461).size      = '[]';

pt(462).blockname = 'CAN OUT (Platform)/Truck/CanOutTruckVariant/SpeedgoatCanOutTruckVariant/Write (v3)';
pt(462).paramname = 'P1';
pt(462).class     = 'scalar';
pt(462).nrows     = 1;
pt(462).ncols     = 1;
pt(462).subsource = 'SS_DOUBLE';
pt(462).ndims     = '2';
pt(462).size      = '[]';

pt(463).blockname = 'CAN OUT (Platform)/Truck/CanOutTruckVariant/SpeedgoatCanOutTruckVariant/Write (v3)';
pt(463).paramname = 'P2';
pt(463).class     = 'scalar';
pt(463).nrows     = 1;
pt(463).ncols     = 1;
pt(463).subsource = 'SS_DOUBLE';
pt(463).ndims     = '2';
pt(463).size      = '[]';

pt(464).blockname = 'CAN OUT (Platform)/Truck/CanOutTruckVariant/SpeedgoatCanOutTruckVariant/Write (v3)';
pt(464).paramname = 'P3';
pt(464).class     = 'scalar';
pt(464).nrows     = 1;
pt(464).ncols     = 1;
pt(464).subsource = 'SS_DOUBLE';
pt(464).ndims     = '2';
pt(464).size      = '[]';

pt(465).blockname = 'CAN OUT (Platform)/Truck/CanOutTruckVariant/SpeedgoatCanOutTruckVariant/Write (v3)';
pt(465).paramname = 'P4';
pt(465).class     = 'scalar';
pt(465).nrows     = 1;
pt(465).ncols     = 1;
pt(465).subsource = 'SS_DOUBLE';
pt(465).ndims     = '2';
pt(465).size      = '[]';

pt(466).blockname = 'CAN OUT (Platform)/Truck/CanOutTruckVariant/SpeedgoatCanOutTruckVariant/Write (v3)';
pt(466).paramname = 'P5';
pt(466).class     = 'scalar';
pt(466).nrows     = 1;
pt(466).ncols     = 1;
pt(466).subsource = 'SS_DOUBLE';
pt(466).ndims     = '2';
pt(466).size      = '[]';

pt(467).blockname = 'CAN OUT (Platform)/Truck/CanOutTruckVariant/SpeedgoatCanOutTruckVariant/Write (v3)';
pt(467).paramname = 'P6';
pt(467).class     = 'scalar';
pt(467).nrows     = 1;
pt(467).ncols     = 1;
pt(467).subsource = 'SS_DOUBLE';
pt(467).ndims     = '2';
pt(467).size      = '[]';

pt(468).blockname = 'CAN OUT (Platform)/Truck/CanOutTruckVariant/SpeedgoatCanOutTruckVariant/Write (v3)';
pt(468).paramname = 'P7';
pt(468).class     = 'scalar';
pt(468).nrows     = 1;
pt(468).ncols     = 1;
pt(468).subsource = 'SS_DOUBLE';
pt(468).ndims     = '2';
pt(468).size      = '[]';

pt(469).blockname = 'CAN OUT (Platform)/Truck/CanOutTruckVariant/SpeedgoatCanOutTruckVariant/Write (v3)';
pt(469).paramname = 'P8';
pt(469).class     = 'scalar';
pt(469).nrows     = 1;
pt(469).ncols     = 1;
pt(469).subsource = 'SS_DOUBLE';
pt(469).ndims     = '2';
pt(469).size      = '[]';

pt(470).blockname = 'CAN OUT (Platform)/Truck/CanOutTruckVariant/SpeedgoatCanOutTruckVariant/Write (v3)';
pt(470).paramname = 'P9';
pt(470).class     = 'scalar';
pt(470).nrows     = 1;
pt(470).ncols     = 1;
pt(470).subsource = 'SS_DOUBLE';
pt(470).ndims     = '2';
pt(470).size      = '[]';

pt(471).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 1/Constant';
pt(471).paramname = 'Value';
pt(471).class     = 'scalar';
pt(471).nrows     = 1;
pt(471).ncols     = 1;
pt(471).subsource = 'SS_UINT32';
pt(471).ndims     = '2';
pt(471).size      = '[]';

pt(472).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 1/FIFO write 1';
pt(472).paramname = 'P1';
pt(472).class     = 'scalar';
pt(472).nrows     = 1;
pt(472).ncols     = 1;
pt(472).subsource = 'SS_DOUBLE';
pt(472).ndims     = '2';
pt(472).size      = '[]';

pt(473).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 1/FIFO write 1';
pt(473).paramname = 'P2';
pt(473).class     = 'scalar';
pt(473).nrows     = 1;
pt(473).ncols     = 1;
pt(473).subsource = 'SS_DOUBLE';
pt(473).ndims     = '2';
pt(473).size      = '[]';

pt(474).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 1/FIFO write 1';
pt(474).paramname = 'P3';
pt(474).class     = 'scalar';
pt(474).nrows     = 1;
pt(474).ncols     = 1;
pt(474).subsource = 'SS_DOUBLE';
pt(474).ndims     = '2';
pt(474).size      = '[]';

pt(475).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 1/FIFO write 1';
pt(475).paramname = 'P4';
pt(475).class     = 'scalar';
pt(475).nrows     = 1;
pt(475).ncols     = 1;
pt(475).subsource = 'SS_DOUBLE';
pt(475).ndims     = '2';
pt(475).size      = '[]';

pt(476).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 1/FIFO write 1';
pt(476).paramname = 'P5';
pt(476).class     = 'vector';
pt(476).nrows     = 1;
pt(476).ncols     = 20;
pt(476).subsource = 'SS_DOUBLE';
pt(476).ndims     = '2';
pt(476).size      = '[]';

pt(477).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 2/Constant';
pt(477).paramname = 'Value';
pt(477).class     = 'scalar';
pt(477).nrows     = 1;
pt(477).ncols     = 1;
pt(477).subsource = 'SS_UINT32';
pt(477).ndims     = '2';
pt(477).size      = '[]';

pt(478).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 2/FIFO write 2';
pt(478).paramname = 'P1';
pt(478).class     = 'scalar';
pt(478).nrows     = 1;
pt(478).ncols     = 1;
pt(478).subsource = 'SS_DOUBLE';
pt(478).ndims     = '2';
pt(478).size      = '[]';

pt(479).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 2/FIFO write 2';
pt(479).paramname = 'P2';
pt(479).class     = 'scalar';
pt(479).nrows     = 1;
pt(479).ncols     = 1;
pt(479).subsource = 'SS_DOUBLE';
pt(479).ndims     = '2';
pt(479).size      = '[]';

pt(480).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 2/FIFO write 2';
pt(480).paramname = 'P3';
pt(480).class     = 'scalar';
pt(480).nrows     = 1;
pt(480).ncols     = 1;
pt(480).subsource = 'SS_DOUBLE';
pt(480).ndims     = '2';
pt(480).size      = '[]';

pt(481).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 2/FIFO write 2';
pt(481).paramname = 'P4';
pt(481).class     = 'scalar';
pt(481).nrows     = 1;
pt(481).ncols     = 1;
pt(481).subsource = 'SS_DOUBLE';
pt(481).ndims     = '2';
pt(481).size      = '[]';

pt(482).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Receive 2/FIFO write 2';
pt(482).paramname = 'P5';
pt(482).class     = 'vector';
pt(482).nrows     = 1;
pt(482).ncols     = 20;
pt(482).subsource = 'SS_DOUBLE';
pt(482).ndims     = '2';
pt(482).size      = '[]';

pt(483).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/Constant1';
pt(483).paramname = 'Value';
pt(483).class     = 'scalar';
pt(483).nrows     = 1;
pt(483).ncols     = 1;
pt(483).subsource = 'SS_UINT32';
pt(483).ndims     = '2';
pt(483).size      = '[]';

pt(484).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1';
pt(484).paramname = 'P1';
pt(484).class     = 'scalar';
pt(484).nrows     = 1;
pt(484).ncols     = 1;
pt(484).subsource = 'SS_DOUBLE';
pt(484).ndims     = '2';
pt(484).size      = '[]';

pt(485).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1';
pt(485).paramname = 'P2';
pt(485).class     = 'scalar';
pt(485).nrows     = 1;
pt(485).ncols     = 1;
pt(485).subsource = 'SS_DOUBLE';
pt(485).ndims     = '2';
pt(485).size      = '[]';

pt(486).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1';
pt(486).paramname = 'P3';
pt(486).class     = 'scalar';
pt(486).nrows     = 1;
pt(486).ncols     = 1;
pt(486).subsource = 'SS_DOUBLE';
pt(486).ndims     = '2';
pt(486).size      = '[]';

pt(487).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1';
pt(487).paramname = 'P4';
pt(487).class     = 'scalar';
pt(487).nrows     = 1;
pt(487).ncols     = 1;
pt(487).subsource = 'SS_DOUBLE';
pt(487).ndims     = '2';
pt(487).size      = '[]';

pt(488).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1';
pt(488).paramname = 'P5';
pt(488).class     = 'scalar';
pt(488).nrows     = 1;
pt(488).ncols     = 1;
pt(488).subsource = 'SS_DOUBLE';
pt(488).ndims     = '2';
pt(488).size      = '[]';

pt(489).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1';
pt(489).paramname = 'P6';
pt(489).class     = 'scalar';
pt(489).nrows     = 1;
pt(489).ncols     = 1;
pt(489).subsource = 'SS_DOUBLE';
pt(489).ndims     = '2';
pt(489).size      = '[]';

pt(490).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1';
pt(490).paramname = 'P7';
pt(490).class     = 'scalar';
pt(490).nrows     = 1;
pt(490).ncols     = 1;
pt(490).subsource = 'SS_DOUBLE';
pt(490).ndims     = '2';
pt(490).size      = '[]';

pt(491).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 1/FIFO read 1';
pt(491).paramname = 'P8';
pt(491).class     = 'scalar';
pt(491).nrows     = 1;
pt(491).ncols     = 1;
pt(491).subsource = 'SS_DOUBLE';
pt(491).ndims     = '2';
pt(491).size      = '[]';

pt(492).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/Constant2';
pt(492).paramname = 'Value';
pt(492).class     = 'scalar';
pt(492).nrows     = 1;
pt(492).ncols     = 1;
pt(492).subsource = 'SS_UINT32';
pt(492).ndims     = '2';
pt(492).size      = '[]';

pt(493).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2';
pt(493).paramname = 'P1';
pt(493).class     = 'scalar';
pt(493).nrows     = 1;
pt(493).ncols     = 1;
pt(493).subsource = 'SS_DOUBLE';
pt(493).ndims     = '2';
pt(493).size      = '[]';

pt(494).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2';
pt(494).paramname = 'P2';
pt(494).class     = 'scalar';
pt(494).nrows     = 1;
pt(494).ncols     = 1;
pt(494).subsource = 'SS_DOUBLE';
pt(494).ndims     = '2';
pt(494).size      = '[]';

pt(495).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2';
pt(495).paramname = 'P3';
pt(495).class     = 'scalar';
pt(495).nrows     = 1;
pt(495).ncols     = 1;
pt(495).subsource = 'SS_DOUBLE';
pt(495).ndims     = '2';
pt(495).size      = '[]';

pt(496).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2';
pt(496).paramname = 'P4';
pt(496).class     = 'scalar';
pt(496).nrows     = 1;
pt(496).ncols     = 1;
pt(496).subsource = 'SS_DOUBLE';
pt(496).ndims     = '2';
pt(496).size      = '[]';

pt(497).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2';
pt(497).paramname = 'P5';
pt(497).class     = 'scalar';
pt(497).nrows     = 1;
pt(497).ncols     = 1;
pt(497).subsource = 'SS_DOUBLE';
pt(497).ndims     = '2';
pt(497).size      = '[]';

pt(498).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2';
pt(498).paramname = 'P6';
pt(498).class     = 'scalar';
pt(498).nrows     = 1;
pt(498).ncols     = 1;
pt(498).subsource = 'SS_DOUBLE';
pt(498).ndims     = '2';
pt(498).size      = '[]';

pt(499).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2';
pt(499).paramname = 'P7';
pt(499).class     = 'scalar';
pt(499).nrows     = 1;
pt(499).ncols     = 1;
pt(499).subsource = 'SS_DOUBLE';
pt(499).ndims     = '2';
pt(499).size      = '[]';

pt(500).blockname = 'Serial IN (Trimble)/Baseboard Serial F1/RS232 ISR/Transmit 2/FIFO read 2';
pt(500).paramname = 'P8';
pt(500).class     = 'scalar';
pt(500).nrows     = 1;
pt(500).ncols     = 1;
pt(500).subsource = 'SS_DOUBLE';
pt(500).ndims     = '2';
pt(500).size      = '[]';

pt(501).blockname = 'Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/TimeSynch/StartUp';
pt(501).paramname = 'Value';
pt(501).class     = 'scalar';
pt(501).nrows     = 1;
pt(501).ncols     = 1;
pt(501).subsource = 'SS_DOUBLE';
pt(501).ndims     = '2';
pt(501).size      = '[]';

pt(502).blockname = 'Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/TimeSynch/Delay';
pt(502).paramname = 'DelayLength';
pt(502).class     = 'scalar';
pt(502).nrows     = 1;
pt(502).ncols     = 1;
pt(502).subsource = 'SS_UINT32';
pt(502).ndims     = '2';
pt(502).size      = '[]';

pt(503).blockname = 'Serial IN (Trimble)/Data Idenfication and unpacking/GPRMC/TimeSynch/Delay';
pt(503).paramname = 'InitialCondition';
pt(503).class     = 'scalar';
pt(503).nrows     = 1;
pt(503).ncols     = 1;
pt(503).subsource = 'SS_DOUBLE';
pt(503).ndims     = '2';
pt(503).size      = '[]';

pt(504).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(504).paramname = 'P1';
pt(504).class     = 'scalar';
pt(504).nrows     = 1;
pt(504).ncols     = 1;
pt(504).subsource = 'SS_DOUBLE';
pt(504).ndims     = '2';
pt(504).size      = '[]';

pt(505).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(505).paramname = 'P2';
pt(505).class     = 'scalar';
pt(505).nrows     = 1;
pt(505).ncols     = 1;
pt(505).subsource = 'SS_DOUBLE';
pt(505).ndims     = '2';
pt(505).size      = '[]';

pt(506).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(506).paramname = 'P3';
pt(506).class     = 'scalar';
pt(506).nrows     = 1;
pt(506).ncols     = 1;
pt(506).subsource = 'SS_DOUBLE';
pt(506).ndims     = '2';
pt(506).size      = '[]';

pt(507).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(507).paramname = 'P4';
pt(507).class     = 'scalar';
pt(507).nrows     = 1;
pt(507).ncols     = 1;
pt(507).subsource = 'SS_DOUBLE';
pt(507).ndims     = '2';
pt(507).size      = '[]';

pt(508).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(508).paramname = 'P5';
pt(508).class     = 'scalar';
pt(508).nrows     = 1;
pt(508).ncols     = 1;
pt(508).subsource = 'SS_DOUBLE';
pt(508).ndims     = '2';
pt(508).size      = '[]';

pt(509).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(509).paramname = 'P6';
pt(509).class     = 'scalar';
pt(509).nrows     = 1;
pt(509).ncols     = 1;
pt(509).subsource = 'SS_DOUBLE';
pt(509).ndims     = '2';
pt(509).size      = '[]';

pt(510).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(510).paramname = 'P7';
pt(510).class     = 'scalar';
pt(510).nrows     = 1;
pt(510).ncols     = 1;
pt(510).subsource = 'SS_DOUBLE';
pt(510).ndims     = '2';
pt(510).size      = '[]';

pt(511).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(511).paramname = 'P8';
pt(511).class     = 'scalar';
pt(511).nrows     = 1;
pt(511).ncols     = 1;
pt(511).subsource = 'SS_DOUBLE';
pt(511).ndims     = '2';
pt(511).size      = '[]';

pt(512).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(512).paramname = 'P9';
pt(512).class     = 'scalar';
pt(512).nrows     = 1;
pt(512).ncols     = 1;
pt(512).subsource = 'SS_DOUBLE';
pt(512).ndims     = '2';
pt(512).size      = '[]';

pt(513).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(513).paramname = 'P10';
pt(513).class     = 'scalar';
pt(513).nrows     = 1;
pt(513).ncols     = 1;
pt(513).subsource = 'SS_DOUBLE';
pt(513).ndims     = '2';
pt(513).size      = '[]';

pt(514).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(514).paramname = 'P11';
pt(514).class     = 'scalar';
pt(514).nrows     = 1;
pt(514).ncols     = 1;
pt(514).subsource = 'SS_DOUBLE';
pt(514).ndims     = '2';
pt(514).size      = '[]';

pt(515).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/ARP Init ';
pt(515).paramname = 'P16';
pt(515).class     = 'scalar';
pt(515).nrows     = 1;
pt(515).ncols     = 1;
pt(515).subsource = 'SS_DOUBLE';
pt(515).ndims     = '2';
pt(515).size      = '[]';

pt(516).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Buffer Mngmt ';
pt(516).paramname = 'P1';
pt(516).class     = 'vector';
pt(516).nrows     = 1;
pt(516).ncols     = 4;
pt(516).subsource = 'SS_DOUBLE';
pt(516).ndims     = '2';
pt(516).size      = '[]';

pt(517).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Buffer Mngmt ';
pt(517).paramname = 'P2';
pt(517).class     = 'scalar';
pt(517).nrows     = 1;
pt(517).ncols     = 1;
pt(517).subsource = 'SS_DOUBLE';
pt(517).ndims     = '2';
pt(517).size      = '[]';

pt(518).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Buffer Mngmt ';
pt(518).paramname = 'P3';
pt(518).class     = 'scalar';
pt(518).nrows     = 1;
pt(518).ncols     = 1;
pt(518).subsource = 'SS_DOUBLE';
pt(518).ndims     = '2';
pt(518).size      = '[]';

pt(519).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(519).paramname = 'P1';
pt(519).class     = 'scalar';
pt(519).nrows     = 1;
pt(519).ncols     = 1;
pt(519).subsource = 'SS_DOUBLE';
pt(519).ndims     = '2';
pt(519).size      = '[]';

pt(520).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(520).paramname = 'P2';
pt(520).class     = 'scalar';
pt(520).nrows     = 1;
pt(520).ncols     = 1;
pt(520).subsource = 'SS_DOUBLE';
pt(520).ndims     = '2';
pt(520).size      = '[]';

pt(521).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(521).paramname = 'P3';
pt(521).class     = 'scalar';
pt(521).nrows     = 1;
pt(521).ncols     = 1;
pt(521).subsource = 'SS_DOUBLE';
pt(521).ndims     = '2';
pt(521).size      = '[]';

pt(522).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(522).paramname = 'P4';
pt(522).class     = 'scalar';
pt(522).nrows     = 1;
pt(522).ncols     = 1;
pt(522).subsource = 'SS_DOUBLE';
pt(522).ndims     = '2';
pt(522).size      = '[]';

pt(523).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(523).paramname = 'P5';
pt(523).class     = 'scalar';
pt(523).nrows     = 1;
pt(523).ncols     = 1;
pt(523).subsource = 'SS_DOUBLE';
pt(523).ndims     = '2';
pt(523).size      = '[]';

pt(524).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(524).paramname = 'P6';
pt(524).class     = 'scalar';
pt(524).nrows     = 1;
pt(524).ncols     = 1;
pt(524).subsource = 'SS_DOUBLE';
pt(524).ndims     = '2';
pt(524).size      = '[]';

pt(525).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(525).paramname = 'P7';
pt(525).class     = 'scalar';
pt(525).nrows     = 1;
pt(525).ncols     = 1;
pt(525).subsource = 'SS_DOUBLE';
pt(525).ndims     = '2';
pt(525).size      = '[]';

pt(526).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(526).paramname = 'P8';
pt(526).class     = 'vector';
pt(526).nrows     = 1;
pt(526).ncols     = 6;
pt(526).subsource = 'SS_DOUBLE';
pt(526).ndims     = '2';
pt(526).size      = '[]';

pt(527).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(527).paramname = 'P9';
pt(527).class     = 'scalar';
pt(527).nrows     = 1;
pt(527).ncols     = 1;
pt(527).subsource = 'SS_DOUBLE';
pt(527).ndims     = '2';
pt(527).size      = '[]';

pt(528).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(528).paramname = 'P10';
pt(528).class     = 'scalar';
pt(528).nrows     = 1;
pt(528).ncols     = 1;
pt(528).subsource = 'SS_DOUBLE';
pt(528).ndims     = '2';
pt(528).size      = '[]';

pt(529).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(529).paramname = 'P11';
pt(529).class     = 'scalar';
pt(529).nrows     = 1;
pt(529).ncols     = 1;
pt(529).subsource = 'SS_DOUBLE';
pt(529).ndims     = '2';
pt(529).size      = '[]';

pt(530).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(530).paramname = 'P12';
pt(530).class     = 'scalar';
pt(530).nrows     = 1;
pt(530).ncols     = 1;
pt(530).subsource = 'SS_DOUBLE';
pt(530).ndims     = '2';
pt(530).size      = '[]';

pt(531).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(531).paramname = 'P13';
pt(531).class     = 'scalar';
pt(531).nrows     = 1;
pt(531).ncols     = 1;
pt(531).subsource = 'SS_DOUBLE';
pt(531).ndims     = '2';
pt(531).size      = '[]';

pt(532).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(532).paramname = 'P14';
pt(532).class     = 'scalar';
pt(532).nrows     = 1;
pt(532).ncols     = 1;
pt(532).subsource = 'SS_DOUBLE';
pt(532).ndims     = '2';
pt(532).size      = '[]';

pt(533).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(533).paramname = 'P15';
pt(533).class     = 'scalar';
pt(533).nrows     = 1;
pt(533).ncols     = 1;
pt(533).subsource = 'SS_DOUBLE';
pt(533).ndims     = '2';
pt(533).size      = '[]';

pt(534).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(534).paramname = 'P16';
pt(534).class     = 'scalar';
pt(534).nrows     = 1;
pt(534).ncols     = 1;
pt(534).subsource = 'SS_DOUBLE';
pt(534).ndims     = '2';
pt(534).size      = '[]';

pt(535).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(535).paramname = 'P17';
pt(535).class     = 'scalar';
pt(535).nrows     = 1;
pt(535).ncols     = 1;
pt(535).subsource = 'SS_DOUBLE';
pt(535).ndims     = '2';
pt(535).size      = '[]';

pt(536).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(536).paramname = 'P18';
pt(536).class     = 'scalar';
pt(536).nrows     = 1;
pt(536).ncols     = 1;
pt(536).subsource = 'SS_DOUBLE';
pt(536).ndims     = '2';
pt(536).size      = '[]';

pt(537).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(537).paramname = 'P20';
pt(537).class     = 'scalar';
pt(537).nrows     = 1;
pt(537).ncols     = 1;
pt(537).subsource = 'SS_DOUBLE';
pt(537).ndims     = '2';
pt(537).size      = '[]';

pt(538).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/Ethernet Init ';
pt(538).paramname = 'P21';
pt(538).class     = 'scalar';
pt(538).nrows     = 1;
pt(538).ncols     = 1;
pt(538).subsource = 'SS_DOUBLE';
pt(538).ndims     = '2';
pt(538).size      = '[]';

pt(539).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(539).paramname = 'P1';
pt(539).class     = 'scalar';
pt(539).nrows     = 1;
pt(539).ncols     = 1;
pt(539).subsource = 'SS_DOUBLE';
pt(539).ndims     = '2';
pt(539).size      = '[]';

pt(540).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(540).paramname = 'P2';
pt(540).class     = 'scalar';
pt(540).nrows     = 1;
pt(540).ncols     = 1;
pt(540).subsource = 'SS_DOUBLE';
pt(540).ndims     = '2';
pt(540).size      = '[]';

pt(541).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(541).paramname = 'P3';
pt(541).class     = 'scalar';
pt(541).nrows     = 1;
pt(541).ncols     = 1;
pt(541).subsource = 'SS_DOUBLE';
pt(541).ndims     = '2';
pt(541).size      = '[]';

pt(542).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(542).paramname = 'P4';
pt(542).class     = 'scalar';
pt(542).nrows     = 1;
pt(542).ncols     = 1;
pt(542).subsource = 'SS_DOUBLE';
pt(542).ndims     = '2';
pt(542).size      = '[]';

pt(543).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(543).paramname = 'P5';
pt(543).class     = 'scalar';
pt(543).nrows     = 1;
pt(543).ncols     = 1;
pt(543).subsource = 'SS_DOUBLE';
pt(543).ndims     = '2';
pt(543).size      = '[]';

pt(544).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(544).paramname = 'P6';
pt(544).class     = 'scalar';
pt(544).nrows     = 1;
pt(544).ncols     = 1;
pt(544).subsource = 'SS_DOUBLE';
pt(544).ndims     = '2';
pt(544).size      = '[]';

pt(545).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(545).paramname = 'P7';
pt(545).class     = 'scalar';
pt(545).nrows     = 1;
pt(545).ncols     = 1;
pt(545).subsource = 'SS_DOUBLE';
pt(545).ndims     = '2';
pt(545).size      = '[]';

pt(546).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(546).paramname = 'P8';
pt(546).class     = 'scalar';
pt(546).nrows     = 1;
pt(546).ncols     = 1;
pt(546).subsource = 'SS_DOUBLE';
pt(546).ndims     = '2';
pt(546).size      = '[]';

pt(547).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(547).paramname = 'P9';
pt(547).class     = 'scalar';
pt(547).nrows     = 1;
pt(547).ncols     = 1;
pt(547).subsource = 'SS_DOUBLE';
pt(547).ndims     = '2';
pt(547).size      = '[]';

pt(548).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(548).paramname = 'P10';
pt(548).class     = 'scalar';
pt(548).nrows     = 1;
pt(548).ncols     = 1;
pt(548).subsource = 'SS_DOUBLE';
pt(548).ndims     = '2';
pt(548).size      = '[]';

pt(549).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(549).paramname = 'P11';
pt(549).class     = 'scalar';
pt(549).nrows     = 1;
pt(549).ncols     = 1;
pt(549).subsource = 'SS_DOUBLE';
pt(549).ndims     = '2';
pt(549).size      = '[]';

pt(550).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/IP Init ';
pt(550).paramname = 'P12';
pt(550).class     = 'scalar';
pt(550).nrows     = 1;
pt(550).ncols     = 1;
pt(550).subsource = 'SS_DOUBLE';
pt(550).ndims     = '2';
pt(550).size      = '[]';

pt(551).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/UDP Init ';
pt(551).paramname = 'P1';
pt(551).class     = 'scalar';
pt(551).nrows     = 1;
pt(551).ncols     = 1;
pt(551).subsource = 'SS_DOUBLE';
pt(551).ndims     = '2';
pt(551).size      = '[]';

pt(552).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/UDP Init ';
pt(552).paramname = 'P2';
pt(552).class     = 'scalar';
pt(552).nrows     = 1;
pt(552).ncols     = 1;
pt(552).subsource = 'SS_DOUBLE';
pt(552).ndims     = '2';
pt(552).size      = '[]';

pt(553).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/UDP Init ';
pt(553).paramname = 'P3';
pt(553).class     = 'scalar';
pt(553).nrows     = 1;
pt(553).ncols     = 1;
pt(553).subsource = 'SS_DOUBLE';
pt(553).ndims     = '2';
pt(553).size      = '[]';

pt(554).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/UDP Init ';
pt(554).paramname = 'P4';
pt(554).class     = 'scalar';
pt(554).nrows     = 1;
pt(554).ncols     = 1;
pt(554).subsource = 'SS_DOUBLE';
pt(554).ndims     = '2';
pt(554).size      = '[]';

pt(555).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/Real-time UDP  Configuration 1/UDP Init ';
pt(555).paramname = 'P5';
pt(555).class     = 'scalar';
pt(555).nrows     = 1;
pt(555).ncols     = 1;
pt(555).subsource = 'SS_DOUBLE';
pt(555).ndims     = '2';
pt(555).size      = '[]';

pt(556).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/Extract ';
pt(556).paramname = 'P1';
pt(556).class     = 'scalar';
pt(556).nrows     = 1;
pt(556).ncols     = 1;
pt(556).subsource = 'SS_DOUBLE';
pt(556).ndims     = '2';
pt(556).size      = '[]';

pt(557).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/Manage ';
pt(557).paramname = 'P1';
pt(557).class     = 'scalar';
pt(557).nrows     = 1;
pt(557).ncols     = 1;
pt(557).subsource = 'SS_DOUBLE';
pt(557).ndims     = '2';
pt(557).size      = '[]';

pt(558).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/Manage ';
pt(558).paramname = 'P2';
pt(558).class     = 'scalar';
pt(558).nrows     = 1;
pt(558).ncols     = 1;
pt(558).subsource = 'SS_DOUBLE';
pt(558).ndims     = '2';
pt(558).size      = '[]';

pt(559).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/Manage ';
pt(559).paramname = 'P3';
pt(559).class     = 'scalar';
pt(559).nrows     = 1;
pt(559).ncols     = 1;
pt(559).subsource = 'SS_DOUBLE';
pt(559).ndims     = '2';
pt(559).size      = '[]';

pt(560).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Consume ';
pt(560).paramname = 'P1';
pt(560).class     = 'scalar';
pt(560).nrows     = 1;
pt(560).ncols     = 1;
pt(560).subsource = 'SS_DOUBLE';
pt(560).ndims     = '2';
pt(560).size      = '[]';

pt(561).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Consume ';
pt(561).paramname = 'P2';
pt(561).class     = 'scalar';
pt(561).nrows     = 1;
pt(561).ncols     = 1;
pt(561).subsource = 'SS_DOUBLE';
pt(561).ndims     = '2';
pt(561).size      = '[]';

pt(562).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Consume ';
pt(562).paramname = 'P3';
pt(562).class     = 'scalar';
pt(562).nrows     = 1;
pt(562).ncols     = 1;
pt(562).subsource = 'SS_DOUBLE';
pt(562).ndims     = '2';
pt(562).size      = '[]';

pt(563).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Rx ';
pt(563).paramname = 'P1';
pt(563).class     = 'scalar';
pt(563).nrows     = 1;
pt(563).ncols     = 1;
pt(563).subsource = 'SS_DOUBLE';
pt(563).ndims     = '2';
pt(563).size      = '[]';

pt(564).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Rx ';
pt(564).paramname = 'P2';
pt(564).class     = 'scalar';
pt(564).nrows     = 1;
pt(564).ncols     = 1;
pt(564).subsource = 'SS_DOUBLE';
pt(564).ndims     = '2';
pt(564).size      = '[]';

pt(565).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Rx ';
pt(565).paramname = 'P3';
pt(565).class     = 'scalar';
pt(565).nrows     = 1;
pt(565).ncols     = 1;
pt(565).subsource = 'SS_DOUBLE';
pt(565).ndims     = '2';
pt(565).size      = '[]';

pt(566).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Rx ';
pt(566).paramname = 'P4';
pt(566).class     = 'scalar';
pt(566).nrows     = 1;
pt(566).ncols     = 1;
pt(566).subsource = 'SS_DOUBLE';
pt(566).ndims     = '2';
pt(566).size      = '[]';

pt(567).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Rx ';
pt(567).paramname = 'P5';
pt(567).class     = 'scalar';
pt(567).nrows     = 1;
pt(567).ncols     = 1;
pt(567).subsource = 'SS_DOUBLE';
pt(567).ndims     = '2';
pt(567).size      = '[]';

pt(568).blockname = 'UDP IN (Cohda)1/UDPInVariants/SpeedgoatUdpOutTruckVariant/UDP_receive_speedgoat/UDP Rx ';
pt(568).paramname = 'P6';
pt(568).class     = 'scalar';
pt(568).nrows     = 1;
pt(568).ncols     = 1;
pt(568).subsource = 'SS_DOUBLE';
pt(568).ndims     = '2';
pt(568).size      = '[]';

pt(569).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(569).paramname = 'P1';
pt(569).class     = 'scalar';
pt(569).nrows     = 1;
pt(569).ncols     = 1;
pt(569).subsource = 'SS_DOUBLE';
pt(569).ndims     = '2';
pt(569).size      = '[]';

pt(570).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(570).paramname = 'P2';
pt(570).class     = 'scalar';
pt(570).nrows     = 1;
pt(570).ncols     = 1;
pt(570).subsource = 'SS_DOUBLE';
pt(570).ndims     = '2';
pt(570).size      = '[]';

pt(571).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(571).paramname = 'P3';
pt(571).class     = 'scalar';
pt(571).nrows     = 1;
pt(571).ncols     = 1;
pt(571).subsource = 'SS_DOUBLE';
pt(571).ndims     = '2';
pt(571).size      = '[]';

pt(572).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(572).paramname = 'P4';
pt(572).class     = 'scalar';
pt(572).nrows     = 1;
pt(572).ncols     = 1;
pt(572).subsource = 'SS_DOUBLE';
pt(572).ndims     = '2';
pt(572).size      = '[]';

pt(573).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(573).paramname = 'P5';
pt(573).class     = 'scalar';
pt(573).nrows     = 1;
pt(573).ncols     = 1;
pt(573).subsource = 'SS_DOUBLE';
pt(573).ndims     = '2';
pt(573).size      = '[]';

pt(574).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(574).paramname = 'P6';
pt(574).class     = 'scalar';
pt(574).nrows     = 1;
pt(574).ncols     = 1;
pt(574).subsource = 'SS_DOUBLE';
pt(574).ndims     = '2';
pt(574).size      = '[]';

pt(575).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(575).paramname = 'P7';
pt(575).class     = 'scalar';
pt(575).nrows     = 1;
pt(575).ncols     = 1;
pt(575).subsource = 'SS_DOUBLE';
pt(575).ndims     = '2';
pt(575).size      = '[]';

pt(576).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(576).paramname = 'P8';
pt(576).class     = 'scalar';
pt(576).nrows     = 1;
pt(576).ncols     = 1;
pt(576).subsource = 'SS_DOUBLE';
pt(576).ndims     = '2';
pt(576).size      = '[]';

pt(577).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(577).paramname = 'P9';
pt(577).class     = 'scalar';
pt(577).nrows     = 1;
pt(577).ncols     = 1;
pt(577).subsource = 'SS_DOUBLE';
pt(577).ndims     = '2';
pt(577).size      = '[]';

pt(578).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Produce ';
pt(578).paramname = 'P10';
pt(578).class     = 'scalar';
pt(578).nrows     = 1;
pt(578).ncols     = 1;
pt(578).subsource = 'SS_DOUBLE';
pt(578).ndims     = '2';
pt(578).size      = '[]';

pt(579).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Tx ';
pt(579).paramname = 'P1';
pt(579).class     = 'scalar';
pt(579).nrows     = 1;
pt(579).ncols     = 1;
pt(579).subsource = 'SS_DOUBLE';
pt(579).ndims     = '2';
pt(579).size      = '[]';

pt(580).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Tx ';
pt(580).paramname = 'P2';
pt(580).class     = 'scalar';
pt(580).nrows     = 1;
pt(580).ncols     = 1;
pt(580).subsource = 'SS_DOUBLE';
pt(580).ndims     = '2';
pt(580).size      = '[]';

pt(581).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Tx ';
pt(581).paramname = 'P3';
pt(581).class     = 'scalar';
pt(581).nrows     = 1;
pt(581).ncols     = 1;
pt(581).subsource = 'SS_DOUBLE';
pt(581).ndims     = '2';
pt(581).size      = '[]';

pt(582).blockname = 'UDP OUT (Cohda)1/UDPOutTruckVariant/SpeedgoatUdpOutTruckVariant/Send LAN/UDP Tx ';
pt(582).paramname = 'P4';
pt(582).class     = 'scalar';
pt(582).nrows     = 1;
pt(582).ncols     = 1;
pt(582).subsource = 'SS_DOUBLE';
pt(582).ndims     = '2';
pt(582).size      = '[]';

pt(583).blockname = '';
pt(583).paramname = 'delta_Ref_tests';
pt(583).class     = 'vector';
pt(583).nrows     = 40;
pt(583).ncols     = 1;
pt(583).subsource = 'SS_DOUBLE';
pt(583).ndims     = '2';
pt(583).size      = '[]';

function len = getlenPT
len = 583;

