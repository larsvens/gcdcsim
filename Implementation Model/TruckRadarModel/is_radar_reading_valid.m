function [ is_valid ] = is_radar_reading_valid( ...
    distance_reading, relative_speed_reading, ...
    minimum_allowed_distance, maximum_allowed_distance, ...
    minimum_allowed_relative_speed, maximum_allowed_relative_speed)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    is_valid = true;

    if distance_reading < minimum_allowed_distance
        
        is_valid = false;
        
    else    
        
        if distance_reading > maximum_allowed_distance
            
            is_valid = false;
            
        else
            
            if relative_speed_reading < minimum_allowed_relative_speed
                
                is_valid = false;
                
            else
                
                if relative_speed_reading > maximum_allowed_relative_speed
                    
                    is_valid = false;
                    
                end
                
            end
            
        end
        
    end


end

