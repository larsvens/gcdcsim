Currently the model speedgoat_udp_rui.slx is correctly exchanging UDP messages from the
Speedgoat to another PC.

The setup is as follows:

Host PC - Desktop with IP: 192.168.0.5 (no gateway defined)
Connected PC - SML Laptop with IP: 192.168.0.10 (no gateway defined)
Speedgoat - 192.168.0.7 (gateway 255.255.255.255) in definitions of SLRT (this defines the IP of the
host link)


The speedgoat_udp_rui.slx will no be renamed to speedgoat_udp_rui_stable.slx and not be changed

=======================================================================================

Currently the model blackbox_udp_rui.slx is correctly exchanging UDP messages from the
BlackBox to another PC.

The setup is as follows:

Host PC - Desktop with IP: 192.168.0.5 (no gateway defined)
Connected PC - SML Laptop with IP: 192.168.0.10 (no gateway defined)
Speedgoat - 192.168.0.8 (gateway 255.255.255.255) in definitions of SLRT (this defines the IP of the
host link)

The blackbox_udp_rui.slx will no be renamed to blackbox_udp_rui_stable.slx and not be changed

