function [ udp_section_tracked_vehicle ] = v2vtests_generate_tracked_vehicle( x_local, y_local, yaw_local, id )
%V2VTESTS_GENERATE_MIO Summary of this function goes here
%   Detailed explanation goes here

    udp_section_tracked_vehicle = uint8(zeros(4*4,1));

    udp_section_tracked_vehicle(1:4) = uint8(typecast(swapbytes(single(x_local)), 'uint8'));
    udp_section_tracked_vehicle(5:8) = uint8(typecast(swapbytes(single(y_local)), 'uint8'));
    udp_section_tracked_vehicle(9:12) = uint8(typecast(swapbytes(single(yaw_local)), 'uint8'));
    udp_section_tracked_vehicle(13:16) = uint8(typecast(swapbytes(single(id)), 'uint8'));

end

