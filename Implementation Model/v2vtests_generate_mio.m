function [ udp_section_mio ] = v2vtests_generate_mio( x_local, y_local, yaw_local )
%V2VTESTS_GENERATE_MIO Summary of this function goes here
%   Detailed explanation goes here

    udp_section_mio = uint8(zeros(4*3,1));

    udp_section_mio(1:4) = uint8(typecast(swapbytes(single(x_local)), 'uint8'));
    udp_section_mio(5:8) = uint8(typecast(swapbytes(single(y_local)), 'uint8'));
    udp_section_mio(9:12) = uint8(typecast(swapbytes(single(yaw_local)), 'uint8'));

end

