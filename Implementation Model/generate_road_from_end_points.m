function [ x_east_road_vec, y_north_road_vec ] = ...
    generate_road_from_end_points( start_lat_road, start_lon_road, ...
    end_lat_road, end_lon_road, ECEF_origin, num_points)
%GENERATE_ROAD_FROM_END_POINTS Summary of this function goes here
%   Detailed explanation goes here

start_lat_road = 59.61405; start_lon_road = 17.91959;
[x_east_start_road, y_north_start_road] = ...
    geodetic_2_ENU(start_lat_road, start_lon_road, ...    
ECEF_origin.ECEFg0, ECEF_origin.ECEFr0);

end_lat_road = 59.61405; end_lon_road = 17.91959;
[x_east_end_road, y_north_end_road] = ...
    geodetic_2_ENU(end_lat_road, end_lon_road, ...    
ECEF_origin.ECEFg0, ECEF_origin.ECEFr0);

road_vector = [x_east_end_road - x_east_start_road, ...
    y_north_end_road - y_north_start_road];

x_east_road_vec = zeros(num_points, 1);
y_north_road_vec = zeros(num_points, 1);

x_east_road_vec(1) = x_east_start_road;
y_north_road_vec(1) = y_north_start_road;

x_east_road_vec(num_points) = x_east_end_road;
y_north_road_vec(num_points) = y_north_end_road;

for it = 1:num_points-1

    x_east_road_vec(it + 1) = x_east_start_road + ...
        (it/num_points)*road_vector(1);
    y_north_road_vec(it + 1) = y_north_start_road + ...
        (it/num_points)*road_vector(2);

end

end

