import socket
import time
import struct
import sys
import random

udp_receiver_socket = []
udp_sender_socket = []

def start_udp_sender_socket():
        
    global udp_sender_socket

    udp_sender_socket = socket.socket(socket.AF_INET, # Internet
    socket.SOCK_DGRAM) # UDP
    # udp_sender_socket.setsockopt(socket.SOL_SOCKET, 25, 'eth0')
    # udp_sender_socket.bind(('192.168.1.5', 9000))
7
def start_udp_receiver_socket():
    
    UDP_IP = '' # Allows us to receive from any external PC
    UDP_PORT = 25001

    global udp_receiver_socket
    udp_receiver_socket = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
    udp_receiver_socket.bind((UDP_IP, UDP_PORT))
    udp_receiver_socket.settimeout(0.01)

    return


def close_udp_receiver_socket():

    global udp_receiver_socket
    udp_receiver_socket.close()

    return

def read_latest_packet():

	print "read_latest_packet"

	print_str =  "read_latest_packet()"

	# buffer_size = 1024
	buffer_size = 1024

	traj_id = -1

	try:

		while True:

			global udp_receiver_socket

			data, addr = udp_receiver_socket.recvfrom(buffer_size) 

			time_bytes = data[0:8]
			time_bytes = time_bytes[::-1]

			gps_latitude_bytes = data[8:16]
			gps_latitude_bytes = gps_latitude_bytes[::-1]

			gps_longitude_bytes = data[16:24]
			gps_longitude_bytes = gps_longitude_bytes[::-1]

			heading_bytes = data[24:32]
			heading_bytes = heading_bytes[::-1]

			place_holder_1_bytes = data[32:40]
			place_holder_1_bytes = place_holder_1_bytes[::-1]

			place_holder_2_bytes = data[40:48]
			place_holder_2_bytes = place_holder_2_bytes[::-1]

			traj_id_bytes = data[48:52]
			traj_id_bytes = traj_id_bytes[::-1]

			# print "HERE"
			# data = data[::-1]
			# print "Received from " + str(addr) + "data = " + str(data) + "\n"
			# print "Received from " + str(addr) + " len(data) = " + str(len(data))
			# print data r#+ "data = " + str(data)
			# buffer_test = buffer('')
			# n_bytes, addr = udp_receiver_socket.recvfrom_into(buffer_test) 
			# print "Received bytes " + str(n_bytes) + "from  = " + str(addr)
			# print "data = " + str(data)
			# print "data[::-1] = " + str(data[::-1])

			# try:
			# 	decoded_data = data.decode('utf_8')
			# 	# print "decoded_data = " + str(decoded_data)
				
			# except:
			# 	# e = sys.exc_info()[0]
			# 	# print "Error = " + str(e)
			# 	continue

			# print "UNPACKING STARTED"
			# print "struct.unpack('f', decoded_data) = " + str(struct.unpack('f', time))
			time = struct.unpack('d', time_bytes)
			time = time[0]
			gps_latitude = struct.unpack('d', gps_latitude_bytes)
			gps_latitude = gps_latitude[0]
			gps_longitude = struct.unpack('d', gps_longitude_bytes)
			gps_longitude = gps_longitude[0]
			heading = struct.unpack('d', heading_bytes)
			heading = heading[0]
			place_holder_1 = struct.unpack('d', place_holder_1_bytes)
			place_holder_1 = place_holder_1[0]
			place_holder_2 = struct.unpack('d', place_holder_2_bytes)
			place_holder_2 = place_holder_2[0]
			
			traj_id = struct.unpack('i', traj_id_bytes)
			traj_id = traj_id[0]


			print_str = "time = " + str(time) + " gps_latitude = " + str(gps_latitude)
			print_str += " gps_longitude = " + str(gps_longitude)+ " heading = " + str(heading)
			print_str += " place_holder_1 = " + str(place_holder_1) + " place_holder_2 = " + str(place_holder_2)
			print_str += " traj_id = " + str(traj_id)
			# print print_str


			# print "UNPACKING ENDED"



	except socket.timeout:

		# print print_str
		# If nothing to receive a timeout exception will 
		# be thrown, simply ignore this exception
		# print "Nothing received"
		return traj_id

	# print print_str	

	return traj_id

def send_packet(current_traj_id):

	# print "send_packet()"

	# global sent_packet_counter

	# message_string = str(sent_packet_counter)
	# message_string += ";"

	message_string = ''
	# byte_string = struct.pack('f', 4.14)
	# message_string = byte_string[::-1]
	# message_string = byte_string

	max_num_trajectory_points = 90
	num_trajectory_points = 50

	traj_id = current_traj_id

	# byte_string = struct.pack('f', 4.14)

	traj_id_bytes = struct.pack('i', traj_id)
	traj_id_bytes = traj_id_bytes[::-1]

	message_string += traj_id_bytes

	num_traj_points_bytes = struct.pack('i', num_trajectory_points)
	num_traj_points_bytes = num_traj_points_bytes[::-1]

	message_string += num_traj_points_bytes

	for i in range(num_trajectory_points):

		x_point_bytes = struct.pack('!d', float(i) + 1.0)
		message_string += x_point_bytes

		y_point_bytes = struct.pack('!d', -float(i) - 1.0)
		message_string += y_point_bytes

	remaining_traj_points = max_num_trajectory_points - num_trajectory_points

	for i in range(remaining_traj_points):

		x_point_bytes = struct.pack('!d', -99)
		message_string += x_point_bytes

		y_point_bytes = struct.pack('!d', -99)
		message_string += y_point_bytes


	# print "byte_string = " + str(byte_string)

	# global SML_WORLD_UDP_IP
	BLACK_BOX_IP = '192.168.0.25'
	UDP_PORT = 25002

	global udp_sender_socket

	udp_sender_socket.sendto(message_string, (BLACK_BOX_IP, UDP_PORT) )

	print "len(sent_data) = " + str(len(message_string))
	print "Sent data = " + message_string
	
	return



start_udp_receiver_socket()
start_udp_sender_socket()

print "Program Started"


time.sleep(2)

current_traj_id = 5
speedgoat_traj_id = -1

while True:

	# print "loop"

	if random.random() < 0.01:

		current_traj_id += 1

	speedgoat_traj_id = read_latest_packet()

	# # print "current_traj_id = " + str(current_traj_id)
	# # print "speedgoat_traj_id = " + str(speedgoat_traj_id)

	if speedgoat_traj_id != current_traj_id:

		print "Sending data"
		send_packet(current_traj_id)


	# send_packet(current_traj_id)

	# time.sleep(1.0)

'''
Code ends here
'''


close_udp_receiver_socket()