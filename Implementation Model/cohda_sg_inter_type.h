/*
 * cohda_sg_inter_type.h
 *
 *  Created on: Mar 29, 2016
 *      Author: duser
 */

#ifndef COHDA_SG_INTER_TYPE_H_
#define COHDA_SG_INTER_TYPE_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Local Macros & Constants
//------------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Type Definitions
//-----------------------------------------------------------------------------
typedef struct spdGoat_header
{
	int protocolVersion;
	int messageID;
	unsigned int MsgCounter;
}spd_header;

typedef struct spdGoat_CAM
{
	int protocolVersion;
	int messageID;
	unsigned int MsgCounter;	
	unsigned int stationID;		//removed in spd_CAM_S
	int generationDeltaTime;
	int stationType;			//removed in spd_CAM_S
	int vehicleLengthValue;		//removed in spd_CAM_S
	int vehicleLengthConfidenceIndication;	//removed in spd_CAM_S
	int vehicleWidth;			//removed in spd_CAM_S
	int latitude;
	int longitude;
	int semiMajorConfidence;
	int semiMinorConfidence;
	int semiMajorOrientation;
	// heading;
	int headingValue;
	int headingConfidence;

	int speedValue;
	int speedConfidence;
	//ITSYawRate yawRate;
	int yawRateValue;
	int yawRateConfidence;

	//ITSLongitudinalAcceleration longitudinalAcceleration;
	int longitudinalAccelerationValue;
	int longitudinalAccelerationConfidence;

	int vehicleRole;
}spd_CAM;

typedef struct spdGoat_DENM
{
	int protocolVersion;
	int messageID;
	unsigned int MsgCounter;
	unsigned int stationID;
	int stationType;

	

	//ITSCauseCode eventType;
    int causeCode;
    int subCauseCode;

	//closedLanes
	int hardShoulderStatus_option;
	int hardShoulderStatus;
	unsigned int drivingLaneStatusBuf;
	int Len;

	int lanePosition;

	int ActionID_sequenceNumber;


	//ITSReferencePosition eventPosition;
	int latitude;
	int longitude;
	
	//ITSAltitude altitude;
	int altitudeValue;
	int altitudeConfidence;

	int validityDuration;

	// Reference time
	unsigned long long int RefTime;
	unsigned long long int DecTime;

}spd_DENM;

typedef struct spdGoat_iCLCM
{
	int 			protocolVersion;
	int 			messageID;
	unsigned int    MsgCounter;
	unsigned int 	stationID;		//removed in spd_iCLCM_S
	int	 			vehicleRearAxleLocation;
	int	 			controllerType;
	int	 			targetLongitudinalAcceleration;
	int	 			timeHeadway;
	int	 			cruiseSpeed;
	int	 			vehicleResponseTimeConstant;	//
	int	 			vehicleResponseTimeDelay;
	int				participantsReady;
	int				startPlatoon;
	int				endOfScenario;
	unsigned int	mioID;
	int	 			mioRange;
	int	 			mioBearing;
	int	 			mioRangeRate;
	int	 			lane;
	unsigned int	forwardID;
	unsigned int	backwardID;
	int	 			acknowledgeFlag;
	int	 			mergeRequest;
	int	 			mergeSafeToMerge;
	int	 			mergeFlag;
	int	 			mergeFlagTail;
	int	 			mergeFlagHead;
	int	 			platoonID;
	int	 			distanceTravelledCZ;
	int	 			intention;
	int	 			counterIntersection;
}spd_iCLCM;

typedef struct spdGoat_CAM_iCLCM_Short
{
	int 			protocolVersion;
	int 			messageID;		//12
	int 			generationDeltaTime;
	int 			latitude;
	int 			longitude;
	int 			semiMajorConfidence;
	int 			semiMinorConfidence;
	int 			semiMajorOrientation;
	// heading;
	int 			headingValue;
	int 			headingConfidence;

	int 			speedValue;
	int 			speedConfidence;
	//ITSYawRate yawRate;
	int 			yawRateValue;
	int 			yawRateConfidence;

	//ITSLongitudinalAcceleration longitudinalAcceleration;
	int 			longitudinalAccelerationValue;
	int 			longitudinalAccelerationConfidence;

	int 			vehicleRole;

	///iCLCM information
	int	 			vehicleRearAxleLocation;
	int	 			controllerType;
	int	 			targetLongitudinalAcceleration;
	int	 			timeHeadway;
	int	 			cruiseSpeed;
	int	 			vehicleResponseTimeConstant;	//
	int	 			vehicleResponseTimeDelay;
	int				participantsReady;
	int				startPlatoon;
	int				endOfScenario;
	unsigned int	mioID;
	int	 			mioRange;
	int	 			mioBearing;
	int	 			mioRangeRate;
	int	 			lane;
	unsigned int	forwardID;
	unsigned int	backwardID;
	int	 			acknowledgeFlag;
	int	 			mergeRequest;
	int	 			mergeSafeToMerge;
	int	 			mergeFlag;
	int	 			mergeFlagTail;
	int	 			mergeFlagHead;
	int	 			platoonID;
	int	 			distanceTravelledCZ;
	int	 			intention;
	int	 			counterIntersection;
	long long unsigned int 	UnixTimeStamp;		
}spd_CAM_iCLCM_S;

#endif /* COHDA_SG_INTER_TYPE_H_ */
