function [ gps_latitude_road, gps_longitude_road ] = ...
    create_straight_road_from_gps_end_points( )

%CREATE_STRAIGHT_ROAD_FROM_GPS_END_POINTS Summary of this function goes here
%   Detailed explanation goes here

    
    % ITRL PARK
    start_latitude_degrees = 59.350924; start_longitude_degrees = 18.068227;
    end_latitude_degrees = 59.350789; end_longitude_degrees = 18.067782;    

    ECEFg0 = [start_latitude_degrees*(pi/180); start_longitude_degrees*(pi/180); 0];
    ECEFr0 = geodeticECEF2rectangularECEF(ECEFg0);

    [x_start_east,y_start_north] = geodetic_2_ENU(...
        start_latitude_degrees, start_longitude_degrees,...
    ECEFg0, ECEFr0);

    [x_end_east, y_end_north] = geodetic_2_ENU(...
        end_latitude_degrees, end_longitude_degrees,...
    ECEFg0, ECEFr0);

    resolution_meters = 0.1;
    
    road_length = hypot(x_end_east - x_start_east, y_end_north - y_start_north);
    road_unit_vector = [x_end_east - x_start_east; y_end_north - y_start_north]/road_length;
    
    num_points = floor(road_length/resolution_meters) + 2;
    
    x_east_road = zeros(num_points, 1);
    y_north_road = zeros(num_points, 1);
    
    x_east_road(1) = x_start_east; y_north_road(1) = y_start_north;
    x_east_road(end) = x_end_east; y_north_road(end) = y_end_north;
    
    for it = 2:num_points-1
        
        road_distance = (resolution_meters*(it-1));
        
        x_east_road(it) = x_start_east + road_distance*road_unit_vector(1);
        y_north_road(it) = y_start_north + road_distance*road_unit_vector(2);
        
    end
    
    gps_latitude_road = zeros(num_points, 1);
    gps_longitude_road = zeros(num_points, 1);   

    for it = 1:num_points
            
        ENU = [x_east_road(it); y_north_road(it); 0];
        ECEFr = ENU2rectangularECEF(ECEFg0,ECEFr0,ENU);
        ECEFg = rectangularECEF2geodeticECEF(ECEFr);
        
        gps_latitude_road(it) = ECEFg(1)*(180/pi);
        gps_longitude_road(it) = ECEFg(2)*(180/pi);
        
    end

    


end

