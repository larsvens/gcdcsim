% Initialization script for the implementation code to the Targets
clear;
% add source directories
parentDir = cd(cd('..'));
addpath(parentDir);
addpath(genpath(strcat(parentDir,'\GCDC2012-Library')))
addpath(genpath(strcat(parentDir,'\Models')))
addpath('RadarMeasurementsAnalysis')
addpath('TruckRadarModel');
% load bus definitions
load('busDefinitions');
load('busDefinitionsLDM');
load('BusDefinitionsRadar');
load('RADARCanBusDefinitions');

global GLOBAL_vehicle_1_id; GLOBAL_vehicle_1_id = 1; % TNO Car #1
global GLOBAL_vehicle_2_id; GLOBAL_vehicle_2_id = 2; % TNO Car #2
global GLOBAL_vehicle_3_id; GLOBAL_vehicle_3_id = 3; % TNO Car #3
global GLOBAL_vehicle_4_id; GLOBAL_vehicle_4_id = 100; % KTH Truck
global GLOBAL_vehicle_5_id; GLOBAL_vehicle_5_id = 101; % KTH RCV
global GLOBAL_vehicle_6_id; GLOBAL_vehicle_6_id = 110; % Chalmers Truck
global GLOBAL_vehicle_7_id; GLOBAL_vehicle_7_id = 120; % Halmstad
global GLOBAL_vehicle_8_id; GLOBAL_vehicle_8_id = 130; % Annieway
global GLOBAL_vehicle_9_id; GLOBAL_vehicle_9_id = 140; % ATeam 
global GLOBAL_vehicle_10_id; GLOBAL_vehicle_10_id = 150; % Heudiasyc
global GLOBAL_vehicle_11_id; GLOBAL_vehicle_11_id = 160; % Latvia
global GLOBAL_vehicle_12_id; GLOBAL_vehicle_12_id = 170; % Chalmers Car
global GLOBAL_vehicle_13_id; GLOBAL_vehicle_13_id = 4; % RSU ID
global GLOBAL_vehicle_14_id; GLOBAL_vehicle_14_id = 181; % Empty #2
global GLOBAL_vehicle_15_id; GLOBAL_vehicle_15_id = 99; % Empty #3


%-------------------------------------------------------------------------
% Init distances
%r = 5;                      % Stationary distance INTERSECTION
%h = 1;                      % Time Headway        INTERSECTION
r = 10;                     % Stationary distance HIGHWAY
h = 0.9;                    % Time Headway        HIGHWAY

%filterConst = 0.9;         % filter constant INTERSECTION
filterConst = 0.99;         % filter constant HIGHWAY

%relvelthresh = 10;          % relvelthresh INTERSECTION
relvelthresh = -0.2;        % relvelthresh HIGHWAY

%Lb = [-0.1 0.6 0.4];        % BMS INTERSECTION
Lb = [-0.1 0.5 0.5];        % BMS HIGHWAY
% ------------------- change above when changing scenario ----------------


% Dref = r + h*vel;       % Ref Distance
% dSafe = 0.7*velocity;   % Minimum safe distance
IntersecCrossingOffset = 7; % This is the scaled distance that the 
                            % priority vehicle will cross

% Define Variants 
Truck_variant = Simulink.Variant('vehicle_mode==0');
RCV_variant = Simulink.Variant('vehicle_mode==1');

% Choose Target for the code
vehicle_mode = 0; % 0: Build the code for the Truck
                  % 1: Build the code for the RCV
% Define Variants 
speedgoat_variant = Simulink.Variant('computer_mode==0');
blackbox_variant = Simulink.Variant('computer_mode==1');

% Choose Target for the code
computer_mode = 0; % 0: Build the code for the Speedgoat
                  % 1: Build the code for the Blackbox
                  
% Define Variants 
Log_ego_states_variant  = Simulink.Variant('Log_mode==0');
Log_judgment_variant    = Simulink.Variant('Log_mode==1');
Log_radar_variant       = Simulink.Variant('Log_mode==2');

% Choose Target for the code
Log_mode = 1; % 0: Log: Sensor Bus, Ego Estimator, GPS Bus, Override Bus
              % 1: Log for Judgment criteria
              % 2: Log Radar measyrements


% Initialize rates
Ts_Fast = 0.0005;
Ts_Mid = 1/100;
Ts_Logic = 1/10;
Ts_Sender = 1/60;
Ts_Slow = 1/10;
Ts_Controller = 1/10;
Ts_Estimator = 1/100; %1/25;
Ts_Log_Tx = 0.02;
Ts_Serial_GPS = 0.01;
Ts_MPC = 0.05;
Ts_Long_Planner = 0.01;

% StationIDs
activeStnIDs = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15];


%%% Controllers %%%
H=20;
% dt=1/20;
time_pred=1/4;

delta_Ref_tests=zeros(2*H,1);

% % CZ Road
% load Intersection_CZ_of_VeHil
% 
% WORKSPACE_lane_2_dist_to_cz = WORKSPACE_lane_2_dist_to_cz - 3.35;
% WORKSPACE_lane_3_dist_to_cz = WORKSPACE_lane_3_dist_to_cz - 3.35;

% Competition Intersection
load Intersection_CZ_Competition

WORKSPACE_lane_2_dist_to_cz = WORKSPACE_lane_2_dist_to_cz - 3.35;
WORKSPACE_lane_3_dist_to_cz = WORKSPACE_lane_3_dist_to_cz - 3.35;

load Highway_Center_Lanes

% % CZ Road
% load IntersectionRoadCoordinates
% 
% % figure; hold on; 
% % plot(longitude_road_top, latitude_road_top, ''); 
% % plot(longitude_road_top(1), latitude_road_top(1), 'x'); 
% 
% % plot(longitude_road_bottom, latitude_road_bottom);
% % plot(longitude_road_bottom(1), latitude_road_bottom(1), 'x');
% 
% intersection_center_latitude = 51.475943201;
% intersection_center_longitude =  5.62180946;
% 
% [~, min_idx_bottom] = min( hypot( latitude_road_bottom - intersection_center_latitude, longitude_road_bottom - intersection_center_longitude ) );
% 
% [~, min_idx_top] = min( hypot( latitude_road_top - intersection_center_latitude, longitude_road_top - intersection_center_longitude ) );
% 
% % plot(longitude_road_top(min_idx_top), latitude_road_top(min_idx_top), 'x');
% % plot(longitude_road_bottom(min_idx_bottom), latitude_road_bottom(min_idx_bottom), 'x');
% 
% [x_road_top,y_road_top] = deg2utm(latitude_road_top,longitude_road_top);
% [x_road_bottom,y_road_bottom] = deg2utm(latitude_road_bottom,longitude_road_bottom);
% 
% cz_radius = 50;
% 
% distances_top = [0; hypot( diff(x_road_top), diff(y_road_top) )];
% distances_top = cumsum(distances_top);
% distances_top = distances_top - distances_top(min_idx_top);
% distances_top = distances_top + cz_radius;
% 
% % figure; hold on;
% % plot(distances_top);
% % plot(min_idx_top, distances_top(min_idx_top), 'x')
% 
% 
% distances_bottom = [0; hypot( diff(x_road_bottom), diff(y_road_bottom) )];
% distances_bottom = cumsum(distances_bottom);
% distances_bottom = distances_bottom - distances_bottom(min_idx_bottom);
% distances_bottom = distances_bottom + cz_radius;
% 
% 
% 
% WORKSPACE_longitudes_lane_2 = longitude_road_top;
% WORKSPACE_latitudes_lane_2 = latitude_road_top;
% WORKSPACE_lane_2_dist_to_cz = distances_top;
% 
% WORKSPACE_longitudes_lane_3 = longitude_road_bottom;
% WORKSPACE_latitudes_lane_3 = latitude_road_bottom;
% WORKSPACE_lane_3_dist_to_cz = distances_bottom;
% 
% % figure; hold on;
% % plot(distances_bottom);
% % plot(min_idx_bottom, distances_bottom(min_idx_bottom), 'x')


%% Generate Target Computer Plot
% tg = slrt;
% tg.viewTargetScreen;
% 
% displayEndOfDemoMessage(mfilename)

%% CPU diagnostics
% profileInfo.modelname = 'scoope_v3';
% profData = profile_xpc(profileInfo);
