% raw: blue (first)
%estimated: red (second)

close all;
fig = figure;
fig.Name = '    RUN_1';

% vx
subplot(2,3,1)
hold on;
plot(Results.VehRaw.data(:,1),'b');
plot(Results.GPSRaw.data(:,9),'y');
plot(Results.EgoEstim.data(:,3),'r');
title('vx')

% ax 
subplot(2,3,2)
hold on;
plot(Results.VehRaw.data(:,3),'b');
plot(Results.EgoEstim.data(:,4),'r');
title ('ax')

% heading
subplot(2,3,3)
hold on;
plot(Results.GPSRaw.data(:,7),'b');
plot(90-Results.EgoEstim.data(:,5),'r');
title('heading');

% yawrate
subplot(2,3,4)
hold on;
plot(Results.VehRaw.data(:,9),'b');
plot(Results.EgoEstim.data(:,7),'r');
title('yawrate')

% lon
subplot(2,3,6)
hold on;
plot(Results.GPSRaw.data(:,3),'b');
plot(Results.EgoEstim.data(:,2),'r');
title('lon')

% GPS
subplot(2,3,5)
hold on;
plot(Results.GPSRaw.data(:,3),Results.GPSRaw.data(:,1),'b');
plot(Results.EgoEstim.data(:,1),Results.EgoEstim.data(:,2),'r');
title('GPS')


hold off;