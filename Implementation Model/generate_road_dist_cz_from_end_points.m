function [ x_utm_road, y_utm_road, cz_dist_road ] = ...
    generate_road_dist_cz_from_end_points( start_lat_road, start_lon_road, ...
    end_lat_road, end_lon_road, road_resolution)
%GENERATE_ROAD_FROM_END_POINTS Summary of this function goes here
%   Detailed explanation goes here

[x_utm_start, y_utm_start] = deg2utm(start_lat_road, start_lon_road);
[x_utm_end, y_utm_end] = deg2utm(end_lat_road, end_lon_road);

road_vector = [x_utm_end - x_utm_start, ...
    y_utm_end - y_utm_start];

road_length = hypot(road_vector(1), road_vector(2));

num_points = ceil(road_length/road_resolution) + 1;

x_utm_road(1) = x_utm_start;
y_utm_road(1) = y_utm_start;

for it = 1:num_points - 1
    
   x_utm_road(1 + it) = x_utm_start + (it/(num_points-1))*road_vector(1);
   y_utm_road(1 + it) = y_utm_start + (it/(num_points-1))*road_vector(2);
    
end

% x_utm_road(end) = x_utm_end;
% y_utm_road(end) = y_utm_end;

% yaw_road = 0;

x_utm_road = x_utm_road';
y_utm_road = y_utm_road';

cz_dist_road = cumsum( hypot( diff(x_utm_road) , diff(y_utm_road) ) );
cz_dist_road = [0; cz_dist_road];

cz_dist_road = flipud(cz_dist_road);

end

