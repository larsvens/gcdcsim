% Initialization script for the implementation code to the Targets
clear;
% add source directories
parentDir = cd(cd('..'));
addpath(parentDir);
addpath(genpath(strcat(parentDir,'\GCDC2012-Library')))
addpath(genpath(strcat(parentDir,'\Models')))
addpath('RadarMeasurementsAnalysis')
addpath('TruckRadarModel');
% load bus definitions
load('busDefinitions');
load('busDefinitionsLDM');
load('BusDefinitionsRadar');

global GLOBAL_vehicle_1_id
global GLOBAL_vehicle_2_id
global GLOBAL_vehicle_3_id
global GLOBAL_vehicle_4_id
global GLOBAL_vehicle_5_id

GLOBAL_vehicle_1_id = 110;
GLOBAL_vehicle_2_id = 100;
GLOBAL_vehicle_3_id = 170;
GLOBAL_vehicle_4_id = 120;
GLOBAL_vehicle_5_id = 97;


% Define Variants 
Truck_variant = Simulink.Variant('vehicle_mode==0');
RCV_variant = Simulink.Variant('vehicle_mode==1');

% Choose Target for the code
vehicle_mode = 1; % 0: Build the code for the Truck
                  % 1: Build the code for the RCV
% Define Variants 
speedgoat_variant = Simulink.Variant('computer_mode==0');
blackbox_variant = Simulink.Variant('computer_mode==1');

% Choose Target for the code
computer_mode = 0; % 0: Build the code for the Speedgoat
                  % 1: Build the code for the Blackbox
                  
% Define Variants 
Log_ego_states_variant  = Simulink.Variant('Log_mode==0');
Log_judgment_variant    = Simulink.Variant('Log_mode==1');
Log_radar_variant       = Simulink.Variant('Log_mode==2');

% Choose Target for the code
Log_mode = 1; % 0: Log: Sensor Bus, Ego Estimator, GPS Bus, Override Bus
              % 1: Log for Judgment criteria
              % 2: Log Radar measyrements


% Initialize rates
Ts_Fast = 0.0001;
Ts_Mid = 1/100;
Ts_Logic = 1/10;
Ts_Sender = 1/60;
Ts_Slow = 1/10;
Ts_Controller = 1/10;
Ts_Estimator = 1/100; %1/25;
Ts_Log_Tx = 0.02;
Ts_MPC = 0.05;

% StationIDs
activeStnIDs = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15];


%%% Controllers %%%
H=20;
% dt=1/20;
time_pred=1/4;

delta_Ref_tests=zeros(2*H,1);

%% Generate Target Computer Plot
% tg = slrt;
% tg.viewTargetScreen;
% 
% displayEndOfDemoMessage(mfilename)

%% CPU diagnostics
% profileInfo.modelname = 'scoope_v3';
% profData = profile_xpc(profileInfo);
