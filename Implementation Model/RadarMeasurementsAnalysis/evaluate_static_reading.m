function [ best_static_reading ] = evaluate_static_reading( current_static_reading, best_static_reading )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% best_static_reading comes as [long_dist; relative_long_speed; relative_long_accel]

    best_long_dist = best_static_reading(1);
    
    if (current_static_reading.LongDistance < 50 && ...
         abs(current_static_reading.LateralDistance) < 5)
        % Reading is in interest region

        if current_static_reading.LongDistance < best_long_dist

            best_static_reading = [current_static_reading.LongDistance; 0; 0];

        end

    end
    
end

