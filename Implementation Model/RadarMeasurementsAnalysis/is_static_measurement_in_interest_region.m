function [ in_interest_region ] = is_static_measurement_in_interest_region( ...
    static_measurement )
%IS_IN_INTEREST_REGION Summary of this function goes here
%   Detailed explanation goes here

    long_dist = static_measurement(3);
    lat_dist = static_measurement(4);

    in_interest_region = is_in_interest_region( ...
    long_dist, lat_dist );
        
end

