function [ fwd_mio_confidence_vector, fwd_left_mio_confidence_vector, ...
    fwd_right_mio_confidence_vector] = update_static_measurement( static_measurement, ...
    fwd_mio_confidence_vector, fwd_left_mio_confidence_vector, fwd_right_mio_confidence_vector, ...
    confidence_increase)
%UPDATE_DYNAMIC_MEASUREMENT Summary of this function goes here
%   Detailed explanation goes here

if is_valid_static_measurement(static_measurement)
%     if is_static_measurement_in_interest_region(static_measurement)  
        
    long_dist = static_measurement(4);
    lat_dist = static_measurement(3);
    tracked_id = static_measurement(6);

    [ fwd_mio_confidence_vector, fwd_left_mio_confidence_vector, fwd_right_mio_confidence_vector] = ...
    update_MIO_confidence( lat_dist, long_dist, fwd_mio_confidence_vector, ...
    fwd_left_mio_confidence_vector, fwd_right_mio_confidence_vector, tracked_id, confidence_increase);
        

%     end
end



end

