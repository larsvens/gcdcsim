function [ fwd_mio_confidence_vector, fwd_left_mio_confidence_vector, ...
    fwd_right_mio_confidence_vector] = update_dynamic_measurement( dynamic_measurement, ...
    fwd_mio_confidence_vector, fwd_left_mio_confidence_vector, fwd_right_mio_confidence_vector, ...
    confidence_increase)
%UPDATE_DYNAMIC_MEASUREMENT Summary of this function goes here
%   Detailed explanation goes here

if is_valid_dynamic_measurement(dynamic_measurement)
%     if is_dynamic_measurement_in_interest_region(dynamic_measurement)  
        
    long_dist = dynamic_measurement(3);
    lat_dist = dynamic_measurement(2);
    tracked_id = dynamic_measurement(12);

    [ fwd_mio_confidence_vector, fwd_left_mio_confidence_vector, fwd_right_mio_confidence_vector] = ...
    update_MIO_confidence( lat_dist, long_dist, fwd_mio_confidence_vector, ...
    fwd_left_mio_confidence_vector, fwd_right_mio_confidence_vector, tracked_id, confidence_increase);

%     end
end



end

