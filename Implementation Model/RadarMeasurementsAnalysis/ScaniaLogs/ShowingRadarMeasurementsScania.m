load Run_S_20151102_1604
% load BackAndForthCarsOnSidesGon

if exist('h_waitbar')
    if h_waitbar.isvalid
        close(h_waitbar)
    end
end

clearvars -except truck
clc, close all


fieldnames(truck)

'RTDB_RT1_VREL_E'
'RTDB_RT1_DISTANCE_E'
'RTDB_RT1_LATDISTANCE_E'
'RTDB_RT1_LATVREL_E'
'RTDB_RT1_ID_E'

total_time = truck.endtime - truck.starttime;
time_step = 0.1;
current_time = 0;

% return
 
%  static_object_1 = Results.RAD_RTS1.data(:, 1:6);
%  static_object_2 = Results.RAD_RTS1.data(:, 7:12);
%  static_object_3 = Results.RAD_RTS1.data(:, 13:18);
%  static_object_4 = Results.RAD_RTS1.data(:, 19:24);
%  static_object_5 = Results.RAD_RTS1.data(:, 25:30);
%  static_object_6 = Results.RAD_RTS1.data(:, 31:36);
 
%  time_vector = Results.RAD_RT1.data(:, 11);
%  time_step = mean(diff(time_vector));
 
%  dynamic_object_1 = Results.RAD_RT1.data;
%  dynamic_object_2 = Results.RAD_RT2.data;
%  dynamic_object_3 = Results.RAD_RT3.data;
%  dynamic_object_4 = Results.RAD_RT4.data;
%  dynamic_object_5 = Results.RAD_RT5.data;
%  dynamic_object_6 = Results.RAD_RT6.data;
 
 figure; hold on;
 axis([-20 20 0 100])
%  axis([-10 10 0 30])
 
 
h_d_1 = quiver(0, 0, 0, 0);
h_d_2 = quiver(0, 0, 0, 0);
h_d_3 = quiver(0, 0, 0, 0);
h_d_4 = quiver(0, 0, 0, 0);
h_d_5 = quiver(0, 0, 0, 0);
h_d_6 = quiver(0, 0, 0, 0);
 
h_waitbar = waitbar(0,'Progression');
 
quiver(0, 0 , 0 , 1)

time_step = 0.1;
time_tolerance = 2*time_step;

 while current_time < total_time
     

%      truck.RTDB_RT1_VREL_E
     
              
    plot_dynamic_measure_scania(truck.RTDB_RT1_DISTANCE_E, ...
        truck.RTDB_RT1_LATDISTANCE_E,...
        truck.RTDB_RT1_VREL_E,...
        truck.RTDB_RT1_LATVREL_E,...
        current_time, time_tolerance, h_d_1);
    
    plot_dynamic_measure_scania(truck.RTDB_RT2_DISTANCE_E, ...
        truck.RTDB_RT2_LATDISTANCE_E,...
        truck.RTDB_RT2_VREL_E,...
        truck.RTDB_RT2_LATVREL_E,...
        current_time, time_tolerance, h_d_2);
    
    plot_dynamic_measure_scania(truck.RTDB_RT3_DISTANCE_E, ...
        truck.RTDB_RT3_LATDISTANCE_E,...
        truck.RTDB_RT3_VREL_E,...
        truck.RTDB_RT3_LATVREL_E,...
        current_time, time_tolerance, h_d_3);
    
    plot_dynamic_measure_scania(truck.RTDB_RT4_DISTANCE_E, ...
        truck.RTDB_RT4_LATDISTANCE_E,...
        truck.RTDB_RT4_VREL_E,...
        truck.RTDB_RT4_LATVREL_E,...
        current_time, time_tolerance, h_d_4);
    
    plot_dynamic_measure_scania(truck.RTDB_RT5_DISTANCE_E, ...
        truck.RTDB_RT5_LATDISTANCE_E,...
        truck.RTDB_RT5_VREL_E,...
        truck.RTDB_RT5_LATVREL_E,...
        current_time, time_tolerance, h_d_5);
    
    plot_dynamic_measure_scania(truck.RTDB_RT6_DISTANCE_E, ...
        truck.RTDB_RT6_LATDISTANCE_E,...
        truck.RTDB_RT6_VREL_E,...
        truck.RTDB_RT6_LATVREL_E,...
        current_time, time_tolerance, h_d_6);
    
    
    waitbar(current_time/total_time, h_waitbar)
 

    
    current_time = current_time + time_step;

%     pause(time_step)
    drawnow
         
 end