function [] = plot_dynamic_measure_scania( DISTANCE, LATDISTANCE, VREL, LATVREL, current_time, time_tolerance, plot_handle )
%PLOT_STATIC_MEASURE Summary of this function goes here
%   Detailed explanation goes here

    [delta_t_dist, closest_idx_dist] = min(abs(DISTANCE.time - current_time));
    [delta_t_lat_dist, closest_idx_lat_dist] = min(abs(LATDISTANCE.time - current_time));
    [delta_t_vrel, closest_idx_vrel] = min(abs(VREL.time - current_time));
    [delta_t_lat_vrel, closest_idx_lat_vrel] = min(abs(LATVREL.time - current_time));
    
    if delta_t_dist == delta_t_lat_dist && delta_t_dist < time_tolerance
    
        long_dist = DISTANCE.value(closest_idx_dist);
        lat_dist = LATDISTANCE.value(closest_idx_lat_dist);
        long_speed = VREL.value(closest_idx_vrel);
        lat_speed = LATVREL.value(closest_idx_lat_vrel);
        
    else
        
        long_dist = [];
        lat_dist = [];
        long_speed = [];
        lat_speed = [];
        
    end
     
    set(plot_handle, 'XData', lat_dist)
    set(plot_handle, 'YData', long_dist)
    set(plot_handle, 'UData', lat_speed)
    set(plot_handle, 'VData', long_speed)


end

