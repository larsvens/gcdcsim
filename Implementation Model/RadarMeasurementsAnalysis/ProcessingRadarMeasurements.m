clear
clear radar_processing

iteration_start = 1;
% load LarsFrontAndBackTwizzyWeStopped
% load LarsFrontWeFollowUntilRoad
% load LarsFrontWeFollowPastRoad; % iteration_start = 117; iteration_start = 1; iteration_start = 800;
% load LarsFrontWeFollowFast; iteration_start = 350; iteration_start = 1;
% load BackAndForthCarsOnSidesGon
% load ArlandaFirstRunBehindTruck % iteration_start = 1300;
% load ArlandaSecondRunBehindTruckStephanosZigZagging
% load ArlandaThirdRunBehindTruckStephanos30and50
load ITRLWithTwizzyTestingProcessing; iteration_start = 2000;

if exist('h_waitbar')
    if h_waitbar.isvalid
        close(h_waitbar)
    end
end

clearvars -except Results iteration_start
clc, close all
 
 static_object_1 = Results.RAD_RTS1.data(:, 1:6);
 static_object_2 = Results.RAD_RTS1.data(:, 7:12);
 static_object_3 = Results.RAD_RTS1.data(:, 13:18);
 static_object_4 = Results.RAD_RTS1.data(:, 19:24);
 static_object_5 = Results.RAD_RTS1.data(:, 25:30);
 static_object_6 = Results.RAD_RTS1.data(:, 31:36);
 
 time_vector = Results.RAD_RT1.data(:, 11);
 time_step = mean(diff(time_vector));
 total_time = time_vector(end) - time_vector(1);
 disp(['total_time = ', num2str(total_time)])
 
 dynamic_object_1 = Results.RAD_RT1.data(:, 2:end);
 dynamic_object_2 = Results.RAD_RT2.data;
 dynamic_object_3 = Results.RAD_RT3.data;
 dynamic_object_4 = Results.RAD_RT4.data;
 dynamic_object_5 = Results.RAD_RT5.data;
 dynamic_object_6 = Results.RAD_RT6.data;
%  dynamic_object_4 = Results.RAD_RT46.data(:, 1:9);
%  dynamic_object_5 = Results.RAD_RT46.data(:, 10:18);
%  dynamic_object_6 = Results.RAD_RT46.data(:, 19:27);
 
 figure; hold on;
 
 car_length = 2.5; car_width = 1.5;
 fill(car_width/2*[-1 -1 1 1], car_length*[0 -1 -1 0], 'r')
 
 min_x = -5;
 max_x = 5;
 min_y = -5;
 max_y = 30;
%  axis([-50 50 0 100])
 axis([min_x max_x min_y max_y])
 
 for i = min_x:(max_x-min_x)/20:max_x
     
     for j = min_y:(max_y-min_y)/20:max_y
         
         if is_in_interest_region(j, i)
            
             plot(i, j, 'k.')
             
         end
         
     end
         
 end
 
 h_s_1 = plot(static_object_1(1, 4), static_object_1(1, 3), 'ro');
 h_t_s_1 = text(static_object_1(1, 2), static_object_1(1, 3), ...
    'text');
 h_s_2 = plot(static_object_2(1, 4), static_object_2(1, 3), 'ro');
 h_t_s_2 = text(static_object_1(1, 2), static_object_1(1, 3), ...
    'text');
 h_s_3 = plot(static_object_3(1, 4), static_object_3(1, 3), 'ro');
 h_t_s_3 = text(static_object_1(1, 2), static_object_1(1, 3), ...
    'text');
 h_s_4 = plot(static_object_4(1, 4), static_object_4(1, 3), 'ro');
 h_t_s_4 = text(static_object_1(1, 2), static_object_1(1, 3), ...
    'text');
 h_s_5 = plot(static_object_5(1, 4), static_object_5(1, 3), 'ro');
 h_t_s_5 = text(static_object_1(1, 2), static_object_1(1, 3), ...
    'text');
 h_s_6 = plot(static_object_6(1, 4), static_object_6(1, 3), 'ro');
 h_t_s_6 = text(static_object_1(1, 2), static_object_1(1, 3), ...
    'text');
 
h_dist = plot([-50, 50], [0, 0], 'b--');
% h_fill_dist = fill([-50 -50 50 50], [0 -1 -1 0], 'r');

h_d_1 = quiver(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    dynamic_object_1(1, 4), dynamic_object_1(1, 5));
h_t_d_1 = text(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    'text');
h_d_2 = quiver(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    dynamic_object_1(1, 4), dynamic_object_1(1, 5));
h_t_d_2 = text(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    'text');
h_d_3 = quiver(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    dynamic_object_1(1, 4), dynamic_object_1(1, 5));
h_t_d_3 = text(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    'text');
h_d_4 = quiver(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    dynamic_object_1(1, 4), dynamic_object_1(1, 5));
h_t_d_4 = text(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    'text');
h_d_5 = quiver(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    dynamic_object_1(1, 4), dynamic_object_1(1, 5));
h_t_d_5 = text(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    'text');
h_d_6 = quiver(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    dynamic_object_1(1, 4), dynamic_object_1(1, 5));
h_t_d_6 = text(dynamic_object_1(1, 2), dynamic_object_1(1, 3), ...
    'text');
 
h_waitbar = waitbar(0,'Progression');
 
data_length = size(static_object_1, 1);

quiver(0, 0 , 0 , 1)

start_time = tic;



 for iteration = iteration_start:1:data_length
     
%      iteration
    current_time = time_vector(iteration);
     
    dynamic_1 = dynamic_object_1(iteration, :);  dynamic_2 = dynamic_object_2(iteration, :);
    dynamic_3 = dynamic_object_3(iteration, :);  dynamic_4 = dynamic_object_4(iteration, :);
    dynamic_5 = dynamic_object_5(iteration, :);  dynamic_6 = dynamic_object_6(iteration, :);
    static_1 = static_object_1(iteration, :);  static_2 = static_object_2(iteration, :);
    static_3 = static_object_3(iteration, :);  static_4 = static_object_4(iteration, :);
    static_5 = static_object_5(iteration, :);  static_6 = static_object_6(iteration, :);
     
    [distance_to_front, tracked_id, continuous_valid_readings, confidence] ...
    = radar_processing(current_time, dynamic_1, dynamic_2, dynamic_3, ...
    dynamic_4, dynamic_5, dynamic_6, ...
    static_1, static_2, static_3, ...
    static_4, static_5, static_6);

    h_dist.YData = distance_to_front*[1, 1];
%     disp(continuous_valid_readings)
%     disp(tracked_id)
%     confidence = 0.5;
%     h_fill_dist.Vertices(:, 2) = [distance_to_front - (1-confidence); distance_to_front + (1-confidence);...
%         distance_to_front + (1-confidence); distance_to_front - (1-confidence)];
%     h_fill_dist.FaceAlpha = confidence;

     
    plot_static_measure(static_object_1, iteration, 1, h_s_1, h_t_s_1);
    plot_static_measure(static_object_2, iteration, 2, h_s_2, h_t_s_2);
    plot_static_measure(static_object_3, iteration, 3, h_s_3, h_t_s_3);
    plot_static_measure(static_object_4, iteration, 4, h_s_4, h_t_s_4);
    plot_static_measure(static_object_5, iteration, 5, h_s_5, h_t_s_5);
    plot_static_measure(static_object_6, iteration, 6, h_s_6, h_t_s_6);
        
    plot_dynamic_measure(dynamic_object_1, iteration, 1, h_d_1, h_t_d_1);
    plot_dynamic_measure(dynamic_object_2, iteration, 2, h_d_2, h_t_d_2);
    plot_dynamic_measure(dynamic_object_3, iteration, 3, h_d_3, h_t_d_3);
    plot_dynamic_measure(dynamic_object_4, iteration, 4, h_d_4, h_t_d_4);
    plot_dynamic_measure(dynamic_object_5, iteration, 5, h_d_5, h_t_d_5);
    plot_dynamic_measure(dynamic_object_6, iteration, 6, h_d_6, h_t_d_6);
    
    waitbar(iteration/data_length, h_waitbar)
    
%     desired_current_time = iteration*time_step;
%     current_time = toc(start_time);
%     
%     if ( current_time > desired_current_time)
%         disp(['Late by ', num2str(desired_current_time - current_time)])
%     else
%         pause(desired_current_time - current_time)
%     end
    
    drawnow
    
     
 end