function [ radar_reading_bytes ] = create_static_radar_reading_bytes( static_radar_reading )
%CREATE_STATIC_RADAR_READING_BYTES Summary of this function goes here
%   Detailed explanation goes here

%     static_radar_reading.Lane;
%     static_radar_reading.LaneConfidence;
%     static_radar_reading.LateralDistance;
%     static_radar_reading.LongDistance;
%     static_radar_reading.MsgCounter;
%     static_radar_reading.TrackIdNumber;
%     static_radar_reading.TrackStatus;
%     static_radar_reading.Type;
           
    radar_reading_bytes = uint8(zeros(64,1));

    % Double precision
    radar_reading_bytes(1:8) = uint8(typecast(swapbytes(double(static_radar_reading.Lane)), 'uint8'));
    radar_reading_bytes(9:16) = uint8(typecast(swapbytes(double(static_radar_reading.LaneConfidence)), 'uint8'));
    radar_reading_bytes(17:24) = uint8(typecast(swapbytes(double(static_radar_reading.LateralDistance)), 'uint8'));    
    radar_reading_bytes(25:32) = uint8(typecast(swapbytes(double(static_radar_reading.LongDistance)), 'uint8'));
    radar_reading_bytes(33:40) = uint8(typecast(swapbytes(double(static_radar_reading.MsgCounter)), 'uint8'));    
    radar_reading_bytes(41:48) = uint8(typecast(swapbytes(double(static_radar_reading.TrackIdNumber)), 'uint8'));
    radar_reading_bytes(49:56) = uint8(typecast(swapbytes(double(static_radar_reading.TrackStatus)), 'uint8'));   
    radar_reading_bytes(57:64) = uint8(typecast(swapbytes(double(static_radar_reading.Type)), 'uint8'));     
    
end

