function [distance_to_front, tracked_id, continuous_valid_readings, confidence] ...
    = radar_processing_2(time, dynamic_1, dynamic_2, dynamic_3, ...
    dynamic_4, dynamic_5, dynamic_6, ...
    static_1, static_2, static_3, ...
    static_4, static_5, static_6)
%RADAR_PROCESSING Summary of this function goes here
%   Detailed explanation goes here

persistent FWD_MIO_CONFIDENCE
persistent FWD_LEFT_MIO_CONFIDENCE
persistent FWD_RIGHT_MIO_CONFIDENCE

persistent ITERATIONS
persistent DISTANCE_TO_FRONT
persistent LAST_TRACKED_ID
persistent CONTINUOUS_VALID_READINGS
persistent LOCKED_ID
persistent INDEX_CONFIDENCE
persistent INDEX_DISTANCE_TO_FRONT
persistent PREVIOUS_TIME

if sum(static_1) == 0
    distance_to_front = -1;
    tracked_id = -1;
    continuous_valid_readings = -1;
    confidence = 0;
    PREVIOUS_TIME = time;
    return;
end

if isempty(FWD_MIO_CONFIDENCE)
    FWD_MIO_CONFIDENCE = zeros(256, 1);
end
if isempty(FWD_LEFT_MIO_CONFIDENCE)
    FWD_LEFT_MIO_CONFIDENCE = zeros(256, 1);
end
if isempty(FWD_RIGHT_MIO_CONFIDENCE)
    FWD_RIGHT_MIO_CONFIDENCE = zeros(256, 1);
end

if isempty(PREVIOUS_TIME)
    PREVIOUS_TIME = time;    
end

if isempty(ITERATIONS)
    ITERATIONS = 0;
end
ITERATIONS = ITERATIONS + 1;

if isempty(DISTANCE_TO_FRONT)
   DISTANCE_TO_FRONT = -1;
end

if isempty(LAST_TRACKED_ID)
   LAST_TRACKED_ID = -1;
end

if isempty(LOCKED_ID)
   LOCKED_ID = -1;
end

if isempty(CONTINUOUS_VALID_READINGS)
   CONTINUOUS_VALID_READINGS = 0;
end

if isempty(INDEX_CONFIDENCE)
    INDEX_CONFIDENCE = zeros(256, 1);
end

if isempty(INDEX_DISTANCE_TO_FRONT)
    INDEX_DISTANCE_TO_FRONT = zeros(256, 1);
end

distance_to_front = -1;
tracked_id = -1;

delta_time = time - PREVIOUS_TIME;
confidence_increase = delta_time;
confidence_decrease_magnitude = confidence_increase/2;

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE ] = ...
    update_dynamic_measurement( dynamic_1, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE ] = ...
    update_dynamic_measurement( dynamic_2, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE ] = ...
    update_dynamic_measurement( dynamic_3, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE ] = ...
    update_dynamic_measurement( dynamic_4, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE ] = ...
    update_dynamic_measurement( dynamic_5, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE ] = ...
    update_dynamic_measurement( dynamic_6, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);


[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE] = ...
    update_static_measurement( static_1, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE] = ...
    update_static_measurement( static_2, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE] = ...
    update_static_measurement( static_3, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE] = ...
    update_static_measurement( static_4, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE] = ...
    update_static_measurement( static_5, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);

[ FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE] = ...
    update_static_measurement( static_6, ...
    FWD_MIO_CONFIDENCE, FWD_LEFT_MIO_CONFIDENCE, FWD_RIGHT_MIO_CONFIDENCE, ...
    confidence_increase);


disp(max(FWD_MIO_CONFIDENCE))
disp(max(FWD_LEFT_MIO_CONFIDENCE))
disp(max(FWD_RIGHT_MIO_CONFIDENCE))

if LOCKED_ID ~= -1
   
    if distance_to_front ~= -1 && LOCKED_ID == tracked_id
   
        DISTANCE_TO_FRONT = distance_to_front;

    end
    
end

% tracked_id
% LAST_TRACKED_ID

if tracked_id ~= -1 % If there is an object to track
    
    if LAST_TRACKED_ID ~= -1 % If there was an object being tracked before
       
        if LAST_TRACKED_ID == tracked_id % If currently tracked is the same
            % as the previously tracked     
            
            CONTINUOUS_VALID_READINGS = CONTINUOUS_VALID_READINGS + 1;
            
            if CONTINUOUS_VALID_READINGS > 20
                
                LOCKED_ID = tracked_id;
                
            end
            
        else
            % If we are tracking a different object than the previous one
            
            if CONTINUOUS_VALID_READINGS > 0
                
                CONTINUOUS_VALID_READINGS = 0;
                
            else
                
                CONTINUOUS_VALID_READINGS = CONTINUOUS_VALID_READINGS - 1;
                                
            end
                           
        end
        
    else % If there was no object being tracked before
        
        LAST_TRACKED_ID = tracked_id;
        CONTINUOUS_VALID_READINGS = 1;
        
    end
    
else % If there is no object to track

    if LAST_TRACKED_ID ~= -1 % If there was no object to track before
       
        tracked_id = LAST_TRACKED_ID;
      
    end
    
end

% INDEX_CONFIDENCE(tracked_id)

[value, index] = max(INDEX_CONFIDENCE);

[values, indexes] = sort(-INDEX_CONFIDENCE);

temp_i = 1;
min_dist = 10000;
% disp('========')
% disp(sum(INDEX_CONFIDENCE==1))
while 1
%     values(temp_i+1)
%     values(temp_i)
    
    if values(temp_i) == 0
        break;
    end

    if (values(temp_i)) ~= values(1)
       break; 
    end
    
%     values(temp_i+1)
    
%     values(temp_i+1)
%     values(temp_i)
    
    if INDEX_DISTANCE_TO_FRONT(indexes(temp_i)) < min_dist
        index = indexes(temp_i);
        min_dist = INDEX_DISTANCE_TO_FRONT(indexes(temp_i));
        
    end
    temp_i = temp_i + 1;
    
end

continuous_valid_readings = -1;
confidence = value;
if INDEX_DISTANCE_TO_FRONT(index) ~= -1
    
    if value ~= 0
    
        distance_to_front = INDEX_DISTANCE_TO_FRONT(index);
        continuous_valid_readings = index;
        disp(index)
        
    end

end




% [value, index]

INDEX_CONFIDENCE = INDEX_CONFIDENCE - confidence_decrease_magnitude*ones(256, 1);
INDEX_CONFIDENCE = max(0, INDEX_CONFIDENCE);


% LAST_TRACKED_ID

% distance_to_front = DISTANCE_TO_FRONT;
% tracked_id = LAST_TRACKED_ID;
% continuous_valid_readings = CONTINUOUS_VALID_READINGS;


% INDEX_DISTANCE_TO_FRONT'

PREVIOUS_TIME = time;   

end

