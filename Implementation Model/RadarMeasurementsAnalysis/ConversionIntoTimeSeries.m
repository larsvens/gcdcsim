clear
clc

% load LarsFrontAndBackTwizzyWeStopped
% load LarsFrontWeFollowUntilRoad
% load LarsFrontWeFollowPastRoad
load LarsFrontWeFollowFast
% load ITRLWithTwizzyTestingProcessing
% load ITRLGoncaloUTurnTestingProcessing

clearvars -except Results

load BusDefinitionsRadar

RAD_RT1 = timeseries;
RAD_RT1.Data = Results.RAD_RT1.data(:, 2:10);
RAD_RT1.Time = Results.RAD_RT1.data(:, 11);

save RAD_RT1 -v7.3 RAD_RT1

RAD_RT2 = timeseries;
RAD_RT2.Data = Results.RAD_RT2.data(:, 1:10);
RAD_RT2.Time = Results.RAD_RT2.data(:, 10);

save RAD_RT2 -v7.3 RAD_RT2

RAD_RT3 = timeseries;
RAD_RT3.Data = Results.RAD_RT3.data(:, 1:9);
RAD_RT3.Time = Results.RAD_RT3.data(:, 10);

save RAD_RT3 -v7.3 RAD_RT3

RAD_RT4 = timeseries;
RAD_RT4.Data = Results.RAD_RT4.data(:, 1:9);
RAD_RT4.Time = Results.RAD_RT4.data(:, 10);

save RAD_RT4 -v7.3 RAD_RT4

RAD_RT5 = timeseries;
RAD_RT5.Data = Results.RAD_RT5.data(:, 1:9);
RAD_RT5.Time = Results.RAD_RT5.data(:, 10);

save RAD_RT5 -v7.3 RAD_RT5

RAD_RT6 = timeseries;
RAD_RT6.Data = Results.RAD_RT6.data(:, 1:9);
RAD_RT6.Time = Results.RAD_RT6.data(:, 10);

save RAD_RT6 -v7.3 RAD_RT6

RAD_RTS1 = timeseries;
RAD_RTS1.Data = Results.RAD_RTS1.data(:, 1:6);
RAD_RTS1.Time = Results.RAD_RTS1.data(:, end);

save RAD_RTS1 -v7.3 RAD_RTS1

RAD_RTS2 = timeseries;
RAD_RTS2.Data = Results.RAD_RTS1.data(:, 7:12);
RAD_RTS2.Time = Results.RAD_RTS1.data(:, end);

save RAD_RTS2 -v7.3 RAD_RTS2

RAD_RTS3 = timeseries;
RAD_RTS3.Data = Results.RAD_RTS1.data(:, 13:18);
RAD_RTS3.Time = Results.RAD_RTS1.data(:, end);

save RAD_RTS3 -v7.3 RAD_RTS3

RAD_RTS4 = timeseries;
RAD_RTS4.Data = Results.RAD_RTS1.data(:, 19:24);
RAD_RTS4.Time = Results.RAD_RTS1.data(:, end);

save RAD_RTS4 -v7.3 RAD_RTS4

RAD_RTS5 = timeseries;
RAD_RTS5.Data = Results.RAD_RTS1.data(:, 25:30);
RAD_RTS5.Time = Results.RAD_RTS1.data(:, end);

save RAD_RTS5 -v7.3 RAD_RTS5

RAD_RTS6 = timeseries;
RAD_RTS6.Data = Results.RAD_RTS1.data(:, 31:36);
RAD_RTS6.Time = Results.RAD_RTS1.data(:, end);

save RAD_RTS6 -v7.3 RAD_RTS6

total_time = RAD_RTS6.Time(end) - RAD_RTS6.Time(1);