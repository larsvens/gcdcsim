function [ in_interest_region ] = is_in_interest_region( ...
    longitudinal_distance, lateral_distance )
%IS_IN_INTEREST_REGION Summary of this function goes here
%   Detailed explanation goes here

    in_interest_region = 0;
    
    interest_region_width = 4;
    interest_region_length = 30;

    if longitudinal_distance <= interest_region_length
        
        if lateral_distance >= -interest_region_width/2 && lateral_distance <= interest_region_width/2
            
            if longitudinal_distance >= -lateral_distance*2
                
                if longitudinal_distance >= lateral_distance*2
                    
                    in_interest_region = 1;

                end
            
            end
            
        end
                
    end
        
end

