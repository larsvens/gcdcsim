function [distance_to_front, tracked_id, continuous_valid_readings, confidence] ...
    = radar_processing(time, dynamic_1, dynamic_2, dynamic_3, ...
    dynamic_4, dynamic_5, dynamic_6, ...
    static_1, static_2, static_3, ...
    static_4, static_5, static_6)
%RADAR_PROCESSING Summary of this function goes here
%   Detailed explanation goes here

persistent ITERATIONS
persistent DISTANCE_TO_FRONT
persistent LAST_TRACKED_ID
persistent CONTINUOUS_VALID_READINGS
persistent LOCKED_ID
persistent INDEX_CONFIDENCE
persistent INDEX_DISTANCE_TO_FRONT
persistent PREVIOUS_TIME

if sum(static_1) == 0
    distance_to_front = -1;
    tracked_id = -1;
    continuous_valid_readings = -1;
    confidence = 0;
    PREVIOUS_TIME = time;
    return;
end

if isempty(PREVIOUS_TIME)
    PREVIOUS_TIME = time;    
end

if isempty(ITERATIONS)
    ITERATIONS = 0;
end
ITERATIONS = ITERATIONS + 1;

if isempty(DISTANCE_TO_FRONT)
   DISTANCE_TO_FRONT = -1;
end

if isempty(LAST_TRACKED_ID)
   LAST_TRACKED_ID = -1;
end

if isempty(LOCKED_ID)
   LOCKED_ID = -1;
end

if isempty(CONTINUOUS_VALID_READINGS)
   CONTINUOUS_VALID_READINGS = 0;
end

if isempty(INDEX_CONFIDENCE)
    INDEX_CONFIDENCE = zeros(256, 1);
end

if isempty(INDEX_DISTANCE_TO_FRONT)
    INDEX_DISTANCE_TO_FRONT = zeros(256, 1);
end

distance_to_front = -1;
tracked_id = -1;

delta_time = time - PREVIOUS_TIME;
confidence_increase = delta_time;
confidence_decrease_magnitude = confidence_increase/2;

if is_valid_dynamic_measurement(dynamic_1)
    if is_dynamic_measurement_in_interest_region(dynamic_1)    
        distance_to_front = dynamic_1(1);
        tracked_id = dynamic_1(8);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('dynamic_1')
%         disp(distance_to_front)
%         disp(dynamic_1)
        
%         dynamic_1
    end
end
if is_valid_dynamic_measurement(dynamic_2)
    if is_dynamic_measurement_in_interest_region(dynamic_2)    
        distance_to_front = dynamic_2(1);
        tracked_id = dynamic_2(8);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('dynamic_2')
%         disp(distance_to_front)
%         disp(dynamic_2)
        
    end
end
if is_valid_dynamic_measurement(dynamic_3)
    if is_dynamic_measurement_in_interest_region(dynamic_3)    
        distance_to_front = dynamic_3(1);
        tracked_id = dynamic_3(8);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('dynamic_3')
%         disp(distance_to_front)
%         disp(dynamic_3)

    end
end
if is_valid_dynamic_measurement(dynamic_4)
    if is_dynamic_measurement_in_interest_region(dynamic_4)    
        distance_to_front = dynamic_4(1);
        tracked_id = dynamic_4(8);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('dynamic_4')
%         disp(distance_to_front)
%         disp(dynamic_4)

    end
end
if is_valid_dynamic_measurement(dynamic_5)
    if is_dynamic_measurement_in_interest_region(dynamic_5)    
        distance_to_front = dynamic_5(1);
        tracked_id = dynamic_5(8);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('dynamic_5')
%         disp(distance_to_front)
%         disp(dynamic_5)
        
    end
end
if is_valid_dynamic_measurement(dynamic_6)
    if is_dynamic_measurement_in_interest_region(dynamic_6)    
        distance_to_front = dynamic_6(1);
        tracked_id = dynamic_6(8);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('dynamic_6')
%         disp(distance_to_front)
%         disp(dynamic_6)

    end
end

if is_valid_static_measurement(static_1)
    if is_static_measurement_in_interest_region(static_1)    
        distance_to_front = static_1(3);
        tracked_id = static_1(5);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('static_1')
%         disp(distance_to_front)       
%         disp(static_1)
%         static_1
    end
end
if is_valid_static_measurement(static_2)
    if is_static_measurement_in_interest_region(static_2)    
        distance_to_front = static_2(3);
        tracked_id = static_2(5);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('static_2')
%         disp(distance_to_front)        
%         disp(static_2)
        
    end
end
if is_valid_static_measurement(static_3)
    if is_static_measurement_in_interest_region(static_3)    
        distance_to_front = static_3(3);
        tracked_id = static_3(5);        
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('static_3')
%         disp(distance_to_front)       
%         disp(static_3)
%         static_3
    end
end
if is_valid_static_measurement(static_4)
    if is_static_measurement_in_interest_region(static_4)    
        
        distance_to_front = static_4(3);
        tracked_id = static_4(5);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('static_4')
%         disp(distance_to_front)       
%         disp(static_4)
%         static_4
    end
end
if is_valid_static_measurement(static_5)
    if is_static_measurement_in_interest_region(static_5)    
        distance_to_front = static_5(3);
        tracked_id = static_5(5);        
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('static_5')
%         disp(distance_to_front)       
%         disp(static_5)
%         static_5
    end
end
if is_valid_static_measurement(static_6)
    if is_static_measurement_in_interest_region(static_6)    
        distance_to_front = static_6(3);
        tracked_id = static_6(5);
        INDEX_DISTANCE_TO_FRONT(tracked_id) = distance_to_front;
        INDEX_CONFIDENCE(tracked_id) = INDEX_CONFIDENCE(tracked_id) + confidence_increase;
        if INDEX_CONFIDENCE(tracked_id) > 1
            INDEX_CONFIDENCE(tracked_id) = 1;
        end
%         disp('static_6')
%         disp(distance_to_front)       
%         disp(static_6)
%         static_6
    end
end

if LOCKED_ID ~= -1
   
    if distance_to_front ~= -1 && LOCKED_ID == tracked_id
   
        DISTANCE_TO_FRONT = distance_to_front;

    end
    
end

% tracked_id
% LAST_TRACKED_ID

if tracked_id ~= -1 % If there is an object to track
    
    if LAST_TRACKED_ID ~= -1 % If there was an object being tracked before
       
        if LAST_TRACKED_ID == tracked_id % If currently tracked is the same
            % as the previously tracked     
            
            CONTINUOUS_VALID_READINGS = CONTINUOUS_VALID_READINGS + 1;
            
            if CONTINUOUS_VALID_READINGS > 20
                
                LOCKED_ID = tracked_id;
                
            end
            
        else
            % If we are tracking a different object than the previous one
            
            if CONTINUOUS_VALID_READINGS > 0
                
                CONTINUOUS_VALID_READINGS = 0;
                
            else
                
                CONTINUOUS_VALID_READINGS = CONTINUOUS_VALID_READINGS - 1;
                                
            end
                           
        end
        
    else % If there was no object being tracked before
        
        LAST_TRACKED_ID = tracked_id;
        CONTINUOUS_VALID_READINGS = 1;
        
    end
    
else % If there is no object to track

    if LAST_TRACKED_ID ~= -1 % If there was no object to track before
       
        tracked_id = LAST_TRACKED_ID;
      
    end
    
end

% INDEX_CONFIDENCE(tracked_id)

[value, index] = max(INDEX_CONFIDENCE);

[values, indexes] = sort(-INDEX_CONFIDENCE);

temp_i = 1;
min_dist = 10000;
% disp('========')
% disp(sum(INDEX_CONFIDENCE==1))
while 1
%     values(temp_i+1)
%     values(temp_i)
    
    if values(temp_i) == 0
        break;
    end

    if (values(temp_i)) ~= values(1)
       break; 
    end
    
%     values(temp_i+1)
    
%     values(temp_i+1)
%     values(temp_i)
    
    if INDEX_DISTANCE_TO_FRONT(indexes(temp_i)) < min_dist
        index = indexes(temp_i);
        min_dist = INDEX_DISTANCE_TO_FRONT(indexes(temp_i));
        
    end
    temp_i = temp_i + 1;
    
end


confidence = value;
if INDEX_DISTANCE_TO_FRONT(index) ~= -1
    
    if value ~= 0
    
        distance_to_front = INDEX_DISTANCE_TO_FRONT(index);
        
    end

end




% [value, index]

INDEX_CONFIDENCE = INDEX_CONFIDENCE - confidence_decrease_magnitude*ones(256, 1);
INDEX_CONFIDENCE = max(0, INDEX_CONFIDENCE);


% LAST_TRACKED_ID

% distance_to_front = DISTANCE_TO_FRONT;
% tracked_id = LAST_TRACKED_ID;
continuous_valid_readings = CONTINUOUS_VALID_READINGS;

% INDEX_DISTANCE_TO_FRONT'

PREVIOUS_TIME = time;   

end

