function [] = plot_static_measure( static_object, index, radar_index, ...
    plot_handle, text_handle )
%PLOT_STATIC_MEASURE Summary of this function goes here
%   Detailed explanation goes here

    if static_object(index, 3) == 401.5
        
        long_dist = [];
        lat_dist = [];
                
        set(text_handle, 'String', '');
        
    else
        
        long_dist = static_object(index, 3);
        lat_dist = static_object(index, 4);
        
        set(text_handle, 'Position', [lat_dist long_dist]);
        set(text_handle, 'String', [num2str(radar_index), '-', num2str(static_object(index, 5))]);
        
    end
     
    set(plot_handle, 'XData', lat_dist, 'YData', long_dist)


end

