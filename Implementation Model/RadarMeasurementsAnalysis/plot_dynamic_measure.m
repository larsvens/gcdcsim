function [] = plot_dynamic_measure( dynamic_object, index, radar_index, ...
    plot_handle, text_handle )
%PLOT_STATIC_MEASURE Summary of this function goes here
%   Detailed explanation goes here

    if dynamic_object(index, 2) == 401.5
        
%         long_dist = -10;
%         lat_dist = -10;
%         long_speed = 1;
%         lat_speed = 1;
        long_dist = [];
        lat_dist = [];
        long_speed = [];
        lat_speed = [];
        
        set(text_handle, 'String', '')
        
    else
        
        long_dist = dynamic_object(index, 1);
        lat_dist = dynamic_object(index, 2);
        long_speed = dynamic_object(index, 3);
        lat_speed = dynamic_object(index, 4);
        
        set(text_handle, 'Position', [lat_dist long_dist]);
%         set(text_handle, 'String', num2str(dynamic_object(index, 8)));
        
        set(text_handle, 'String', [num2str(radar_index), '-', num2str(dynamic_object(index, 8))]);
        
    end
        
    set(plot_handle, 'XData', lat_dist, 'YData', long_dist, ...
                    'UData', lat_speed, 'VData', long_speed)

    
end

