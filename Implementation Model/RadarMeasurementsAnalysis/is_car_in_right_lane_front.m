function [ right_lane_front ] = is_car_in_right_lane_front...
    ( x_local_other, y_local_other )
%IS_CAR_IN_SAME_LANE_FRONT Summary of this function goes here
%   Detailed explanation goes here

    if x_local_other > 2 && x_local_other < 40 && ... 
           y_local_other < -2.5 && y_local_other > -7.5

        right_lane_front = true;
        
    else
        
        right_lane_front = false;

    end


end

