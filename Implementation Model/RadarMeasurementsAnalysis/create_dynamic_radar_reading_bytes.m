function [ radar_reading_bytes ] = create_dynamic_radar_reading_bytes( dynamic_radar_reading )
%CREATE_STATIC_RADAR_READING_BYTES Summary of this function goes here
%   Detailed explanation goes here

%     dynamic_radar_reading.MsgCounter;
%     dynamic_radar_reading.LateralDistance;
%     dynamic_radar_reading.LongDistance;
%     dynamic_radar_reading.RelLongAcceleration;
%     dynamic_radar_reading.RelativeLateralSpeed;
%     dynamic_radar_reading.RelativeLongSpeed;
%     dynamic_radar_reading.Type;
%     dynamic_radar_reading.MsgCounterB;
%     dynamic_radar_reading.Lane;
%     dynamic_radar_reading.LaneConfidence;
%     dynamic_radar_reading.Movement;
%     dynamic_radar_reading.TrackIdNumber;
%     dynamic_radar_reading.TrackStatus;
           
    radar_reading_bytes = uint8(zeros(104,1));

    % Double precision
    radar_reading_bytes(1:8) = uint8(typecast(swapbytes(double(dynamic_radar_reading.MsgCounter)), 'uint8'));
    radar_reading_bytes(9:16) = uint8(typecast(swapbytes(double(dynamic_radar_reading.LateralDistance)), 'uint8'));
    radar_reading_bytes(17:24) = uint8(typecast(swapbytes(double(dynamic_radar_reading.LongDistance)), 'uint8'));    
    radar_reading_bytes(25:32) = uint8(typecast(swapbytes(double(dynamic_radar_reading.RelLongAcceleration)), 'uint8'));
    radar_reading_bytes(33:40) = uint8(typecast(swapbytes(double(dynamic_radar_reading.RelativeLateralSpeed)), 'uint8'));    
    radar_reading_bytes(41:48) = uint8(typecast(swapbytes(double(dynamic_radar_reading.RelativeLongSpeed)), 'uint8'));
    radar_reading_bytes(49:56) = uint8(typecast(swapbytes(double(dynamic_radar_reading.Type)), 'uint8'));   
    radar_reading_bytes(57:64) = uint8(typecast(swapbytes(double(dynamic_radar_reading.MsgCounterB)), 'uint8'));        
    radar_reading_bytes(65:72) = uint8(typecast(swapbytes(double(dynamic_radar_reading.Lane)), 'uint8'));        
    radar_reading_bytes(73:80) = uint8(typecast(swapbytes(double(dynamic_radar_reading.LaneConfidence)), 'uint8'));        
    radar_reading_bytes(81:88) = uint8(typecast(swapbytes(double(dynamic_radar_reading.Movement)), 'uint8'));      
    radar_reading_bytes(89:96) = uint8(typecast(swapbytes(double(dynamic_radar_reading.TrackIdNumber)), 'uint8'));      
    radar_reading_bytes(97:104) = uint8(typecast(swapbytes(double(dynamic_radar_reading.TrackStatus)), 'uint8'));     
    
end

