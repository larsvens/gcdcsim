function [ best_dynamic_reading ] = evaluate_dynamic_reading( current_dynamic_reading, best_dynamic_reading )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% best_dynamic_reading comes as [long_dist; relative_long_speed; relative_long_accel]

    best_long_dist = best_dynamic_reading(1);
    
    if (current_dynamic_reading.LongDistance < 50 && ...
         abs(current_dynamic_reading.LateralDistance) < 5)
        % Reading is in interest region

        if current_dynamic_reading.LongDistance < best_long_dist

            best_dynamic_reading = [current_dynamic_reading.LongDistance; ...
                current_dynamic_reading.RelativeLongSpeed; ...
                current_dynamic_reading.RelLongAcceleration];

        end

    end
    
end

