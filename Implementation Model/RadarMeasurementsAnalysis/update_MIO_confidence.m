function [fwd_mio_confidence_vector, fwd_left_mio_confidence_vector, ...
    fwd_right_mio_confidence_vector ] = update_MIO_confidence( lat_dist, long_dist, ...
    fwd_mio_confidence_vector, fwd_left_mio_confidence_vector, fwd_right_mio_confidence_vector, ...
    tracked_id, confidence_increase)
%UPDATE_MIO_CONFIDENCE Summary of this function goes here
%   Detailed explanation goes here

    x_local_other = long_dist;
    y_local_other = lat_dist;

    if is_car_in_left_lane_front(x_local_other, y_local_other)
        
        fwd_left_mio_confidence_vector(tracked_id) = ...
            fwd_left_mio_confidence_vector(tracked_id) + confidence_increase;
        
    else
        
        if is_car_in_right_lane_front(x_local_other, y_local_other)
        
            fwd_right_mio_confidence_vector(tracked_id) = ...
            fwd_right_mio_confidence_vector(tracked_id) + confidence_increase;
            
        else
        
            if is_car_in_same_lane_front(x_local_other, y_local_other)
        
                fwd_mio_confidence_vector(tracked_id) = ...
                fwd_mio_confidence_vector(tracked_id) + confidence_increase;
                
            end
            
        end
                    
    end

end

