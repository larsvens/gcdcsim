function [ in_interest_region ] = is_dynamic_measurement_in_interest_region( ...
    dynamic_measurement )
%IS_IN_INTEREST_REGION Summary of this function goes here
%   Detailed explanation goes here

    long_dist = dynamic_measurement(1);
    lat_dist = dynamic_measurement(2);

    in_interest_region = is_in_interest_region( ...
    long_dist, lat_dist);
        
end

