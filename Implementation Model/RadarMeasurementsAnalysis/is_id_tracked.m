function [ id_is_tracked, MIO_bus ] = is_id_tracked(id, ...
            dynamic_1, dynamic_2, dynamic_3, ...
            dynamic_4, dynamic_5, dynamic_6, ...
            static_1, static_2, static_3, ...
            static_4, static_5, static_6)
%IS_ID_TRACKED Summary of this function goes here
%   Detailed explanation goes here

    id_is_tracked = 0;
    
    MIO_bus = struct('is_valid', 0, 'long_distance', 0, 'lat_distance', 0,...
    'relative_long_velocity', 0, 'relative_lat_velocity', 0, ...
    'relative_long_acceleration', 0);
    
    tracked_id = static_1(5);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = static_1(3);
        MIO_bus.lat_distance = static_1(4);
        return;    
    end
    
    tracked_id = static_2(5);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = static_2(3);
        MIO_bus.lat_distance = static_2(4);
        return;    
    end
    
    tracked_id = static_3(5);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = static_3(3);
        MIO_bus.lat_distance = static_3(4);
        return;    
    end
    
    tracked_id = static_4(5);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = static_4(3);
        MIO_bus.lat_distance = static_4(4);
        return;    
    end
    
    tracked_id = static_5(5);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = static_5(3);
        MIO_bus.lat_distance = static_5(4);
        return;    
    end
    
    tracked_id = static_6(5);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = static_6(3);
        MIO_bus.lat_distance = static_6(4);
        return;    
    end
    
    tracked_id = dynamic_1(8);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = dynamic_1(1);
        MIO_bus.lat_distance = dynamic_1(2);
        MIO_bus.relative_long_velocity = dynamic_1(3);
        MIO_bus.relative_lat_velocity = dynamic_1(4);
        MIO_bus.relative_long_acceleration = dynamic_1(5);
        return;    
    end
    
    tracked_id = dynamic_2(8);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = dynamic_2(1);
        MIO_bus.lat_distance = dynamic_2(2);
        MIO_bus.relative_long_velocity = dynamic_2(3);
        MIO_bus.relative_lat_velocity = dynamic_2(4);
        MIO_bus.relative_long_acceleration = dynamic_2(5);
        return;    
    end
    
    tracked_id = dynamic_3(8);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = dynamic_3(1);
        MIO_bus.lat_distance = dynamic_3(2);
        MIO_bus.relative_long_velocity = dynamic_3(3);
        MIO_bus.relative_lat_velocity = dynamic_3(4);
        MIO_bus.relative_long_acceleration = dynamic_3(5);
        return;    
    end
    
    tracked_id = dynamic_4(8);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = dynamic_4(1);
        MIO_bus.lat_distance = dynamic_4(2);
        MIO_bus.relative_long_velocity = dynamic_4(3);
        MIO_bus.relative_lat_velocity = dynamic_4(4);
        MIO_bus.relative_long_acceleration = dynamic_4(5);
        return;    
    end
    
    tracked_id = dynamic_5(8);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = dynamic_5(1);
        MIO_bus.lat_distance = dynamic_5(2);
        MIO_bus.relative_long_velocity = dynamic_5(3);
        MIO_bus.relative_lat_velocity = dynamic_5(4);
        MIO_bus.relative_long_acceleration = dynamic_5(5);
        return;    
    end
    
    tracked_id = dynamic_6(8);    
    if tracked_id == id        
        id_is_tracked = 1;
        MIO_bus.is_valid = 1;
        MIO_bus.long_distance = dynamic_6(1);
        MIO_bus.lat_distance = dynamic_6(2);
        MIO_bus.relative_long_velocity = dynamic_6(3);
        MIO_bus.relative_lat_velocity = dynamic_6(4);
        MIO_bus.relative_long_acceleration = dynamic_6(5);
        return;    
    end
    
    
%     tracked_id = dynamic_1(8);

end

