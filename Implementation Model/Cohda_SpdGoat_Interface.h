/*
 * Cohda_SpdGoat_Interface.h
 *
 *  Created on: Feb 4, 2016
 *      Author: Xinhai Zhang
 */

#ifndef Cohad_SpdGoat_Interface_H_
#define Cohad_SpdGoat_Interface_H_

//------------------------------------------------------------------------------
// Included headers
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Local Macros & Constants
//------------------------------------------------------------------------------
enum {
  ITSSemiAxisLength_oneCentimeter = 1,
  ITSSemiAxisLength_outOfRange = 4094,
  ITSSemiAxisLength_unavailable = 4095,
};


enum {
  ITSHeadingConfidence_equalOrWithinZeroPointOneDegree = 1,
  ITSHeadingConfidence_equalOrWithinOneDegree = 10,
  ITSHeadingConfidence_outOfRange = 126,
  ITSHeadingConfidence_unavailable = 127,
};


enum {
  ITSSpeedConfidence_equalOrWithinOneCentimeterPerSec = 1,
  ITSSpeedConfidence_equalOrWithinOneMeterPerSec = 100,
  ITSSpeedConfidence_outOfRange = 126,
  ITSSpeedConfidence_unavailable = 127,
};


typedef enum ITSVehicleRole {
  ITSVehicleRole_Default,
  ITSVehicleRole_publicTransport,
  ITSVehicleRole_specialTransport,
  ITSVehicleRole_dangerousGoods,
  ITSVehicleRole_roadWork,
  ITSVehicleRole_rescue,
  ITSVehicleRole_emergency,
  ITSVehicleRole_safetyCar,
  ITSVehicleRole_agriculture,
  ITSVehicleRole_commercial,
  ITSVehicleRole_military,
  ITSVehicleRole_roadOperator,
  ITSVehicleRole_taxi,
  ITSVehicleRole_reserved1,
  ITSVehicleRole_reserved2,
  ITSVehicleRole_reserved3,
} ITSVehicleRole;

enum {
  ITSCauseCodeType_reserved = 0,
  ITSCauseCodeType_trafficCondition = 1,
  ITSCauseCodeType_accident = 2,
  ITSCauseCodeType_roadworks = 3,
  ITSCauseCodeType_adverseWeatherCondition_Adhesion = 6,
  ITSCauseCodeType_hazardousLocation_SurfaceCondition = 9,
  ITSCauseCodeType_hazardousLocation_ObstacleOnTheRoad = 10,
  ITSCauseCodeType_hazardousLocation_AnimalOnTheRoad = 11,
  ITSCauseCodeType_humanPresenceOnTheRoad = 12,
  ITSCauseCodeType_wrongWayDriving = 14,
  ITSCauseCodeType_rescueAndRecoveryWorkInProgress = 15,
  ITSCauseCodeType_adverseWeatherCondition_ExtremeWeatherCondition = 17,
  ITSCauseCodeType_adverseWeatherCondition_Visibility = 18,
  ITSCauseCodeType_adverseWeatherCondition_Precipitation = 19,
  ITSCauseCodeType_slowVehicle = 26,
  ITSCauseCodeType_dangerousEndOfQueue = 27,
  ITSCauseCodeType_vehicleBreakdown = 91,
  ITSCauseCodeType_postCrash = 92,
  ITSCauseCodeType_humanProblem = 93,
  ITSCauseCodeType_stationaryVehicle = 94,
  ITSCauseCodeType_emergencyVehicleApproaching = 95,
  ITSCauseCodeType_hazardousLocation_DangerousCurve = 96,
  ITSCauseCodeType_collisionRisk = 97,
  ITSCauseCodeType_signalViolation = 98,
  ITSCauseCodeType_dangerousSituation = 99,
};


typedef enum ITSAltitudeConfidence {
  ITSAltitudeConfidence_alt_000_01,
  ITSAltitudeConfidence_alt_000_02,
  ITSAltitudeConfidence_alt_000_05,
  ITSAltitudeConfidence_alt_000_10,
  ITSAltitudeConfidence_alt_000_20,
  ITSAltitudeConfidence_alt_000_50,
  ITSAltitudeConfidence_alt_001_00,
  ITSAltitudeConfidence_alt_002_00,
  ITSAltitudeConfidence_alt_005_00,
  ITSAltitudeConfidence_alt_010_00,
  ITSAltitudeConfidence_alt_020_00,
  ITSAltitudeConfidence_alt_050_00,
  ITSAltitudeConfidence_alt_100_00,
  ITSAltitudeConfidence_alt_200_00,
  ITSAltitudeConfidence_outOfRange,
  ITSAltitudeConfidence_unavailable,
} ITSAltitudeConfidence;


typedef enum ParticipantsReady {
	ParticipantsReady_notReady	= 0,
	ParticipantsReady_ready	= 1
} e_ParticipantsReady;


typedef enum StartPlatoon {
	StartPlatoon_startPlatoonAAtSpeed80kph	= 0,
	StartPlatoon_startPlatoonBAt60kph	= 1
} e_StartPlatoon;

typedef enum EndOfScenario {
	EndOfScenario_endOfScenario	= 1
} e_EndOfScenario;


//-----------------------------------------------------------------------------
// Type Definitions
//-----------------------------------------------------------------------------

typedef struct spdGoat_header
{
	int protocolVersion;
	int messageID;
}spd_header;

typedef struct spdGoat_CAM
{
	int protocolVersion;
	int messageID;
	unsigned int stationID;
	int generationDeltaTime;
	int stationType;
	int vehicleLengthValue;
	int vehicleLengthConfidenceIndication;
	int vehicleWidth;
	int latitude;
	int longitude;
	int semiMajorConfidence;
	int semiMinorConfidence;
	int semiMajorOrientation;
	// heading;
	int headingValue;
	int headingConfidence;

	int speedValue;
	int speedConfidence;
	//ITSYawRate yawRate;
	int yawRateValue;
	int yawRateConfidence;

	//ITSLongitudinalAcceleration longitudinalAcceleration;
	int longitudinalAccelerationValue;
	int longitudinalAccelerationConfidence;

	int vehicleRole;
}spd_CAM;

typedef struct spdGoat_DENM
{
	int protocolVersion;
	int messageID;
	unsigned int stationID;
	int stationType;


	unsigned int RefTime_data;
    int RefTime_len; /* 0 is represented as len = 0 and negative = 0 or 1 */
    int RefTime_allocated_len; /* length (>= len) allocated for tab */
    int RefTime_negative; /* 0 = positive, 1 = negative */



	//ITSCauseCode eventType;
    int causeCode;
    int subCauseCode;

	//closedLanes
	int hardShoulderStatus_option;
	int hardShoulderStatus;
	unsigned char drivingLaneStatusBuf;
	int Len;

	int lanePosition;

	int ActionID_sequenceNumber;

	unsigned int DecTime_data;
	int DecTime_len; /* 0 is represented as len = 0 and negative = 0 or 1 */
	int DecTime_allocated_len; /* length (>= len) allocated for tab */
	int DecTime_negative; /* 0 = positive, 1 = negative */

	//ITSReferencePosition eventPosition;
	int latitude;
	int longitude;
	//ITSPosConfidenceEllipse positionConfidenceEllipse;
	int eventPosition_semiMajorConfidence;
	int eventPosition_semiMinorConfidence;
	int eventPosition_semiMajorOrientation;

	//ITSAltitude altitude;
	int altitudeValue;
	int altitudeConfidence;

	int validityDuration;
}spd_DENM;


typedef struct spdGoat_iCLCM
{
	int 			protocolVersion;
	int 			messageID;
	unsigned int 	stationID;
	int	 			vehicleRearAxleLocation;
	int	 			controllerType;
	int	 			targetLongitudinalAcceleration;
	int	 			timeHeadway;
	int	 			cruiseSpeed;
	int	 			vehicleResponseTimeConstant;
	int	 			vehicleResponseTimeDelay;
	int				participantsReady;
	int				startPlatoon;
	int				endOfScenario;
	unsigned int	mioID;
	int	 			mioRange;
	int	 			mioBearing;
	int	 			mioRangeRate;
	int	 			lane;
	unsigned int	forwardID;
	unsigned int	backwardID;
	int	 			acknowledgeFlag;
	int	 			mergeRequest;
	int	 			mergeSafeToMerge;
	int	 			mergeFlag;
	int	 			mergeFlagTail;
	int	 			mergeFlagHead;
	int	 			platoonID;
	int	 			distanceTravelledCZ;
	int	 			intention;
	int	 			counterIntersection;
}spd_iCLCM;

#endif /* Cohad_SpdGoat_Interface_H_ */
