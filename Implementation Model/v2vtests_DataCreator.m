clear
close all

load v2vtests_comms_log_19_53_lane_change_of_rcv_in_front
% load v2vtests_comms_log_19_56_lane_change_of_rcv_in_front_to_center_to_left
% load v2vtests_comms_log_20_33_intersection_rcv_crosses_in_front_with_antenna

% % Initialization script for the implementation code to the Targets
% clear;
% add source directories
parentDir = cd(cd('..'));
addpath(parentDir);
addpath(genpath(strcat(parentDir,'\GCDC2012-Library')))
addpath(genpath(strcat(parentDir,'\Models')))
addpath('RadarMeasurementsAnalysis')
addpath('TruckRadarModel');
% load bus definitions
load('busDefinitions');
load('busDefinitionsLDM');
load('BusDefinitionsRadar');
load('RADARCanBusDefinitions');


global GLOBAL_vehicle_1_id
global GLOBAL_vehicle_2_id
global GLOBAL_vehicle_3_id
global GLOBAL_vehicle_4_id
global GLOBAL_vehicle_5_id

GLOBAL_vehicle_1_id = 110;
GLOBAL_vehicle_2_id = 100;
GLOBAL_vehicle_3_id = 170;
GLOBAL_vehicle_4_id = 120;
GLOBAL_vehicle_5_id = 97;

lat_origin_degrees = 59.613595;
lon_origin_degrees = 17.919750;
hgt_origin_degrees = 0;

% Initialize rates
Ts_Fast = 0.0005;
Ts_Mid = 1/100;
Ts_Logic = 1/10;
Ts_Sender = 1/60;
Ts_Slow = 1/10;
Ts_Controller = 1/10;
Ts_Estimator = 1/100; %1/25;
Ts_Log_Tx = 0.02;
Ts_Serial_GPS = 0.01;
Ts_MPC = 0.05;




RX_log = Results.V2V_RX1;

CAM_Generation_Time_idx = 1;
stationd_id_idx = 4;
latitude_idx = 8;
longitude_idx = 9;
heading_idx = 10;

valid_idx = RX_log.data(:, CAM_Generation_Time_idx) ~= 0;

received_latitude_vector = RX_log.data(valid_idx, latitude_idx);
received_longitude_vector = RX_log.data(valid_idx, longitude_idx);
received_heading_vector = RX_log.data(valid_idx, heading_idx);
received_station_id_vector = RX_log.data(valid_idx, stationd_id_idx);
time_vector = RX_log.data(valid_idx, end);

repeat_end_time = max(time_vector);

received_latitude_timeseries = timeseries(received_latitude_vector, time_vector);
received_longitude_timeseries = timeseries(received_longitude_vector, time_vector);
received_heading_timeseries = timeseries(received_heading_vector, time_vector);
received_station_id_timeseries = timeseries(received_station_id_vector, time_vector);
% save('received_log.mat',...
%     'received_latitude_timeseries',...
%     'received_longitude_timeseries',...
%     'received_heading_timeseries',...
%     'received_station_id_timeseries');

% plot(received_heading_vector)
% plot(V2V_log.data(:, stationd_id_idx))


TX_log = Results.V2V_TX;

CAM_Generation_Time_idx = 1;
stationd_id_idx = 4;
latitude_idx = 8;
longitude_idx = 9;
heading_idx = 10;

valid_idx = TX_log.data(:, CAM_Generation_Time_idx) ~= 0;

sent_latitude_vector = TX_log.data(valid_idx, latitude_idx)/(10^7);
sent_longitude_vector = TX_log.data(valid_idx, longitude_idx)/(10^7);
sent_heading_vector = TX_log.data(valid_idx, heading_idx)/10;
sent_station_id_vector = TX_log.data(valid_idx, stationd_id_idx);
time_vector = TX_log.data(valid_idx, end);

sent_latitude_timeseries = timeseries(sent_latitude_vector, time_vector);
sent_longitude_timeseries = timeseries(sent_longitude_vector, time_vector);
sent_heading_timeseries = timeseries(sent_heading_vector, time_vector);
sent_station_id_timeseries = timeseries(sent_station_id_vector, time_vector);

% save('received_log.mat',...
%     'sent_latitude_timeseries',...
%     'sent_longitude_timeseries',...
%     'sent_heading_timeseries',...
%     'sent_station_id_timeseries');

repeat_end_time = max(repeat_end_time, max(time_vector));

% plot(sent_station_id_vector)