%% Setup
%%%%%%%%%%%%%%%% System Configuration File %%%%%%%%%%%%%%%%%%%%%%%%
addpath(genpath(fullfile(pwd,'GCDC2012-Library')));
%%%%%%%%%%%%%%% Sampling Times %%%%%%%%%%%%%%%
% Ts_Fast = 0.0005;
% Ts_Mid = 1/100;
% Ts_Logic = 1/10;
% Ts_Sender = 1/60;
% Ts_Slow = 1/10;
% Ts_Controller = 1/10;
% Ts_Estimator = 1/100;
% Ts_CANSend = 1/100;

% % FILESCOPEDATALOGGINGDEMO - Demonstrates xPC Target Data Logging Using a File Scope
% %
% %    This M-file was created in support of the xPC Target Application Note
% %    (xPCTAN-0100): "xPC Target Data logging Methodologies"
% 
% % Open, build, and run model.
% open_system('sosModel4');                   % Open the model
% set_param('sosModel4','RTWVerbose','off');  % Configure RTW for a non-Verbose build
% rtwbuild('sosModel4');                      % Build and download to the target PC
% close_system('sosModel4',0);                % Close the model
% tg = xpc;  % Create an xPC Target Object
%%
% tg = xpc; 
% start(tg);                                  % Start xPC Target Object (run the model)
%%
tg = xpc; 
stop(tg);                                    % Stop xPC Target Object (run the model)
% Wait until model has finished running.
while ~strcmpi(tg.Status,'stopped');        % Is the run complete?
end;
% Get data.
%tg = xpc;
for i=tg.Scopes(1:end)                         % Get file scope (Id: 2)
    sc = getscope(tg,i);
    if strcmpi(sc.Type,'File');
        
%         f = SimulinkRealTime.fileSystem;
%         h = fopen(f, filename);
%         raw = fread(f, h);
%         f.fclose(h);
%         raw = read_file(filename);
%         data = SimulinkRealTime.utils.getFileScopeData(raw);
        
        
        fsys = xpctarget.fs;                        % Connect to the target PC file system
        h = fsys.fopen(sc.FileName);                % Open the log file
        fsysData = fsys.fread(h);                   % Read the data
        fsys.fclose(h);                             % Close the log file
        current_name=sc.FileName(1:end-4);
        Results.(genvarname(current_name)) = readxpcfile(fsysData);   % Convert uint8 log data to double and log under Scope name
        Results.(genvarname(current_name)).sc=sc;
        Results.(genvarname(current_name)).tg=tg;
        clear fsys fsysData current_name h sc 
    end
end

%%
% fsTime = dataLog.data(:,end);               % Get file scope time log (last column)
% fsData = dataLog.data(:,1:end-1);           % Get file scope data log (other columns)
%
% % Plot and save data.
% plot(fsTime, fsData); grid on;              % Plot the data
% title('DEMO: File Scope Data Logging');
% save sosFSDataLog.mat fsTime fsData;        % Save the data
% disp('Data saved in: sosFSDataLog.mat')



% SimulinkRealTime.copyFileToHost('V2V_TX.dat');
% V2V_TX = SimulinkRealTime.utils.getFileScopeData('V2V_TX.dat');
% 
% SimulinkRealTime.copyFileToHost('V2V_RX.dat');
% V2V_RX = SimulinkRealTime.utils.getFileScopeData('V2V_RX.dat');