function [ x_east, y_north ] = geodetic_2_ENU( latitude_degrees, longitude_degrees, ...
    ECEFg0_radians, ECEFr0)
%GEODETIC_2_ENU Summary of this function goes here
%   Detailed explanation goes here

gps = [latitude_degrees, longitude_degrees, 0]'*(pi/180);
temp_1 = geodeticECEF2rectangularECEF(gps);
other_ENU = rectangularECEF2ENU(ECEFg0_radians, ECEFr0,temp_1);
x_east = other_ENU(1);
y_north = other_ENU(2);

end

