clear
close all

% load v2vtests_after_sim_comms_log_20_33_intersection_rcv_crosses_in_front_with_antenna
% axis_values = 20*[-1 1 -1 1];
% jump_it = 2;

load v2vtests_after_sim_comms_log_20_12_lane_change_of_rcv_in_front_fast
axis_values = 40*[-1 1 -1 1];
jump_it = 2;

% load v2vtests_after_sim_comms_log_19_56_lane_change_of_rcv_in_front_to_center_to_left
% axis_values = 40*[-1 1 -1 1];
% jump_it = 2;

num_points = length(long_dist.Data);

figure; hold on;
axis(axis_values)
plot([-1 0 0], [0 0 2], 'r')
h_radar = plot(0, 0, 'kx');
h_v2v = plot(0, 0, 'bo');

h_wait = waitbar(0, 'wait');
h_title = title('NO MIO');

for it = 1:jump_it:num_points

    h_radar.YData = long_dist.Data(it);
    h_radar.XData = -lat_dist.Data(it);

    h_v2v.YData = x_local_other.Data(it);
    h_v2v.XData = -y_local_other.Data(it);

    if mio_bus_log.FWD_MIO_Info_Bus.valid.Data(it);
        h_title.String = 'MIO FWD';
    elseif mio_bus_log.FWD_L_MIO_Info_Bus.valid.Data(it);
        h_title.String = 'MIO FWD LEFT';
    elseif mio_bus_log.FWD_R_MIO_Info_Bus.valid.Data(it);
        h_title.String = 'MIO FWD RIGHT';
    elseif mio_bus_log.BWD_MIO_Info_Bus.valid.Data(it);
        h_title.String = 'MIO BWD';
    elseif mio_bus_log.BWD_L_MIO_Info_Bus.valid.Data(it);
        h_title.String = 'MIO BWD LEFT';
    elseif mio_bus_log.BWD_R_MIO_Info_Bus.valid.Data(it);
        h_title.String = 'MIO BWD RIGHT';
    else
        h_title.String = 'NO MIO';
    end

    waitbar(it/num_points, h_wait)

    drawnow
    
end

