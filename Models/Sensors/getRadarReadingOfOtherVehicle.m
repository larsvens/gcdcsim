function [distanceToOther, relativeSpeedOfOther, angleToOther] = getRadarReadingOfOtherVehicle( ...
    egoXEast, egoYNorth, egoHeading, egoVx, egoVy, ...
    otherXEast, otherYNorth, otherHeading, otherVx, otherVy)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


% Radar Model
% This function takes the ego state and another vehicle state and it will
% output a simulated radar reading (or the absence of it).

% Heading comes in degrees and is defined in clockwise starting 
% from yNorth, convert to yaw:
egoYaw = - egoHeading + 90;
egoYaw = egoYaw*(pi/180);

% Same conversion for the other vehicle angle
otherYaw = - otherHeading + 90;
otherYaw = otherYaw*(pi/180);

% The maximum distance that the radar can sense other vehicles
radarRange = 100;

% Compute the distance to the other vehicle
distanceToOther = ((otherXEast-egoXEast)^2 + (otherYNorth-egoYNorth)^2)^.5;

% If distance to other vehicle is bigger than the radar range, 
% simply output a non-reading
if distanceToOther > radarRange
   
    distanceToOther = -1;
    relativeSpeedOfOther = -1;
    angleToOther = -1;
    
    return
    
end

% The radar is located in the front bumper
% TO DO: Actually place it in the bumper.
frontBumperX = egoXEast;
frontBumperY = egoYNorth;

% Compute the angle to the other vehicle
tempAngle = atan2(otherYNorth - frontBumperY, otherXEast - frontBumperX);
angleToOther = tempAngle - egoYaw;

% The angle of vision of the radar (for one side only)
% The total angle visible is double of radarSweepAngle
radarSweepAngle = 90*(pi/180);

% Check if the angle to the other vehicle is inside of the radar angle of
% vision. If not simply output a non-reading
if abs(angleToOther) > radarSweepAngle
    
    distanceToOther = -1;
    relativeSpeedOfOther = -1;
    angleToOther = -1;
    
    return
    
end

% Compute the relative dpeed of the other vehicle
relativeSpeedOfOther = ((otherVx - egoVx)^2 + (otherVy - egoVy)^2)^.5;


end

