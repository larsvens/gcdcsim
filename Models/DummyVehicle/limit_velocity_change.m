function [ actual_velocity ] = limit_velocity_change...
    ( prev_velocity, desired_velocity, delta_time, allowed_acceleration )
%LIMIT_VELOCITY_CHANGE Summary of this function goes here
%   Detailed explanation goes here
    
    delta_velocity = desired_velocity - prev_velocity;
            
    
    
    allowed_delta_velocity_increase = 2*delta_time;
    allowed_delta_velocity_decrease = -4*delta_time;
    
%     disp('=====')
%     disp('Delta_velocity')
%     disp(delta_velocity)
    
    
    possible_delta_velocity = delta_velocity;
    
    if delta_velocity > allowed_delta_velocity_increase
        
        possible_delta_velocity = allowed_delta_velocity_increase;
        
    else
        
        if delta_velocity < allowed_delta_velocity_decrease
        
            possible_delta_velocity = allowed_delta_velocity_decrease;

        end
        
    end
    
%     disp('Possible delta_velocity')
%     disp(possible_delta_velocity)
    
    
%     possible_delta_velocity = sign(delta_velocity)*min(abs(delta_velocity), allowed_acceleration*delta_time);
    
%     possible_delta_velocity = delta_velocity;

    actual_velocity = prev_velocity + possible_delta_velocity;

end

