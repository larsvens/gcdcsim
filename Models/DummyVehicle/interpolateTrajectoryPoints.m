function [ interpolatedXY ] = interpolateTrajectoryPoints( ...
    prevDistance, currentDistance, nextDistance, ...
    prevXY, nextXY)
%interpolateTrajectoryPoints Interpolates an intermediate point from two
%neighbouring points. 
% prevDistance, the distance, measured from the start of the trajectory, to
% the previously adjacent trajectory point
% currentDistance, the distance, measured from the start of the trajectory, to
% the following adjacent trajectory point
% nextDistance, the distance, measured from the start of the trajectory, to
% the following adjacent trajectory point

    interpolatedXY = 0*prevXY;

    prevWeight = nextDistance - currentDistance;
    nextWeight = currentDistance - prevDistance;
    normPrevWeight = prevWeight/(prevWeight + nextWeight);
    normNextWeight = nextWeight/(prevWeight + nextWeight);

    interpolatedXY(1) = normPrevWeight*prevXY(1) + ...
        normNextWeight*nextXY(1);
    interpolatedXY(2) = normPrevWeight*prevXY(2) + ...
        normNextWeight*nextXY(2);

end

