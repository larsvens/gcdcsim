function [ interpolatedXY, heading, atStart, atEnd ] = trajectoryTrackingDummyVehicle( laneDataENU, currentTravelledDistance, rightLane)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


persistent travelledDistance3

if isempty(travelledDistance3)   

%     deltas = ((diff(laneDataENU(:,1)).^2) + (diff(laneDataENU(:,2)).^2)).^0.5;
    deltas = ((diff(laneDataENU(:,3)).^2) + (diff(laneDataENU(:,4)).^2)).^0.5;
%     deltas2 = ((diff(laneDataENU(:,1)).^2) + (diff(laneDataENU(:,2)).^2)).^0.5;
%     deltas3 = ((diff(laneDataENU(:,1)).^2) + (diff(laneDataENU(:,2)).^2)).^0.5;
%     deltas = (deltas1 + deltas2 + deltas3)/3;    
    travelledDistance3 = [0; cumsum(deltas)];
    
end

atStart = 0;
atEnd = 0;

distanceToTrajPoints = travelledDistance3 - currentTravelledDistance;
[~,closestIndex] = min( distanceToTrajPoints.^2 );

if rightLane
    xIdx = 3;
    yIdx = 4;
else
    xIdx = 1;
    yIdx = 2;
end
    
if closestIndex >= length(travelledDistance3)-1
                
    interpolatedXY = [(laneDataENU(end,xIdx) + laneDataENU(end,xIdx+2))/2, ...
         (laneDataENU(end,yIdx) + laneDataENU(end,yIdx+2))/2,];
    
    deltaX = laneDataENU(end,1)-laneDataENU(end-1,1);
    deltaY = laneDataENU(end,2)-laneDataENU(end-1,2);

    atEnd = 1;
    % AT END
    
else
    
    if closestIndex == 1
    
%         if rightLane
%             
        interpolatedXY = [(laneDataENU(1,xIdx)+laneDataENU(1,xIdx+2))/2, ...
            (laneDataENU(1,yIdx)+laneDataENU(1,yIdx+2))/2];
%             
%         else
%             
%             interpolatedXY = [(laneDataENU(1,3)+laneDataENU(1,5))/2, ...
%                 (laneDataENU(1,4)+laneDataENU(1,6))/2];
%             
%         end
        
        
        deltaX = laneDataENU(2,1)-laneDataENU(1,1);
        deltaY = laneDataENU(2,2)-laneDataENU(1,2);

        atStart = 1;        
        % AT START
     
    else
        
        if currentTravelledDistance > travelledDistance3(closestIndex)

            prevPoint = [(laneDataENU(closestIndex,xIdx)+laneDataENU(closestIndex,xIdx+2))/2  , ...
                (laneDataENU(closestIndex,yIdx)+laneDataENU(closestIndex,yIdx+2))/2];
            nextPoint = [(laneDataENU(closestIndex+1,xIdx)+laneDataENU(closestIndex+1,xIdx+2))/2 , ...
                (laneDataENU(closestIndex+1,yIdx) + laneDataENU(closestIndex+1,yIdx+2) )/2 ];
            
%             [ interpolatedXY ] = interpolateTrajectoryPoints( ...
%             travelledDistance3(closestIndex), currentTravelledDistance, travelledDistance3(closestIndex+1), ...
%             [laneDataENU(closestIndex,1), laneDataENU(closestIndex,2)], ...
%             [laneDataENU(closestIndex+1,1), laneDataENU(closestIndex+1,2) ]);
        
%             disp([laneDataENU(closestIndex,1), laneDataENU(closestIndex,2)])
%             disp()
        
            [ interpolatedXY ] = interpolateTrajectoryPoints( ...
            travelledDistance3(closestIndex), currentTravelledDistance, travelledDistance3(closestIndex+1), ...
            prevPoint, nextPoint);

        else

            prevPoint = [(laneDataENU(closestIndex-1,xIdx)+laneDataENU(closestIndex-1,xIdx+2))/2  , ...
                (laneDataENU(closestIndex-1,yIdx)+laneDataENU(closestIndex-1,yIdx+2))/2];
            nextPoint = [(laneDataENU(closestIndex,xIdx)+laneDataENU(closestIndex,xIdx+2))/2 , ...
                (laneDataENU(closestIndex,yIdx) + laneDataENU(closestIndex,yIdx+2) )/2 ];
            
%             [ interpolatedXY ] = interpolateTrajectoryPoints( ...
%             travelledDistance3(closestIndex-1), currentTravelledDistance, travelledDistance3(closestIndex), ...
%             [laneDataENU(closestIndex-1,1), laneDataENU(closestIndex-1,2)], ...
%             [laneDataENU(closestIndex,1), laneDataENU(closestIndex,2) ]);
            [ interpolatedXY ] = interpolateTrajectoryPoints( ...
            travelledDistance3(closestIndex-1), currentTravelledDistance, travelledDistance3(closestIndex), ...
            prevPoint, nextPoint);

        end
        
        deltaX = laneDataENU(closestIndex+1,1)-laneDataENU(closestIndex-1,1);
        deltaY = laneDataENU(closestIndex+1,2)-laneDataENU(closestIndex-1,2);
        
    end
    
end

heading = atan2(deltaY, deltaX);

end

