load simlog1

WorldStateSim = simlog1.WorldState;

vehicle1X = WorldStateSim.vehicle1.vehicleStateBus.xEast.Data;
vehicle1Y = WorldStateSim.vehicle1.vehicleStateBus.yNorth.Data;
vehicle1T = WorldStateSim.vehicle1.vehicleStateBus.yNorth.Time;

vehicle2X = WorldStateSim.vehicle2.vehicleStateBus.xEast.Data;
vehicle2Y = WorldStateSim.vehicle2.vehicleStateBus.yNorth.Data;

vehicle3X = WorldStateSim.vehicle3.vehicleStateBus.xEast.Data;
vehicle3Y = WorldStateSim.vehicle3.vehicleStateBus.yNorth.Data;

vehicle4X = WorldStateSim.vehicle4.vehicleStateBus.xEast.Data;
vehicle4Y = WorldStateSim.vehicle4.vehicleStateBus.yNorth.Data;

vehicle5X = WorldStateSim.vehicle5.vehicleStateBus.xEast.Data;
vehicle5Y = WorldStateSim.vehicle5.vehicleStateBus.yNorth.Data;

vehicle6X = WorldStateSim.vehicle6.vehicleStateBus.xEast.Data;
vehicle6Y = WorldStateSim.vehicle6.vehicleStateBus.yNorth.Data;

vehicle7X = WorldStateSim.vehicle7.vehicleStateBus.xEast.Data;
vehicle7Y = WorldStateSim.vehicle7.vehicleStateBus.yNorth.Data;

vehicle8X = WorldStateSim.vehicle8.vehicleStateBus.xEast.Data;
vehicle8Y = WorldStateSim.vehicle8.vehicleStateBus.yNorth.Data;

close all;
% 
% figure; hold on;
% plot(vehicle1T, vehicle1X);
% plot(vehicle1T, vehicle2X);


% figure; hold on;
% dist_2_to_1 = ( (vehicle2X - vehicle1X).^2 + (vehicle2Y - vehicle1Y).^2 ).^.5;
% plot(vehicle1T, dist_2_to_1);
% 
% figure; hold on;
% dist_3_to_2 = ( (vehicle3X - vehicle2X).^2 + (vehicle3Y - vehicle2Y).^2 ).^.5;
% plot(vehicle1T, dist_3_to_2);
% 
% figure; hold on;
% dist_4_to_3 = ( (vehicle4X - vehicle3X).^2 + (vehicle4Y - vehicle3Y).^2 ).^.5;
% plot(vehicle1T, dist_4_to_3);
% 
% figure; hold on;
% dist_5_to_4 = ( (vehicle5X - vehicle4X).^2 + (vehicle5Y - vehicle4Y).^2 ).^.5;
% plot(vehicle1T, dist_5_to_4);

figure; hold on;
plot(laneDataENU(:,1), laneDataENU(:,2), 'k--')
plot(laneDataENU(:,3), laneDataENU(:,4), 'k--')
plot(laneDataENU(:,5), laneDataENU(:,6), 'k--')
% plot(vehicle1X, vehicle1Y)
% plot(vehicle2X, vehicle2Y)
% plot(vehicle3X, vehicle3Y)
% plot(vehicle4X, vehicle4Y)
% plot(vehicle5X, vehicle5Y)

% h0 = plot(vehicleEgoX(1), vehicleEgoY(1), 'rx');
h1 = plot(vehicle1X(1), vehicle1Y(1), 'yo');
h2 = plot(vehicle2X(1), vehicle2Y(1), 'mo');
h3 = plot(vehicle3X(1), vehicle3Y(1), 'co');
h4 = plot(vehicle4X(1), vehicle4Y(1), 'go');
h5 = plot(vehicle5X(1), vehicle5Y(1), 'bo');
h6 = plot(vehicle6X(1), vehicle6Y(1), 'o');
h7 = plot(vehicle7X(1), vehicle7Y(1), 'o');
h8 = plot(vehicle8X(1), vehicle8Y(1), 'o');
% h1 = plot(vehicle1X(1), vehicle1Y(1), 'LineWidth', 2);
% h2 = plot(vehicle2X(1), vehicle2Y(1), 'LineWidth', 2);
% h3 = plot(vehicle3X(1), vehicle3Y(1), 'LineWidth', 2);
% h4 = plot(vehicle4X(1), vehicle4Y(1), 'LineWidth', 2);
% h5 = plot(vehicle5X(1), vehicle5Y(1), 'LineWidth', 2);

h_q_ego = quiver(0, 0, 0, 0);

windowSize = 10;

for i = 2:2:length(vehicle1X)-1
   
%     set(h0, 'XData', [vehicleEgoX(i-1), vehicleEgoX(i+1)])
%     set(h0, 'YData', [vehicleEgoY(i-1), vehicleEgoY(i+1)])
%     set(h0, 'XData', vehicleEgoX(i))
%     set(h0, 'YData', vehicleEgoY(i))
    
%     vehicleYaw = 90 - vehicleEgoHeading(i);
%     h_q_ego.XData = vehicleEgoX(i);
%     h_q_ego.YData = vehicleEgoY(i);
%     h_q_ego.UData = 10*cosd(vehicleYaw);
%     h_q_ego.VData = 10*sind(vehicleYaw);
    
    
    
    h1.XData = vehicle1X(i); h1.YData = vehicle1Y(i);
    h2.XData = vehicle2X(i); h2.YData = vehicle2Y(i);
    h3.XData = vehicle3X(i); h3.YData = vehicle3Y(i);
    h4.XData = vehicle4X(i); h4.YData = vehicle4Y(i);
    h5.XData = vehicle5X(i); h5.YData = vehicle5Y(i);
    h6.XData = vehicle6X(i); h6.YData = vehicle6Y(i);
    h7.XData = vehicle7X(i); h7.YData = vehicle7Y(i);
    h8.XData = vehicle8X(i); h8.YData = vehicle8Y(i);
%     set(h1, 'XData', [vehicle1X(i-1), vehicle1X(i+1)])
%     set(h1, 'YData', [vehicle1Y(i-1), vehicle1Y(i+1)])
%     
%     set(h2, 'XData', [vehicle2X(i-1), vehicle2X(i+1)])
%     set(h2, 'YData', [vehicle2Y(i-1), vehicle2Y(i+1)])
%     
%     set(h3, 'XData', [vehicle3X(i-1), vehicle3X(i+1)])
%     set(h3, 'YData', [vehicle3Y(i-1), vehicle3Y(i+1)])
%     
%     set(h4, 'XData', [vehicle4X(i-1), vehicle4X(i+1)])
%     set(h4, 'YData', [vehicle4Y(i-1), vehicle4Y(i+1)])
%     
%     set(h5, 'XData', [vehicle5X(i-1), vehicle5X(i+1)])
%     set(h5, 'YData', [vehicle5Y(i-1), vehicle5Y(i+1)])
    
    otherXEast = WorldStateSim.vehicle5.vehicleStateBus.xEast.Data(i);
    otherYNorth = WorldStateSim.vehicle5.vehicleStateBus.yNorth.Data(i);
    otherHeading = WorldStateSim.vehicle5.vehicleStateBus.heading.Data(i); 
    otherVx = 0;
    otherVy = 0;
        
%     [lane, longitudinal_distance, lateral_distance, ...
%     relative_longitudinal_speed, relative_lateral_speed, ...
%     relative_longitudinal_acceleration] = ...
%     get_radar_reading_of_other_vehicle( ...
%     egoXEast, egoYNorth, egoHeading, egoVx, egoVy, ...
%     otherXEast, otherYNorth, otherHeading, otherVx, otherVy);
    
%     disp(lateral_distance)    
            
    xPositions = [vehicle1X(i), vehicle2X(i), ...
        vehicle3X(i), vehicle4X(i), vehicle5X(i), ...
        vehicle6X(i), vehicle7X(i), vehicle8X(i)];
    yPositions = [vehicle1Y(i), vehicle2Y(i), ...
        vehicle3Y(i), vehicle4Y(i), vehicle5Y(i), ...
        vehicle6Y(i), vehicle7Y(i), vehicle8Y(i)];
    
    
    
    centerMassX = mean(xPositions);
    centerMassY = mean(yPositions);
    
    maxXDeviation = max(abs(xPositions - centerMassX));
    maxYDeviation = max(abs(yPositions - centerMassY));
    
    if maxXDeviation > maxYDeviation
        windowSize = maxXDeviation;
    else
        windowSize = maxYDeviation;
    end
    
    windowSize = 1.2*windowSize;
    
    axis([centerMassX-windowSize centerMassX+windowSize centerMassY-windowSize centerMassY+windowSize])
    pause(0.01)
    
end

