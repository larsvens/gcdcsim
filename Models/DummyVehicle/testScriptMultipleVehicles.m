

vehicle1X = WorldStateSim.vehicle1.vehicleStateBus.xEast.Data;
vehicle1Y = WorldStateSim.vehicle1.vehicleStateBus.yNorth.Data;
vehicle1T = WorldStateSim.vehicle1.vehicleStateBus.yNorth.Time;

vehicle2X = WorldStateSim.vehicle2.vehicleStateBus.xEast.Data;
vehicle2Y = WorldStateSim.vehicle2.vehicleStateBus.yNorth.Data;

vehicle3X = WorldStateSim.vehicle3.vehicleStateBus.xEast.Data;
vehicle3Y = WorldStateSim.vehicle3.vehicleStateBus.yNorth.Data;

vehicle4X = WorldStateSim.vehicle4.vehicleStateBus.xEast.Data;
vehicle4Y = WorldStateSim.vehicle4.vehicleStateBus.yNorth.Data;

vehicle5X = WorldStateSim.vehicle5.vehicleStateBus.xEast.Data;
vehicle5Y = WorldStateSim.vehicle5.vehicleStateBus.yNorth.Data;

close all;
% 
% figure; hold on;
% plot(vehicle1T, vehicle1X);
% plot(vehicle1T, vehicle2X);


figure; hold on;
dist_2_to_1 = ( (vehicle2X - vehicle1X).^2 + (vehicle2Y - vehicle1Y).^2 ).^.5;
plot(vehicle1T, dist_2_to_1);

figure; hold on;
dist_3_to_2 = ( (vehicle3X - vehicle2X).^2 + (vehicle3Y - vehicle2Y).^2 ).^.5;
plot(vehicle1T, dist_3_to_2);

figure; hold on;
dist_4_to_3 = ( (vehicle4X - vehicle3X).^2 + (vehicle4Y - vehicle3Y).^2 ).^.5;
plot(vehicle1T, dist_4_to_3);

figure; hold on;
dist_5_to_4 = ( (vehicle5X - vehicle4X).^2 + (vehicle5Y - vehicle4Y).^2 ).^.5;
plot(vehicle1T, dist_5_to_4);

figure; hold on;
plot(laneDataENU(:,1), laneDataENU(:,2), 'k--')
plot(laneDataENU(:,3), laneDataENU(:,4), 'k--')
plot(laneDataENU(:,5), laneDataENU(:,6), 'k--')
plot(vehicle1X, vehicle1Y)
plot(vehicle2X, vehicle2Y)
plot(vehicle3X, vehicle3Y)
plot(vehicle4X, vehicle4Y)
plot(vehicle5X, vehicle5Y)
