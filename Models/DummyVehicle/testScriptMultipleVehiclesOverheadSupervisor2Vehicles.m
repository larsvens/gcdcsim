
vehicle1X = WorldStateSim.vehicle1.vehicleStateBus.xEast.Data;
vehicle1Y = WorldStateSim.vehicle1.vehicleStateBus.yNorth.Data;
vehicle1T = WorldStateSim.vehicle1.vehicleStateBus.yNorth.Time;

vehicle2X = WorldStateSim.vehicle2.vehicleStateBus.xEast.Data;
vehicle2Y = WorldStateSim.vehicle2.vehicleStateBus.yNorth.Data;

close all;
% 
% figure; hold on;
% plot(vehicle1T, vehicle1X);
% plot(vehicle1T, vehicle2X);


% figure; hold on;
% dist_2_to_1 = ( (vehicle2X - vehicle1X).^2 + (vehicle2Y - vehicle1Y).^2 ).^.5;
% plot(vehicle1T, dist_2_to_1);
% 
% figure; hold on;
% dist_3_to_2 = ( (vehicle3X - vehicle2X).^2 + (vehicle3Y - vehicle2Y).^2 ).^.5;
% plot(vehicle1T, dist_3_to_2);
% 
% figure; hold on;
% dist_4_to_3 = ( (vehicle4X - vehicle3X).^2 + (vehicle4Y - vehicle3Y).^2 ).^.5;
% plot(vehicle1T, dist_4_to_3);
% 
% figure; hold on;
% dist_5_to_4 = ( (vehicle5X - vehicle4X).^2 + (vehicle5Y - vehicle4Y).^2 ).^.5;
% plot(vehicle1T, dist_5_to_4);

figure; hold on;
plot(laneDataENU(:,1), laneDataENU(:,2), 'k--')
plot(laneDataENU(:,3), laneDataENU(:,4), 'k--')
plot(laneDataENU(:,5), laneDataENU(:,6), 'k--')

h1 = plot(vehicle1X(1), vehicle1Y(1), 'yo');
h_text_1 = text(vehicle1X(1), vehicle1Y(1), '1');
h2 = plot(vehicle2X(1), vehicle2Y(1), 'mo');
h_text_2 = text(vehicle2X(1), vehicle2Y(1), '2');


h_q_ego = quiver(0, 0, 0, 0);

windowSize = 10;

for i = 2:2:length(vehicle1X)-1

    h1.XData = vehicle1X(i); h1.YData = vehicle1Y(i);
    h_text_1.Position = [vehicle1X(i) vehicle1Y(i) 0];
    h2.XData = vehicle2X(i); h2.YData = vehicle2Y(i);
    h_text_2.Position = [vehicle2X(i) vehicle2Y(i) 0];
     
    
    otherXEast = WorldStateSim.vehicle5.vehicleStateBus.xEast.Data(i);
    otherYNorth = WorldStateSim.vehicle5.vehicleStateBus.yNorth.Data(i);
    otherHeading = WorldStateSim.vehicle5.vehicleStateBus.heading.Data(i); 
    otherVx = 0;
    otherVy = 0;

            
    xPositions = [vehicle1X(i), vehicle2X(i)];
    yPositions = [vehicle1Y(i), vehicle2Y(i)];
    
    
    
    centerMassX = mean(xPositions);
    centerMassY = mean(yPositions);
    
    maxXDeviation = max(abs(xPositions - centerMassX));
    maxYDeviation = max(abs(yPositions - centerMassY));
    
    if maxXDeviation > maxYDeviation
        windowSize = maxXDeviation;
    else
        windowSize = maxYDeviation;
    end
    
    windowSize = 1.2*windowSize;
    
    axis([centerMassX-windowSize centerMassX+windowSize centerMassY-windowSize centerMassY+windowSize])
    pause(0.01)
    
end




