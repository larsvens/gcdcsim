function [xEast, yNorth, heading, vx, vy, yawRate, ax, ay, closestIndex] = ...
    gcdcDummyVehicleFunction(vehicleId,...
    initialVelocity, initialPosition, desiredVelocity, ...
    vehicleToFollow, distanceToFollow, rightLane,...
    currentTime, laneDataENU, worldState)
%dummyVehicleFunction This is a simulink block function which will simulate
%a vehicle on a given trajectory, as defined by the laneDataENU info. The
%user can specify the vehicle velocity, its initial position and its lane.
%The vehicle will then be placed in the correct trajectory position for
%the current simulation time.

persistent travelledDistance
persistent lastIndex
persistent iterations
persistent prevState

searchRange = 100;

if isempty(travelledDistance)
    
    travelledDistance = cell(5, 1);
    
end


if isempty(travelledDistance{vehicleId})   

    deltas = ((diff(laneDataENU(:,1)).^2) + (diff(laneDataENU(:,2)).^2)).^0.5;
    travelledDistance{vehicleId} = [0; cumsum(deltas)];
    lastIndex = 1;
    searchRange = length(laneDataENU)-100;
    iterations.vehicleId = 0;
    
    prevState.t = 0;
    
    prevState.x = 0;
    prevState.y = 0;
    prevState.yaw = 0;
    
    prevState.vx = 0;
    prevState.vy = 0;
    prevState.yawRate = 0;
        
    prevState.ax = 0;
    prevState.ay = 0;
    
    prevState.firstIteration = true;
    prevState.secondIteration = true;
    

end

iterations = iterations + 1;

if vehicleToFollow ~= -1
    
    otherVehicleXEast = worldState.vehicle1.vehicleStateBus.xEast;
    otherVehicleYNorth = worldState.vehicle1.vehicleStateBus.yNorth;
    
    distToOtherCar = ((otherVehicleXEast - prevState.x)^2 + ...
        (otherVehicleYNorth - prevState.y)^2)^.5;
    
    distError = distToOtherCar - distanceToFollow;
    
    desiredVelocity = desiredVelocity + 10*distError;
    
    
end


currentVehicleTravelledDistance = initialPosition + currentTime*desiredVelocity;

if lastIndex+searchRange > length(travelledDistance{vehicleId})
    
    searchRange = length(travelledDistance{vehicleId}) - lastIndex;
    
end

distanceToTrajPoints = travelledDistance{vehicleId}(lastIndex:lastIndex+searchRange) - currentVehicleTravelledDistance;
[~,closestIndex] = min( distanceToTrajPoints.^2 );
closestIndex = closestIndex + lastIndex - 1;

lastIndex = closestIndex;

if closestIndex >= length(travelledDistance{vehicleId})-1
    
    xEast = laneDataENU(end,1);
    yNorth = laneDataENU(end,2);

    vx = 0;
    vy = 0;
    yawRate = 0;
    
    ax = 0;
    ay = 0;
    
    deltaX = laneDataENU(end,1)-laneDataENU(end-1,1);
    deltaY = laneDataENU(end,2)-laneDataENU(end-1,2);
    heading = atan2(deltaY, deltaX);
    
    disp('Dummy vehicle at end of trajectory')
        
    return;
    
end

if closestIndex == 1
    
    xEast = laneDataENU(1,1);
    yNorth = laneDataENU(1,2);

    vx = 0;
    vy = 0;
    yawRate = 0;
    
    ax = 0;
    ay = 0;
    
    deltaX = laneDataENU(2,1)-laneDataENU(1,1);
    deltaY = laneDataENU(2,2)-laneDataENU(1,2);
    heading = atan2(deltaY, deltaX);
    
    disp('Dummy vehicle at start of trajectory')
        
    return;
    
end


if currentVehicleTravelledDistance > travelledDistance{vehicleId}(closestIndex)

    [ interpolatedXY ] = interpolateTrajectoryPoints( ...
    travelledDistance{vehicleId}(closestIndex), currentVehicleTravelledDistance, travelledDistance{vehicleId}(closestIndex+1), ...
    [laneDataENU(closestIndex,1), laneDataENU(closestIndex,2)], ...
    [laneDataENU(closestIndex+1,1), laneDataENU(closestIndex+1,2) ]);

else
   
    [ interpolatedXY ] = interpolateTrajectoryPoints( ...
    travelledDistance{vehicleId}(closestIndex-1), currentVehicleTravelledDistance, travelledDistance{vehicleId}(closestIndex), ...
    [laneDataENU(closestIndex-1,1), laneDataENU(closestIndex-1,2)], ...
    [laneDataENU(closestIndex,1), laneDataENU(closestIndex,2) ]);

end

deltaX = laneDataENU(closestIndex+1,1)-laneDataENU(closestIndex-1,1);
deltaY = laneDataENU(closestIndex+1,2)-laneDataENU(closestIndex-1,2);

heading = atan2(deltaY, deltaX);

xEast = interpolatedXY(1);
yNorth = interpolatedXY(2);

deltaT = currentTime - prevState.t; % = 0.02

if prevState.firstIteration
    
    vx = 0;
    vy = 0;
    yawRate = 0;
    
    ax = 0;
    ay = 0;
    
    prevState.firstIteration = false;
        
else
    
    % Compute the velocity along xEast and yNorth
    vxEast = (xEast - prevState.x)/deltaT;
    vyNorth = (yNorth - prevState.y)/deltaT;
    yawRate = (heading - prevState.yaw)/deltaT;
    
    % Convert velocity to ego referential velocities
    vx = vxEast*cos(heading) + vyNorth*sin(heading);
    vy = vxEast*cos(heading+pi/2) + vyNorth*sin(heading+pi/2);
    
    if prevState.secondIteration
    
        ax = 0;
        ay = 0;
        
        prevState.secondIteration = false;
        
    else
        
        ax = (vx - prevState.vx)/deltaT;
        ay = (vy - prevState.vy)/deltaT;        
        
    end
    
end

prevState.t = currentTime;
    
prevState.x = xEast;
prevState.y = yNorth;
prevState.yaw = heading;

prevState.vx = vx;
prevState.vy = vy;
prevState.yawRate = yawRate;

prevState.ax = ax;
prevState.ay = ay;

end

