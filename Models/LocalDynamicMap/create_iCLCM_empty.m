function [ v2v_iCLCM ] = create_iCLCM_empty( )
%IS_CAM_MESSAGE_VALID Summary of this function goes here

    v2v_iCLCM = struct(...
    'StationIdXpc', 0, ...
    'VehicleRearAxleLocationXpc', 2,...
    'CtrlTypeXpc', 2,...
    'DesLongAccXpc', 3,...
    'TimeDelayXpc', 1,...
    'CruiseSpeedXpc', 10,...
    'VehRespoTimeCnstXpc', 1,...
    'VehRespoTimeDelayXpc', 1,...
    'ParticipantsReadyXpc', 1,...
    'StartScenarioXpc', 0,...
    'EoSXpc', 0,...
    'MioIdXpc', 0,...
    'MioRangeXpc', 0,...
    'MioBearingXpc', 0,...
    'MioRangeRateXpc', 0,... %Later
    'LanePosXpc', 0,...
    'ForwardIdXpc', 0,...
    'BackwardIdXpc', 0,...
    'PairAckFlagXpc', 0,...
    'MergeReqFlagXpc', 0,...
    'STOMXpc', 0,...
    'MergingFlagXpc', 0,...
    'TailVehFlagXpc', 0,...
    'HeadVehFlagXpc', 0,...
    'PlatoonIdXpc', 0,...
    'TraveledDistCZXpc', 0,...
    'IntentionXpc', 0,...
    'IntersVehCntrXpc', 0);

end

