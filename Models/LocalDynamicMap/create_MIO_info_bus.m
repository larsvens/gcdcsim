function [ MIO_info_bus ] = create_MIO_info_bus(...
    v2v_sorted_by_vehicle, MIO_id, tracked_vehicles)
%IS_CAM_MESSAGE_VALID Summary of this function goes here
%   Using an index iterator between 1 and 15, it will return the correct
%   Bus message.
    
    if MIO_id == -1
        
        MIO_info_bus.valid = false;
        MIO_info_bus.longitudinal_distance = 0;
        MIO_info_bus.bearing_angle_radians = 0;
        MIO_info_bus.iCLCM = create_iCLCM_empty( );
        MIO_info_bus.CAM = create_CAM_empty( );
        MIO_info_bus.x_local = -99;
        MIO_info_bus.y_local = -99;
        MIO_info_bus.yaw_local = -99;
        
    else
        MIO_info_bus.valid = false;
        
        longitudinal_distance = -1;
        bearing_angle_radians = -1;
        x_local = -99;
        y_local = -99;
        yaw_local = -99;
        
        for it = 1:length(tracked_vehicles)
            
            vehicle_id = tracked_vehicles(it).id;
            
            if vehicle_id == MIO_id
                
                MIO_info_bus.valid = true;
                
                longitudinal_distance = tracked_vehicles(it).x_position;
                bearing_angle_radians = atan2(...
                    tracked_vehicles(it).y_position,...
                    tracked_vehicles(it).x_position);
                x_local = tracked_vehicles(it).x_position;
                y_local = tracked_vehicles(it).y_position;
                yaw_local = tracked_vehicles(it).yaw;
                
                break;
                
            end
            
        end
        
%         disp([longitudinal_distance (180/pi)*bearing_angle_radians])
        
        MIO_info_bus.longitudinal_distance = longitudinal_distance;
        MIO_info_bus.bearing_angle_radians = bearing_angle_radians;
%         MIO_info_bus.iCLCM = get_iCLCM_bus_iterator( v2v_sorted_by_vehicle, MIO_id, ...
%             GLOBAL_vehicle_1_id, GLOBAL_vehicle_2_id, GLOBAL_vehicle_3_id, GLOBAL_vehicle_4_id, GLOBAL_vehicle_5_id);
%         MIO_info_bus.CAM = get_cam_bus_iterator( v2v_sorted_by_vehicle, MIO_id, ...
%             GLOBAL_vehicle_1_id, GLOBAL_vehicle_2_id, GLOBAL_vehicle_3_id, GLOBAL_vehicle_4_id, GLOBAL_vehicle_5_id);
        MIO_info_bus.iCLCM = get_iCLCM_bus_by_id( v2v_sorted_by_vehicle, MIO_id);
        MIO_info_bus.CAM = get_cam_bus_by_id( v2v_sorted_by_vehicle, MIO_id);
        MIO_info_bus.x_local = x_local;
        MIO_info_bus.y_local = y_local;
        MIO_info_bus.yaw_local = yaw_local;
        
    end

end

