function [ is_valid ] = is_iCLCM_bus_valid( iCMCL_bus )
%IS_CAM_MESSAGE_VALID Summary of this function goes here
%   Returns true if the iCMCL bus has info on it, i.e. a message, 
%   or false otherwise

    if iCMCL_bus.StationIdXpc == 0
        
        % Id is zero (not allowed), the bus has no message
        is_valid = 0;
        
    else
        
        % Id is not zero, the bus has a message in it
        is_valid = 1;
        
    end


end

