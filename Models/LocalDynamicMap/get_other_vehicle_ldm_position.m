function [ output_args ] = get_other_vehicle_ldm_position( ego_lat, ego_lon,...
    other_lat, other_lon, GLOBAL_ECEFg0, GLOBAL_ECEFr0 )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


other_gps = [other_lat, other_lon, 0]'*(pi/180);

temp_1 = geodeticECEF2rectangularECEF(other_gps);
other_ENU = rectangularECEF2ENU(GLOBAL_ECEFg0, GLOBAL_ECEFr0,temp_1);

my_gps = [ego_lat, ego_lon, 0]'*(pi/180);

temp_2 = geodeticECEF2rectangularECEF(my_gps);
ego_ENU = rectangularECEF2ENU(GLOBAL_ECEFg0, GLOBAL_ECEFr0,temp_2);



end

