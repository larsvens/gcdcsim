load('simlog1')

WorldStateSim = simlog1.WorldState;

vehicle1X = WorldStateSim.vehicle1.vehicleStateBus.xEast.Data;
vehicle1Y = WorldStateSim.vehicle1.vehicleStateBus.yNorth.Data;
vehicle1T = WorldStateSim.vehicle1.vehicleStateBus.yNorth.Time;

vehicle2X = WorldStateSim.vehicle2.vehicleStateBus.xEast.Data;
vehicle2Y = WorldStateSim.vehicle2.vehicleStateBus.yNorth.Data;

vehicle3X = WorldStateSim.vehicle3.vehicleStateBus.xEast.Data;
vehicle3Y = WorldStateSim.vehicle3.vehicleStateBus.yNorth.Data;

vehicle4X = WorldStateSim.vehicle4.vehicleStateBus.xEast.Data;
vehicle4Y = WorldStateSim.vehicle4.vehicleStateBus.yNorth.Data;

vehicle5X = WorldStateSim.vehicle5.vehicleStateBus.xEast.Data;
vehicle5Y = WorldStateSim.vehicle5.vehicleStateBus.yNorth.Data;

vehicleEgoX = simlog1.egoVehicleData.xEast.Data(1:end);
vehicleEgoY = simlog1.egoVehicleData.yNorth.Data(1:end);

close all;
% 
% figure; hold on;
% plot(vehicle1T, vehicle1X);
% plot(vehicle1T, vehicle2X);


% figure; hold on;
% dist_2_to_1 = ( (vehicle2X - vehicle1X).^2 + (vehicle2Y - vehicle1Y).^2 ).^.5;
% plot(vehicle1T, dist_2_to_1);
% 
% figure; hold on;
% dist_3_to_2 = ( (vehicle3X - vehicle2X).^2 + (vehicle3Y - vehicle2Y).^2 ).^.5;
% plot(vehicle1T, dist_3_to_2);
% 
% figure; hold on;
% dist_4_to_3 = ( (vehicle4X - vehicle3X).^2 + (vehicle4Y - vehicle3Y).^2 ).^.5;
% plot(vehicle1T, dist_4_to_3);
% 
% figure; hold on;
% dist_5_to_4 = ( (vehicle5X - vehicle4X).^2 + (vehicle5Y - vehicle4Y).^2 ).^.5;
% plot(vehicle1T, dist_5_to_4);

figure; hold on;
% plot(vehicle1X, vehicle1Y)
% plot(vehicle2X, vehicle2Y)
% plot(vehicle3X, vehicle3Y)
% plot(vehicle4X, vehicle4Y)
% plot(vehicle5X, vehicle5Y)

h1 = plot(simlog1.local_dynamic_map_message.object_1.x_position.Data(1),...
    simlog1.local_dynamic_map_message.object_1.y_position.Data(1),...
    'ro');
h2 = plot(simlog1.local_dynamic_map_message.object_2.x_position.Data(1),...
    simlog1.local_dynamic_map_message.object_2.y_position.Data(1),...
    'ro');
h3 = plot(simlog1.local_dynamic_map_message.object_3.x_position.Data(1),...
    simlog1.local_dynamic_map_message.object_3.y_position.Data(1),...
    'ro');
h4 = plot(simlog1.local_dynamic_map_message.object_4.x_position.Data(1),...
    simlog1.local_dynamic_map_message.object_4.y_position.Data(1),...
    'ro');
h5 = plot(simlog1.local_dynamic_map_message.object_5.x_position.Data(1),...
    simlog1.local_dynamic_map_message.object_5.y_position.Data(1),...
    'ro');
h6 = plot(simlog1.local_dynamic_map_message.object_6.x_position.Data(1),...
    simlog1.local_dynamic_map_message.object_6.y_position.Data(1),...
    'ro');
h7 = plot(simlog1.local_dynamic_map_message.object_7.x_position.Data(1),...
    simlog1.local_dynamic_map_message.object_7.y_position.Data(1),...
    'ro');
h8 = plot(simlog1.local_dynamic_map_message.object_8.x_position.Data(1),...
    simlog1.local_dynamic_map_message.object_8.y_position.Data(1),...
    'ro');
% h1 = plot(vehicle1X(1), vehicle1Y(1), 'LineWidth', 2);
% h2 = plot(vehicle2X(1), vehicle2Y(1), 'LineWidth', 2);
% h3 = plot(vehicle3X(1), vehicle3Y(1), 'LineWidth', 2);
% h4 = plot(vehicle4X(1), vehicle4Y(1), 'LineWidth', 2);
% h5 = plot(vehicle5X(1), vehicle5Y(1), 'LineWidth', 2);

windowSize = 10;

quiver(0, 0, 2, 0)

for i = 2:10:10000-1
   
    set(h1, 'XData', simlog1.local_dynamic_map_message.object_1.x_position.Data(i))
    set(h1, 'YData', simlog1.local_dynamic_map_message.object_1.y_position.Data(i))
    if simlog1.local_dynamic_map_message.object_1.yaw.Data(i) == -99
        set(h1, 'Marker', 'o'); set(h1, 'Color', 'r');
    else
        set(h1, 'Marker', 'x'); set(h1, 'Color', 'k');
    end
        
    set(h2, 'XData', simlog1.local_dynamic_map_message.object_2.x_position.Data(i))
    set(h2, 'YData', simlog1.local_dynamic_map_message.object_2.y_position.Data(i))
    if simlog1.local_dynamic_map_message.object_2.yaw.Data(i) == -99
        set(h2, 'Marker', 'o'); set(h2, 'Color', 'r');
    else
        set(h2, 'Marker', 'x'); set(h2, 'Color', 'k');
    end
    
    set(h3, 'XData', simlog1.local_dynamic_map_message.object_3.x_position.Data(i))
    set(h3, 'YData', simlog1.local_dynamic_map_message.object_3.y_position.Data(i))
    if simlog1.local_dynamic_map_message.object_3.yaw.Data(i) == -99
        set(h3, 'Marker', 'o'); set(h3, 'Color', 'r');
    else
        set(h3, 'Marker', 'x'); set(h3, 'Color', 'k');
    end
    
    set(h4, 'XData', simlog1.local_dynamic_map_message.object_4.x_position.Data(i))
    set(h4, 'YData', simlog1.local_dynamic_map_message.object_4.y_position.Data(i))
    if simlog1.local_dynamic_map_message.object_4.yaw.Data(i) == -99
        set(h4, 'Marker', 'o'); set(h4, 'Color', 'r');
    else
        set(h4, 'Marker', 'x'); set(h4, 'Color', 'k');
    end
    
    set(h5, 'XData', simlog1.local_dynamic_map_message.object_5.x_position.Data(i))
    set(h5, 'YData', simlog1.local_dynamic_map_message.object_5.y_position.Data(i))
    if simlog1.local_dynamic_map_message.object_5.yaw.Data(i) == -99
        set(h5, 'Marker', 'o'); set(h5, 'Color', 'r');
    else
        set(h5, 'Marker', 'x'); set(h5, 'Color', 'k');
    end
    
    set(h6, 'XData', simlog1.local_dynamic_map_message.object_6.x_position.Data(i))
    set(h6, 'YData', simlog1.local_dynamic_map_message.object_6.y_position.Data(i))
    if simlog1.local_dynamic_map_message.object_6.yaw.Data(i) == -99
        set(h6, 'Marker', 'o'); set(h6, 'Color', 'r');
    else
        set(h6, 'Marker', 'x'); set(h6, 'Color', 'k');
    end
    
    set(h7, 'XData', simlog1.local_dynamic_map_message.object_7.x_position.Data(i))
    set(h7, 'YData', simlog1.local_dynamic_map_message.object_7.y_position.Data(i))
    if simlog1.local_dynamic_map_message.object_7.yaw.Data(i) == -99
        set(h7, 'Marker', 'o'); set(h7, 'Color', 'r');
    else
        set(h7, 'Marker', 'x'); set(h7, 'Color', 'k');
    end
    
    set(h8, 'XData', simlog1.local_dynamic_map_message.object_8.x_position.Data(i))
    set(h8, 'YData', simlog1.local_dynamic_map_message.object_8.y_position.Data(i))
    if simlog1.local_dynamic_map_message.object_8.yaw.Data(i) == -99
        set(h8, 'Marker', 'o'); set(h8, 'Color', 'r');
    else
        set(h8, 'Marker', 'x'); set(h8, 'Color', 'k');
    end
    
%     set(h1, 'XData', [vehicle1X(i-1), vehicle1X(i+1)])
%     set(h1, 'YData', [vehicle1Y(i-1), vehicle1Y(i+1)])
    
%     set(h2, 'XData', [vehicle2X(i-1), vehicle2X(i+1)])
%     set(h2, 'YData', [vehicle2Y(i-1), vehicle2Y(i+1)])
    
%     set(h3, 'XData', [vehicle3X(i-1), vehicle3X(i+1)])
%     set(h3, 'YData', [vehicle3Y(i-1), vehicle3Y(i+1)])
    
%     set(h4, 'XData', [vehicle4X(i-1), vehicle4X(i+1)])
%     set(h4, 'YData', [vehicle4Y(i-1), vehicle4Y(i+1)])
    
%     set(h5, 'XData', [vehicle5X(i-1), vehicle5X(i+1)])
%     set(h5, 'YData', [vehicle5Y(i-1), vehicle5Y(i+1)])
    
    egoXEast = simlog1.egoVehicleData.xEast.Data(i);
    egoYNorth = simlog1.egoVehicleData.yNorth.Data(i);
    egoHeading = simlog1.egoVehicleData.heading.Data(i);
    egoVx = 0;
    egoVy = 0;
    otherXEast = WorldStateSim.vehicle5.vehicleStateBus.xEast.Data(i);
    otherYNorth = WorldStateSim.vehicle5.vehicleStateBus.yNorth.Data(i);
    otherHeading = WorldStateSim.vehicle5.vehicleStateBus.heading.Data(i); 
    otherVx = 0;
    otherVy = 0;
        
%     [lane, longitudinal_distance, lateral_distance, ...
%     relative_longitudinal_speed, relative_lateral_speed, ...
%     relative_longitudinal_acceleration] = ...
%     get_radar_reading_of_other_vehicle( ...
%     egoXEast, egoYNorth, egoHeading, egoVx, egoVy, ...
%     otherXEast, otherYNorth, otherHeading, otherVx, otherVy);
    
%     disp(lateral_distance)    
            
    xPositions = [vehicleEgoX(i), vehicle1X(i), vehicle2X(i), ...
        vehicle3X(i), vehicle4X(i), vehicle5X(i)];
    yPositions = [vehicleEgoY(i), vehicle1Y(i), vehicle2Y(i), ...
        vehicle3Y(i), vehicle4Y(i), vehicle5Y(i)];
    
    
    
    centerMassX = mean(xPositions);
    centerMassY = mean(yPositions);
    
    maxXDeviation = max(abs(xPositions - centerMassX));
    maxYDeviation = max(abs(yPositions - centerMassY));
    
    if maxXDeviation > maxYDeviation
        windowSize = maxXDeviation;
    else
        windowSize = maxYDeviation;
    end
    
    windowSize = 1.2*windowSize;
    
%     axis([centerMassX-windowSize centerMassX+windowSize centerMassY-windowSize centerMassY+windowSize])
%     axis([-10 60 -20 20])
    axis([-10 500 -100 100])
    drawnow
    pause(0.001)
    
end

