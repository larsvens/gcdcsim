function [ is_valid ] = is_cam_bus_valid( cam_bus, use_timestamp_watchdog, allowed_delay_milliseconds, current_timestamp )
%IS_CAM_MESSAGE_VALID Summary of this function goes here
%   Returns true if the CAM bus has info on it, i.e. a message, 
%   or false otherwise

    if cam_bus.PositionGenerationDeltaTimeXpc == 0
        
        % Time of generation is zero, the bus has no message
        is_valid = 0;
        
    else
        
        if use_timestamp_watchdog
            
            % Use the timestamp to check if message is recent or old
        
            message_delay_ms = current_timestamp - cam_bus.PositionGenerationDeltaTimeXpc;
            
            message_delay_ms = mod(message_delay_ms, 65536);
            
            if message_delay_ms < allowed_delay_milliseconds
                % Delay is within allowed limit, message is valid.
                is_valid = 1;
            else
                % Delay is bigger than allowed, message not valid.
                is_valid = 0;
            end
            
        else
            
            % Time of generation is not zero, the bus has a message in it
            is_valid = 1;
            
        end
        
    end


end

