function [ vehicle_iCLCM_bus ] = get_iCLCM_bus_by_id( v2v_sorted_by_vehicle, id)
%IS_CAM_MESSAGE_VALID Summary of this function goes here
%   Using an index iterator between 1 and 15, it will return the correct
%   Bus message.

    % Dummy Bus
    vehicle_iCLCM_bus = v2v_sorted_by_vehicle.A.iCLCM;

    for it = 1:15        
        
        temp_vehicle_iCLCM_bus = get_iCLCM_bus_iterator( v2v_sorted_by_vehicle, it);
        
        if temp_vehicle_iCLCM_bus.StationIdXpc == id
            
            vehicle_iCLCM_bus = temp_vehicle_iCLCM_bus;
            break;
            
        end
        
        
    end
    
end

