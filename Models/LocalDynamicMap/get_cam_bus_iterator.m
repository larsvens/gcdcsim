function [ vehicle_cam_bus ] = get_cam_bus_iterator( v2v_sorted_by_vehicle, iterator_index)
%IS_CAM_MESSAGE_VALID Summary of this function goes here
%   Using an index iterator between 1 and 15, it will return the correct
%   Bus message.

    vehicle_cam_bus = v2v_sorted_by_vehicle.A.CAM;

    if iterator_index < 1 
       
        vehicle_cam_bus = v2v_sorted_by_vehicle.A.CAM;
        disp('Error: Number smaller than 1!')
        
    end
    
    if iterator_index > 15
       
        vehicle_cam_bus = v2v_sorted_by_vehicle.O.CAM;
        disp('Error: Number bigger than 15!')
        
    end

    switch iterator_index
        case 1
            vehicle_cam_bus = v2v_sorted_by_vehicle.A.CAM;
        case 2
            vehicle_cam_bus = v2v_sorted_by_vehicle.B.CAM;
        case 3
            vehicle_cam_bus = v2v_sorted_by_vehicle.C.CAM;
        case 4
            vehicle_cam_bus = v2v_sorted_by_vehicle.D.CAM;
        case 5
            vehicle_cam_bus = v2v_sorted_by_vehicle.E.CAM;
        case 6
            vehicle_cam_bus = v2v_sorted_by_vehicle.F.CAM;
        case 7
            vehicle_cam_bus = v2v_sorted_by_vehicle.G.CAM;
        case 8
            vehicle_cam_bus = v2v_sorted_by_vehicle.H.CAM;
        case 9
            vehicle_cam_bus = v2v_sorted_by_vehicle.I.CAM;
        case 10
            vehicle_cam_bus = v2v_sorted_by_vehicle.J.CAM;
        case 11
            vehicle_cam_bus = v2v_sorted_by_vehicle.K.CAM;
        case 12
            vehicle_cam_bus = v2v_sorted_by_vehicle.L.CAM;
        case 13
            vehicle_cam_bus = v2v_sorted_by_vehicle.M.CAM;
        case 14
            vehicle_cam_bus = v2v_sorted_by_vehicle.N.CAM;
        case 15
            vehicle_cam_bus = v2v_sorted_by_vehicle.O.CAM;
    end

end

