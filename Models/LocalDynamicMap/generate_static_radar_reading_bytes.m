function [ radar_reading_bytes ] = generate_static_radar_reading_bytes( static_radar_reading )
%CREATE_STATIC_RADAR_READING_BYTES Summary of this function goes here
%   Detailed explanation goes here
           
    radar_reading_bytes = uint8(zeros(8,1));

    % Single precision
    radar_reading_bytes(1:4) = uint8(typecast(swapbytes(single(static_radar_reading.LongDistance)), 'uint8'));
    radar_reading_bytes(5:8) = uint8(typecast(swapbytes(single(static_radar_reading.LateralDistance)), 'uint8'));    
    
end

