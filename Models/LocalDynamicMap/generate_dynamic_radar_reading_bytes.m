function [ radar_reading_bytes ] = generate_dynamic_radar_reading_bytes( dynamic_radar_reading )
%CREATE_STATIC_RADAR_READING_BYTES Summary of this function goes here
%   Detailed explanation goes here
           
    radar_reading_bytes = uint8(zeros(20,1));

    % Single precision
    radar_reading_bytes(1:4) = uint8(typecast(swapbytes(single(dynamic_radar_reading.LongDistance)), 'uint8'));
    radar_reading_bytes(5:8) = uint8(typecast(swapbytes(single(dynamic_radar_reading.LateralDistance)), 'uint8'));  
    radar_reading_bytes(9:12) = uint8(typecast(swapbytes(single(dynamic_radar_reading.RelativeLongSpeed)), 'uint8'));
    radar_reading_bytes(13:16) = uint8(typecast(swapbytes(single(dynamic_radar_reading.RelativeLateralSpeed)), 'uint8'));
    radar_reading_bytes(17:20) = uint8(typecast(swapbytes(single(dynamic_radar_reading.RelLongAcceleration)), 'uint8'));  

    
end

