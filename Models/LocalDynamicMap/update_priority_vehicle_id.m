function [ PRIORITY_VEHICLE_ID, distance_to_vehicle ] = update_priority_vehicle_id...
    ( tracked_vehicles, PRIORITY_VEHICLE_ID_IN)
%IS_CAR_IN_SAME_LANE_FRONT Summary of this function goes here
%   Detailed explanation goes here

PRIORITY_VEHICLE_ID = -1;
distance_to_vehicle = -1;

for it = 1:15
    if tracked_vehicles(it).id == PRIORITY_VEHICLE_ID_IN;
        PRIORITY_VEHICLE_ID = tracked_vehicles(it).id;
        distance_to_vehicle = tracked_vehicles(it).x_position;
        return
    end
end
