function [ left_lane_front ] = ldm_is_car_in_left_lane_front...
    ( x_local_other, y_local_other, ...
    left_boundary, right_boundary, front_boundary, rear_boundary )
%IS_CAR_IN_SAME_LANE_FRONT Summary of this function goes here
%   Detailed explanation goes here

    if x_local_other > front_boundary && x_local_other < 200 && ... 
           y_local_other > left_boundary && y_local_other < 200
%     if x_local_other > 0

        left_lane_front = true;
        
    else
        
        left_lane_front = false;

    end

end

