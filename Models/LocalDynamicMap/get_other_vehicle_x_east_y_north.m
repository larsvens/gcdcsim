function [ x_east_other, y_north_other ] = get_other_vehicle_x_east_y_north( ...
    other_lat, other_lon, other_heading, GLOBAL_ECEFg0, GLOBAL_ECEFr0, use_utm)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if use_utm

    [x_east_other, y_north_other] = deg2utm(other_lat, other_lon);
        
else
    
    other_gps = [other_lat, other_lon, 0]'*(pi/180);

    temp_1 = geodeticECEF2rectangularECEF(other_gps);
    other_ENU = rectangularECEF2ENU(GLOBAL_ECEFg0, GLOBAL_ECEFr0,temp_1);
    x_east_other = other_ENU(1);
    y_north_other = other_ENU(2);
    
    
end

end