function [ vehicle_cam_bus ] = get_cam_bus_by_id( v2v_sorted_by_vehicle, id)
%IS_CAM_MESSAGE_VALID Summary of this function goes here
%   Using an index iterator between 1 and 15, it will return the correct
%   Bus message.

    % Dummy bus
    vehicle_cam_bus = v2v_sorted_by_vehicle.A.CAM;
    
    for it = 1:15
    
        temp_vehicle_cam_bus = get_cam_bus_iterator( v2v_sorted_by_vehicle, it);
        
        if temp_vehicle_cam_bus.StationIdXPC == id
           
            vehicle_cam_bus = temp_vehicle_cam_bus;
            break;
            
        end
        
    end

end

