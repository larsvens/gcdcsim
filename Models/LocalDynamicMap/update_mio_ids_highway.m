function [FWD_MIO_ID, FWD_LATERAL_MIO_ID, BWD_MIO_ID, BWD_LATERAL_MIO_ID] = update_mio_ids_highway...
    ( tracked_vehicles, is_right_lane )
%IS_CAR_IN_SAME_LANE_FRONT Summary of this function goes here
%   Detailed explanation goes here

closest_front_vehicle_dists = [inf; inf]; % First index is closest
closest_front_vehicle_ids = [-1; -1];
closest_front_vehicles_y_local = [0; 0];

closest_rear_vehicle_dists = [inf; inf]; % First index is closest
closest_rear_vehicle_ids = [-1; -1];
closest_rear_vehicles_y_local = [0; 0];

for it = 1:15    
    
    current_vehicle = tracked_vehicles(it);
    
    if ~current_vehicle.valid   
        % No tracked vehicle here, check for next one
        continue        
    end    
  
    x_local_other = current_vehicle.x_position;
    y_local_other = current_vehicle.y_position;
    
    if x_local_other > 0
        % Vehicle is in front of us
        
        if x_local_other < closest_front_vehicle_dists(1)
            % Current vehicle is closer than the current closest vehicle
            
            % The previous closest vehicle becomes the second closest vehicle
            closest_front_vehicle_dists(2) = closest_front_vehicle_dists(1);
            closest_front_vehicle_ids(2) = closest_front_vehicle_ids(1);
            closest_front_vehicles_y_local(2) = closest_front_vehicles_y_local(1);
            
            % The closest vehicle is the current vehicle in front of us
            closest_front_vehicle_dists(1) = x_local_other;
            closest_front_vehicle_ids(1) = current_vehicle.id;
            closest_front_vehicles_y_local(1) = y_local_other;
            
        else
            
            if x_local_other < closest_front_vehicle_dists(2)
                % Current vehicle is farther away than the current closest
                % vehicle, but closer than the second closest vehicle

                closest_front_vehicle_dists(2) = x_local_other;
                closest_front_vehicle_ids(2) = current_vehicle.id;
                closest_front_vehicles_y_local(2) = y_local_other;

            end
            
            
        end
        
    else
        % Vehicle is behind us
        if abs(x_local_other) < closest_rear_vehicle_dists(1)
            % The current vehicle is behind us and closer than the closest

            % Previous closest vehicle becomes the second closest vehicle
            closest_rear_vehicle_dists(2) = closest_rear_vehicle_dists(1);
            closest_rear_vehicle_ids(2) = closest_rear_vehicle_ids(1);
            closest_rear_vehicles_y_local(2) = closest_rear_vehicles_y_local(1);

            % The current vehicle becomes the closest one
            closest_rear_vehicle_dists(1) =  abs(x_local_other);
            closest_rear_vehicle_ids(1) = current_vehicle.id;
            closest_rear_vehicles_y_local(1) = y_local_other;
            
            
        else
                        
            if abs(x_local_other) < closest_rear_vehicle_dists(2)
                % The current vehicle is farther away than the closest but
                % closer than the second closest vehicle

                % The current vehicle becomes the second closest one
                closest_rear_vehicle_dists(2) =  abs(x_local_other);
                closest_rear_vehicle_ids(2) = current_vehicle.id;
                closest_rear_vehicles_y_local(2) = y_local_other;

            end
            
        end
            
            
            
            
    end
        
end


if is_right_lane
    % We are in the right lane
    % Need to assign laterals as vehicles to the left
    
    if closest_front_vehicles_y_local(1) > closest_front_vehicles_y_local(2)
        % The closest front vehicle is to the left of the second closest
        % front vehicle
        
        FWD_MIO_ID = closest_front_vehicle_ids(2);  
        FWD_LATERAL_MIO_ID = closest_front_vehicle_ids(1);   
        
    else
        % The closest front vehicle is to the right of the second closest
        % front vehicle
        
        FWD_MIO_ID = closest_front_vehicle_ids(1);
        FWD_LATERAL_MIO_ID = closest_front_vehicle_ids(2);
        
    end
    
    if closest_rear_vehicles_y_local(1) > closest_rear_vehicles_y_local(2)
        % The closest rear vehicle is to the left of the second closest
        % front vehicle
        
        BWD_MIO_ID = closest_rear_vehicle_ids(2);  
        BWD_LATERAL_MIO_ID = closest_rear_vehicle_ids(1);   
        
    else
        % The closest rear vehicle is to the right of the second closest
        % front vehicle
        
        BWD_MIO_ID = closest_rear_vehicle_ids(1);
        BWD_LATERAL_MIO_ID = closest_rear_vehicle_ids(2);
        
    end
    
else
    % We are in the left lane    
    % Need to assign lateral vehicles as right mios
    
    if closest_front_vehicles_y_local(1) > closest_front_vehicles_y_local(2)
        % Closest front vehicle is to the left of the second closest
       
        % The vehicle to the left will be our FWD_MIO and the vehicle to
        % the right will be our FWD_RIGHT_MIO
        FWD_MIO_ID = closest_front_vehicle_ids(1);
        FWD_LATERAL_MIO_ID = closest_front_vehicle_ids(2);
                
    else
        % Closest vehicle is to the right of the second closest
        % The closest vehicle will be the FWD_RIGHT_MIO
        
        % The vehicle to the left will be our FWD_MIO and the vehicle to
        % the right will be our FWD_RIGHT_MIO
        FWD_MIO_ID = closest_front_vehicle_ids(2);
        FWD_LATERAL_MIO_ID = closest_front_vehicle_ids(1);
        
    end
    
     if closest_rear_vehicles_y_local(1) > closest_rear_vehicles_y_local(2)
        % Closest rear vehicle is to the left of the second closest
       
        % The vehicle to the left will be our BWD_MIO and the vehicle to
        % the right will be our BWD_RIGHT_MIO
        BWD_MIO_ID = closest_rear_vehicle_ids(1);
        BWD_LATERAL_MIO_ID = closest_rear_vehicle_ids(2);
                
    else
        % Closest vehicle is to the right of the second closest
        % The closest vehicle will be the FWD_RIGHT_MIO
        
        % The vehicle to the left will be our FWD_MIO and the vehicle to
        % the right will be our FWD_RIGHT_MIO
        BWD_MIO_ID = closest_rear_vehicle_ids(2);
        BWD_LATERAL_MIO_ID = closest_rear_vehicle_ids(1);
        
    end
    
    
    
end
    
%     if ldm_is_car_in_same_lane_front(x_local_other, y_local_other,...
%     left_boundary, right_boundary, front_boundary, rear_boundary )     
%         
%         if x_local_other < closest_car_distance
%             
%             FWD_MIO_ID = current_vehicle.id;
%             closest_car_distance = x_local_other;
%             
%         end
%         
%     end

end

