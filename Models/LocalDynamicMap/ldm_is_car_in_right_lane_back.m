function [ right_lane_back ] = ldm_is_car_in_right_lane_back...
    ( x_local_other, y_local_other, ...
    left_boundary, right_boundary, front_boundary, rear_boundary )
%IS_CAR_IN_SAME_LANE_FRONT Summary of this function goes here
%   Detailed explanation goes here

    if x_local_other < rear_boundary && x_local_other > -40 && ... 
           y_local_other < right_boundary && y_local_other > -7.5
%     if x_local_other < 0
        
        right_lane_back = true;
        
    else
        
        right_lane_back = false;

    end


end

