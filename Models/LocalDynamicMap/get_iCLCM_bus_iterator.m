function [ vehicle_iCLCM_bus ] = get_iCLCM_bus_iterator( v2v_sorted_by_vehicle, iterator_index)
%IS_CAM_MESSAGE_VALID Summary of this function goes here
%   Using an index iterator between 1 and 15, it will return the correct
%   Bus message.

    vehicle_iCLCM_bus = v2v_sorted_by_vehicle.A.iCLCM;

    if iterator_index < 1 
       
        vehicle_iCLCM_bus = v2v_sorted_by_vehicle.A.iCLCM;
        disp('Error: Number smaller than 1!')
        
    end
    
    if iterator_index > 15
       
        vehicle_iCLCM_bus = v2v_sorted_by_vehicle.O.iCLCM;
        disp('Error: Number bigger than 15!')
        
    end

    switch iterator_index
        case 1
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.A.iCLCM;
        case 2
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.B.iCLCM;
        case 3
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.C.iCLCM;
        case 4
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.D.iCLCM;
        case 5
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.E.iCLCM;
        case 6
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.F.iCLCM;
        case 7
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.G.iCLCM;
        case 8
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.H.iCLCM;
        case 9
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.I.iCLCM;
        case 10
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.J.iCLCM;
        case 11
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.K.iCLCM;
        case 12
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.L.iCLCM;
        case 13
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.M.iCLCM;
        case 14
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.N.iCLCM;
        case 15
            vehicle_iCLCM_bus = v2v_sorted_by_vehicle.O.iCLCM;
    end

end

