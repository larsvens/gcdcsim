function [ x_local_other, y_local_other, yaw_local_other ] = get_other_vehicle_ldm_pose( ego_lat, ego_lon, ego_heading,...
    other_lat, other_lon, other_heading, GLOBAL_ECEFg0, GLOBAL_ECEFr0, use_utm)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if use_utm

    [x_east_other, y_north_other] = deg2utm(other_lat, other_lon);
    [x_east_ego, y_north_ego] = deg2utm(ego_lat, ego_lon);
    
else
    
    other_gps = [other_lat, other_lon, 0]'*(pi/180);

    temp_1 = geodeticECEF2rectangularECEF(other_gps);
    other_ENU = rectangularECEF2ENU(GLOBAL_ECEFg0, GLOBAL_ECEFr0,temp_1);
    x_east_other = other_ENU(1);
    y_north_other = other_ENU(2);

    my_gps = [ego_lat, ego_lon, 0]'*(pi/180);

    temp_2 = geodeticECEF2rectangularECEF(my_gps);
    ego_ENU = rectangularECEF2ENU(GLOBAL_ECEFg0, GLOBAL_ECEFr0,temp_2);
    x_east_ego = ego_ENU(1);
    y_north_ego = ego_ENU(2);
    
end

x_east_local_unrotated = x_east_other - x_east_ego;
y_north_local_unrotated = y_north_other - y_north_ego;

ego_yaw = (pi/180)*(90 - ego_heading);

x_local_other = cos(-ego_yaw)*x_east_local_unrotated - sin(-ego_yaw)*y_north_local_unrotated;
y_local_other = sin(-ego_yaw)*x_east_local_unrotated + cos(-ego_yaw)*y_north_local_unrotated;

other_yaw =  (pi/180)*(90 - other_heading);

yaw_local_other = other_yaw - ego_yaw;

if yaw_local_other > pi
    yaw_local_other = yaw_local_other - 2*pi;
end

if yaw_local_other < -pi
    yaw_local_other = yaw_local_other + 2*pi;
end

end