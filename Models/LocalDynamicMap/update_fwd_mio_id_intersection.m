function [ FWD_MIO_ID ] = update_fwd_mio_id_intersection( tracked_vehicles )
%IS_CAR_IN_SAME_LANE_FRONT Summary of this function goes here
%   Detailed explanation goes here

closest_car_distance = inf;
FWD_MIO_ID = -1;

for it = 1:15    
    
    current_vehicle = tracked_vehicles(it);
    
    if ~current_vehicle.valid   
        % No tracked vehicle here, check for next one
        continue        
    end    
  
    x_local_other = current_vehicle.x_position;
    y_local_other = current_vehicle.y_position;
    
    current_vehicle_dist = hypot(x_local_other, y_local_other);
        
    if current_vehicle_dist < closest_car_distance

        FWD_MIO_ID = current_vehicle.id;
        closest_car_distance = x_local_other;

    end
       
end

