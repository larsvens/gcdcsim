function [ BWD_MIO_RIGHT_ID ] = update_bwd_mio_right_id...
    ( tracked_vehicles, ...
    left_boundary, right_boundary, front_boundary, rear_boundary )
%IS_CAR_IN_SAME_LANE_FRONT Summary of this function goes here
%   Detailed explanation goes here

closest_car_distance = -inf;
BWD_MIO_RIGHT_ID = -1;

for it = 1:15
        
    current_vehicle = tracked_vehicles(it);
    
    if ~current_vehicle.valid   
        % No tracked vehicle here, check for next one
        continue        
    end
      
    x_local_other = current_vehicle.x_position;
    y_local_other = current_vehicle.y_position;
    
    if ldm_is_car_in_right_lane_back(x_local_other, y_local_other,...
    left_boundary, right_boundary, front_boundary, rear_boundary )     
        
        if x_local_other > closest_car_distance
            
            BWD_MIO_RIGHT_ID = current_vehicle.id;
            closest_car_distance = x_local_other;
                        
        end
        

    end


end

