/* Produced by CVXGEN, 2014-07-10 12:21:32 -0400.  */
/* CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2012 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: testsolver.c. */
/* Description: Basic test harness for solver.c. */
#include "solver.h"
Vars vars;
Params params;
Workspace work;
Settings settings;

void MPCLat(double *optval, double *res, int *num, int *conv, double *Hess, double *f, double *A_ineq, double *b_ineq, double *ub, double *lb) {    
  int num_iters = 0;
  int i = 0;

  set_defaults();
  setup_indexing();

  for (i = 0; i < (2*20*2*20); i++) 
  {
	  params.Hess[i] = Hess[i]; // *(H + i);
  }

  for (i = 0; i < (2*20); i++) 
  {
	  params.f[i] = f[i];
  }

  for (i = 0; i < (4*20*2*20); i++) 
  {
	  params.A_ineq[i] = A_ineq[i];
  }

  for (i = 0; i < (4*20); i++) 
  {
	  params.b_ineq[i] = b_ineq[i];
  }
  
  for (i = 0; i < (2*20); i++) 
  {
	  params.ub[i] = ub[i];
  }
  
  for (i = 0; i < (2*20); i++) 
  {
	  params.lb[i] = lb[i];
  }

  num_iters = solve();
  
  //return variables
  *optval = work.optval;
  for (i = 0; i < (2*20); i++) 
  {
	  res[i] = vars.u[i];
  } 
  *num = num_iters;
  *conv = work.converged;
  
  return;
}