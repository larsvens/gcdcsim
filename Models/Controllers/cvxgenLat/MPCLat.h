/* Produced by CVXGEN, 2014-07-10 12:21:32 -0400.  */
/* CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2012 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */
extern void MPCLat(double *optval, double *res, int *num, int *conv, double *Hess, double *f, double *A_ineq, double *b_ineq, double *ub, double *lb);