data_vx=Results.Ctrl_Log.data(109:end,4);
data_Tor=Results.Ctrl_Log.data(109:end,6);
data_yawrate=Results.Ctrl_Log.data(109:end,9);



vx=data_vx(1);
vy=0;
yawrate=0;
vx_vec=zeros(length(data_vx)+1,1);
vx_vec(1)=vx;
for i=1:length(data_vx)
    [vx_vec(i+1),vy,yawrate]=EgoVelsPred(vx_vec(i), vy, data_yawrate(i), 0,0,data_Tor(i),0.07);
end
figure;
plot(data_vx);
hold on;
plot(vx_vec,'Color','r')
% figure;
% plot(data_yawrate)