function [d_ref,v_ref,a_ref]=refslong(xref,yref,yawref,dt,delta_ref,vx,H,a_max,a_min,time_pred)
%     persistent init_flag mat_aux;
%     if isempty(init_flag)
%         init_flag = 1;
%         mat_aux=-eye(H+2);
%         
%         for i=1:H+1
%             mat_aux(i,i+1)=1; %it will make the difference between the heading of the next point and the current one, which will give the yaw ref that the car should have on its frame
%         end
%     end
%     yaw_dif=mat_aux*yawref;
    xref(1)
    yref(1)
    d_ref=zeros(H+1,1);
    v_ref=d_ref;
    a_ref=zeros(H,1);
%     pos_virt=rot(yawref(1))*[xref(1);yref(1)];
%     d_ref(1)=pos_virt(1);
    pos_virt=rot(yawref(2))*[(xref(2)-xref(1));(yref(2)-yref(1))];
    d_ref(1)=pos_virt(1);%d_ref(1)+pos_virt(1);%dref(1) estava aqui por causa das linhas comentadas em cima
    v_ref(1)=d_ref(1)/dt;
    
    i=2;
    pos_virt=rot(yawref(i+1))*[(xref(i+1)-xref(i));(yref(i+1)-yref(i))];
    d_ref(i)=pos_virt(1);
    v_ref(i)=d_ref(i)/dt;
    a_ref(i-1)=(v_ref(i)-v_ref(i-1))/(dt*[1/2 1/2]*cos([delta_ref(2*i-3);delta_ref(2*i-2)]));
    
    for i=3:H+1
%         R=(f+r)/tan(yaw_dif(i));
        pos_virt=rot(yawref(i+1))*[(xref(i+1)-xref(i));(yref(i+1)-yref(i))];
        d_ref(i)=pos_virt(1);
        v_ref(i)=d_ref(i)/time_pred;
        a_ref(i-1)=(v_ref(i)-v_ref(i-1))/(time_pred*[1/2 1/2]*cos([delta_ref(2*i-3);delta_ref(2*i-2)]));
        
%         if(a_ref(i-1)>a_max)
%             a_ref(i-1)=a_max;
%             v_ref(i)=v_ref(i-1)+a_ref(i-1)*dt*[1/2 1/2]*cos([delta_ref(2*i-3);delta_ref(2*i-2)]);
%         elseif(a_ref(i-1)<a_min)
%             a_ref(i-1)=a_min;
%             v_ref(i)=v_ref(i-1)+a_ref(i-1)*dt*[1/2 1/2]*cos([delta_ref(2*i-3);delta_ref(2*i-2)]);
%         end
    end

end