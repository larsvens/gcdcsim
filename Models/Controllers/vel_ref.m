function [vx,vy]=vel_ref(xref,yref,yawref,dt)
    persistent init_flag rot;
    if isempty(init_flag)
        init_flag = 1;
        rot=@(x)[cos(x),sin(x);-sin(x),cos(x)];
    end
    vx=zeros(length(yawref)-1,1);
    vy=vx;
    
    for i=1:length(yawref)-1
        pos_virt=rot(yawref(i+1))*[(xref(i+2)-xref(i+1));(yref(i+2)-yref(i+1))];
        vy(i)=pos_virt(2)/dt;
        vx(i)=pos_virt(1)/dt;
    end

end