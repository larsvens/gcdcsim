/* Produced by CVXGEN, 2016-06-03 10:12:42 -0400.  */
/* CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2012 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: solver_Long.h. */
/* Description: Header file with relevant definitions. */
#ifndef SOLVER_H_LONG
#define SOLVER_H_LONG
/* Uncomment the next line to remove all library dependencies. */
/*#define ZERO_LIBRARY_MODE */
#ifdef MATLAB_MEX_FILE
/* Matlab functions. MATLAB_MEX_FILE_LONG will be defined by the mex compiler. */
/* If you are not using the mex compiler, this functionality will not intrude, */
/* as it will be completely disabled at compile-time. */
#include "mex.h"
#else
#ifndef ZERO_LIBRARY_MODE
#include <stdio.h>
#endif
#endif
/* Space must be allocated somewhere (testsolver.c, csolve.c or your own */
/* program) for the global variables vars_Long, params_Long, work_Long and settings_Long. */
/* At the bottom of this file, they are externed. */
#ifndef ZERO_LIBRARY_MODE
#include <math.h>
#define pm(A, m, n) printmatrix_Long(#A, A, m, n, 1)
#endif
typedef struct Params_t_Long {
  double Hess[441];
  double f[21];
  double A_ineq[1260];
  double b_ineq[60];
  double ub[21];
  double lb[21];
  double maxtrq[21];
  double mintrq[21];
} Params_Long;
typedef struct Vars_t_Long {
  double *u; /* 21 rows. */
} Vars_Long;
typedef struct Workspace_t_Long {
  double h[144];
  double s_inv[144];
  double s_inv_z[144];
  double *b;
  double q[21];
  double rhs[309];
  double x[309];
  double *s;
  double *z;
  double *y;
  double lhs_aff[309];
  double lhs_cc[309];
  double buffer[309];
  double buffer2[309];
  double KKT[2007];
  double L[1698];
  double d[309];
  double v[309];
  double d_inv[309];
  double gap;
  double optval;
  double ineq_resid_squared;
  double eq_resid_squared;
  double block_33[1];
  /* Pre-op symbols. */
  int converged;
} Workspace_Long;
typedef struct Settings_t_Long {
  double resid_tol;
  double eps;
  int max_iters;
  int refine_steps;
  int better_start_Long;
  /* Better start obviates the need for s_init and z_init. */
  double s_init;
  double z_init;
  int verbose;
  /* Show extra details of the iterative refinement steps. */
  int verbose_refinement;
  int debug;
  /* For regularization. Minimum value of abs(D_ii) in the kkt D factor. */
  double kkt_reg;
} Settings_Long;
extern Vars_Long vars_Long;
extern Params_Long params_Long;
extern Workspace_Long work_Long;
extern Settings_Long settings_Long;
/* Function definitions in ldl_Long.c: */
void ldl_solve_Long(double *target, double *var);
void ldl_factor_Long(void);
double check_factorization_Long(void);
void matrix_multiply_Long(double *result, double *source);
double check_residual_Long(double *target, double *multiplicand);
void fill_KKT_Long(void);

/* Function definitions in matrix_support_Long.c: */
void multbymA_Long(double *lhs, double *rhs);
void multbymAT_Long(double *lhs, double *rhs);
void multbymG_Long(double *lhs, double *rhs);
void multbymGT_Long(double *lhs, double *rhs);
void multbyP_Long(double *lhs, double *rhs);
void fillq_Long(void);
void fillh_Long(void);
void fillb_Long(void);
void pre_ops_Long(void);

/* Function definitions in solver_Long.c: */
double eval_gap_Long(void);
void set_defaults_Long(void);
void setup_pointers_Long(void);
void setup_indexing_Long(void);
void set_start_Long(void);
double eval_objv_Long(void);
void fillrhs_aff_Long(void);
void fillrhs_cc_Long(void);
void refine_Long(double *target, double *var);
double calc_ineq_resid_squared_Long(void);
double calc_eq_resid_squared_Long(void);
void better_start_Long(void);
void fillrhs_start_Long(void);
long solve_Long(void);

/* Function definitions in testsolver.c: */
/*int main(int argc, char **argv);
void load_default_data(void);*/

/* Function definitions in util_Long.c: */
void tic_Long(void);
float toc_Long(void);
float tocq_Long(void);
void printmatrix_Long(char *name, double *A, int m, int n, int sparse);
double unif_Long(double lower, double upper);
float ran1_Long(long*idum, int reset);
float randn_internal_Long(long *idum, int reset);
double randn_Long(void);
void reset_rand_Long(void);

#endif
