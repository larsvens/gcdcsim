/* Produced by CVXGEN, 2016-06-03 10:12:42 -0400.  */
/* CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2012 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: util_Long.c. */
/* Description: Common utility file for all cvxgen code. */
#include "solver_Long.h"
#include <time.h>
#include <stdlib.h>
#include <math.h>
long global_seed_Long = 1;
static clock_t tic_timestart_Long;
void tic_Long(void) {
  tic_timestart_Long = clock();
}
float toc_Long(void) {
  clock_t tic_timestop;
  tic_timestop = clock();
  printf("time: %8.2f.\n", (float)(tic_timestop - tic_timestart_Long) / CLOCKS_PER_SEC);
  return (float)(tic_timestop - tic_timestart_Long) / CLOCKS_PER_SEC;
}
float tocq_Long(void) {
  clock_t tic_timestop;
  tic_timestop = clock();
  return (float)(tic_timestop - tic_timestart_Long) / CLOCKS_PER_SEC;
}
void printmatrix_Long(char *name, double *A, int m, int n, int sparse) {
  int i, j;
  printf("%s = [...\n", name);
  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++)
      if ((sparse == 1) && (A[i+j*m] == 0))
        printf("         0");
      else
        printf("  % 9.4f", A[i+j*m]);
    printf(",\n");
  }
  printf("];\n");
}
double unif_Long(double lower, double upper) {
  return lower + ((upper - lower)*rand())/RAND_MAX;
}
/* Next function is from numerical recipes in C. */
#define IA_Long 16807
#define IM_Long 2147483647
#define AM_Long (1.0/IM_Long)
#define IQ_Long 127773
#define IR_Long 2836
#define NTAB_Long 32
#define NDIV_Long (1+(IM_Long-1)/NTAB_Long)
#define EPS_Long 1.2e-7
#define RNMX_Long (1.0-EPS_Long)
float ran1_Long(long*idum, int reset) {
  int j;
  long k;
  static long iy=0;
  static long iv[NTAB_Long];
  float temp;
  if (reset) {
    iy = 0;
  }
  if (*idum<=0||!iy) {
    if (-(*idum)<1)*idum=1;
    else *idum=-(*idum);
    for (j=NTAB_Long+7; j>=0; j--) {
      k = (*idum)/IQ_Long;
      *idum=IA_Long*(*idum-k*IQ_Long)-IR_Long*k;
      if (*idum<0)*idum+=IM_Long;
      if (j<NTAB_Long)iv[j]=*idum;
    }
    iy = iv[0];
  }
  k = (*idum)/IQ_Long;
  *idum = IA_Long*(*idum-k*IQ_Long)-IR_Long*k;
  if (*idum<0)*idum += IM_Long;
  j = iy/NDIV_Long;
  iy = iv[j];
  iv[j] = *idum;
  if ((temp=AM_Long*iy)> RNMX_Long) return RNMX_Long;
  else return temp;
}
/* Next function is from numerical recipes in C. */
float randn_internal_Long(long *idum, int reset) {
  static int iset=0;
  static float gset;
  float fac, rsq, v1, v2;
  if (reset) {
    iset = 0;
  }
  if (iset==0) {
    do {
      v1 = 2.0*ran1_Long(idum, reset)-1.0;
      v2 = 2.0*ran1_Long(idum, reset)-1.0;
      rsq = v1*v1+v2*v2;
    } while(rsq >= 1.0 || rsq == 0.0);
    fac = sqrt(-2.0*log(rsq)/rsq);
    gset = v1*fac;
    iset = 1;
    return v2*fac;
  } else {
    iset = 0;
    return gset;
  }
}
double randn_Long(void) {
  return randn_internal_Long(&global_seed_Long, 0);
}
void reset_rand_Long(void) {
  srand(15);
  global_seed_Long = 1;
  randn_internal_Long(&global_seed_Long, 1);
}
