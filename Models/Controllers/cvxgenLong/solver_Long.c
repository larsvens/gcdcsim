/* Produced by CVXGEN, 2016-06-03 10:12:42 -0400.  */
/* CVXGEN is Copyright (C) 2006-2012 Jacob Mattingley, jem@cvxgen.com. */
/* The code in this file is Copyright (C) 2006-2012 Jacob Mattingley. */
/* CVXGEN, or solvers produced by CVXGEN, cannot be used for commercial */
/* applications without prior written permission from Jacob Mattingley. */

/* Filename: solver_Long.c. */
/* Description: Main solver_Long file. */
#include "solver_Long.h"
double eval_gap_Long(void) {
  int i;
  double gap;
  gap = 0;
  for (i = 0; i < 144; i++)
    gap += work_Long.z[i]*work_Long.s[i];
  return gap;
}
void set_defaults_Long(void) {
  settings_Long.resid_tol = 1e-4;
  settings_Long.eps = 1e-4;
  settings_Long.max_iters = 25;
  settings_Long.refine_steps = 1;
  settings_Long.s_init = 1;
  settings_Long.z_init = 1;
  settings_Long.debug = 0;
  settings_Long.verbose = 0;
  settings_Long.verbose_refinement = 0;
  settings_Long.better_start_Long = 1;
  settings_Long.kkt_reg = 1e-7;
}
void setup_pointers_Long(void) {
  work_Long.y = work_Long.x + 21;
  work_Long.s = work_Long.x + 21;
  work_Long.z = work_Long.x + 165;
  vars_Long.u = work_Long.x + 0;
}
void setup_indexing_Long(void) {
  setup_pointers_Long();
}
void set_start_Long(void) {
  int i;
  for (i = 0; i < 21; i++)
    work_Long.x[i] = 0;
  for (i = 0; i < 0; i++)
    work_Long.y[i] = 0;
  for (i = 0; i < 144; i++)
    work_Long.s[i] = (work_Long.h[i] > 0) ? work_Long.h[i] : settings_Long.s_init;
  for (i = 0; i < 144; i++)
    work_Long.z[i] = settings_Long.z_init;
}
double eval_objv_Long(void) {
  int i;
  double objv;
  /* Borrow space in work_Long.rhs. */
  multbyP_Long(work_Long.rhs, work_Long.x);
  objv = 0;
  for (i = 0; i < 21; i++)
    objv += work_Long.x[i]*work_Long.rhs[i];
  objv *= 0.5;
  for (i = 0; i < 21; i++)
    objv += work_Long.q[i]*work_Long.x[i];
  objv += 0;
  return objv;
}
void fillrhs_aff_Long(void) {
  int i;
  double *r1, *r2, *r3, *r4;
  r1 = work_Long.rhs;
  r2 = work_Long.rhs + 21;
  r3 = work_Long.rhs + 165;
  r4 = work_Long.rhs + 309;
  /* r1 = -A^Ty - G^Tz - Px - q. */
  multbymAT_Long(r1, work_Long.y);
  multbymGT_Long(work_Long.buffer, work_Long.z);
  for (i = 0; i < 21; i++)
    r1[i] += work_Long.buffer[i];
  multbyP_Long(work_Long.buffer, work_Long.x);
  for (i = 0; i < 21; i++)
    r1[i] -= work_Long.buffer[i] + work_Long.q[i];
  /* r2 = -z. */
  for (i = 0; i < 144; i++)
    r2[i] = -work_Long.z[i];
  /* r3 = -Gx - s + h. */
  multbymG_Long(r3, work_Long.x);
  for (i = 0; i < 144; i++)
    r3[i] += -work_Long.s[i] + work_Long.h[i];
  /* r4 = -Ax + b. */
  multbymA_Long(r4, work_Long.x);
  for (i = 0; i < 0; i++)
    r4[i] += work_Long.b[i];
}
void fillrhs_cc_Long(void) {
  int i;
  double *r2;
  double *ds_aff, *dz_aff;
  double mu;
  double alpha;
  double sigma;
  double smu;
  double minval;
  r2 = work_Long.rhs + 21;
  ds_aff = work_Long.lhs_aff + 21;
  dz_aff = work_Long.lhs_aff + 165;
  mu = 0;
  for (i = 0; i < 144; i++)
    mu += work_Long.s[i]*work_Long.z[i];
  /* Don't finish calculating mu quite yet. */
  /* Find min(min(ds./s), min(dz./z)). */
  minval = 0;
  for (i = 0; i < 144; i++)
    if (ds_aff[i] < minval*work_Long.s[i])
      minval = ds_aff[i]/work_Long.s[i];
  for (i = 0; i < 144; i++)
    if (dz_aff[i] < minval*work_Long.z[i])
      minval = dz_aff[i]/work_Long.z[i];
  /* Find alpha. */
  if (-1 < minval)
      alpha = 1;
  else
      alpha = -1/minval;
  sigma = 0;
  for (i = 0; i < 144; i++)
    sigma += (work_Long.s[i] + alpha*ds_aff[i])*
      (work_Long.z[i] + alpha*dz_aff[i]);
  sigma /= mu;
  sigma = sigma*sigma*sigma;
  /* Finish calculating mu now. */
  mu *= 0.006944444444444444;
  smu = sigma*mu;
  /* Fill-in the rhs. */
  for (i = 0; i < 21; i++)
    work_Long.rhs[i] = 0;
  for (i = 165; i < 309; i++)
    work_Long.rhs[i] = 0;
  for (i = 0; i < 144; i++)
    r2[i] = work_Long.s_inv[i]*(smu - ds_aff[i]*dz_aff[i]);
}
void refine_Long(double *target, double *var) {
  int i, j;
  double *residual = work_Long.buffer;
  double norm2;
  double *new_var = work_Long.buffer2;
  for (j = 0; j < settings_Long.refine_steps; j++) {
    norm2 = 0;
    matrix_multiply_Long(residual, var);
    for (i = 0; i < 309; i++) {
      residual[i] = residual[i] - target[i];
      norm2 += residual[i]*residual[i];
    }
#ifndef ZERO_LIBRARY_MODE
    if (settings_Long.verbose_refinement) {
      if (j == 0)
        printf("Initial residual before refinement has norm squared %.6g.\n", norm2);
      else
        printf("After refinement we get squared norm %.6g.\n", norm2);
    }
#endif
    /* Solve to find new_var = KKT \ (target - A*var). */
    ldl_solve_Long(residual, new_var);
    /* Update var += new_var, or var += KKT \ (target - A*var). */
    for (i = 0; i < 309; i++) {
      var[i] -= new_var[i];
    }
  }
#ifndef ZERO_LIBRARY_MODE
  if (settings_Long.verbose_refinement) {
    /* Check the residual once more, but only if we're reporting it, since */
    /* it's expensive. */
    norm2 = 0;
    matrix_multiply_Long(residual, var);
    for (i = 0; i < 309; i++) {
      residual[i] = residual[i] - target[i];
      norm2 += residual[i]*residual[i];
    }
    if (j == 0)
      printf("Initial residual before refinement has norm squared %.6g.\n", norm2);
    else
      printf("After refinement we get squared norm %.6g.\n", norm2);
  }
#endif
}
double calc_ineq_resid_squared_Long(void) {
  /* Calculates the norm ||-Gx - s + h||. */
  double norm2_squared;
  int i;
  /* Find -Gx. */
  multbymG_Long(work_Long.buffer, work_Long.x);
  /* Add -s + h. */
  for (i = 0; i < 144; i++)
    work_Long.buffer[i] += -work_Long.s[i] + work_Long.h[i];
  /* Now find the squared norm. */
  norm2_squared = 0;
  for (i = 0; i < 144; i++)
    norm2_squared += work_Long.buffer[i]*work_Long.buffer[i];
  return norm2_squared;
}
double calc_eq_resid_squared_Long(void) {
  /* Calculates the norm ||-Ax + b||. */
  double norm2_squared;
  int i;
  /* Find -Ax. */
  multbymA_Long(work_Long.buffer, work_Long.x);
  /* Add +b. */
  for (i = 0; i < 0; i++)
    work_Long.buffer[i] += work_Long.b[i];
  /* Now find the squared norm. */
  norm2_squared = 0;
  for (i = 0; i < 0; i++)
    norm2_squared += work_Long.buffer[i]*work_Long.buffer[i];
  return norm2_squared;
}
void better_start_Long(void) {
  /* Calculates a better starting point, using a similar approach to CVXOPT. */
  /* Not yet speed optimized. */
  int i;
  double *x, *s, *z, *y;
  double alpha;
  work_Long.block_33[0] = -1;
  /* Make sure sinvz is 1 to make hijacked KKT system ok. */
  for (i = 0; i < 144; i++)
    work_Long.s_inv_z[i] = 1;
  fill_KKT_Long();
  ldl_factor_Long();
  fillrhs_start_Long();
  /* Borrow work_Long.lhs_aff for the solution. */
  ldl_solve_Long(work_Long.rhs, work_Long.lhs_aff);
  /* Don't do any refinement for now. Precision doesn't matter too much. */
  x = work_Long.lhs_aff;
  s = work_Long.lhs_aff + 21;
  z = work_Long.lhs_aff + 165;
  y = work_Long.lhs_aff + 309;
  /* Just set x and y as is. */
  for (i = 0; i < 21; i++)
    work_Long.x[i] = x[i];
  for (i = 0; i < 0; i++)
    work_Long.y[i] = y[i];
  /* Now complete the initialization. Start with s. */
  /* Must have alpha > max(z). */
  alpha = -1e99;
  for (i = 0; i < 144; i++)
    if (alpha < z[i])
      alpha = z[i];
  if (alpha < 0) {
    for (i = 0; i < 144; i++)
      work_Long.s[i] = -z[i];
  } else {
    alpha += 1;
    for (i = 0; i < 144; i++)
      work_Long.s[i] = -z[i] + alpha;
  }
  /* Now initialize z. */
  /* Now must have alpha > max(-z). */
  alpha = -1e99;
  for (i = 0; i < 144; i++)
    if (alpha < -z[i])
      alpha = -z[i];
  if (alpha < 0) {
    for (i = 0; i < 144; i++)
      work_Long.z[i] = z[i];
  } else {
    alpha += 1;
    for (i = 0; i < 144; i++)
      work_Long.z[i] = z[i] + alpha;
  }
}
void fillrhs_start_Long(void) {
  /* Fill rhs with (-q, 0, h, b). */
  int i;
  double *r1, *r2, *r3, *r4;
  r1 = work_Long.rhs;
  r2 = work_Long.rhs + 21;
  r3 = work_Long.rhs + 165;
  r4 = work_Long.rhs + 309;
  for (i = 0; i < 21; i++)
    r1[i] = -work_Long.q[i];
  for (i = 0; i < 144; i++)
    r2[i] = 0;
  for (i = 0; i < 144; i++)
    r3[i] = work_Long.h[i];
  for (i = 0; i < 0; i++)
    r4[i] = work_Long.b[i];
}
long solve_Long(void) {
  int i;
  int iter;
  double *dx, *ds, *dy, *dz;
  double minval;
  double alpha;
  work_Long.converged = 0;
  setup_pointers_Long();
  pre_ops_Long();
#ifndef ZERO_LIBRARY_MODE
  if (settings_Long.verbose)
    printf("iter     objv        gap       |Ax-b|    |Gx+s-h|    step\n");
#endif
  fillq_Long();
  fillh_Long();
  fillb_Long();
  if (settings_Long.better_start_Long)
    better_start_Long();
  else
    set_start_Long();
  for (iter = 0; iter < settings_Long.max_iters; iter++) {
    for (i = 0; i < 144; i++) {
      work_Long.s_inv[i] = 1.0 / work_Long.s[i];
      work_Long.s_inv_z[i] = work_Long.s_inv[i]*work_Long.z[i];
    }
    work_Long.block_33[0] = 0;
    fill_KKT_Long();
    ldl_factor_Long();
    /* Affine scaling directions. */
    fillrhs_aff_Long();
    ldl_solve_Long(work_Long.rhs, work_Long.lhs_aff);
    refine_Long(work_Long.rhs, work_Long.lhs_aff);
    /* Centering plus corrector directions. */
    fillrhs_cc_Long();
    ldl_solve_Long(work_Long.rhs, work_Long.lhs_cc);
    refine_Long(work_Long.rhs, work_Long.lhs_cc);
    /* Add the two together and store in aff. */
    for (i = 0; i < 309; i++)
      work_Long.lhs_aff[i] += work_Long.lhs_cc[i];
    /* Rename aff to reflect its new meaning. */
    dx = work_Long.lhs_aff;
    ds = work_Long.lhs_aff + 21;
    dz = work_Long.lhs_aff + 165;
    dy = work_Long.lhs_aff + 309;
    /* Find min(min(ds./s), min(dz./z)). */
    minval = 0;
    for (i = 0; i < 144; i++)
      if (ds[i] < minval*work_Long.s[i])
        minval = ds[i]/work_Long.s[i];
    for (i = 0; i < 144; i++)
      if (dz[i] < minval*work_Long.z[i])
        minval = dz[i]/work_Long.z[i];
    /* Find alpha. */
    if (-0.99 < minval)
      alpha = 1;
    else
      alpha = -0.99/minval;
    /* Update the primal and dual variables. */
    for (i = 0; i < 21; i++)
      work_Long.x[i] += alpha*dx[i];
    for (i = 0; i < 144; i++)
      work_Long.s[i] += alpha*ds[i];
    for (i = 0; i < 144; i++)
      work_Long.z[i] += alpha*dz[i];
    for (i = 0; i < 0; i++)
      work_Long.y[i] += alpha*dy[i];
    work_Long.gap = eval_gap_Long();
    work_Long.eq_resid_squared = calc_eq_resid_squared_Long();
    work_Long.ineq_resid_squared = calc_ineq_resid_squared_Long();
#ifndef ZERO_LIBRARY_MODE
    if (settings_Long.verbose) {
      work_Long.optval = eval_objv_Long();
      printf("%3d   %10.3e  %9.2e  %9.2e  %9.2e  % 6.4f\n",
          iter+1, work_Long.optval, work_Long.gap, sqrt(work_Long.eq_resid_squared),
          sqrt(work_Long.ineq_resid_squared), alpha);
    }
#endif
    /* Test termination conditions. Requires optimality, and satisfied */
    /* constraints. */
    if (   (work_Long.gap < settings_Long.eps)
        && (work_Long.eq_resid_squared <= settings_Long.resid_tol*settings_Long.resid_tol)
        && (work_Long.ineq_resid_squared <= settings_Long.resid_tol*settings_Long.resid_tol)
       ) {
      work_Long.converged = 1;
      work_Long.optval = eval_objv_Long();
      return iter+1;
    }
  }
  return iter;
}
