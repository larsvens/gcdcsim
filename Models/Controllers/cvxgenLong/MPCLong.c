#include "solver_Long.h"
Vars_Long vars_Long;
Params_Long params_Long;
Workspace_Long work_Long;
Settings_Long settings_Long;
double time_out_Long;

void MPCLong(double *optval, double *res, int *num, int *conv, double *Hess, double *f, double *A_ineq, double *b_ineq, double *ub, double *lb,double *time_pointer,double * maxtrq, double * mintrq) {    

	
  int num_iters = 0;
  int i = 0;
  
	tic_Long();
  set_defaults_Long();
  setup_indexing_Long();

  for (i = 0; i < (21*21); i++) 
  {
	  params_Long.Hess[i] = Hess[i]; // *(H + i);
  }

  for (i = 0; i < 21; i++) 
  {
	  params_Long.f[i] = f[i];
  }

  for (i = 0; i < (3*20*21); i++) 
  {
	  params_Long.A_ineq[i] = A_ineq[i];
  }

  for (i = 0; i < (3*20); i++) 
  {
	  params_Long.b_ineq[i] = b_ineq[i];
  }
  
  for (i = 0; i < 21; i++) 
  {
	  params_Long.ub[i] = ub[i];
  }
  
  for (i = 0; i < 21; i++) 
  {
	  params_Long.lb[i] = lb[i];
  }
  
  for (i = 0; i < 21; i++) 
  {
	  params_Long.maxtrq[i] = maxtrq[i];
  }
  
  for (i = 0; i < 21; i++) 
  {
	  params_Long.mintrq[i] = mintrq[i];
  }

  num_iters = solve_Long();
  
  //return variables
  *optval = work_Long.optval;
  for (i = 0; i < 21; i++) 
  {
	  res[i] = vars_Long.u[i];
  } 
  *num = num_iters;
  *conv = work_Long.converged;
  
  time_out_Long = tocq_Long();;
  *time_pointer = time_out_Long;
  
  return;
}