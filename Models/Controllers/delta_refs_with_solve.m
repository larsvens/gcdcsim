tic
m  = [276 332 608];          %m(1)-mass front axle, m(2)-mass rear axle, m(3)-total mass [kg]
mtot = m(3);                %Total mass of vehicle
mj = 40;                    %Equivalent mass of rotating parts (due to inertia) [kg]
g  = 9.81;                   %[m/s^2]
lambda = m(2)/m(3);
l  = 2;                      %Wheel base [m]
f  = l*lambda;               %Distance from front axle to CoG [m]
r  = l*(1-lambda);           %Distance from rear axle to CoG [m]
%         cv = 1;                     %Longitudinal velocity coefficient. cv=1 when vehicle starts to move, otherwise cv=0.3


Izz = 1000;                 %Still rough estimation [kgm^2]
Cf  = 2*224*180/pi;
Cr  = 2*224*180/pi;
radius  = 0.3;


c   = 1.0834;
rho = 1.23;                 %Air density [kg/m^3]
Area   = 1.5*1.5;                %Projected frontal area [m]
Fl  = 0.5*rho*c*Area;

T_split=0.5;
delta_solve=zeros(2*(H+1),1);
syms delta_f delta_r;
j=1;
    if abs(vxref(j)) <= 0.05
        fr = 0;
    else
        fr  = 0.0027;    
    end

    if(abs(vxref(j))<0.1)
        aux7=0;
        aux8=0;
    else
        aux7=Cf*(atan((vyref(j)+yawrateref(j)*f)/max(0.5,vxref(j)))-delta_f);
        aux8=Cr*(atan((vyref(j)-yawrateref(j)*r)/max(0.5,vxref(j)))-delta_r);
    end
    
    vx_eq=vxref(j)+dt*((aux7*sin(delta_f)+aux8*sin(delta_r)+(Torque(j)*T_split*cos(delta_f)/radius)+(Torque(j)*(1-T_split)*cos(delta_r)/radius)-mtot*g*fr-Fl*vxref(j)^2)/(mtot+mj)+yawrateref(j)*vyref(j));
    vy_eq=vyref(j)+dt*((-aux7*cos(delta_f)-aux8*cos(delta_r)+(Torque(j)*T_split*sin(delta_f)/radius)+(Torque(j)*(1-T_split)*sin(delta_r)/radius))/(mtot+mj)-yawrateref(j)*vxref(j));
    yawrate_eq=yawrateref(j)+dt*((-f*aux7*cos(delta_f)+f*(Torque(j)*T_split*sin(delta_f)/radius)+r*aux8*cos(delta_r)-r*(Torque(j)*(1-T_split)*sin(delta_r)/radius))/Izz);
        
    S= vpasolve([yawrate_eq==yawrateref(j+1)],[delta_f,delta_r]);
    delta_solve(2*j-1,1)=S.delta_f;
    delta_solve(2*j,1)=S.delta_r;
%     [sol_delta_f,sol_delta_r]
%     ,delta_f<=pi/2,delta_f>=-pi/2,delta_r<=pi/2,delta_r>=-pi/2  ,vx_eq==vxref(j+1),vy_eq==vyref(j+1),
for j=2:H
    
    if abs(vxref(j)) <= 0.05
        fr = 0;
    else
        fr  = 0.0027;    
    end

    if(abs(vxref(j))<0.1)
        aux7=0;
        aux8=0;
    else
        aux7=Cf*(atan((vyref(j)+yawrateref(j)*f)/max(0.5,vxref(j)))-delta_f);
        aux8=Cr*(atan((vyref(j)-yawrateref(j)*r)/max(0.5,vxref(j)))-delta_r);
    end
    
    vx_eq=vxref(j)+time_pred*((aux7*sin(delta_f)+aux8*sin(delta_r)+(Torque(j)*T_split*cos(delta_f)/radius)+(Torque(j)*(1-T_split)*cos(delta_r)/radius)-mtot*g*fr-Fl*vxref(j)^2)/(mtot+mj)+yawrateref(j)*vyref(j));
    vy_eq=vyref(j)+time_pred*((-aux7*cos(delta_f)-aux8*cos(delta_r)+(Torque(j)*T_split*sin(delta_f)/radius)+(Torque(j)*(1-T_split)*sin(delta_r)/radius))/(mtot+mj)-yawrateref(j)*vxref(j));
    yawrate_eq=yawrateref(j)+time_pred*((-f*aux7*cos(delta_f)+f*(Torque(j)*T_split*sin(delta_f)/radius)+r*aux8*cos(delta_r)-r*(Torque(j)*(1-T_split)*sin(delta_r)/radius))/Izz);
        
    S= vpasolve([yawrate_eq==yawrateref(j+1)],[delta_f,delta_r]);
    delta_solve(2*j-1,1)=S.delta_f;
    delta_solve(2*j,1)=S.delta_r;
end

toc