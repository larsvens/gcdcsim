function [ delta_out ] = LinMPCla3( H,xref,yref,yawref,vx,vy,vref,yawrate,delta_f,delta_r,dt )
    persistent init_flag mindeg maxdeg aux_yr aux_maxminyr maxyawrate minyawrate delta_ref Q R options f r delta_dif;
    if isempty(init_flag)
        init_flag = 1;
        aux_yr=zeros(4*H,2*H); %auxiliar matrix yawrate
        aux_maxminyr=zeros(4*H,1);%auxiliar max min matrix yawrate
        delta_dif=zeros(2*H,1);
        R=zeros(2*H);
        Q=zeros(3*H);
    end
    if(init_flag==1)
       init_flag=0;
       m  = [276 332 608];          %m(1)-mass front axle, m(2)-mass rear axle, m(3)-total mass [kg]
       lambda = m(2)/m(3);
       l  = 2;                      %Wheel base [m]
       f  = l*lambda;               %Distance from front axle to CoG [m]
       r  = l*(1-lambda);           %Distance from rear axle to CoG [m]
              
       maxdeg=25*pi/180;
       mindeg=-25*pi/180;
       maxyawrate=35*pi/180;
       minyawrate=-35*pi/180;
        
       aux_yr(1,1)=1;
       aux_yr(2,2)=1;
       for i=3:2*H
           aux_yr(i,i)=1;
           aux_yr(i,i-2)=-1;
       end
       aux_yr(2*H+1:end,:)=-aux_yr(1:2*H,:);
       last=19;
       for i=1:H-last-1
           Q(3*i-2,3*i-2)=0*2^(i);%X weight
           Q(3*i-1,3*i-1)=i;%Y weight
           Q(3*i,3*i)=100*2^(i+1);%Heading weight
%            R(2*i-1,2*i-1)=1;
%            R(2*i,2*i)=1;
       end
       for i=H-last:H
           Q(3*i-2,3*i-2)=0*3*2^(i);%X weight
           Q(3*i-1,3*i-1)=300*2^(i);%Y weight
           Q(3*i,3*i)=30000*2^(i+1);%Heading weight
%            R(2*i-1,2*i-1)=1;
%            R(2*i,2*i)=1;
       end
       options=optimoptions('quadprog');%,'Algorithm','trust-region-reflective',,'Algorithm','active-set'
    end
    [delta_ref,yaw_dif]=refs(f,r,xref,yref,yawref);

%     z_dif=[-xref(1);-yref(1);-yawref(1)];
        z_dif=[0;-yref(1);-0];
%     
%     yawref=yawref-yawref(1);
    
    ub=maxdeg-delta_ref;
    lb=mindeg-delta_ref;
    ub(1:2)=ub(1:2);%-[delta_f; delta_r]
    lb(1:2)=lb(1:2);%-[delta_f; delta_r]
    aux_maxminyr(1:2*H)=maxyawrate*dt;
    aux_maxminyr(2*H+1:end)=-minyawrate*dt;
    aux_maxminyr(1:2)=aux_maxminyr(1:2)+[delta_f; delta_r];
    aux_maxminyr(2*H+1:2*H+2)=aux_maxminyr(2*H+1:2*H+2)-[delta_f; delta_r];
    b=aux_maxminyr-aux_yr*delta_ref;

    A=zeros(3*H,3);
    Ai=zeros(3,3,H);
    B=zeros(3*H,2*H);
    Bi=zeros(3,2,H);

    Ai(:,:,1)=[1,0,dt*vref(1)*(-sin(yawref(1))*[1/2,1/2]*cos([delta_f;delta_r])-cos(yawref(1))*[1/2,1/2]*sin([delta_f;delta_r]));
              0,1 ,dt*vref(1)*(-sin(yawref(1))*[1/2,1/2]*sin([delta_f;delta_r])+cos(yawref(1))*[1/2,1/2]*cos([delta_f;delta_r]));
              0,0,1];
          
    Bi(:,:,1)=[dt*vref(1)*(-sin(delta_f)*cos(yawref(1))/2-cos(delta_f)*sin(yawref(1))/2),dt*vref(1)*(-sin(delta_r)*cos(yawref(1))/2-cos(delta_r)*sin(yawref(1))/2);
               dt*vref(1)*(cos(delta_f)*cos(yawref(1))/2-sin(delta_f)*sin(yawref(1))/2),dt*vref(1)*(cos(delta_r)*cos(yawref(1))/2-sin(delta_r)*sin(yawref(1))/2);
               dt*vref(1)*((sec(delta_f))^2)/f,-dt*vref(1)*((sec(delta_r))^2)/r];
    



    A(1:3,:)=Ai(:,:,1);
    B(1:3,1:2)=Bi(:,:,1);
    for i=1:H-1
        Ai(:,:,i+1)=[1,0,dt*vref(i+1)*(-sin(yawref(i+1))*[1/2,1/2]*cos([delta_ref(2*i-1);delta_ref(2*i)])-cos(yawref(i+1))*[1/2,1/2]*sin([delta_ref(2*i-1);delta_ref(2*i)]));
                     0,1,dt*vref(i+1)*(-sin(yawref(i+1))*[1/2,1/2]*sin([delta_ref(2*i-1);delta_ref(2*i)])+cos(yawref(i+1))*[1/2,1/2]*cos([delta_ref(2*i-1);delta_ref(2*i)]));
                     0,0,1];
                  
        Bi(:,:,i+1)=[dt*vref(i+1)*(-sin(delta_ref(2*i-1))*cos(yawref(i+1))/2-cos(delta_ref(2*i-1))*sin(yawref(i+1))/2),dt*vref(i+1)*(-sin(delta_ref(2*i))*cos(yawref(i+1))/2-cos(delta_ref(2*i))*sin(yawref(i+1))/2);
                     dt*vref(i+1)*(cos(delta_ref(2*i-1))*cos(yawref(i+1))/2-sin(delta_ref(2*i-1))*sin(yawref(i+1))/2),dt*vref(i+1)*(cos(delta_ref(2*i))*cos(yawref(i+1))/2-sin(delta_ref(2*i))*sin(yawref(i+1))/2);
                     dt*vref(i+1)*((sec(delta_ref(2*i-1)))^2)/f,-dt*vref(i+1)*((sec(delta_ref(2*i)))^2)/r];

        A(3*i+1:3*i+3,:)=Ai(:,:,i+1)*A(3*i-2:3*i,:);
        for j=1:i
            B(3*i+1:3*i+3,2*j-1:2*j)=Ai(:,:,i+1)*B(3*i-2:3*i,2*j-1:2*j);
        end
        B(3*i+1:3*i+3,2*i+1:2*i+2)=Bi(:,:,i+1);
    end
    delta_dif(:)=[delta_dif(3:end);delta_dif(end-1:end)];
    Hess=2*(B'*Q*B+R);
%     norm(Hess-Hess',inf)
%     L=(Hess+Hess')/2;
%     L-Hess
    lin=2*B'*Q*A*z_dif;
%     fun=@(x)(1/2*x'*Hess*x+lin'*x);
    delta_dif = quadprog(Hess,lin,aux_yr,b,[],[],lb,ub,delta_dif,options);
%     delta_dif = fmincon(fun,delta_dif,aux_yr,b,[],[],lb,ub,[],options);
    delta_out=delta_dif+delta_ref;
%     delta_ref(1:2)=delta_out(1:2)
end