function [vx_new, vy_new, yawrate_new, xEast, yNorth, heading]=...
    UKF(vx, vy, yawrate, delta_f,delta_r,Torque, vx_init,vy_init,yawrate_init, xEast_init, yNorth_init,heading_init, dt)
%% initialize states
persistent init_flag xEast_old yNorth_old heading_old;
if isempty(init_flag)
    init_flag = 0;
end

if init_flag == 0;
    vx       = vx_init;
    vy       = vy_init;
    yawrate = yawrate_init;
    init_flag = 1;
end

if isempty(xEast_old)
    xEast_old = xEast_init;
end

if isempty(yNorth_old)
    yNorth_old = yNorth_init;
end

if isempty(heading_old)
    heading_old = heading_init;
end


%% Vehicle parameters
h = dt;
vx_cons = 6;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Vehicle data of RCV %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m  = [276 332 608];          %m(1)-mass front axle, m(2)-mass rear axle, m(3)-total mass [kg]
mtot = m(3);                %Total mass of vehicle
mj = 40;                    %Equivalent mass of rotating parts (due to inertia) [kg]
g  = 9.81;                   %[m/s^2]
lambda = m(2)/m(3);
l  = 2;                      %Wheel base [m]
f  = l*lambda;               %Distance from front axle to CoG [m]
r  = l*(1-lambda);           %Distance from rear axle to CoG [m]
cv = 1;                     %Longitudinal velocity coefficient. cv=1 when vehicle starts to move, otherwise cv=0.3


Izz = 1000;                 %Still rough estimation [kgm^2]
Cf  = 2*224*180/pi;
Cr  = 2*224*180/pi;
rr  = 0.3;
fr  = 0.0027;    %Initial value 0.027
c   = 1.0834;

rho = 1.23;                 %Air density [kg/m^3]
A   = 1.5*1.5;                %Projected frontal area [m]
Fl  = 0.5*rho*c*A;



if abs(vx) <= 0.05
    fr = 0;
end

if abs(vx) < 0.1
    af=0;
    ar=0;
else
    af = atan((vy + yawrate*f)/max(0.5,vx)) - delta_f;           %Slip angle on front axle
    ar = atan((vy - yawrate*r)/max(0.5,vx)) - delta_r;           %Slip angle on rear axle       
end

Ff = -Cf*af;                    %Lateral force on front axle
Fr = -Cr*ar;                    %Lateral force on rear axle
Fm = mtot*g*fr;                 %Motion resistance due to tire rolling resistance and road slope

% Torque split between front and rear axle
T_split = 0.5;                  %Torque split
Fxf = Torque*T_split/rr;      %Amount of available torque on the front axle
Fxr = Torque*(1-T_split)/rr;


%% Bicycle model 2
vx_new = (vx + h*((-Ff*sin(delta_f) - Fr*sin(delta_r) + Fxf*cos(delta_f) + Fxr*cos(delta_r) - Fm - Fl*vx^2)/(mtot+mj) + yawrate*vy));
vy_new = vy + h*((Fr*cos(delta_r) + Ff*cos(delta_f) + Fxr*sin(delta_r) + Fxf*sin(delta_f))/(mtot+mj) - yawrate*vx); 
yawrate_new = yawrate + h*((f*(Ff*cos(delta_f)+Fxf*sin(delta_f)) - r*(Fxr*sin(delta_r)+Fr*cos(delta_r)))/Izz);



%% Update Global position



vx_ref = vx*cos(heading_old) + vy * cos(pi/2 + heading_old);    %Calculate velocity xEgo
vy_ref = vx*sin(heading_old) + vy * sin(pi/2 + heading_old);    %Calculate velocity yEgo

xEast = xEast_old + vx_ref*dt;        %switched sign
yNorth = yNorth_old + vy_ref*dt;     
heading = heading_old + yawrate*dt; 

xEast_old  = xEast;
yNorth_old  = yNorth;
heading_old = heading;




end

