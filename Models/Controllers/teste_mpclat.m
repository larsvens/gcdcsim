clear all
close all

ref=zeros(2,300);
refyaw=zeros(300,1);
error=0;
ref(2,1)=error;

ref(1,1)=0;
for i=1:300
    if(i<10)
        ref(2,i+1)=error;
    elseif(i<100)
        ref(2,i+1)=ref(2,i)+0.035;
        refyaw(i+1)=0;%2.5*pi/180;
    else
        ref(2,i+1)=ref(2,i);
        refyaw(i+1)=0;%-2.5*pi/180;
    end
    ref(1,i+1)=ref(1,i)+10/20;
end
H=15;
vx=10;
vy=0;
yawrate=0;
delta_f=0;
delta_r=0;
dt=1/20;
a=zeros(H,1);
% a=0.25625;


rot=@(x)[cos(x),sin(x);-sin(x),cos(x)];

xEastnew=0;
yNorthnew=0;
headingnew=-0.18;
xEast=0;
yNorth=0;
figure(1)
hold on
plot(ref(1,:),ref(2,:));
heading_old=headingnew;
ang=headingnew;

h_ref_yaw = quiver(xEast, yNorth, ...
    2*cos(headingnew), 2*sin(headingnew),'color',[0.82 0.41 0.12]);


T=[xEastnew;yNorthnew];
ref_res=rot(ang)*(ref(:,1:H+2)-repmat(T,1,length(ref(:,1:H+2))));
yaw_res=refyaw(1:H+1)-headingnew;
Torque_ref=zeros(H,1);
mat_data=zeros(9,300);
for i=1:300-H-2
    
%     if i==49
%         ref(2,:)=ref(2,:)+3;
%         figure(1)
%         hold on
%         plot(ref(1,i+1:end),ref(2,i+1:end));
%     end
%     vref(1)=sqrt(vx^2+vy^2);
%     for j=2:H
%         vref(j)=vref(j-1)+a(j)*dt;
%     end
%     vref(H+1)=vref(H)+a(H)*dt; %repeat the last prediction
    tic
%     if(i==1)
    delta_ref=LinMPClat( H,ref_res(1,:),ref_res(2,:),yaw_res,vx,vy,Torque_ref,yawrate,delta_f,delta_r,dt)
%     else
%         delta_ref=[delta_ref(3:end);delta_ref(end-1:end)];
%     end
    toc
%     v=sqrt(vx^2+vy^2);
%     xEastnew=xEast+v*[1/2 1/2]*cos([delta_f,delta_r]')*dt;
%     yNorthnew=yNorth+v*[1/2 1/2]*sin([delta_f,delta_r]')*dt;
%     headingnew=heading_old+delta_f-delta_r;
    delta_f=delta_ref(1);
    delta_r=delta_ref(2);
    a = LinMPClong( H,ref_res(1,:),ref_res(2,:),yaw_res,vx,delta_ref,dt);
    
    mat_data(1,i)=xEastnew;
    mat_data(2,i)=yNorthnew;
    mat_data(3,i)=headingnew;
    mat_data(4,i)=vx;
    mat_data(5,i)=vy;
    mat_data(6,i)=yawrate;
    mat_data(7,i)=delta_f;
    mat_data(8,i)=delta_r;
    mat_data(9,i)=a(1);
    
    Tor=Torque(648,a,0.3);
    [vx_new, vy_new, yawrate_new,xEastnew, yNorthnew, headingnew]=UKF(vx, vy, yawrate, delta_f,delta_r,Tor(1), 10,0,0, 0, 0,headingnew, dt);
    Torque_ref=[Tor(2:end);Tor(end)];
    vx=vx_new;
    vy=vy_new;
    yawrate=yawrate_new;
    figure(1)
    hold on
    plot(xEastnew,yNorthnew,'c*');
     h_ref_yaw = quiver(xEast, yNorth, ...
    5*cos(headingnew), 5*sin(headingnew),'color',[0.82 0.41 0.12]);
    figure(2)
    hold on
    plot(i,delta_ref(1),'r*');
    plot(i,delta_ref(2),'c*');
    ang=headingnew
    T=[xEastnew;yNorthnew];
   ref_res=rot(ang)*(ref(:,i+1:i+H+2)-repmat(T,1,length(ref(:,i+1:i+H+2))));
%    ref_res(:,1)
%     refx(i+1:end)=refx(i+1:end)+(xEast-xEastnew);
%     refy(i+1:end)=refy(i+1:end)+(yNorth-yNorthnew);
    yaw_res=refyaw(i+1:i+H+1)-headingnew;
    xEast=xEastnew;
    yNorth=yNorthnew;
    heading_old=headingnew;
    
    pause
end