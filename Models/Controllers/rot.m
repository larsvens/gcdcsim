function [ out ] = rot( x )
    out=[cos(x),sin(x);-sin(x),cos(x)];
end

