function [trajx_new,trajy_new,trajyaw_new,vx_new,vy_new,yawrate_new]=State_Prediction_Lateral(trajx,trajy,trajyaw,vx,vy,yawrate,prev_Tor_req,Ts_MPC,Td,prev_delta_req)

    vx_new=vx;
    vy_new=vy;
    yawrate_new=yawrate;
    trajx_new=trajx;
    trajy_new=trajy;
    trajyaw_new=trajyaw;

    Td_aux=Td;
    n=0;
    while Td_aux>Ts_MPC
        n=n+1;
        Td_aux=Td_aux-Ts_MPC;
    end
    n=n+1;
    
    x_pos=0;
    y_pos=0;
    yaw=0;
    if(sum(abs(trajx))+sum(abs(trajy))+sum(abs(trajyaw))>0.001)
        for i=0:n-1
            if(i==0)
                dt=Td_aux;
            else
                dt=Ts_MPC;
            end

            while dt>0.01
                x_pos = x_pos + (vx_new*cos(yaw) + vy_new * cos(pi/2 + yaw))*0.01;       
                y_pos = y_pos + (vx_new*sin(yaw) + vy_new * sin(pi/2 + yaw))*0.01;     
                yaw = yaw + yawrate_new*0.01; 

                [vx_new, vy_new, yawrate_new]=EgoVelsPred(vx_new, vy_new, yawrate_new, prev_delta_req(2*(n-i)-1),prev_delta_req(2*(n-i)),prev_Tor_req(n-i),0.01);
                dt=dt-0.01;
            end
            if(dt>0.001)
                x_pos = x_pos + (vx_new*cos(yaw) + vy_new * cos(pi/2 + yaw))*dt;       
                y_pos = y_pos + (vx_new*sin(yaw) + vy_new * sin(pi/2 + yaw))*dt;     
                yaw = yaw + yawrate_new*dt; 

                [vx_new, vy_new, yawrate_new]=EgoVelsPred(vx_new, vy_new, yawrate_new, prev_delta_req(2*(n-i)-1),prev_delta_req(2*(n-i)),prev_Tor_req(n-i),dt);
            end
        end
        traj=rot(yaw)*([trajx';trajy']-repmat([x_pos;y_pos],1,length(trajx)));

        trajyaw=trajyaw-yaw;
        trajx=traj(1,:);
        trajy=traj(2,:);

        dist = (traj(1,:)).^2+(traj(2,:)).^2;
        [~,Index] = min(dist);

        Index_1 = Index;
        dist(Index) = inf;
        [~,Index_2] = min(dist);

        line=createLine(traj(:,Index_1)',traj(:,Index_2)');
        point=projPointOnLine([0 0], line);

        if(Index_2>Index_1)
            Index_aux=Index_1;
            Index_1=Index_2;
            Index_2=Index_aux;
        end

        d_point=sqrt((point(1)-traj(1,Index_1+1))^2+(point(2)-traj(2,Index_1+1))^2);
        d_I1=sqrt((traj(1,Index_1)-traj(1,Index_1+1))^2+(traj(2,Index_1)-traj(2,Index_1+1))^2);

        if(d_point<=d_I1) %point is after both traj_points
            trajx_new=[point(1);trajx(Index_1+1:end)';repmat(trajx(end),Index_1-1,1)];
            trajy_new=[point(2);trajy(Index_1+1:end)';repmat(trajy(end),Index_1-1,1)];
            trajyaw_new=[trajyaw(Index_1)+((d_I1-d_point)/d_I1)*(trajyaw(Index_1+1)-trajyaw(Index_1));trajyaw(Index_1+1:end);repmat(trajyaw(end),Index_1-1,1)];
        else
            d_point=sqrt((point(1)-traj(1,Index_1))^2+(point(2)-traj(2,Index_1))^2);
            d_I2=sqrt((traj(1,Index_2)-traj(1,Index_1))^2+(traj(2,Index_2)-traj(2,Index_1))^2);
            if(d_point<=d_I2) %point is between Index_1 and Index_2 traj_points
                trajx_new=[point(1);trajx(Index_2+1:end)';repmat(trajx(end),Index_2-1,1)];
                trajy_new=[point(2);trajy(Index_2+1:end)';repmat(trajy(end),Index_2-1,1)];
                trajyaw_new=[trajyaw(Index_2)+((d_I2-d_point)/d_I2)*(trajyaw(Index_2+1)-trajyaw(Index_2));trajyaw(Index_2+1:end);repmat(trajyaw(end),Index_2-1,1)];
            else %point is before both traj_points
                if(Index_2>=2)
                    d_point=sqrt((point(1)-traj(1,Index_2))^2+(point(2)-traj(2,Index_2))^2);
                    d_aux=sqrt((traj(1,Index_2-1)-traj(1,Index_2))^2+(traj(2,Index_2-1)-traj(2,Index_2))^2);

                    trajx_new=[point(1);trajx(Index_2:end)';repmat(trajx(end),Index_2-2,1)];
                    trajy_new=[point(2);trajy(Index_2:end)';repmat(trajy(end),Index_2-2,1)];
                    trajyaw_new=[trajyaw(Index_2-1)+((d_aux-d_point)/d_aux)*(trajyaw(Index_2)-trajyaw(Index_2-1));trajyaw(Index_2:end);repmat(trajyaw(end),Index_2-2,1)];
                else%point is before the given trajectory, very strange if it happens
                    trajx_new=[point(1);trajx(Index_2:end-1)'];
                    trajy_new=[point(2);trajy(Index_2:end-1)'];
                    trajyaw_new=[trajyaw(Index_2);trajyaw(Index_2:end-1)];
                end
            end
        end
    end
end
