function [d_frente_new,v_frente_new,vx_new,vy_new,yawrate_new]=State_Prediction(d_frente,v_frente,a_frente,vx,vy,yawrate,prev_Tor_req,Ts_MPC,Td,prev_delta_req)

    vx_new=vx;
    vy_new=vy;
    yawrate_new=yawrate;
    d_frente_new=d_frente;
    v_frente_new=v_frente;

    Td_aux=Td;
    n=0;
    while Td_aux>Ts_MPC
        n=n+1;
        Td_aux=Td_aux-Ts_MPC;
    end
    n=n+1;
    for i=0:n-1
        if(i==0)
            dt=Td_aux;
        else
            dt=Ts_MPC;
        end
        
        while dt>0.01
            d_frente_new=d_frente_new+v_frente_new*0.01-vx_new*0.01;
            v_frente_new=v_frente_new+a_frente*0.01;
            [vx_new, vy_new, yawrate_new]=EgoVelsPred(vx_new, vy_new, yawrate_new, prev_delta_req(2*(n-i)-1),prev_delta_req(2*(n-i)),prev_Tor_req(n-i),0.01);
            dt=dt-0.01;
        end
        if(dt>0.001)
            d_frente_new=d_frente_new+v_frente_new*dt-vx_new*dt;
            v_frente_new=v_frente_new+a_frente*dt;
            [vx_new, vy_new, yawrate_new]=EgoVelsPred(vx_new, vy_new, yawrate_new, prev_delta_req(2*(n-i)-1),prev_delta_req(2*(n-i)),prev_Tor_req(n-i),dt);
        end
    end
end

