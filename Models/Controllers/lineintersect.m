function [ point ] = lineintersect( l1,l2 )
%gives the intersection point between two lines
%unspected behaviour when sending Inf inside the lines
%l1=[x1,y1,x2,y2] l2=[x3,y3,x4,y4]
    m1=(l1(4)-l1(2))/(l1(3)-l1(1));
    b1=l1(2)-m1*l1(1);
    m2=(l2(4)-l2(2))/(l2(3)-l2(1));
    b2=l2(2)-m2*l2(1);
    
    if (abs(m2)~=Inf && abs(m1)~=Inf )
        x=(b1-b2)/(m2-m1);

        if(isnan(x))
           x=Inf;
           y=Inf;
        else
            if(abs(x)==Inf)
                y=x;
            else
                y=m1*x+b1;
            end
        end
    else
        if(abs(m1)==Inf && abs(m2)~=Inf)
            x=l1(1);
            y=m2*x+b2;
        elseif(abs(m2)==Inf && abs(m1)~=Inf)
            x=l2(1);
            y=m1*x+b1;
        else
            x=Inf;
            y=Inf;
        end
    end
        point=[x;y];
end

