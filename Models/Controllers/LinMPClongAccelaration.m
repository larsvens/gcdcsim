function a_out  = LinMPClong( xref,yref,yawref,vx,delta_ref,d_frente,v_frente,a_frente,flagCACC,EstInit, H, dt,time_pred)
% coder.extrinsic('tic');
% coder.extrinsic('toc');
% % tic
%     coder.extrinsic('optimoptions');
%     coder.extrinsic('quadprog');
% coder.ceval('csolve');
    
    persistent init_flag d_ant v_ant mina maxa Q R options flagCACC_prev a_dif vmax mataux_vmax maxjerk minjerk mataux_jerk a_ant print;
    if isempty(init_flag)
        print=0;
        init_flag = 1;
        R=zeros(H+1);
        Q=zeros(3*(H+1));
        flagCACC_prev=-1;
        
        maxa=2;
        mina=-4;
        vmax=25;%Verificar, tanto as aceleraçoes como a velocidade maxima podem estar a limitar a aceleraçao e velocidade do carro sem estar estar nos limites na realidade, depende do modelo
        maxjerk=1;
        minjerk=-1;
        
        mataux_vmax=zeros(H,H+1);
        mataux_jerk=zeros(H,H+1);
        
%         options=optimoptions('quadprog','Algorithm','interior-point-convex','Display','off');%,'Algorithm','active-set', 'Display','final-detailed','TolCon', 1.0000e-08,'TolFun', 1.0000e-04,'TolX', 1.0000e-04
        a_dif=zeros(H+1,1);
    end
    
    a_out=zeros(H,1);
    
    if isempty(d_ant)
       d_ant = 0; 
    end
    
    if isempty(v_ant)
       v_ant = 0; 
    end
    
    if isempty(a_ant)
       a_ant = 0; 
    end
    
    if(init_flag)
       init_flag=0;
        last=10;
        for i=1:H-last-1
            Q(3*i-2,3*i-2)=1;%dist_dif weight
            Q(3*i-1,3*i-1)=1;%dist weight
            Q(3*i,3*i)=1;%vel weight
            for j=1:i
               if(i==1)
                   mataux_vmax(i,j)=1;
                   mataux_vmax(i,H+1)=-1/dt;
               else
                   if(j==1)
                       mataux_vmax(i,j)=dt/time_pred;
                   else
                       mataux_vmax(i,j)=1;
                   end
                   mataux_vmax(i,H+1)=-1/time_pred;
               end
            end
            if(i==1)
                mataux_jerk(i,i)=1;
            else
                mataux_jerk(i,i)=1;
                mataux_jerk(i,i-1)=-1;
            end
        end
        for i=H-last:H
            Q(3*i-2,3*i-2)=1;%dist_dif weight
            Q(3*i-1,3*i-1)=1;%dist weight
            Q(3*i,3*i)=1;%vel weight
            for j=1:i
               if(j==1)
                   mataux_vmax(i,j)=dt/time_pred;
               else
                   mataux_vmax(i,j)=1;
               end
               mataux_vmax(i,H+1)=-1/time_pred;
            end
            
            mataux_jerk(i,i)=1;
            mataux_jerk(i,i-1)=-1;
        end
%         i=H+1;
%         Q(3*i-2,3*i-2)=100;%dist_dif weight
%         Q(3*i-1,3*i-1)=10;%dist weight
%         Q(3*i,3*i)=0;%vel weight
        R(H+1,H+1)=10^10;
%         mataux_vmax
        
    end
    if(EstInit)
        if (flagCACC==1)
            %safe_factor=0.7;%change to the correct value before competition
            th1=0.5;%change to the correct value before competition
            th2=0.5;
            r_safe=5;
%             if(print<150)
                [d_ref,v_ref,a_ref]=refsplatoon(H,th1,dt,d_frente,r_safe,v_frente,a_frente,delta_ref,vx,time_pred)
%             else
%                 [d_ref,v_ref,a_ref]=refsplatoon(H,th,dt,d_frente,r_safe,v_frente,a_frente,delta_ref,vx,time_pred);
%             end
        elseif(flagCACC==0)
            %safe_factor=1;
            [d_ref,v_ref,a_ref]=refslong(xref,yref,yawref,dt,delta_ref,vx,H,maxa,mina,time_pred)
            th1=dt;
            th2=time_pred;
            r_safe=0;
            v_frente=0;
            d_frente=d_ref(1);
        else
            %erro!!!!!
            return;
        end

        if(flagCACC~=flagCACC_prev)
%             err='flagCACC changed'
           d_ant=d_ref(1);
           v_ant=v_ref(1); 
        end
        flagCACC_prev=flagCACC;
        
%         if(print<150)
            z_dif=[d_frente-th1*vx-r_safe;-d_ref(1)+d_frente;-v_ref(1)+vx] %[-(d_ant-th1*v_ant)+(d_frente-th1*vx);-d_ant+d_frente;-v_ant+vx]
%         else
%            z_dif=[-(d_ant-th*v_ant-d_ref(1)+th*vx);-d_ant+d_ref(1);-v_ant+vx];
%         end
       

        ub=zeros(H+1,1);
        lb=zeros(H+1,1);
        ub(1:H,1)=maxa-a_ref;
        lb(1:H,1)=mina-a_ref;
        ub(H+1,1)=5;%5m/s is the max value that the slack velocity variable is allowed to have;
        lb(H+1,1)=-5;
        
        b=zeros(3*H,1);
        A_opt=zeros(3*H,H+1);
        %fazer com que apenas tome em conta a velocidade em x para contabilizar para
        %o maximo
        b(1)=(vmax-vx)/dt-a_ref(1);
        b(2:H)=(vmax-vx)/time_pred-mataux_vmax(2:H,:)*[a_ref;0];
        A_opt(1:H,:)=mataux_vmax;
        b(H+1)=maxjerk*dt+a_ant-a_ref(1);
        b(H+2:2*H)=maxjerk*time_pred-mataux_jerk(2:H,:)*[a_ref;0];
        A_opt(H+1:2*H,:)=mataux_jerk;
        b(2*H+1)=-minjerk*dt-a_ant+a_ref(1);
        b(2*H+2:3*H)=-minjerk*time_pred+mataux_jerk(2:H,:)*[a_ref;0];
        A_opt(2*H+1:3*H,:)=-mataux_jerk;
        

        A=zeros(3*(H+1),3);
        Ai=zeros(3,3,H+1);
        B=zeros(3*(H+1),H+1);
        Bi=zeros(3,1,H+1);

        Ai(:,:,1)=[0,1,-th1;
                  0,1,-dt;
                  0,0,1];

        Bi(:,:,1)=[0;
                   0;
                   dt*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)])];
        A(1:3,:)=Ai(:,:,1);
        B(1:3,1)=Bi(:,:,1);
        for i=1:H-1

            Ai(:,:,i+1)=[0,1,-th2;
                  0,1,-time_pred;
                  0,0,1];

            Bi(:,:,i+1)=[0;
                   0;
                   time_pred*[1/2 1/2]*cos([delta_ref(2*i+1);delta_ref(2*i+2)])];

            A(i*3+1:i*3+3,:)=Ai(:,:,i+1)*A((i-1)*3+1:(i-1)*3+3,:);
            for j=1:i
                B(i*3+1:i*3+3,j)=Ai(:,:,i+1)*B((i-1)*3+1:(i-1)*3+3,j);
            end
            B(i*3+1:i*3+3,i+1)=Bi(:,:,i+1);
        end
        
%         i=H;%slack variable
%         Ai(:,:,i+1)=[0,1,-th2;
%                   0,1,-time_pred;
%                   0,0,1];
% 
%         Bi(:,:,i+1)=[0;
%                0;
%                0];
% 
%         A(i*3+1:i*3+3,:)=Ai(:,:,i+1)*A((i-1)*3+1:(i-1)*3+3,:);
%         for j=1:i
%             B(i*3+1:i*3+3,j)=Ai(:,:,i+1)*B((i-1)*3+1:(i-1)*3+3,j);
%         end
%         B(i*3+1:i*3+3,i+1)=Bi(:,:,i+1);
        
        
        Hess=2*(B'*Q*B+R);
        
        Hess=(Hess+Hess')/2;
            
        lin=2*B'*Q*A*z_dif;
%         if(print<150)
%         a_dif = quadprog(Hess,lin,A_opt,b,[],[],lb,ub,[],options)
        
%         else
%             a_dif = quadprog(Hess,lin,A_opt,b,[],[],lb,ub,[],options);
%         a_out = a_ref+a_dif(1:end-1);
%         end
        
        [a_dif,coverged,num_iters,opt_val]=CallMPCLong( Hess,lin,A_opt,b,ub,lb,H )
%         [a_dif,status]=csolve_mex(params)
        a_out = a_ref+a_dif(1:end-1)
        a_ant=a_out(1);
        if (flagCACC)
%            if(print<150)
        d_ant=d_frente+(v_frente-vx)*dt;
           v_ant=vx+dt*a_out(1)*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]);%v_ref(2);%
%         else
%             d_ant=d_ref(1)+(v_frente-v_ref(1))*dt;
%            v_ant=v_ref(1)+dt*a_out(1)*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]);
%         end 
           
        else
           d_ant=d_ref(1)-v_ref(1)*dt+d_ref(2);
           v_ant=v_ref(1)+dt*a_out(1)*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]);
        end
%         print=print+1;
        if(print>1500)
           debug=1; 
        end
        print=print+1;
    end
%     toc
end