function [ delta_out ] = LinMPClat( H,xref,yref,yawref,vx,vy,Torque,yawrate,delta_f,delta_r,dt )
    persistent init_flag beq Aeq mindeg maxdeg aux_yr aux_maxminyr maxyawrate minyawrate  Q R options f r vxexpc vyexpc yawrateexpc ;
    persistent Cf Cr T_split radius mtot g fr mj Fl Izz;
    if isempty(init_flag)
        init_flag = 1;
        aux_yr=zeros(4*H,2*H); %auxiliar matrix yawrate
        aux_maxminyr=zeros(4*H,1);%auxiliar max min matrix yawrate
        R=zeros(2*H);
        Q=zeros(6*H);
        vant=sqrt(vx^2+vy^2);
        vxexpc=vx;
        vyexpc=vy;
        vx_ant=vx;
        vy_ant=vy;
        yawrateexpc=yawrate;
        xexpc_err=0;
        yexpc_err=0;
        yawexpc_err=0;
    end
    if(init_flag==1)
       init_flag=0;
       
       % Vehicle parameters (taken out of the ego vehicle model in the simulation)
        h = 0.006;
        %vx_cons = 6;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%% Vehicle data of RCV %%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        m  = [276 332 608];          %m(1)-mass front axle, m(2)-mass rear axle, m(3)-total mass [kg]
        mtot = m(3);                %Total mass of vehicle
        mj = 40;                    %Equivalent mass of rotating parts (due to inertia) [kg]
        g  = 9.81;                   %[m/s^2]
        lambda = m(2)/m(3);
        l  = 2;                      %Wheel base [m]
        f  = l*lambda;               %Distance from front axle to CoG [m]
        r  = l*(1-lambda);           %Distance from rear axle to CoG [m]
%         cv = 1;                     %Longitudinal velocity coefficient. cv=1 when vehicle starts to move, otherwise cv=0.3


        Izz = 1000;                 %Still rough estimation [kgm^2]
        Cf  = 2*224*180/pi;
        Cr  = 2*224*180/pi;
        radius  = 0.3;
        fr  = 0.0027;    %Initial value 0.027
        
        c   = 1.0834;
        rho = 1.23;                 %Air density [kg/m^3]
        Area   = 1.5*1.5;                %Projected frontal area [m]
        Fl  = 0.5*rho*c*Area;
        
        T_split=0.5;
        
        maxdeg=25*pi/180;
        mindeg=-25*pi/180;
        maxyawrate=35*pi/180;
        minyawrate=-35*pi/180;
        
        
        Aeq=zeros(H,2*H);
        beq=zeros(H,1);
        Aeq(1,1)=r;
        Aeq(1,2)=f;
        
        for i=2:H
            Aeq(i,2*i-1)=r;
            Aeq(i,2*i)=f;
        end
        
        aux_yr(1,1)=1;
        aux_yr(2,2)=1;
        for i=3:2*H
            aux_yr(i,i)=1;
            aux_yr(i,i-2)=-1;
        end
        aux_yr(2*H+1:end,:)=-aux_yr(1:2*H,:);
        last=1;
        for i=1:H-last-1
            Q(6*i-5,6*i-5)=1;%X weight
            Q(6*i-4,6*i-4)=100;%Y weight
            Q(6*i-3,6*i-3)=1000;%Heading weight
            Q(6*i-2,6*i-2)=1;%vx weight
            Q(6*i-1,6*i-1)=200;%vy weight
            Q(6*i,6*i)=10000;%yawrate weight
        end
        for i=H-last:H
            Q(6*i-5,6*i-5)=200*1;%X weight
            Q(6*i-4,6*i-4)=200*10;%Y weight
            Q(6*i-3,6*i-3)=200*10000;%Heading weight
            Q(6*i-2,6*i-2)=200*1;%vx weight
            Q(6*i-1,6*i-1)=200*100;%vy weight
            Q(6*i,6*i)=200*10000;%yawrate weight
        end
        options=optimoptions('quadprog');%,'Algorithm','active-set'
    end
    
%     [vxref,vyref]=vel_ref(xref,yref,yawref,dt) 

    [delta_ref]=refs(f,r,xref,yref,yawref)
  
    z_dif=[-xref(1);-yref(1);-yawref(1);-vxexpc+vx;-vyexpc+vy;-yawrateexpc+yawrate]

    yawref=yawref-yawref(1);
%     z_dif=[-xref(1);-yref(1);-yawref(1);vx-vref(1)*(cos(yawref(1))*[1/2,1/2]*cos([delta_ref(1);delta_ref(2)])-sin(yawref(1))*[1/2,1/2]*sin([delta_ref(1);delta_ref(2)]));
%            vy-vref(1)*(cos(yawref(1))*[1/2,1/2]*sin([delta_ref(1);delta_ref(2)])+sin(yawref(1))*[1/2,1/2]*cos([delta_ref(1);delta_ref(2)]));yawrate-vref(1)*([1/f,-1/r]*tan([delta_ref(1);delta_ref(2)]))]% -xref(1)...-xref(1)/dt%vx-vxref(1);vy-vyref(1);yawrate-(vref(1)*[1/f -1/r]*tan([delta_ref(1);delta_ref(2)]))
   
% 
%     delta_ref(:)=[delta_ref(3:end);delta_ref(end-1);delta_ref(end)];
    
    
    ub=maxdeg-delta_ref(3:end);
    lb=mindeg-delta_ref(3:end);
    aux_maxminyr(1:2*H)=maxyawrate*dt;
    aux_maxminyr(2*H+1:end)=-minyawrate*dt;
    aux_maxminyr(1:2)=aux_maxminyr(1:2)+[delta_f; delta_r];
    aux_maxminyr(2*H+1:2*H+2)=aux_maxminyr(2*H+1:2*H+2)-[delta_f; delta_r];
    b=aux_maxminyr-aux_yr*delta_ref(3:end);

    

    A=zeros(6*H,6);
    Ai=zeros(6,6,H);
    B=zeros(6*H,2*H);
    Bi=zeros(6,2,H);
    
    vx_next=vx;
    vy_next=vy;
    yawrate_next=yawrate;
    
    if(((vy_next+f*yawrate_next)^2+vx_next^2)==0)
        aux1=0;
        aux3=0;
        aux5=0;
    else
       aux1=-Cf*((vy_next+yawrate_next*f)/((vy_next+f*yawrate_next)^2+vx_next^2));
       aux3=Cf*(vx_next/((vy_next+f*yawrate_next)^2+vx_next^2));
       aux5=Cf*((f*vx_next)/((vy_next+f*yawrate_next)^2+vx_next^2));
    end
    
    if(((vy_next-r*yawrate_next)^2+vx_next^2)==0)
        aux2=0;
        aux4=0;
        aux6=0;
    else
       aux2=-Cr*((vy_next-yawrate_next*r)/((vy_next-r*yawrate_next)^2+vx_next^2));
       aux4=Cr*(vx_next/((vy_next-r*yawrate_next)^2+vx_next^2));
       aux6=Cr*((-r*vx_next)/((vy_next-r*yawrate_next)^2+vx_next^2));
    end
    
    
    Ai(:,:,1)=[1,0,dt*(-vx_next*sin(yawref(1))-vy_next*cos(yawref(1))),dt*cos(yawref(1)),(-dt*sin(yawref(1))),0;
              0,1,dt*(vx_next*cos(yawref(1))-vy_next*sin(yawref(1))),dt*sin(yawref(1)),dt*cos(yawref(1)),0;
              0,0,1,0,0,dt;
              0,0,0,1+dt*((aux1*sin(delta_ref(3))+aux2*sin(delta_ref(4))-Fl*2*vx_next)/(mtot+mj)),dt*((aux3*sin(delta_ref(3))+aux4*sin(delta_ref(4)))/(mtot+mj)+yawrate_next),dt*((aux5*sin(delta_ref(3))+aux6*sin(delta_ref(4)))/(mtot+mj)+vy_next);
              0,0,0,dt*((-aux1*cos(delta_ref(3))-aux2*cos(delta_ref(4)))/(mtot+mj)-yawrate_next),1+dt*((-aux3*cos(delta_ref(3))-aux4*cos(delta_ref(4)))/(mtot+mj)),dt*((-aux5*cos(delta_ref(3))-aux6*cos(delta_ref(4)))/(mtot+mj)-vx_next);
              0,0,0,dt*(-aux1*f*cos(delta_ref(3))+aux2*r*cos(delta_ref(4)))/Izz,dt*(-aux3*f*cos(delta_ref(3))+aux4*r*cos(delta_ref(4)))/Izz,1+dt*(-aux5*f*cos(delta_ref(3))+aux6*r*cos(delta_ref(4)))/Izz];
    
    if(vx_next==0)
        aux7=-Cf*delta_ref(3);
        aux8=-Cr*delta_ref(4);
    else
        aux7=Cf*(atan((vy_next+yawrate_next*f)/vx_next)-delta_ref(3));
        aux8=Cr*(atan((vy_next-yawrate_next*r)/vx_next)-delta_ref(4));
    end
    Bi(:,:,1)=[0,0;
               0,0;
               0,0;
               dt*(cos(delta_ref(3))*aux7-sin(delta_ref(3))*Cf-(Torque(1)*T_split*sin(delta_ref(3))/radius))/(mtot+mj),dt*(cos(delta_ref(4))*aux8-sin(delta_ref(4))*Cr-(Torque(1)*(1-T_split)*sin(delta_ref(4))/radius))/(mtot+mj);
               dt*(aux7*sin(delta_ref(3))+Cf*cos(delta_ref(3))+(Torque(1)*T_split*cos(delta_ref(3))/radius))/(mtot+mj),dt*(aux8*sin(delta_ref(4))+Cr*cos(delta_ref(4))+(Torque(1)*(1-T_split)*cos(delta_ref(4))/radius))/(mtot+mj);
               dt*(f*aux7*sin(delta_ref(3))+f*Cf*cos(delta_ref(3))+(f*Torque(1)*T_split*cos(delta_ref(3))/radius))/Izz,dt*(-r*aux8*sin(delta_ref(4))-r*Cr*cos(delta_ref(4))-r*(Torque(1)*(1-T_split)*cos(delta_ref(4))/radius))/Izz];

    A(1:6,:)=Ai(:,:,1);
    B(1:6,1:2)=Bi(:,:,1);
    for i=1:H-1
        
        vx_aux=vx_next+dt*((aux7*sin(delta_ref(2*i+1))+aux8*sin(delta_ref(2*i+2))+(Torque(i+1)*T_split*cos(delta_ref(2*i+1))/radius)+(Torque(i+1)*(1-T_split)*cos(delta_ref(2*i+2))/radius)-mtot*g*fr-Fl*vx_next^2)/(mtot+mj)+yawrate_next*vy_next);
        vy_aux=vy_next+dt*((-aux7*cos(delta_ref(2*i+1))-aux8*cos(delta_ref(2*i+2))+(Torque(i+1)*T_split*sin(delta_ref(2*i+1))/radius)+(Torque(i+1)*(1-T_split)*sin(delta_ref(2*i+2))/radius))/(mtot+mj)-yawrate_next*vx_next);
        yawrate_aux=yawrate_next+dt*((-f*aux7*cos(delta_ref(2*i+1))+f*(Torque(i+1)*T_split*sin(delta_ref(2*i+1))/radius)+r*aux8*cos(delta_ref(2*i+2))-r*(Torque(i+1)*(1-T_split)*sin(delta_ref(2*i+2))/radius))/Izz);
        
        vx_next=vx_aux;
        vy_next=vy_aux;
        yawrate_next=yawrate_aux;

        if(((vy_next+f*yawrate_next)^2+vx_next^2)==0)
            aux1=0;
            aux3=0;
            aux5=0;
        else
           aux1=-Cf*((vy_next+yawrate_next*f)/((vy_next+f*yawrate_next)^2+vx_next^2));
           aux3=Cf*(vx_next/((vy_next+f*yawrate_next)^2+vx_next^2));
           aux5=Cf*((f*vx_next)/((vy_next+f*yawrate_next)^2+vx_next^2));
        end

        if(((vy_next-r*yawrate_next)^2+vx_next^2)==0)
            aux2=0;
            aux4=0;
            aux6=0;
        else
           aux2=-Cr*((vy_next-yawrate_next*r)/((vy_next-r*yawrate_next)^2+vx_next^2));
           aux4=Cr*(vx_next/((vy_next-r*yawrate_next)^2+vx_next^2));
           aux6=Cr*((-r*vx_next)/((vy_next-r*yawrate_next)^2+vx_next^2));
        end

        Ai(:,:,i+1)=[1,0,dt*(-vx_next*sin(yawref(i+1))-vy_next*cos(yawref(i+1))),dt*cos(yawref(i+1)),(-dt*sin(yawref(i+1))),0;
                     0,1,dt*(vx_next*cos(yawref(i+1))-vy_next*sin(yawref(i+1))),dt*sin(yawref(i+1)),dt*cos(yawref(i+1)),0;
                     0,0,1,0,0,dt;
                     0,0,0,1+dt*((aux1*sin(delta_ref(2*i+3))+aux2*sin(delta_ref(2*i+4))-Fl*2*vx_next)/(mtot+mj)),dt*((aux3*sin(delta_ref(2*i+3))+aux4*sin(delta_ref(2*i+4)))/(mtot+mj)+yawrate_next),dt*((aux5*sin(delta_ref(2*i+3))+aux6*sin(delta_ref(2*i+4)))/(mtot+mj)+vy_next);
                     0,0,0,dt*((-aux1*cos(delta_ref(2*i+3))-aux2*cos(delta_ref(2*i+4)))/(mtot+mj)-yawrate_next),1+dt*((-aux3*cos(delta_ref(2*i+3))-aux4*cos(delta_ref(2*i+4)))/(mtot+mj)),dt*((-aux5*cos(delta_ref(2*i+3))-aux6*cos(delta_ref(2*i+4)))/(mtot+mj)-vx_next);
                     0,0,0,dt*(-aux1*f*cos(delta_ref(2*i+3))+aux2*r*cos(delta_ref(2*i+4)))/Izz,dt*(-aux3*f*cos(delta_ref(2*i+3))+aux4*r*cos(delta_ref(2*i+4)))/Izz,1+dt*(-aux5*f*cos(delta_ref(2*i+3))+aux6*r*cos(delta_ref(2*i+4)))/Izz];

        if(vx_next==0)
            aux7=-Cf*delta_ref(3);
            aux8=-Cr*delta_ref(4);
        else
            aux7=Cf*(atan((vy_next+yawrate_next*f)/vx_next)-delta_ref(3));
            aux8=Cr*(atan((vy_next-yawrate_next*r)/vx_next)-delta_ref(4));
        end
        Bi(:,:,i+1)=[0,0;
                     0,0;
                     0,0;
                     dt*(cos(delta_ref(2*i+3))*aux7-sin(delta_ref(2*i+3))*Cf-(Torque(i+1)*T_split*sin(delta_ref(2*i+3))/radius))/(mtot+mj),dt*(cos(delta_ref(2*i+4))*aux8-sin(delta_ref(2*i+4))*Cr-(Torque(i+1)*(1-T_split)*sin(delta_ref(2*i+4))/radius))/(mtot+mj);
                     dt*(aux7*sin(delta_ref(2*i+3))+Cf*cos(delta_ref(2*i+3))+(Torque(i+1)*T_split*cos(delta_ref(2*i+3))/radius))/(mtot+mj),dt*(aux8*sin(delta_ref(2*i+4))+Cr*cos(delta_ref(2*i+4))+(Torque(i+1)*(1-T_split)*cos(delta_ref(2*i+4))/radius))/(mtot+mj);
                     dt*(f*aux7*sin(delta_ref(2*i+3))+f*Cf*cos(delta_ref(2*i+3))+(f*Torque(i+1)*T_split*cos(delta_ref(2*i+3))/radius))/Izz,dt*(-r*aux8*sin(delta_ref(2*i+4))-r*Cr*cos(delta_ref(2*i+4))-r*(Torque(i+1)*(1-T_split)*cos(delta_ref(2*i+4))/radius))/Izz];

        A(i*6+1:i*6+6,:)=Ai(:,:,i+1)*A((i-1)*6+1:(i-1)*6+6,:);
        for j=1:i
            B(i*6+1:i*6+6,2*j-1:2*j)=Ai(:,:,i+1)*B((i-1)*6+1:(i-1)*6+6,2*j-1:2*j);
        end
        B(i*6+1:i*6+6,2*i+1:2*i+2)=Bi(:,:,i+1);
    end
    Hess=2*(B'*Q*B+R);
%     norm(Hess-Hess',inf)
%     L=(Hess+Hess')/2;
%     L-Hess
    lin=2*B'*Q*A*z_dif;
%     delta_dif(:)=[delta_dif(3:end);delta_dif(end-1:end)];
    delta_dif = quadprog(Hess,lin,aux_yr,b,[],[],lb,ub,[],options)
    delta_out=delta_dif+delta_ref(3:end);
    
    
    if(vx==0)
        aux7=-Cf*delta_out(1);
        aux8=-Cr*delta_out(2);
    else
        aux7=Cf*(atan((vy+yawrate*f)/vx)-delta_out(1));
        aux8=Cr*(atan((vy-yawrate*r)/vx)-delta_out(2));
    end
    
    vxexpc=vx+dt*((aux7*sin(delta_out(1))+aux8*sin(delta_out(2))+(Torque(1)*T_split*cos(delta_out(1))/radius)+(Torque(1)*(1-T_split)*cos(delta_out(2))/radius)-mtot*g*fr-Fl*vx^2)/(mtot+mj)+yawrate*vy);
    vyexpc=vy+dt*((-aux7*cos(delta_out(1))-aux8*cos(delta_out(2))+(Torque(1)*T_split*sin(delta_out(1))/radius)+(Torque(1)*(1-T_split)*sin(delta_out(2))/radius))/(mtot+mj)-yawrate*vx);
    yawrateexpc=yawrate+dt*((-f*aux7*cos(delta_out(1))+f*(Torque(1)*T_split*sin(delta_out(1))/radius)+r*aux8*cos(delta_out(2))-r*(Torque(1)*(1-T_split)*sin(delta_out(2))/radius))/Izz);

%     figure(3)
%     hold off;
%     quiver(xref, yref, ...
%     5*cos([yawref;yawref(end)]'), 5*sin([yawref;yawref(end)]'),'color',[0.82 0.41 0.12]);
%     hold on;
%     plot(xref,yref,'Marker','*','Color','c')
%     
%     time_pred=dt;
%     
%     x_plot=0;
%     y_plot=0;
%     yaw_plot=0;
%     vx_plot=vx;
%     vy_plot=vy;
%     yawrate_plot=yawrate;
%     plot(x_plot,y_plot,'Marker','*','Color','r');
%     quiver(x_plot, y_plot, ...
%     5*cos(yaw_plot), 5*sin(yaw_plot),'color',[0.82 0.41 0.12]);
% 
%     
% 
%     x_plot=x_plot+dt*(vx_plot*cos(yaw_plot)-vy_plot*sin(yaw_plot));
%     y_plot=y_plot+dt*(vx_plot*sin(yaw_plot)+vy_plot*cos(yaw_plot));
%     yaw_plot=yaw_plot+dt*yawrate_plot;
%     vx_plot=vxexpc;
%     vy_plot=vyexpc;
%     yawrate_plot=yawrateexpc;
%     plot(x_plot,y_plot,'Marker','*','Color','r');
%     quiver(x_plot, y_plot, ...
%     5*cos(yaw_plot), 5*sin(yaw_plot),'color',[0.82 0.41 0.12]);
%     
%     
%     
%     for j=2:H
%         x_plot=x_plot+time_pred*(vx_plot*cos(yaw_plot)-vy_plot*sin(yaw_plot));
%         y_plot=y_plot+time_pred*(vx_plot*sin(yaw_plot)+vy_plot*cos(yaw_plot));
%         yaw_plot=yaw_plot+time_pred*yawrate_plot;
%         
%         if(vx_plot==0)
%             aux7=-Cf*delta_out(2*j-1);
%             aux8=-Cr*delta_out(2*j);
%         else
%             aux7=Cf*(atan((vy_plot+yawrate_plot*f)/vx_plot)-delta_out(2*j-1));
%             aux8=Cr*(atan((vy_plot-yawrate_plot*r)/vx_plot)-delta_out(2*j));
%         end
%     
%         vx_next=vx_plot+time_pred*((aux7*sin(delta_out(2*j-1))+aux8*sin(delta_out(2*j))+(Torque(j)*T_split*cos(delta_out(2*j-1))/radius)+(Torque(j)*(1-T_split)*cos(delta_out(2*j))/radius)-mtot*g*fr-Fl*vx_plot^2)/(mtot+mj)+yawrate_plot*vy_plot);
%         vy_next=vy_plot+time_pred*((-aux7*cos(delta_out(2*j-1))-aux8*cos(delta_out(2*j))+(Torque(j)*T_split*sin(delta_out(2*j-1))/radius)+(Torque(j)*(1-T_split)*sin(delta_out(2*j))/radius))/(mtot+mj)-yawrate_plot*vx_plot);
%         yawrate_next=yawrate_plot+time_pred*((-f*aux7*cos(delta_out(2*j-1))+f*(Torque(j)*T_split*sin(delta_out(2*j-1))/radius)+r*aux8*cos(delta_out(2*j))-r*(Torque(j)*(1-T_split)*sin(delta_out(2*j))/radius))/Izz);
%         
%         vx_plot=vx_next;
%         vy_plot=vy_next;
%         yawrate_plot=yawrate_next;
%         
%         plot(x_plot,y_plot,'Marker','*','Color','r');
%         quiver(x_plot, y_plot, ...
%         5*cos(yaw_plot), 5*sin(yaw_plot),'color',[0.82 0.41 0.12]);
%     end
    
end