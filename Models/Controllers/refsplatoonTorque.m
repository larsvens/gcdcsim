function [d_ref,vx_ref,Tor_ref,vy_ref,yawrate_ref]=refsplatoonTorque(H,th,dt,d_frente,r_safe,v_frente,a_frente,delta_ref,vx,vy,yawrate,time_pred)

      persistent init_flag Cf Cr radius mtot g mj Fl f r;%T_split
      
      if isempty(init_flag)
        init_flag = 1;
        
        m  = [276 332 608];          %m(1)-mass front axle, m(2)-mass rear axle, m(3)-total mass [kg]
        mtot = m(3);                %Total mass of vehicle
        mj = 40;                    %Equivalent mass of rotating parts (due to inertia) [kg]
        g  = 9.81;                   %[m/s^2]
        lambda = m(2)/m(3);
        l  = 2;                      %Wheel base [m]
        f  = l*lambda;               %Distance from front axle to CoG [m]
        r  = l*(1-lambda);           %Distance from rear axle to CoG [m]
%         cv = 1;                     %Longitudinal velocity coefficient. cv=1 when vehicle starts to move, otherwise cv=0.3


%         Izz = 1000;                 %Still rough estimation [kgm^2]
        Cf  = 2*224*180/pi;
        Cr  = 2*224*180/pi;
        radius  = 0.3;
                
        
        c   = 1.0834;
        rho = 1.23;                 %Air density [kg/m^3]
        Area   = 1.5*1.5;                %Projected frontal area [m]
        Fl  = 0.5*rho*c*Area;
        
%         T_split=0.5;
      end
      
%     d_ref=zeros(H+1,1);
%     d_ref(1)=d_frente;
%     
%     v_ref=zeros(H+1,1);
%     a_ref=zeros(H,1);
%   
%     v_ref(1)=vx;%(d_ref(1)-r_safe)/th;
%     
%     d_ref(2)=d_ref(1)+(v_frente-v_ref(1))*dt;
%     v_frente=v_frente+a_frente*dt;
%     v_ref(2)=(d_ref(2)-r_safe)/th;
%     a_ref(1)=(v_ref(2)-v_ref(1))/(dt*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]));
%     
%     for i=2:H
%         d_ref(i+1)=d_ref(i)+(v_frente-v_ref(i))*time_pred;
%         v_frente=v_frente+a_frente*time_pred;
%         v_ref(i+1)=(d_ref(i+1)-r_safe)/th;
%         a_ref(i)=(v_ref(i+1)-v_ref(i))/(time_pred*[1/2 1/2]*cos([delta_ref(2*i-1);delta_ref(2*i)]));
%     end

    
    
    d_ref=zeros(H+1,1);
    
    
    vx_ref=zeros(H+1,1);
    Tor_ref=zeros(H,1);
    vy_ref=zeros(H+1,1);
    yawrate_ref=zeros(H+1,1);
  
    vx_ref(1)=v_frente;%(d_ref(1)-r_safe)/th;
    d_ref(1)=vx_ref(1)*th+r_safe;%d_frente;
    
    
    v_frente=v_frente+a_frente*dt;
    vx_ref(2)=v_frente;%(d_ref(2)-r_safe)/th;
    d_ref(2)=vx_ref(2)*th+r_safe;
    
    vy_ref(1)=vy;%yawrate e vy verdadeiros...mudar?� preciso?
    yawrate_ref(1)=yawrate;
    if abs(vx) <= 0.05
        fr = 0;
    else
        fr  = 0.0027;    
    end
    
    if(abs(vx)<0.1)
        aux7=0;
        aux8=0;
    else
        aux7=2*Cf*(atan2((vy_ref(1)+yawrate_ref(1)*f),abs(vx))-delta_ref(1));
        aux8=2*Cr*(atan2((vy_ref(1)-yawrate_ref(1)*r),abs(vx))-delta_ref(2));
    end    
    
    
    Tor_ref(1)=(((vx_ref(2)-vx_ref(1))/dt-yawrate_ref(1)*vy_ref(1))*(mtot+mj)-((aux7*sin(delta_ref(1))+aux8*sin(delta_ref(2))-mtot*g*fr-Fl*vx^2)))/((2*cos(delta_ref(1))+2*cos(delta_ref(2)))/radius);
   
    [vx_ego, vy_ref(2), yawrate_ref(2)]=EgoVelsPred(vx, vy_ref(1), yawrate_ref(1), delta_ref(1),delta_ref(2),Tor_ref(1),dt);
    
%     vy_ref(2)=vy_ref(1)+dt*((-aux7*cos(delta_ref(1))-aux8*cos(delta_ref(2))+(Tor_ref(1)*T_split*sin(delta_ref(1))/radius)+(Tor_ref(1)*(1-T_split)*sin(delta_ref(2))/radius))/(mtot+mj)-yawrate_ref(1)*vx_ref(1));
%     yawrate_ref(2)=yawrate_ref(1)+dt*((-f*aux7*cos(delta_ref(1))+f*(Tor_ref(1)*T_split*sin(delta_ref(1))/radius)+r*aux8*cos(delta_ref(2))-r*(Tor_ref(1)*(1-T_split)*sin(delta_ref(2))/radius))/Izz);
            
    
    for i=2:H
        
        v_frente=v_frente+a_frente*time_pred;
        vx_ref(i+1)=v_frente;%(d_ref(i+1)-r_safe)/th;
        d_ref(i+1)=vx_ref(i+1)*th+r_safe;
        
        
        if abs(vx_ego) <= 0.05
            fr = 0;
        else
            fr  = 0.0027;    
        end

        if(abs(vx_ego)<0.1)
            aux7=0;
            aux8=0;
        else
            aux7=2*Cf*(atan2((vy_ref(i)+yawrate_ref(i)*f),abs(vx_ego))-delta_ref(2*i-1));
            aux8=2*Cr*(atan2((vy_ref(i)-yawrate_ref(i)*r),abs(vx_ego))-delta_ref(2*i));
        end    
        
        Tor_ref(i)=(((vx_ref(i+1)-vx_ref(i))/time_pred-yawrate_ref(i)*vy_ref(i))*(mtot+mj)-((aux7*sin(delta_ref(2*i-1))+aux8*sin(delta_ref(2*i))-mtot*g*fr-Fl*vx_ego^2)))/((2*cos(delta_ref(2*i-1))+2*cos(delta_ref(2*i)))/radius);
        
        [vx_ego, vy_ref(i+1), yawrate_ref(i+1)]=EgoVelsPred(vx_ego, vy_ref(i), yawrate_ref(i), delta_ref(2*i-1),delta_ref(2*i),Tor_ref(i),time_pred);
        
%         vy_ref(i+1)=vy_ref(i)+time_pred*((-aux7*cos(delta_ref(2*i-1))-aux8*cos(delta_ref(2*i))+(Tor_ref(i)*T_split*sin(delta_ref(2*i-1))/radius)+(Tor_ref(i)*(1-T_split)*sin(delta_ref(2*i))/radius))/(mtot+mj)-yawrate_ref(i)*vx_ref(i));
%         yawrate_ref(i+1)=yawrate_ref(i)+time_pred*((-f*aux7*cos(delta_ref(2*i-1))+f*(Tor_ref(i)*T_split*sin(delta_ref(2*i-1))/radius)+r*aux8*cos(delta_ref(2*i))-r*(Tor_ref(i)*(1-T_split)*sin(delta_ref(2*i))/radius))/Izz);
            
   end
end