function Tor_out  = LinMPClongTorque( xref,yref,yawref,vx,vy,yawrate,delta_ref,d_frente,v_frente,a_frente,flagCACC,EstInit, H, dt,time_pred)
% coder.extrinsic('tic');
% coder.extrinsic('toc');
% % tic
%     coder.extrinsic('optimoptions');
%     coder.extrinsic('quadprog');
    
    persistent init_flag d_ant vx_ant mina maxa Q R Tor_dif vmax maxjerk minjerk Tor_ant print;
    persistent Cf Cr T_split radius mtot g mj Fl f r;
    if isempty(init_flag)
        print=0;
        init_flag = 1;
        R=zeros(H+1);
        Q=zeros(3*(H+1));
%         flagCACC_prev=-1;
        
        maxa=2;
        mina=-4;
        vmax=25;%Verificar, tanto as aceleraçoes como a velocidade maxima podem estar a limitar a aceleraçao e velocidade do carro sem estar estar nos limites na realidade, depende do modelo
        maxjerk=1;
        minjerk=-1;
        
        
%         mataux_jerk=zeros(H,H+1);
        
%         options=optimoptions('quadprog','Algorithm','interior-point-convex','Display','off');%,'Algorithm','active-set', 'Display','final-detailed','TolCon', 1.0000e-08,'TolFun', 1.0000e-04,'TolX', 1.0000e-04
        Tor_dif=zeros(H+1,1);
        
        m  = [276 332 608];          %m(1)-mass front axle, m(2)-mass rear axle, m(3)-total mass [kg]
        mtot = m(3);                %Total mass of vehicle
        mj = 40;                    %Equivalent mass of rotating parts (due to inertia) [kg]
        g  = 9.81;                   %[m/s^2]
        lambda = m(2)/m(3);
        l  = 2;                      %Wheel base [m]
        f  = l*lambda;               %Distance from front axle to CoG [m]
        r  = l*(1-lambda);           %Distance from rear axle to CoG [m]
%         cv = 1;                     %Longitudinal velocity coefficient. cv=1 when vehicle starts to move, otherwise cv=0.3


%         Izz = 1000;                 %Still rough estimation [kgm^2]
        Cf  = 2*224*180/pi;
        Cr  = 2*224*180/pi;
        radius  = 0.3;
                
        
        c   = 1.0834;
        rho = 1.23;                 %Air density [kg/m^3]
        Area   = 1.5*1.5;                %Projected frontal area [m]
        Fl  = 0.5*rho*c*Area;
        
        T_split=0.5;
    end
    
    a_out=zeros(H,1);
    
    if isempty(d_ant)
       d_ant = 0; 
    end
    
    if isempty(vx_ant)
       vx_ant = 0; 
    end
    
    if isempty(Tor_ant)
       Tor_ant = 0; 
    end
    
    if(init_flag)
       init_flag=0;
        last=10;
        for i=1:H-last-1
            Q(3*i-2,3*i-2)=1;%dist_dif weight
            Q(3*i-1,3*i-1)=1;%dist weight
            Q(3*i,3*i)=1;%vel weight
%             for j=1:i
% %                if(i==1)
% %                    mataux_vmax(i,j)=1;
% %                    mataux_vmax(i,H+1)=-1/dt;
% %                else
% %                    if(j==1)
% %                        mataux_vmax(i,j)=dt/time_pred;
% %                    else
% %                        mataux_vmax(i,j)=1;
% %                    end
% %                    mataux_vmax(i,H+1)=-1/time_pred;
% %                end
%             end
%             if(i==1)
%                 mataux_jerk(i,i)=1;
%             else
%                 mataux_jerk(i,i)=1;
%                 mataux_jerk(i,i-1)=-1;
%             end
        end
        for i=H-last:H
            Q(3*i-2,3*i-2)=1;%dist_dif weight
            Q(3*i-1,3*i-1)=1;%dist weight
            Q(3*i,3*i)=1;%vel weight
%             for j=1:i
%                if(j==1)
%                    mataux_vmax(i,j)=dt/time_pred;
%                else
%                    mataux_vmax(i,j)=1;
%                end
%                mataux_vmax(i,H+1)=-1/time_pred;
%             end
%             
%             mataux_jerk(i,i)=1;
%             mataux_jerk(i,i-1)=-1;
        end
%         i=H+1;
%         Q(3*i-2,3*i-2)=100;%dist_dif weight
%         Q(3*i-1,3*i-1)=10;%dist weight
%         Q(3*i,3*i)=0;%vel weight
        R(H+1,H+1)=10^10;
%         mataux_vmax
        
    end
    if(EstInit)
        if (flagCACC==1)
            %safe_factor=0.7;%change to the correct value before competition
            th1=0.5;%change to the correct value before competition
            th2=0.5;
            r_safe=5;
%             if(print<150)
                [d_ref,vxref,Tor_ref,vyref,yawrateref]=refsplatoonTorque(H,th1,dt,d_frente,r_safe,v_frente,a_frente,delta_ref,vx,vy,yawrate,time_pred)
%             else
%                 [d_ref,v_ref,a_ref]=refsplatoon(H,th,dt,d_frente,r_safe,v_frente,a_frente,delta_ref,vx,time_pred);
%             end
        elseif(flagCACC==0)
            %safe_factor=1;
            [d_ref,v_ref,a_ref]=refslong(xref,yref,yawref,dt,delta_ref,vx,H,maxa,mina,time_pred)
            th1=dt;
            th2=time_pred;
            r_safe=0;
            v_frente=0;
            d_frente=d_ref(1);
        else
            %erro!!!!!
            return;
        end

%         if(flagCACC~=flagCACC_prev)
% %             err='flagCACC changed'
%            d_ant=d_ref(1);
%            v_ant=v_ref(1); 
%         end
%         flagCACC_prev=flagCACC;
        
%         if(print<150)
            z_dif=[d_frente-th1*vx-r_safe;-d_ref(1)+d_frente;-v_ref(1)+vx] %[-(d_ant-th1*v_ant)+(d_frente-th1*vx);-d_ant+d_frente;-v_ant+vx]
%         else
%            z_dif=[-(d_ant-th*v_ant-d_ref(1)+th*vx);-d_ant+d_ref(1);-v_ant+vx];
%         end
       

        ub=zeros(H+1,1);
        lb=zeros(H+1,1);
%         mataux_vmax=zeros(H,H+1);
%         ub(1:H,1)=maxa-a_ref;
%         lb(1:H,1)=mina-a_ref;
        ub(H+1,1)=5;%5m/s is the max value that the slack velocity variable is allowed to have;
        lb(H+1,1)=-5;
        
        b=zeros(3*H,1);
        A_opt=zeros(3*H,H+1);
        %fazer com que apenas tome em conta a velocidade em x para contabilizar para
        %o maximo
%         b(1)=(vmax-vx)/dt-a_ref(1);
%         b(2:H)=(vmax-vx)/time_pred-mataux_vmax(2:H,:)*[a_ref;0];
%          A_opt(1:H,:)=mataux_vmax;
%         b(H+1)=maxjerk*dt+a_ant-a_ref(1);
%         b(H+2:2*H)=maxjerk*time_pred-mataux_jerk(2:H,:)*[a_ref;0];
%         A_opt(H+1:2*H,:)=mataux_jerk;        
%         b(2*H+1)=-minjerk*dt-a_ant+a_ref(1);
%         b(2*H+2:3*H)=-minjerk*time_pred+mataux_jerk(2:H,:)*[a_ref;0];
%         A_opt(2*H+1:3*H,:)=-mataux_jerk;
        

        A=zeros(3*(H+1),3);
        Ai=zeros(3,3,H+1);
        B=zeros(3*(H+1),H+1);
        Bi=zeros(3,1,H+1);
        
        vx_next=vxref(1);%experimentar com vx vy e yawrate verdadeiros
        vy_next=vyref(1);
        yawrate_next=yawrateref(1);
        
        if abs(vx) <= 0.05
            fr = 0;
        else
            fr  = 0.0027;    
        end

        if(abs(vx_next)<0.1 || ((vy_next+f*yawrate_next)^2+vx_next^2)==0)
            aux1=0;
        else
           aux1=-Cf*((vy_next+yawrate_next*f)/((vy_next+f*yawrate_next)^2+vx_next^2));
        end

        if(abs(vx_next)<0.1 || ((vy_next-r*yawrate_next)^2+vx_next^2)==0)
            aux2=0;
        else
           aux2=-Cr*((vy_next-yawrate_next*r)/((vy_next-r*yawrate_next)^2+vx_next^2));
        end

        Ai(:,:,1)=[0,1,-th1;
                  0,1,-dt;
                  0,0,1+dt*((aux1*sin(delta_ref(1))+aux2*sin(delta_ref(2))-Fl*2*vx_next)/(mtot+mj))];
        
        if(abs(vx)<0.1)
            aux7=0;
            aux8=0;
        else
            aux7=Cf*(atan((vy+yawrate*f)/max(0.5,vx))-delta_ref(1));
            aux8=Cr*(atan((vy-yawrate*r)/max(0.5,vx))-delta_ref(2));
        end      
              
        Bi(:,:,1)=[0;
                   0;
                   dt*((T_split*cos(delta_ref(1))+(1-T_split)*cos(delta_ref(2)))/radius)/(mtot+mj)];
        A(1:3,:)=Ai(:,:,1);
        B(1:3,1)=Bi(:,:,1);
        
        %since the constraints of the first point can be done with the real
        %values of vx,vy,yawrate they use them, the rest will be based on
        %the references
        ub(1,1)=((maxa-yawrate*vy)*(mtot+mj)-(aux7*sin(delta_ref(1))+aux8*sin(delta_ref(2))-mtot*g*fr-0.5-rho*c*A*vx^2))/((T_split*cos(delta_ref(1))+(1-T_split)*cos(delta_ref(2)))/radius)-Tor_ref(1);
        lb(1,1)=((mina-yawrate*vy)*(mtot+mj)-(aux7*sin(delta_ref(1))+aux8*sin(delta_ref(2))-mtot*g*fr-0.5-rho*c*A*vx^2))/((T_split*cos(delta_ref(1))+(1-T_split)*cos(delta_ref(2)))/radius)-Tor_ref(1);
        
        b(1)=vmax-vx-dt*((aux7*sin(delta_ref(1))+aux8*sin(delta_ref(2))+(Tor_ref(1)*T_split*cos(delta_ref(1))/radius)+(Tor_ref(1)*(1-T_split)*cos(delta_ref(2))/radius)-mtot*g*fr-Fl*vx^2)/(mtot+mj)+yawrate*vy);
        A_opt(1:H,1)=dt*((T_split*cos(delta_ref(1))+(1-T_split)*cos(delta_ref(2)))/radius)/(mtot+mj);
        A_opt(1,H+1)=-1; %verificar se esta certo slack variable vmax
        
        b(H+1)=maxjerk*dt+(vx-vx_ant)/dt-((aux7*sin(delta_ref(1))+aux8*sin(delta_ref(2))+(Tor_ref(1)*T_split*cos(delta_ref(1))/radius)+(Tor_ref(1)*(1-T_split)*cos(delta_ref(2))/radius)-mtot*g*fr-Fl*vx^2)/(mtot+mj)+yawrate*vy);
        jerk_aux=((aux7*sin(delta_ref(1))+aux8*sin(delta_ref(2))+(Tor_ref(1)*T_split*cos(delta_ref(1))/radius)+(Tor_ref(1)*(1-T_split)*cos(delta_ref(2))/radius)-mtot*g*fr-Fl*vx^2)/(mtot+mj)+yawrate*vy);
        A_opt(H+1,1)=((T_split*cos(delta_ref(1))+(1-T_split)*cos(delta_ref(2)))/radius)/(mtot+mj);
        
        b(2*H+1)=-minjerk*dt-(vx-vx_ant)/dt+((aux7*sin(delta_ref(1))+aux8*sin(delta_ref(2))+(Tor_ref(1)*T_split*cos(delta_ref(1))/radius)+(Tor_ref(1)*(1-T_split)*cos(delta_ref(2))/radius)-mtot*g*fr-Fl*vx^2)/(mtot+mj)+yawrate*vy);
        A_opt(2*H+1,1)=-((T_split*cos(delta_ref(1))+(1-T_split)*cos(delta_ref(2)))/radius)/(mtot+mj);
        
        
        for i=1:H-1
            vx_next=vxref(i+1);
            vy_next=vyref(i+1);
            yawrate_next=yawrateref(i+1);
            
            if abs(vx_next) <= 0.05
                fr = 0;
            else
                fr  = 0.0027;    
            end
            
            if(abs(vx_next)<0.1 || ((vy_next+f*yawrate_next)^2+vx_next^2)==0)
               aux1=0;
            else
               aux1=-Cf*((vy_next+yawrate_next*f)/((vy_next+f*yawrate_next)^2+vx_next^2));
            end

            if(abs(vx_next)<0.1 || ((vy_next-r*yawrate_next)^2+vx_next^2)==0)
               aux2=0;
            else
               aux2=-Cr*((vy_next-yawrate_next*r)/((vy_next-r*yawrate_next)^2+vx_next^2));
            end

            Ai(:,:,i+1)=[0,1,-th2;
                  0,1,-time_pred;
                  0,0,1+time_pred*((aux1*sin(delta_ref(2*i+1))+aux2*sin(delta_ref(2*i+2))-Fl*2*vx_next)/(mtot+mj))];
              
            if(abs(vx_next)<0.1)
                aux7=0;
                aux8=0;
            else
                aux7=Cf*(atan((vy_next+yawrate_next*f)/max(0.5,vx_next))-delta_ref(2*i+1));
                aux8=Cr*(atan((vy_next-yawrate_next*r)/max(0.5,vx_next))-delta_ref(2*i+2));
            end     

            Bi(:,:,i+1)=[0;
                   0;
                   time_pred*((T_split*cos(delta_ref(2*i+1))+(1-T_split)*cos(delta_ref(2*i+2)))/radius)/(mtot+mj)];

            A(i*3+1:i*3+3,:)=Ai(:,:,i+1)*A((i-1)*3+1:(i-1)*3+3,:);
            for j=1:i
                B(i*3+1:i*3+3,j)=Ai(:,:,i+1)*B((i-1)*3+1:(i-1)*3+3,j);
            end
            B(i*3+1:i*3+3,i+1)=Bi(:,:,i+1);
            
            ub(i+1,1)=((maxa-yawrate_next*vy_next)*(mtot+mj)-(aux7*sin(delta_ref(2*i+1))+aux8*sin(delta_ref(2*i+2))-mtot*g*fr-0.5-rho*c*A*vx_next^2))/((T_split*cos(delta_ref(2*i+1))+(1-T_split)*cos(delta_ref(2*i+2)))/radius)-Tor_ref(i+1);
            lb(i+1,1)=((mina-yawrate_next*vy_next)*(mtot+mj)-(aux7*sin(delta_ref(2*i+1))+aux8*sin(delta_ref(2*i+2))-mtot*g*fr-0.5-rho*c*A*vx_next^2))/((T_split*cos(delta_ref(2*i+1))+(1-T_split)*cos(delta_ref(2*i+2)))/radius)-Tor_ref(i+1);

            b(i+1)=b(i)-time_pred*((aux7*sin(delta_ref(2*i+1))+aux8*sin(delta_ref(2*i+2))+(Tor_ref(i+1)*T_split*cos(delta_ref(2*i+1))/radius)+(Tor_ref(i+1)*(1-T_split)*cos(delta_ref(2*i+2))/radius)-mtot*g*fr-Fl*vx_next^2)/(mtot+mj)+yawrate_next*vy_next);
            A_opt(i+1:H,i+1)=time_pred*((T_split*cos(delta_ref(2*i+1))+(1-T_split)*cos(delta_ref(2*i+2)))/radius)/(mtot+mj);
            A_opt(i+1,H+1)=-1; %verificar se esta certo slack variable vmax
            
            b(H+i+1)=maxjerk*time_pred+jerk_aux-((aux7*sin(delta_ref(2*i+1))+aux8*sin(delta_ref(2*i+2))+(Tor_ref(i+1)*T_split*cos(delta_ref(2*i+1))/radius)+(Tor_ref(i+1)*(1-T_split)*cos(delta_ref(2*i+2))/radius)-mtot*g*fr-Fl*vx_next^2)/(mtot+mj)+yawrate_next*vy_next);
            
            A_opt(H+1+i,i)=-((T_split*cos(delta_ref(2*i-1))+(1-T_split)*cos(delta_ref(2*i)))/radius)/(mtot+mj);
            A_opt(H+1+i,i+1)=((T_split*cos(delta_ref(2*i+1))+(1-T_split)*cos(delta_ref(2*i+2)))/radius)/(mtot+mj);
            
            b(2*H+1+i)=-minjerk*time_pred-jerk_aux+((aux7*sin(delta_ref(2*i+1))+aux8*sin(delta_ref(2*i+2))+(Tor_ref(i+1)*T_split*cos(delta_ref(2*i+1))/radius)+(Tor_ref(i+1)*(1-T_split)*cos(delta_ref(2*i+2))/radius)-mtot*g*fr-Fl*vx_next^2)/(mtot+mj)+yawrate_next*vy_next);
            
            A_opt(2*H+1+i,i)=((T_split*cos(delta_ref(2*i-1))+(1-T_split)*cos(delta_ref(2*i)))/radius)/(mtot+mj);
            A_opt(2*H+1+i,i+1)=-((T_split*cos(delta_ref(2*i+1))+(1-T_split)*cos(delta_ref(2*i+2)))/radius)/(mtot+mj);
        
            jerk_aux=((aux7*sin(delta_ref(2*i+1))+aux8*sin(delta_ref(2*i+2))+(Tor_ref(i+1)*T_split*cos(delta_ref(2*i+1))/radius)+(Tor_ref(i+1)*(1-T_split)*cos(delta_ref(2*i+2))/radius)-mtot*g*fr-Fl*vx_next^2)/(mtot+mj)+yawrate_next*vy_next);
        end
        
%         i=H;%slack variable
%         Ai(:,:,i+1)=[0,1,-th2;
%                   0,1,-time_pred;
%                   0,0,1];
% 
%         Bi(:,:,i+1)=[0;
%                0;
%                0];
% 
%         A(i*3+1:i*3+3,:)=Ai(:,:,i+1)*A((i-1)*3+1:(i-1)*3+3,:);
%         for j=1:i
%             B(i*3+1:i*3+3,j)=Ai(:,:,i+1)*B((i-1)*3+1:(i-1)*3+3,j);
%         end
%         B(i*3+1:i*3+3,i+1)=Bi(:,:,i+1);
       
        
        Hess=2*(B'*Q*B+R);
        
        Hess=(Hess+Hess')/2;
            
        lin=2*B'*Q*A*z_dif;
%         if(print<150)
        [Tor_dif,~,~,~]=CallMPCLong( Hess,lin,A_opt,b,ub,lb,H )
%         Tor_dif = quadprog(Hess,lin,A_opt,b,[],[],lb,ub,[],options)
        Tor_out = Tor_ref+Tor_dif(1:end-1)
%         else
%             a_dif = quadprog(Hess,lin,A_opt,b,[],[],lb,ub,[],options);
%         a_out = a_ref+a_dif(1:end-1);
%         end
        vx_ant=vx;
        
        
        
%         a_ant=a_out(1);
        if (flagCACC)
%            if(print<150)
        d_ant=d_frente+(v_frente-vx)*dt;
           v_ant=vx+dt*a_out(1)*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]);%v_ref(2);%
%         else
%             d_ant=d_ref(1)+(v_frente-v_ref(1))*dt;
%            v_ant=v_ref(1)+dt*a_out(1)*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]);
%         end 
           
        else
           d_ant=d_ref(1)-v_ref(1)*dt+d_ref(2);
           v_ant=v_ref(1)+dt*a_out(1)*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]);
        end
%         print=print+1;
%         if(print>1500)
%            debug=1; 
%         end
%         print=print+1;
    end
%     toc
end