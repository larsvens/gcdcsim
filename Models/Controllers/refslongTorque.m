function [Tor_ref,vx_exp_out]=refslongTorque(vx_ref,dt,delta_ref,vx,H,vy,yawrate,time_pred)
    persistent init_flag Cf Cr T_split radius mtot g mj Fl  f r vx_expected;
      
      if isempty(init_flag)
        init_flag = 1;
        
        m  = [276 332 608];          %m(1)-mass front axle, m(2)-mass rear axle, m(3)-total mass [kg]
        mtot = m(3);                %Total mass of vehicle
        mj = 40;                    %Equivalent mass of rotating parts (due to inertia) [kg]
        g  = 9.81;                   %[m/s^2]
        lambda = m(2)/m(3);
        l  = 2;                      %Wheel base [m]
        f  = l*lambda;               %Distance from front axle to CoG [m]
        r  = l*(1-lambda);           %Distance from rear axle to CoG [m]
%         cv = 1;                     %Longitudinal velocity coefficient. cv=1 when vehicle starts to move, otherwise cv=0.3


%         Izz = 1000;                 %Still rough estimation [kgm^2]
        Cf  = 2*224*180/pi;
        Cr  = 2*224*180/pi;
        radius  = 0.3;
                
        
        c   = 1.0834;
        rho = 1.23;                 %Air density [kg/m^3]
        Area   = 1.5*1.5;                %Projected frontal area [m]
        Fl  = 0.5*rho*c*Area;
        
        T_split=0.5;
        
        vx_expected=vx_ref(1); %First time vx_expected is assumed to be the velocity that I have
      end

%     d_ref=zeros(H+1,1);
%     vx_ref=d_ref;
%     vy_ref=vx_ref;
%     yawrate_ref=vx_ref;
    Tor_ref=zeros(H,1);


%     vy_ref(1)=vy;%yawrate e vy verdadeiros...mudar?� preciso?
%     yawrate_ref(1)=yawrate;
% 
%     pos_virt=rot(yawref(2))*[(xref(2)-xref(1));(yref(2)-yref(1))];
%     d_ref(1)=pos_virt(1);
%     vx_ref(1)=d_ref(1)/dt;
%     
%     i=2;
%     pos_virt=rot(yawref(i+1))*[(xref(i+1)-xref(i));(yref(i+1)-yref(i))];
%     d_ref(i)=pos_virt(1);
%     vx_ref(i)=d_ref(i)/time_pred;
%     
    if abs(vx) <= 0.05
        fr = 0;
    else
        fr  = 0.0027;    
    end
    
    if(abs(vx)<0.1)
        aux7=0;
        aux8=0;
    else
        aux7=2*Cf*(atan2((vy+yawrate*f),abs(vx))-delta_ref(1));
        aux8=2*Cr*(atan2((vy-yawrate*r),abs(vx))-delta_ref(2));
    end    
    
    
    Tor_ref(1)=(((vx_ref(1)-vx_expected)/dt-yawrate*vy)*(mtot+mj)-((aux7*sin(delta_ref(1))+aux8*sin(delta_ref(2))-mtot*g*fr-Fl*vx^2)))/((2*cos(delta_ref(1))+2*cos(delta_ref(2)))/radius);
   
    [vx_ego, vy_ego, yawrate_ego]=EgoVelsPred(vx, vy, yawrate, delta_ref(1),delta_ref(2),Tor_ref(1),dt);
    
%     vy_ref(2)=vy_ref(1)+dt*((-aux7*cos(delta_ref(1))-aux8*cos(delta_ref(2))+(Tor_ref(1)*T_split*sin(delta_ref(1))/radius)+(Tor_ref(1)*(1-T_split)*sin(delta_ref(2))/radius))/(mtot+mj)-yawrate_ref(1)*vx_ref(1));
%     yawrate_ref(2)=yawrate_ref(1)+dt*((-f*aux7*cos(delta_ref(1))+f*(Tor_ref(1)*T_split*sin(delta_ref(1))/radius)+r*aux8*cos(delta_ref(2))-r*(Tor_ref(1)*(1-T_split)*sin(delta_ref(2))/radius))/Izz);
            
    
    for i=2:H

%         pos_virt=rot(yawref(i+2))*[(xref(i+2)-xref(i+1));(yref(i+2)-yref(i+1))];
%         d_ref(i+1)=pos_virt(1);
%         vx_ref(i+1)=d_ref(i+1)/time_pred;
        
        if abs(vx_ego) <= 0.05
            fr = 0;
        else
            fr  = 0.0027;    
        end

        if(abs(vx_ego)<0.1)
            aux7=0;
            aux8=0;
        else
            aux7=2*Cf*(atan2((vy_ego+yawrate_ego*f),abs(vx_ego))-delta_ref(2*i-1));
            aux8=2*Cr*(atan2((vy_ego-yawrate_ego*r),abs(vx_ego))-delta_ref(2*i));
        end    
        
        Tor_ref(i)=(((vx_ref(i)-vx_ref(i-1))/time_pred-yawrate_ego*vy_ego)*(mtot+mj)-((aux7*sin(delta_ref(2*i-1))+aux8*sin(delta_ref(2*i))-mtot*g*fr-Fl*vx_ego^2)))/((2*cos(delta_ref(2*i-1))+2*cos(delta_ref(2*i)))/radius);
        
%         vy_ref(i+1)=vy_ref(i)+time_pred*((-aux7*cos(delta_ref(2*i-1))-aux8*cos(delta_ref(2*i))+(Tor_ref(i)*T_split*sin(delta_ref(2*i-1))/radius)+(Tor_ref(i)*(1-T_split)*sin(delta_ref(2*i))/radius))/(mtot+mj)-yawrate_ref(i)*vx_ref(i));
%         yawrate_ref(i+1)=yawrate_ref(i)+time_pred*((-f*aux7*cos(delta_ref(2*i-1))+f*(Tor_ref(i)*T_split*sin(delta_ref(2*i-1))/radius)+r*aux8*cos(delta_ref(2*i))-r*(Tor_ref(i)*(1-T_split)*sin(delta_ref(2*i))/radius))/Izz);
        
        [vx_ego, vy_ego, yawrate_ego]=EgoVelsPred(vx_ego, vy_ego, yawrate_ego, delta_ref(2*i-1),delta_ref(2*i),Tor_ref(i),time_pred);
    end
    vx_exp_out=vx_expected;
    vx_expected=vx_ref(1);
end