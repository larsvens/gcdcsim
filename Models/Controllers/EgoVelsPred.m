function [vx_new, vy_new, yawrate_new]=EgoVelsPred(vx, vy, yawrate, delta_f,delta_r,Torque,dt)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Vehicle data of RCV %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 persistent init_flag Cf Cr T_split radius mtot g mj Fl f r Izz;
    if isempty(init_flag)
        init_flag=1;
        m  = [276 332 608];          %m(1)-mass front axle, m(2)-mass rear axle, m(3)-total mass [kg]
        mtot = m(3);                %Total mass of vehicle
        mj = 40;                    %Equivalent mass of rotating parts (due to inertia) [kg]
        g  = 9.81;                   %[m/s^2]
        lambda = m(2)/m(3);
        l  = 2;                      %Wheel base [m]
        f  = l*lambda;               %Distance from front axle to CoG [m]
        r  = l*(1-lambda);           %Distance from rear axle to CoG [m]
        cv = 1;                     %Longitudinal velocity coefficient. cv=1 when vehicle starts to move, otherwise cv=0.3


        Izz = 1000;                 %Still rough estimation [kgm^2]
        Cf  = 2*224*180/pi;
        Cr  = 2*224*180/pi;
        radius  = 0.3;
        fr  = 0.0027;    %Initial value 0.027
        c   = 1.0834;

        rho = 1.23;                 %Air density [kg/m^3]
        A   = 1.5*1.5;                %Projected frontal area [m]
        Fl  = 0.5*rho*c*A;
        
        T_split = 0.5;                  %Torque split
    end
    
    vx_new=vx;
    vy_new=vy;
    yawrate_new=yawrate;
    
    while dt>1e-3

        if abs(vx) <= 0.05
            fr = 0;
        else
            fr=0.0027;
        end

        if abs(vx) < 0.1
            af=0;
            ar=0;
        else
            af = atan2((vy + yawrate*f),abs(vx)) - delta_f;           %Slip angle on front axle
            ar = atan2((vy - yawrate*r),abs(vx)) - delta_r;           %Slip angle on rear axle       
        end

        Ff = -2*Cf*af;                    %Lateral force on front axle, times two, one for each wheel of the axis
        Fr = -2*Cr*ar;                    %Lateral force on rear axle, times two, one for each wheel of the axis
        Fm = mtot*g*fr;                 %Motion resistance due to tire rolling resistance and road slope

        % Torque split between front and rear axle

        Fxf = 2*Torque/radius;      %Amount of available torque on the front axle, times two, one for each wheel of the axis
        Fxr = 2*Torque/radius;
        
        if(dt>0.01)
            vx_new = (vx + 0.01*((-Ff*sin(delta_f) - Fr*sin(delta_r) + Fxf*cos(delta_f) + Fxr*cos(delta_r) - Fm - Fl*vx^2)/(mtot+mj) + yawrate*vy));
            vy_new = vy + 0.01*((Fr*cos(delta_r) + Ff*cos(delta_f) + Fxr*sin(delta_r) + Fxf*sin(delta_f))/(mtot+mj) - yawrate*vx); 
            yawrate_new = yawrate + 0.01*((f*(Ff*cos(delta_f)+Fxf*sin(delta_f)) - r*(Fxr*sin(delta_r)+Fr*cos(delta_r)))/Izz);
        else
            vx_new = (vx + dt*((-Ff*sin(delta_f) - Fr*sin(delta_r) + Fxf*cos(delta_f) + Fxr*cos(delta_r) - Fm - Fl*vx^2)/(mtot+mj) + yawrate*vy));
            vy_new = vy + dt*((Fr*cos(delta_r) + Ff*cos(delta_f) + Fxr*sin(delta_r) + Fxf*sin(delta_f))/(mtot+mj) - yawrate*vx); 
            yawrate_new = yawrate + dt*((f*(Ff*cos(delta_f)+Fxf*sin(delta_f)) - r*(Fxr*sin(delta_r)+Fr*cos(delta_r)))/Izz);
        end

        vx=vx_new;
        vy=vy_new;
        yawrate=yawrate_new;

        dt=dt-0.01;

    end
end

