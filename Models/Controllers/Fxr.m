function [ out ] = Fxr( T_split,m,vx1,vy1,vx2,vy2,r,dt,T )
    if isempty(T)
        T=Torque(m,vx1,vy1,vx2,vy2,r,dt);
    end
    
    out = T*(1-T_split)/r;
end