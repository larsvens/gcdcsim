function [delta_ref]=refs(f,r,xref,yref,yawref)
%%ERRADO!!! FALTA CORRIGIR O YAWREF DO ULTIMO PONTO
    persistent init_flag rot mat_aux ;
    if isempty(init_flag)
        init_flag = 1;
        rot=@(x)[cos(x),sin(x);-sin(x),cos(x)];
        mat_aux=-eye(length(yawref));
        
        for i=1:length(yawref)-1
            mat_aux(i,i+1)=1; %it will make the difference between the heading of the next point and the current one, which will give the yaw ref that the car should have on its frame
        end
    end
    delta_ref=zeros(2*(length(yawref)),1);
    traj=zeros(2,length(xref));
%     yawref=yawref-yawref(1);
    yaw_dif=mat_aux*yawref;
    traj(:,1)=rot(yawref(1))*[xref(1);yref(1)];
    for i=1:length(yawref)-1
        traj(:,i+1)=rot(yawref(i+1))*[xref(i+1);yref(i+1)];
        R=(f+r)/tan(yaw_dif(i));
        pos_virt=[(traj(1,i+1)-traj(1,i));(traj(2,i+1)-traj(2,i))];
        delta_ref(2*i-1:2*i)=atan(pos_virt(2)/pos_virt(1));
        delta_ref(2*i-1)=delta_ref(2*i-1)+atan(f/R);
        delta_ref(2*i)=delta_ref(2*i)-atan(r/R);
    end

end