function [ out ] = Fxf( T_split,m,vx1,vy1,vx2,vy2,r,dt,T )
    if isempty(T)
        T=Torque(m,vx1,vy1,vx2,vy2,r,dt);
    end
    
    out = T*T_split/r;
end

