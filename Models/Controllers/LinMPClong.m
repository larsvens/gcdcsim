function [ a_out ] = LinMPClong( H,xref,yref,yawref,vx,delta_ref,dt,d_frente,v_frente,a_frente)
    persistent init_flag d_ant v_ant mina maxa Q R options r_safe;
    if isempty(init_flag)
        init_flag = 1;
        R=zeros(H);
        Q=zeros(3*H);
        
        r_safe=0; %change to the correct value before competition
        maxa=2;
        mina=-4;
    end
    
    yaw_ref=[yawref;0];%deboche para corrigir as refslong
    
    if (exist('d_frente','var')==0)
        safe_factor=1;
        [d_ref,v_ref,a_ref]=refslong(xref,yref,yaw_ref,dt,delta_ref,vx,H,maxa,mina);
        th=dt;
        v_frente=0;
    else
        safe_factor=1;%change to the correct value before competition
        th=dt;%change to the correct value before competition
        [d_ref,v_ref,a_ref]=refsplatoon(H,th,dt,d_frente,r_safe,safe_factor,v_frente,a_frente,delta_ref,vx);
    end
    
    if(init_flag==1)
       init_flag=0;
       
%        % Vehicle parameters (taken out of the ego vehicle model in the simulation)
%         h = 0.006;
%         %vx_cons = 6;
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         %%%%% Vehicle data of RCV %%%%%%%
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         m  = [276 332 608];          %m(1)-mass front axle, m(2)-mass rear axle, m(3)-total mass [kg]
%         mtot = m(3);                %Total mass of vehicle
%         mj = 40;                    %Equivalent mass of rotating parts (due to inertia) [kg]
%         g  = 9.81;                   %[m/s^2]
%         lambda = m(2)/m(3);
%         l  = 2;                      %Wheel base [m]
%         f  = l*lambda;               %Distance from front axle to CoG [m]
%         r  = l*(1-lambda);           %Distance from rear axle to CoG [m]
%         cv = 1;                     %Longitudinal velocity coefficient. cv=1 when vehicle starts to move, otherwise cv=0.3
% 
% 
%         Izz = 1000;                 %Still rough estimation [kgm^2]
%         Cf  = 2*224*180/pi;
%         Cr  = 2*224*180/pi;
%         rr  = 0.3;
%         fr  = 0.0027;    %Initial value 0.027
%         c   = 1.0834;
% 
%         rho = 1.23;                 %Air density [kg/m^3]
%         A   = 1.5*1.5;                %Projected frontal area [m]
        
        d_ant=d_ref(1);
        v_ant=v_ref(1);
        
        last=5;
        for i=1:H-last-1
            Q(3*i-2,3*i-2)=100;%dist_dif weight
            Q(3*i-1,3*i-1)=1;%dist weight
            Q(3*i,3*i)=1;%vel weight
        end
        for i=H-last:H
            Q(3*i-2,3*i-2)=1000;%dist_dif weight
            Q(3*i-1,3*i-1)=1;%dist weight
            Q(3*i,3*i)=1;%vel weight
        end
        options=optimoptions('quadprog');%,'Algorithm','active-set'
    end

    
    
    z_dif=[-(d_ant-safe_factor*th*v_ant-d_ref(1)+safe_factor*th*vx);-d_ant+d_ref(1);-v_ant+vx];
    
    
    ub=maxa-a_ref;
    lb=mina-a_ref;
%     aux_maxminyr(1:2*H)=maxyawrate*dt;
%     aux_maxminyr(2*H+1:end)=-minyawrate*dt;
%     aux_maxminyr(1:2)=aux_maxminyr(1:2)+[delta_f; delta_r];
%     aux_maxminyr(2*H+1:2*H+2)=aux_maxminyr(2*H+1:2*H+2)-[delta_f; delta_r];
%     b=aux_maxminyr-aux_yr*delta_ref(3:end);

    A=zeros(3*H,3);
    Ai=zeros(3,3,H);
    B=zeros(3*H,H);
    Bi=zeros(3,1,H);
    
    Ai(:,:,1)=[0,1,-safe_factor*th;
              0,1,-dt;
              0,0,1];
          
    Bi(:,:,1)=[0;
               0;
               dt*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)])];
    A(1:3,:)=Ai(:,:,1);
    B(1:3,1)=Bi(:,:,1);
    for i=1:H-1

        Ai(:,:,i+1)=[0,1,-safe_factor*th;
              0,1,-dt;
              0,0,1];
          
         Bi(:,:,i+1)=[0;
               0;
               dt*[1/2 1/2]*cos([delta_ref(2*i+1);delta_ref(2*i+2)])];

        A(i*3+1:i*3+3,:)=Ai(:,:,i+1)*A((i-1)*3+1:(i-1)*3+3,:);
        for j=1:i
            B(i*3+1:i*3+3,j)=Ai(:,:,i+1)*B((i-1)*3+1:(i-1)*3+3,j);
        end
        B(i*3+1:i*3+3,i+1)=Bi(:,:,i+1);
    end
    Hess=2*(B'*Q*B+R);

    lin=2*B'*Q*A*z_dif;

    a_dif = quadprog(Hess,lin,[],[],[],[],lb,ub,[],options);
    a_out=a_ref+a_dif;
    
    if (exist('d_frente','var')==0)
       d_ant=d_ref(1)-v_ref(1)*dt+d_ref(2);
       v_ant=v_ref(1)+dt*a_out(1)*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]);
    else
        d_ant=d_ref(1)+(v_frente-v_ref(1))*dt;
        v_ant=v_ref(1)+dt*a_out(1)*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]);
    end
   
end