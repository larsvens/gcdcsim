function [d_ref,v_ref,a_ref]=refsplatoon(H,th,dt,d_frente,r_safe,v_frente,a_frente,delta_ref,vx,time_pred)

%     d_ref=zeros(H+1,1);
%     d_ref(1)=d_frente;
%     
%     v_ref=zeros(H+1,1);
%     a_ref=zeros(H,1);
%   
%     v_ref(1)=vx;%(d_ref(1)-r_safe)/th;
%     
%     d_ref(2)=d_ref(1)+(v_frente-v_ref(1))*dt;
%     v_frente=v_frente+a_frente*dt;
%     v_ref(2)=(d_ref(2)-r_safe)/th;
%     a_ref(1)=(v_ref(2)-v_ref(1))/(dt*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]));
%     
%     for i=2:H
%         d_ref(i+1)=d_ref(i)+(v_frente-v_ref(i))*time_pred;
%         v_frente=v_frente+a_frente*time_pred;
%         v_ref(i+1)=(d_ref(i+1)-r_safe)/th;
%         a_ref(i)=(v_ref(i+1)-v_ref(i))/(time_pred*[1/2 1/2]*cos([delta_ref(2*i-1);delta_ref(2*i)]));
%     end

    
    
    d_ref=zeros(H+1,1);
    
    
    v_ref=zeros(H+1,1);
    a_ref=zeros(H,1);
  
    v_ref(1)=v_frente;%vx%(d_ref(1)-r_safe)/th;
    d_ref(1)=v_ref(1)*th+r_safe;%d_frente;
    
    
    v_frente=v_frente+a_frente*dt;
    v_ref(2)=v_frente;%(d_ref(2)-r_safe)/th;
    d_ref(2)=v_ref(2)*th+r_safe;
    a_ref(1)=(v_ref(2)-v_ref(1))/(dt*[1/2 1/2]*cos([delta_ref(1);delta_ref(2)]));
    
    for i=2:H
        
        v_frente=v_frente+a_frente*time_pred;
        v_ref(i+1)=v_frente;%(d_ref(i+1)-r_safe)/th;
        d_ref(i+1)=v_ref(i+1)*th+r_safe;
        a_ref(i)=(v_ref(i+1)-v_ref(i))/(time_pred*[1/2 1/2]*cos([delta_ref(2*i-1);delta_ref(2*i)]));
    end
end