%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Init script for GCDCSim environment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% clear workspace and add source dirs
clear; close all;
currentDir = pwd;
dirToAdd = '\GCDC2012-Library';
addpath(genpath(strcat(currentDir,'\',dirToAdd)));
dirToAdd = '\Models\DummyVehicle';
addpath(genpath(strcat(currentDir,'\',dirToAdd)));
dirToAdd = '\Models\Sensors';
addpath(genpath(strcat(currentDir,'\',dirToAdd)));
dirToAdd = '\Models\Controllers';
addpath(genpath(strcat(currentDir,'\',dirToAdd)));
dirToAdd = '\Models\LocalDynamicMap';
addpath(genpath(strcat(currentDir,'\',dirToAdd)));
dirToAdd = '\Models\Supervisor';
addpath(genpath(strcat(currentDir,'\',dirToAdd)));

load('busDefinitions');
load('Implementation Model\busDefinitionsLDM');
load('busDefinitionsDummyVehicleSupervisorTesting');
% load('lateralController_conversion_data');
% load('world_conversion_data');

% set sim duration (s)
Tend = 100;

% define sampling Times
Ts_Fast = 1/1200;
Ts_Mid = 1/100;
Ts_Logic = 1/10;
Ts_Sender = 1/60;
Ts_Slow = 1/10;
Ts_Controller = 1/10;
Ts_Estimator = 1/100;
Ts_CANSend = 1/10;
Ts_log = 1/100;
Ts_dummy_vehicles = 1/100; % Defines the time step for each dummy vehicle 
% simulation step.


% load global map data
S = load('DualLaneData');
laneData = struct2array(S.laneData);
laneDataENU = struct2array(S.laneDataENU);
% load new_map_data
laneDataHeading = createLaneDataHeading(laneDataENU);

GLOBAL_lane_markings_num_points = 200;
for it = 1:length(laneMarkingsBus.Elements)
    laneMarkingsBus.Elements(it).Dimensions = GLOBAL_lane_markings_num_points;
end
centerLineBus.Elements(1).Dimensions = GLOBAL_lane_markings_num_points;
centerLineBus.Elements(2).Dimensions = GLOBAL_lane_markings_num_points;

% GLOBAL_latitude_origin = S.laneData.latML(1); 
GLOBAL_latitude_origin = laneData(1, 4);
% GLOBAL_longitude_origin = S.laneData.lonML(1);
GLOBAL_longitude_origin = laneData(1, 3);

% get first point of middle lane marking
% lat0 = S.laneData.latML(1);
lat0 = laneData(1, 4);
% lon0 = S.laneData.lonML(1);
lon0 = laneData(1, 3);
h0 = 0;
% define start coordinates in ECEFg and ECEFr
GLOBAL_ECEFg0 = [(pi/180)*lat0; (pi/180)*lon0; h0]; 
GLOBAL_ECEFr0 = geodeticECEF2rectangularECEF(GLOBAL_ECEFg0);

% set vehicle init states   [vx(km/h),vy(km/h),yawrate(rad/s),dx(m),dy(m),yaw_angle(degrees)]
initStateEgoVehicle = [15 0 0 -10 -3.5 240];
%%%%%%%%% PARAMETERS FOR TESTING %%%%%%%%%%

%%% Platoon Action %%%

nodeID_lsb = 7;
nodeID_hsb = 0;
nodeID2_lsb = 6;
nodeID2_hsb = 0;
action = 1;

%%% Maneuver Request/Maneuver OffsetActive %%%
maneuverID_sec = 1;
maneuverID_msec = 2;
nodeIDdes_lsb = 3;
nodeIDdes_hsb = 4;
nodeIDref_lsb = 5;
nodeIDref_hsb = 6;
laneOffset = 7;
longitudinalOffset = 1;

%% Simulation id init
global GLOBAL_vehicle_1_id
global GLOBAL_vehicle_2_id
global GLOBAL_vehicle_3_id
global GLOBAL_vehicle_4_id
global GLOBAL_vehicle_5_id

GLOBAL_vehicle_1_id = 1;
GLOBAL_vehicle_2_id = 2;
GLOBAL_vehicle_3_id = 3;
GLOBAL_vehicle_4_id = 4;
GLOBAL_vehicle_5_id = 5;

%%% Maneuver State %%%
maneuverIDS_sec = 3;
maneuverIDS_msec = 4;
maneuverState = 1;

%%% Controllers %%%
H=20;
dt=1/20;
time_pred=1/4;

% Dummy vehicle initialization
xInit = 25;   %Last vehicle on each lane
velInit = 20; %Initial dummy vehicle velocity
rightLaneOffset = 30;
